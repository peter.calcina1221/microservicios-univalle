package com.CAMEBOL.calificacion.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.calificacion.entity.Estadistica;
import com.CAMEBOL.calificacion.entity.Mensaje;
import com.CAMEBOL.calificacion.service.EstadisticaService;

@RestController
@RequestMapping("/estadistica")
@CrossOrigin(origins = "*")
public class EstadisticaController {
	@Autowired
	private EstadisticaService estadisticaService;

	
	@GetMapping("/lista")
	public ResponseEntity<List<Estadistica>> getAll(){
		List<Estadistica> list = estadisticaService.getAll();
		return  new ResponseEntity<List<Estadistica>>(list,HttpStatus.OK);
	}
	@GetMapping("/obtenerIdOrganizacionIdElementoTipoElemento/{idOrganizacion}/{idElemento}/{tipoElemento}")
	public ResponseEntity<Estadistica> getByIdOrganizacionIdElementoTipoElemento(@PathVariable("idOrganizacion") int idOrganizacion,@PathVariable("idElemento") int idElemento,@PathVariable("tipoElemento") String tipoElemento){
		Estadistica list = estadisticaService.getByIdOrgizacionIdElementoTipoElemento(idOrganizacion,idElemento,tipoElemento);
		return  new ResponseEntity<Estadistica>(list,HttpStatus.OK);
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Estadistica estadistica, BindingResult bindingResult){
	
		estadisticaService.save(estadistica);
        
		return new ResponseEntity(new Mensaje("Estadistica exitosa"),HttpStatus.CREATED);
    }
	@PutMapping("/actualizar/{idCalificaion}")
	public ResponseEntity<?> actualizar(@PathVariable("idEstadistica") int idEstadistica, @RequestBody Estadistica estadistica){

			
		Estadistica estad=estadisticaService.getById(idEstadistica).get();
		estad.setIdOrganizacion(estadistica.getIdOrganizacion());
		estad.setIdElemento(estadistica.getIdElemento());
		estad.setTipoElemento(estadistica.getTipoElemento());
		estad.setPromedio(estadistica.getPromedio());
		estad.setCantidadCalificaciones(estadistica.getCantidadCalificaciones());
		estad.setFechaRegistro(estadistica.getFechaRegistro());
		estad.setEstado(estadistica.getEstado());
	
		estadisticaService.save(estad);
		return new ResponseEntity(new Mensaje("Estadistica Actualizada"),HttpStatus.OK);
	
	}	
	@DeleteMapping("/elimnar/{idEstadistica}")
	public ResponseEntity<?> delete(@PathVariable("idEstadistica") int idEstadistica){
		estadisticaService.delete(idEstadistica);
		return new ResponseEntity(new Mensaje("Estadistica Eliminada"),HttpStatus.OK);
	}
}

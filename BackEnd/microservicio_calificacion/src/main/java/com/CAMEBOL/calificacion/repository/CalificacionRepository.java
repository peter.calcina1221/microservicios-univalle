package com.CAMEBOL.calificacion.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.calificacion.entity.Calificacion;

@Repository
public interface CalificacionRepository extends JpaRepository<Calificacion, Integer> {
	List<Calificacion> findByIdOrganizacion(int idOrganizacion);
	List<Calificacion> findByIdOrganizacionAndIdElementoAndTipoElemento(int idOrganizacion,int idElemento,String tipoElemento);
	
	@Query("SELECT c FROM com.CAMEBOL.calificacion.entity.Calificacion c WHERE c.idOrganizacion= :idOrganizacion AND c.idElemento= :idElemento AND c.tipoElemento= :tipoElemento AND DATE (c.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<Calificacion> listarPorIdOrganizacionIdElementoTipoElementoYFecha(@Param("idOrganizacion") int idOrganizacion,@Param("idElemento") int idElemento,@Param("tipoElemento") String tipoElemento, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
}

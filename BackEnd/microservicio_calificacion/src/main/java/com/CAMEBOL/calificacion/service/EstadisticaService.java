package com.CAMEBOL.calificacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.calificacion.entity.Estadistica;
import com.CAMEBOL.calificacion.repository.EstadisticaRepository;



@Service
@Transactional
public class EstadisticaService {
	@Autowired
	EstadisticaRepository estadisticaRepository;
	 public List<Estadistica> getAll(){
	    	return estadisticaRepository.findAll();
	    }
	    
	 public Optional<Estadistica> getById(int id){
	        return estadisticaRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return estadisticaRepository.existsById(id);
	    }
	 public void save(Estadistica estadistica){
		 estadisticaRepository.save(estadistica);
	    }
	  public void delete(int id) {
		  estadisticaRepository.deleteById(id);
	    }
	  public void actualizar(Estadistica estadistica) {
		  Estadistica estadistica2=getById(estadistica.getIdEstadistica()).get();
		  estadistica2.setCantidadCalificaciones(estadistica.getCantidadCalificaciones());
		  estadistica2.setPromedio(estadistica.getPromedio());
		  estadisticaRepository.save(estadistica2);
	    }
	  public Estadistica getByIdOrgizacionIdElementoTipoElemento(int idOrganizacion,int idElemento,String tipoElemento){
	        return estadisticaRepository.findByIdOrganizacionAndIdElementoAndTipoElemento(idOrganizacion,idElemento,tipoElemento);
	    }
}

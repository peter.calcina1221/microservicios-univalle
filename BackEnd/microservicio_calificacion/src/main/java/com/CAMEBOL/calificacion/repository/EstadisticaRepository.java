package com.CAMEBOL.calificacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.calificacion.entity.Estadistica;
@Repository
public interface EstadisticaRepository extends JpaRepository<Estadistica, Integer> {
	Estadistica findByIdOrganizacionAndIdElementoAndTipoElemento(int idOrganizacion,int idElemento,String tipoElemento);
}

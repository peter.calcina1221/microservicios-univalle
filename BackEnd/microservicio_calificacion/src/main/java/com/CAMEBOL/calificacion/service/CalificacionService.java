package com.CAMEBOL.calificacion.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.calificacion.entity.Calificacion;
import com.CAMEBOL.calificacion.repository.CalificacionRepository;


@Service
@Transactional
public class CalificacionService {
	@Autowired
	CalificacionRepository calificacionRepository;
	 public List<Calificacion> getAll(){
	    	return calificacionRepository.findAll();
	    }
	 public List<Calificacion> getByIdOrganizacion(int idOrganizacion){
	    	return calificacionRepository.findByIdOrganizacion(idOrganizacion);
	    }
	 public List<Calificacion> getByIdOrgizacionIdElementoTipoElemento(int idOrganizacion,int idElemento,String tipoElemento){
	        return calificacionRepository.findByIdOrganizacionAndIdElementoAndTipoElemento(idOrganizacion,idElemento,tipoElemento);
	    }
	 public List<Calificacion> getByIdOrgizacionIdElementoTipoElementoYfecha(int idOrganizacion,int idElemento,String tipoElemento,Date fechaInicio,Date fechaFin){
	        return calificacionRepository.listarPorIdOrganizacionIdElementoTipoElementoYFecha(idOrganizacion,idElemento,tipoElemento,fechaInicio,fechaFin);
	    }
	    
	 public Optional<Calificacion> getById(int id){
	        return calificacionRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return calificacionRepository.existsById(id);
	    }
	 public void save(Calificacion detalleVenta){
		 calificacionRepository.save(detalleVenta);
	    }
	  public void delete(int id) {
		  calificacionRepository.deleteById(id);
	    }

}

package com.CAMEBOL.calificacion.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.calificacion.entity.Calificacion;
import com.CAMEBOL.calificacion.entity.Estadistica;
import com.CAMEBOL.calificacion.entity.Mensaje;
import com.CAMEBOL.calificacion.service.CalificacionService;
import com.CAMEBOL.calificacion.service.EstadisticaService;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping("/calificacion")
@CrossOrigin(origins = "*")
public class CalificacionController {
	@Autowired
	private CalificacionService calificacionService;
	@Autowired
	private EstadisticaService estadisticaService;
	@Autowired
	private RestTemplate restTemplate;

	
	@GetMapping("/lista")
	public ResponseEntity<List<Calificacion>> getAll(){
		try {
			List<Calificacion> list = calificacionService.getAll();
			return  new ResponseEntity<List<Calificacion>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<Calificacion>> getByIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion){
		try {
			List<Calificacion> list = calificacionService.getByIdOrganizacion(idOrganizacion);
			return  new ResponseEntity<List<Calificacion>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdOrganizacionIdElementoTipoElemento/{idOrganizacion}/{idElemento}/{tipoElemento}")
	public ResponseEntity<List<Calificacion>> getByIdOrganizacionIdElementoTipoElemento(@PathVariable("idOrganizacion") int idOrganizacion,@PathVariable("idElemento") int idElemento,@PathVariable("tipoElemento") String tipoElemento){
		try {
			List<Calificacion> list = calificacionService.getByIdOrgizacionIdElementoTipoElemento(idOrganizacion,idElemento,tipoElemento);
			return  new ResponseEntity<List<Calificacion>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/listaPorIdOrganizacionIdElementoTipoElementoYFecha/{idOrganizacion}/{idElemento}/{tipoElemento}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Calificacion>> getByIdOrganizacionIdElementoTipoElementoYFecha(@PathVariable("idOrganizacion") int idOrganizacion,@PathVariable("idElemento") int idElemento,@PathVariable("tipoElemento") String tipoElemento,@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio, @PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin){
		try {
			List<Calificacion> list = calificacionService.getByIdOrgizacionIdElementoTipoElementoYfecha(idOrganizacion,idElemento,tipoElemento,fechaInicio,fechaFin);
			return  new ResponseEntity<List<Calificacion>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Calificacion calificacion, BindingResult bindingResult){
	
		try {
			Estadistica estadistica=estadisticaService.getByIdOrgizacionIdElementoTipoElemento(calificacion.getIdOrganizacion(), calificacion.getIdElemento(), calificacion.getTipoElemento());
			List<Calificacion> listCalificacion=new ArrayList<>();
			int cantidad=0;
			Double promedio=0.0;
			if(estadistica!=null) {
				calificacionService.save(calificacion);
				listCalificacion=calificacionService.getByIdOrgizacionIdElementoTipoElemento(calificacion.getIdOrganizacion(), calificacion.getIdElemento(), calificacion.getTipoElemento());
				cantidad=listCalificacion.size();
				for(Calificacion calificacion2:listCalificacion) {
					promedio+=calificacion2.getPuntuacion();
				}
				promedio=promedio/listCalificacion.size();
				estadistica.setCantidadCalificaciones(cantidad);
				estadistica.setPromedio(promedio);
				estadisticaService.actualizar(estadistica);
			}
			else {
				calificacionService.save(calificacion);
				Estadistica estadistica2=new Estadistica();
				estadistica2.setIdOrganizacion(calificacion.getIdOrganizacion());
				estadistica2.setIdElemento(calificacion.getIdElemento());
				estadistica2.setTipoElemento(calificacion.getTipoElemento());
				estadistica2.setCantidadCalificaciones(1);
				estadistica2.setPromedio(calificacion.getPuntuacion());
				estadistica2.setEstado(calificacion.getEstado());
				estadisticaService.save(estadistica2);
			}
			 creandoLog("Calificacion","Insertar","Se inserto la calificacion del elemento "+calificacion.getIdElemento(),"Exito");
			return new ResponseEntity(new Mensaje("Calificacion exitosa"),HttpStatus.CREATED);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	
    }
	@PutMapping("/actualizar/{idCalificaion}")
	public ResponseEntity<?> actualizar(@PathVariable("idCalificaion") int idCalificaion, @RequestBody Calificacion calificacion){

		try {
			Calificacion califi=calificacionService.getById(idCalificaion).get();
			califi.setIdUsuario(calificacion.getIdUsuario());
			califi.setIdOrganizacion(calificacion.getIdOrganizacion());
			califi.setIdElemento(calificacion.getIdElemento());
			califi.setTipoElemento(calificacion.getTipoElemento());
			califi.setPuntuacion(calificacion.getPuntuacion());
			califi.setFechaRegistro(calificacion.getFechaRegistro());
			califi.setEstado(calificacion.getEstado());
			califi.setComentario(calificacion.getComentario());
			calificacionService.save(califi);
			return new ResponseEntity(new Mensaje("Calificacion Actualizada"),HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Calificacion","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
			
		
	
	}	
	@DeleteMapping("/elimnar/{idCalificaion}")
	public ResponseEntity<?> delete(@PathVariable("idCalificaion") int idCalificaion){
		calificacionService.delete(idCalificaion);
		return new ResponseEntity(new Mensaje("Calificacion Eliminada"),HttpStatus.OK);
	}
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
}

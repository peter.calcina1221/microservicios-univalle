package com.CAMEBOL.venta.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenRespuesta {
    @JsonProperty("TRANSACCION")
    private boolean transaccion;

    private String token;

    // Getters y Setters (omitiendo para brevedad)

    @JsonProperty("TRANSACCION")
    public boolean isTransaccion() {
        return transaccion;
    }

    @JsonProperty("TRANSACCION")
    public void setTransaccion(boolean transaccion) {
        this.transaccion = transaccion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "TokenRespuesta{" +
                "TRANSACCION=" + transaccion +
                ", token='" + token + '\'' +
                '}';
    }

}

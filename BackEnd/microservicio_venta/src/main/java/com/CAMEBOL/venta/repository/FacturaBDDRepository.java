package com.CAMEBOL.venta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CAMEBOL.venta.entity.FacturaBDD;

public interface FacturaBDDRepository extends JpaRepository<com.CAMEBOL.venta.entity.FacturaBDD, Integer> {
	List<FacturaBDD> findByIdOrganizacion(int idOrganizacion);
}

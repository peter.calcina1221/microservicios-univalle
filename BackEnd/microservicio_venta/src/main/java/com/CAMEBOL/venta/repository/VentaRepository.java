package com.CAMEBOL.venta.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.venta.entity.Venta;

@Repository
public interface VentaRepository extends JpaRepository<Venta, Integer> {
	List<Venta> findByIdUsuarioAndEstadoAndIdOrganizacion(int idUsuario, String estado, int idOrganizacion);

	List<Venta> findByIdUsuarioAndIdOrganizacion(int idUsuario, int idOrganizacion);

	List<Venta> findByIdOrganizacion(int idOrganizacion);

	List<Venta> findByIdOrganizacionAndEstado(int idOrganizacion, String estado);

	@Query("SELECT v FROM com.CAMEBOL.venta.entity.Venta v WHERE v.idOrganizacion= :idOrganizacion AND DATE (v.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<Venta> findByIdOrganizacionYFecha(@Param("idOrganizacion") int idOrganizacion,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
	@Query("SELECT v FROM com.CAMEBOL.venta.entity.Venta v INNER JOIN v.detalleVentas dv WHERE v.idOrganizacion= :idOrganizacion AND DATE (v.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin AND dv.idEmpresa= :idEmpresa")
	List<Venta> findByIdOrganizacionFechaYIdEmpresa(@Param("idOrganizacion") int idOrganizacion,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin,@Param("idEmpresa") int idEmpresa);

	@Query("SELECT SUM(v.total) FROM Venta v")
	Long sumarValores();
}

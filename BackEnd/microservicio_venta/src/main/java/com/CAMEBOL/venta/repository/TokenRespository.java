package com.CAMEBOL.venta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.venta.entity.TokenFac;

@Repository
public interface TokenRespository extends JpaRepository<TokenFac, Integer> {
}

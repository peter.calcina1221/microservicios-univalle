package com.CAMEBOL.venta.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.venta.entity.Mensaje;
import com.CAMEBOL.venta.entity.TokenFac;
import com.CAMEBOL.venta.entity.TokenRespuesta;
import com.CAMEBOL.venta.service.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/token")
@CrossOrigin(origins = "*")
public class TokenController {
	@Autowired
	private TokenService tokenService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<TokenFac>> getAll() {
		List<TokenFac> list = tokenService.getAll();
		return new ResponseEntity<List<TokenFac>>(list, HttpStatus.OK);
	}

	@GetMapping("/obtenerToken/{idToken}")
	public ResponseEntity<TokenFac> getbyId(@PathVariable("idToken") int idToken) {
		TokenFac token = tokenService.getById(idToken).get();
		return new ResponseEntity<TokenFac>(token, HttpStatus.OK);
	}

	@GetMapping("/tokenRespuesta")
	public ResponseEntity<TokenRespuesta> getTokenRespuesta() {
		TokenRespuesta token = tokenService.generarToken();
		return new ResponseEntity<TokenRespuesta>(token, HttpStatus.OK);
	}

	@GetMapping("/verificarToken")
	public ResponseEntity<TokenFac> getToken() {
		TokenFac token = tokenService.verificarToken();
		return new ResponseEntity<TokenFac>(token, HttpStatus.OK);
	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody TokenFac tokenFac, BindingResult bindingResult) {

		tokenService.save(tokenFac);

		return new ResponseEntity(new Mensaje("Token exitoso"), HttpStatus.CREATED);
	}

	@PutMapping("/actualizar/{idToken}")
	public ResponseEntity<?> actualizar(@PathVariable("idToken") int idToken, @RequestBody TokenFac tokenFac) {

		TokenFac tok = tokenService.getById(idToken).get();
		tok.setToken(tokenFac.getToken());
		tok.setEstado("1");

		tokenService.save(tok);
		return new ResponseEntity(new Mensaje("Token Actualizado"), HttpStatus.OK);

	}

	@DeleteMapping("/elimnar/{idToken}")
	public ResponseEntity<?> delete(@PathVariable("idToken") int idToken) {
		tokenService.delete(idToken);
		return new ResponseEntity(new Mensaje("Token Eliminado"), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}

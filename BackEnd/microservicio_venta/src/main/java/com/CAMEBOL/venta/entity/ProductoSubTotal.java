package com.CAMEBOL.venta.entity;

public class ProductoSubTotal {
	private int idProducto;
	private Double subtotal;

	public ProductoSubTotal() {
		super();
	}

	public ProductoSubTotal(int idProducto, Double subtotal) {
		super();
		this.idProducto = idProducto;
		this.subtotal = subtotal;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

}

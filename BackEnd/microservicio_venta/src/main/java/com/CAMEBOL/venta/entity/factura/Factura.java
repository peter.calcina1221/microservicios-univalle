package com.CAMEBOL.venta.entity.factura;

import java.util.ArrayList;
import java.util.List;

public class Factura {
	private Cabecera cabecera;
	private List<DetalleFactura> detalle;

	public Factura() {
		super();
		this.detalle = new ArrayList<>();
	}

	public Cabecera getCabecera() {
		return cabecera;
	}

	public void setCabecera(Cabecera cabecera) {
		this.cabecera = cabecera;
	}

	public List<DetalleFactura> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetalleFactura> detalle) {
		this.detalle = detalle;
	}

}

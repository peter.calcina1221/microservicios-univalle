package com.CAMEBOL.venta.entity.factura;

public class Cabecera {
	private String idTransaccion;
	private String nombreRazonSocial;
	private String correoElectronico;
	private int codigoTipoDocumentoIdentidad;
	private String numeroDocumento;
	private String complemento;
	private String codigoCliente;
	private int codigoMetodoPago;
	private int numeroTarjeta;
	private double montoTotal;
	private double montoTotalSujetoIva;
	private int codigoMoneda;
	private double tipoCambio;
	private double montoTotalMoneda;
	private double montoGiftCard;
	private double descuentoAdicional;
	private int codigoExcepcion;
	private String cafc;
	private String usuario;
	private int codigoDocumentoSector;
	private int tipoFacturaDocumento;

	public Cabecera() {
		super();
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public int getCodigoTipoDocumentoIdentidad() {
		return codigoTipoDocumentoIdentidad;
	}

	public void setCodigoTipoDocumentoIdentidad(int codigoTipoDocumentoIdentidad) {
		this.codigoTipoDocumentoIdentidad = codigoTipoDocumentoIdentidad;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public int getCodigoMetodoPago() {
		return codigoMetodoPago;
	}

	public void setCodigoMetodoPago(int codigoMetodoPago) {
		this.codigoMetodoPago = codigoMetodoPago;
	}

	public int getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(int numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public double getMontoTotalSujetoIva() {
		return montoTotalSujetoIva;
	}

	public void setMontoTotalSujetoIva(double montoTotalSujetoIva) {
		this.montoTotalSujetoIva = montoTotalSujetoIva;
	}

	public int getCodigoMoneda() {
		return codigoMoneda;
	}

	public void setCodigoMoneda(int codigoMoneda) {
		this.codigoMoneda = codigoMoneda;
	}

	public double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public double getMontoTotalMoneda() {
		return montoTotalMoneda;
	}

	public void setMontoTotalMoneda(double montoTotalMoneda) {
		this.montoTotalMoneda = montoTotalMoneda;
	}

	public double getMontoGiftCard() {
		return montoGiftCard;
	}

	public void setMontoGiftCard(double montoGiftCard) {
		this.montoGiftCard = montoGiftCard;
	}

	public double getDescuentoAdicional() {
		return descuentoAdicional;
	}

	public void setDescuentoAdicional(double descuentoAdicional) {
		this.descuentoAdicional = descuentoAdicional;
	}

	public int getCodigoExcepcion() {
		return codigoExcepcion;
	}

	public void setCodigoExcepcion(int codigoExcepcion) {
		this.codigoExcepcion = codigoExcepcion;
	}

	public String getCafc() {
		return cafc;
	}

	public void setCafc(String cafc) {
		this.cafc = cafc;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public int getCodigoDocumentoSector() {
		return codigoDocumentoSector;
	}

	public void setCodigoDocumentoSector(int codigoDocumentoSector) {
		this.codigoDocumentoSector = codigoDocumentoSector;
	}

	public int getTipoFacturaDocumento() {
		return tipoFacturaDocumento;
	}

	public void setTipoFacturaDocumento(int tipoFacturaDocumento) {
		this.tipoFacturaDocumento = tipoFacturaDocumento;
	}

}

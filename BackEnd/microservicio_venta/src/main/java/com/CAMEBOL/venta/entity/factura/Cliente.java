package com.CAMEBOL.venta.entity.factura;

public class Cliente {
	private long nit;
	private String usuario;
	private String password;
	private int codigo_documentos_sector_id;
	private int sucursal;
	private int pos;
	private String actividadEconomica;
	private String imeimac;

	public Cliente() {
		super();
	}

	public long getNit() {
		return nit;
	}

	public void setNit(long nit) {
		this.nit = nit;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCodigo_documentos_sector_id() {
		return codigo_documentos_sector_id;
	}

	public void setCodigo_documentos_sector_id(int codigo_documentos_sector_id) {
		this.codigo_documentos_sector_id = codigo_documentos_sector_id;
	}

	public int getSucursal() {
		return sucursal;
	}

	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public String getActividadEconomica() {
		return actividadEconomica;
	}

	public void setActividadEconomica(String actividadEconomica) {
		this.actividadEconomica = actividadEconomica;
	}

	public String getImeimac() {
		return imeimac;
	}

	public void setImeimac(String imeimac) {
		this.imeimac = imeimac;
	}

}

package com.CAMEBOL.venta.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.venta.entity.DetalleVenta;
import com.CAMEBOL.venta.entity.EmpresasVenden;
import com.CAMEBOL.venta.entity.ProductoCantidad;
import com.CAMEBOL.venta.entity.ProductoSubTotal;
import com.CAMEBOL.venta.entity.UsuarioProducto;
import com.CAMEBOL.venta.entity.Venta;
import com.CAMEBOL.venta.repository.DetalleVentaRepository;

@Service
@Transactional
public class DetalleVentaService {
	@Autowired
	DetalleVentaRepository detalleVentaRepository;

	public List<DetalleVenta> getAll() {
		return detalleVentaRepository.findAll();
	}

	public List<DetalleVenta> getAllByIdVenta(Venta venta) {
		return detalleVentaRepository.findByVenta(venta);
	}

	public List<DetalleVenta> getIdVentaAndEstado(Venta venta, String estado) {
		return detalleVentaRepository.findByVentaAndEstado(venta, estado);
	}

	public List<DetalleVenta> getByIdEmpresa(int idEmpresa) {
		return detalleVentaRepository.findByIdEmpresa(idEmpresa);
	}

	public List<DetalleVenta> listarPorIdOrganiacion(int idOrganizacion) {
		return detalleVentaRepository.ListarPorIdOrganizacion(idOrganizacion);
	}

	public List<DetalleVenta> listarPorIdOrganiacionYFecha(int idOrganizacion, Date fechaInicio, Date fechaFin) {
		return detalleVentaRepository.ListarPorIdOrganizacionYFecha(idOrganizacion, fechaInicio, fechaFin);
	}

	public List<DetalleVenta> listarPorIdOrganiacionFechaYIdEmpresa(int idOrganizacion, Date fechaInicio, Date fechaFin,int idEmpresa) {
		return detalleVentaRepository.ListarPorIdOrganizacionFechaYIdEmpresa(idOrganizacion, fechaInicio, fechaFin,idEmpresa);
	}

	public List<DetalleVenta> getByIdEmpresaCodigo(int idEmpresa, String codigo) {
		return detalleVentaRepository.findByIdEmpresaAndCodigo(idEmpresa, codigo);
	}

	public List<DetalleVenta> getByIdEmpresaEstado(int idEmpresa, String estado) {
		return detalleVentaRepository.findByIdEmpresaAndEstado(idEmpresa, estado);
	}

	public List<DetalleVenta> getByFechasAndIdEmpresa(Date fechaInicio, Date fechaFin, int idEmpresa) {
		return detalleVentaRepository.listarPorRangoFechaYEmpresa(fechaInicio, fechaFin, idEmpresa);
	}

	public List<ProductoCantidad> listarProductoMasVendidoPorEmpresa(int idEmpresa, int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarProductoMasVendidoPorEmpresa(idEmpresa);
		List<ProductoCantidad> productosMasVendidos = new ArrayList<>();
		List<ProductoCantidad> mostrarProductos = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			ProductoCantidad productoCantidad = new ProductoCantidad(idProducto, cantidad);
			productosMasVendidos.add(productoCantidad);
		}
		if (productosMasVendidos.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarProductos.add(productosMasVendidos.get(i));
			}

			return mostrarProductos;
		} else {
			return productosMasVendidos;
		}

	}

	public List<ProductoCantidad> listarProductoMasVendidoPorEmpresaYFecha(int idEmpresa, Date fechaInicio, Date fechaFin,
			int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarProductoMasVendidoPorEmpresaYFecha(idEmpresa, fechaInicio,
				fechaFin);
		List<ProductoCantidad> productosMasVendidos = new ArrayList<>();
		List<ProductoCantidad> mostrarProductos = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			ProductoCantidad productoCantidad = new ProductoCantidad(idProducto, cantidad);
			productosMasVendidos.add(productoCantidad);
		}
		if (productosMasVendidos.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarProductos.add(productosMasVendidos.get(i));
			}

			return mostrarProductos;
		} else {
			return productosMasVendidos;
		}

	}

	public List<ProductoCantidad> listarProductoMasVendidoPorOrganizacion(int idOrganizacion, int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarProductoMasVendidoPorOrganizacion(idOrganizacion);
		List<ProductoCantidad> productosMasVendidos = new ArrayList<>();
		List<ProductoCantidad> mostrarProductos = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			ProductoCantidad productoCantidad = new ProductoCantidad(idProducto, cantidad);
			productosMasVendidos.add(productoCantidad);
		}
		if (productosMasVendidos.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarProductos.add(productosMasVendidos.get(i));
			}

			return mostrarProductos;
		} else {
			return productosMasVendidos;
		}

	}

	public List<ProductoSubTotal> listarSubTotalProductosPorEmpresa(int idEmpresa) {
		List<Object[]> resultados = detalleVentaRepository.listarSubTotalProductosPorEmpresa(idEmpresa);
		List<ProductoSubTotal> productoSubTotal = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			Double subTotal = (Double) resultado[1]; // Convertir a double

			ProductoSubTotal productoSubTo = new ProductoSubTotal(idProducto, subTotal);
			productoSubTotal.add(productoSubTo);
		}
		return productoSubTotal;

	}

	public List<ProductoSubTotal> listarSubTotalProductosPorEmpresaYFecha(int idEmpresa, Date fechaInicio,
			Date fechaFin) {
		List<Object[]> resultados = detalleVentaRepository.listarSubTotalProductosPorEmpresaYFecha(idEmpresa, fechaInicio,
				fechaFin);
		List<ProductoSubTotal> productoSubTotal = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			Double subTotal = (Double) resultado[1]; // Convertir a double

			ProductoSubTotal productoSubTo = new ProductoSubTotal(idProducto, subTotal);
			productoSubTotal.add(productoSubTo);
		}
		return productoSubTotal;

	}

	public List<UsuarioProducto> listarPorductoCompradosPorUsuarios(int idUsuario) {
		List<Object[]> resultados = detalleVentaRepository.listarProductoMasCompradosPorusuario(idUsuario);
		List<UsuarioProducto> usuarioProductos = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idProducto = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue();
			; // Convertir a double

			UsuarioProducto usuarioProducto = new UsuarioProducto(idProducto, cantidad);
			usuarioProductos.add(usuarioProducto);
		}
		return usuarioProductos;

	}

	public List<DetalleVenta> ListarPorUsuarioYEstado(int idUsuario, String estado) {
		List<DetalleVenta> resultados = detalleVentaRepository.ListarPorUsuarioYEstado(idUsuario, estado);
		return resultados;

	}

	public List<EmpresasVenden> listarEmpresasQueMasVenden(int idOrganizacion, int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarEmpresasQueMasVenden(idOrganizacion);
		List<EmpresasVenden> EmpresaVende = new ArrayList<>();
		List<EmpresasVenden> mostrarEmpresa = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idEmpres = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			EmpresasVenden empresasVenden = new EmpresasVenden(idEmpres, cantidad);
			EmpresaVende.add(empresasVenden);
		}
		if (EmpresaVende.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarEmpresa.add(EmpresaVende.get(i));
			}

			return mostrarEmpresa;
		} else {
			return EmpresaVende;
		}

	}

	public List<EmpresasVenden> listarEmpresasQueMasVendenYFecha(int idOrganizacion, Date fechaInicio, Date fechaFin,
			int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarEmpresasQueMasVendenYFecha(idOrganizacion, fechaInicio,
				fechaFin);
		List<EmpresasVenden> EmpresaVende = new ArrayList<>();
		List<EmpresasVenden> mostrarEmpresa = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idEmpres = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			EmpresasVenden empresasVenden = new EmpresasVenden(idEmpres, cantidad);
			EmpresaVende.add(empresasVenden);
		}
		if (EmpresaVende.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarEmpresa.add(EmpresaVende.get(i));
			}

			return mostrarEmpresa;
		} else {
			return EmpresaVende;
		}

	}

	public List<EmpresasVenden> listarEmpresasQueMasCompranPorUsuario(int idUsuario, int limit) {
		List<Object[]> resultados = detalleVentaRepository.listarEmpresasQueMasCompranPorUsuario(idUsuario);
		List<EmpresasVenden> EmpresaVende = new ArrayList<>();
		List<EmpresasVenden> mostrarEmpresa = new ArrayList<>();

		for (Object[] resultado : resultados) {
			int idEmpres = (int) resultado[0];
			int cantidad = ((Long) resultado[1]).intValue(); // Convertir a int

			EmpresasVenden empresasVenden = new EmpresasVenden(idEmpres, cantidad);
			EmpresaVende.add(empresasVenden);
		}
		if (EmpresaVende.size() >= limit) {
			for (int i = 0; i < limit; i++) {
				mostrarEmpresa.add(EmpresaVende.get(i));
			}

			return mostrarEmpresa;
		} else {
			return EmpresaVende;
		}

	}

	public Optional<DetalleVenta> getById(int id) {
		return detalleVentaRepository.findById(id);
	}

	public boolean existsById(Integer id) {
		return detalleVentaRepository.existsById(id);
	}

	public void save(DetalleVenta detalleVenta) {
		detalleVentaRepository.save(detalleVenta);
	}

	public void delete(int id) {
		detalleVentaRepository.deleteById(id);
	}

}

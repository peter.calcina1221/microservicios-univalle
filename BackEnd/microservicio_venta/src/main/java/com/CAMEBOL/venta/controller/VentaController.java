package com.CAMEBOL.venta.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.venta.entity.Email;
import com.CAMEBOL.venta.entity.Mensaje;
import com.CAMEBOL.venta.entity.Venta;
import com.CAMEBOL.venta.service.VentaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/venta")
@CrossOrigin(origins = "*")
public class VentaController {
	@Autowired
	private VentaService ventaService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<Venta>> getAll() {
		List<Venta> list = ventaService.getAll();
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdUsuarioYEstadoYIdOrganizacion/{idUsuario}/{estado}/{idOrganizacion}")
	public ResponseEntity<List<Venta>> getAllByEstado(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("estado") String estado, @PathVariable("idOrganizacion") int idOrganizacion) {
		List<Venta> list = ventaService.getAllByIdUsuarioAndEstadoAndIdOrganizacion(idUsuario, estado, idOrganizacion);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdUsuarioYIdOrganizacion/{idUsuario}/{idOrganizacion}")
	public ResponseEntity<List<Venta>> getAllByEstado(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("idOrganizacion") int idOrganizacion) {
		List<Venta> list = ventaService.getAllByIdUsuarioAndIdOrganizacion(idUsuario, idOrganizacion);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<Venta>> getByIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion) {
		List<Venta> list = ventaService.getByIdOrganizacion(idOrganizacion);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	// sale error
	@GetMapping("/listaPorIdOrganizacionYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Venta>> getByIdOrganizacionYFecha(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		List<Venta> list = ventaService.getByIdOrganizacionYFecha(idOrganizacion, fechaInicio, fechaFin);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/listaPorIdOrganizacionFechaYIdEmpresa/{idOrganizacion}/{fechaInicio}/{fechaFin}/{idEmpresa}")
	public ResponseEntity<List<Venta>> getByIdOrganizacionFechaYIdEmpresa(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,@PathVariable("idEmpresa") int idEmpresa) {
		List<Venta> list = ventaService.getByIdOrganizacionFechaYIdEmpresa(idOrganizacion, fechaInicio, fechaFin,idEmpresa);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdOrganizacionYEstado/{idOrganizacion}/{estado}")
	public ResponseEntity<List<Venta>> getByIdOrganizacionEstado(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("estado") String estado) {
		List<Venta> list = ventaService.getByIdOrganizacionEstado(idOrganizacion, estado);
		return new ResponseEntity<List<Venta>>(list, HttpStatus.OK);
	}

	@GetMapping("/obtenerPorIdVenta/{idVenta}")
	public ResponseEntity<Venta> getById(@PathVariable("idVenta") int idVenta) {
		if (!ventaService.existsById(idVenta)) {
			return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
		}
		Venta venta = ventaService.getById(idVenta).get();
		return new ResponseEntity(venta, HttpStatus.OK);
	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody Venta venta) {

		ventaService.save(venta);

		return new ResponseEntity(new Mensaje("Venta exitosa"), HttpStatus.CREATED);
	}

	@PostMapping("/enviarFactura")
	public ResponseEntity<String> enviarFactura(@RequestBody Venta venta) {
		try {
			String enviarFactura = ventaService.Facturar(venta);

			return new ResponseEntity<>(enviarFactura, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	// Es el metodo que realiza la venta con transaccion actualizando el stock de
	// los productos
	@PostMapping("/transaccionVenta")
	public ResponseEntity<?> transaccionVenta(@Valid @RequestBody Venta venta, BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				// Si hay errores de validación en el objeto Venta, devolver una respuesta de
				// error con los detalles de validación
				return ResponseEntity.badRequest().body(new Mensaje("Error de validación"));
			}

			ResponseEntity<?> result = ventaService.transaccionVenta(venta);
			if (result.getStatusCode() == HttpStatus.BAD_REQUEST) {
				Mensaje mensaje = (Mensaje) result.getBody();
				creandoLog("Venta", "Insertar", mensaje.getMensaje(), "Error");
				return ResponseEntity.badRequest().body(mensaje);
			} else {
				creandoLog("Venta", "Insertar", "Se creo la venta " + venta.getCodigoVenta(), "Exito");
				return ResponseEntity.ok(result.getBody());
			}
		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	// Generar link de pago
	@PostMapping("/generarLinkPago")
	public ResponseEntity<?> generarLinkPago(@Valid @RequestBody Venta venta, BindingResult bindingResult) {

		try {
			if (bindingResult.hasErrors()) {
				// Si hay errores de validación en el objeto Venta, devolver una respuesta de
				// error con los detalles de validación
				return ResponseEntity.badRequest().body(new Mensaje("Error de validación"));
			}
			Mensaje msg = new Mensaje();
			msg = ventaService.generarLinkPago(venta);
			creandoLog("Venta", "Generar", "Se genero el link de pago para la venta " + venta.getCodigoVenta(), "Exito");
			return new ResponseEntity(msg, HttpStatus.CREATED);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/probarCola")
	public ResponseEntity<?> EnviarCola(@Valid @RequestBody Venta venta, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// Si hay errores de validación en el objeto Venta, devolver una respuesta de
			// error con los detalles de validación
			return ResponseEntity.badRequest().body(new Mensaje("Error de validación"));
		}
		Mensaje msg = new Mensaje();
		msg.setMensaje(ventaService.pruebaEnvioCola(venta));
		return new ResponseEntity(msg, HttpStatus.CREATED);

	}

	@PutMapping("/actualizar/{idVenta}")
	public ResponseEntity<?> actualizar(@PathVariable("idVenta") int idVenta, @RequestBody Venta venta) {

		try {
			Venta vent = ventaService.getById(idVenta).get();
			vent.setIdUsuario(venta.getIdUsuario());
			vent.setTotal(venta.getTotal());
			vent.setFechaRegistro(venta.getFechaRegistro());
			vent.setEstado(venta.getEstado());
			vent.setDetalleVentas(venta.getDetalleVentas());
			vent.setCodigoVenta(venta.getCodigoVenta());
			vent.setIdOrganizacion(venta.getIdOrganizacion());
			vent.setOrderCode(venta.getOrderCode());

			ventaService.save(vent);
			creandoLog("Venta", "Actualizar", "Se actualizo la venta " + venta.getCodigoVenta(), "Exito");
			return new ResponseEntity(new Mensaje("Venta Actualizada"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/elimnar/{idVenta}")
	public ResponseEntity<?> delete(@PathVariable("idVenta") int idVenta) {
		ventaService.delete(idVenta);
		return new ResponseEntity(new Mensaje("Venta Eliminada"), HttpStatus.OK);
	}

	@PostMapping("/enviarCorreo")
	public ResponseEntity<?> sendEmail(@RequestBody Email email) {
		try {
			ventaService.sendEmailTemplate(email);
			creandoLog("Venta", "Enviar", "Se envio un correo con informacion de la venta al correo" + email.getMailTo(),
					"Exito");
			return new ResponseEntity(new Mensaje("Te hemos enviado un correo"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/sumarValores")
	public ResponseEntity<Long> sumarValores() {
		Long suma = ventaService.sumarValores();
		return new ResponseEntity(new Mensaje(suma.toString()), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}

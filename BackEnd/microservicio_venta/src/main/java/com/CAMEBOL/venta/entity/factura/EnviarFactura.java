package com.CAMEBOL.venta.entity.factura;

public class EnviarFactura {
	private Cliente cliente;
	private Factura factura;

	public EnviarFactura() {
		super();
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

}

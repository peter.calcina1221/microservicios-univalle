package com.CAMEBOL.venta.service;

import java.security.cert.X509Certificate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.TemplateEngine;

import com.CAMEBOL.venta.entity.DetalleVenta;
import com.CAMEBOL.venta.entity.Email;
import com.CAMEBOL.venta.entity.FacturaBDD;
import com.CAMEBOL.venta.entity.Mensaje;
import com.CAMEBOL.venta.entity.ReturnModelCreate;
import com.CAMEBOL.venta.entity.TokenFac;
import com.CAMEBOL.venta.entity.Venta;
import com.CAMEBOL.venta.entity.factura.Cabecera;
import com.CAMEBOL.venta.entity.factura.Cliente;
import com.CAMEBOL.venta.entity.factura.DetalleFactura;
import com.CAMEBOL.venta.entity.factura.EnviarFactura;
import com.CAMEBOL.venta.entity.factura.Factura;
import com.CAMEBOL.venta.repository.VentaRepository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

@Service
@Transactional
public class VentaService {
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private FacturaBDDService facturaBDDService;
	@Autowired
	private TokenService tokenService;
	@Autowired
	VentaRepository ventaRepository;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	JavaMailSender javaMailSender;
	@Value("${spring.mail.username}")
	String mailFrom;
	@Autowired
	TemplateEngine templateEngine;

	public VentaService(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	public List<Venta> getAll() {
		return ventaRepository.findAll();
	}

	public List<Venta> getAllByIdUsuarioAndEstadoAndIdOrganizacion(int idUsuario, String estado, int idOrganizacion) {
		return ventaRepository.findByIdUsuarioAndEstadoAndIdOrganizacion(idUsuario, estado, idOrganizacion);
	}

	public List<Venta> getAllByIdUsuarioAndIdOrganizacion(int idUsuario, int idOrganizacion) {
		return ventaRepository.findByIdUsuarioAndIdOrganizacion(idUsuario, idOrganizacion);
	}

	public List<Venta> getByIdOrganizacion(int idOrganizacion) {
		return ventaRepository.findByIdOrganizacion(idOrganizacion);
	}

	public List<Venta> getByIdOrganizacionYFecha(int idOrganizacion, Date fechaInicio, Date fechaFin) {
		return ventaRepository.findByIdOrganizacionYFecha(idOrganizacion, fechaInicio, fechaFin);
	}
	public List<Venta> getByIdOrganizacionFechaYIdEmpresa(int idOrganizacion, Date fechaInicio, Date fechaFin,int idEmpresa) {
		return ventaRepository.findByIdOrganizacionFechaYIdEmpresa(idOrganizacion, fechaInicio, fechaFin,idEmpresa);
	}

	public List<Venta> getByIdOrganizacionEstado(int idOrganizacion, String estado) {
		return ventaRepository.findByIdOrganizacionAndEstado(idOrganizacion, estado);
	}

	public Optional<Venta> getById(int id) {
		return ventaRepository.findById(id);
	}

	public boolean existsById(Integer id) {
		return ventaRepository.existsById(id);
	}

	public void save(Venta venta) {
		ventaRepository.save(venta);
	}

	public void delete(int id) {
		ventaRepository.deleteById(id);
	}

	public Long sumarValores() {
		return ventaRepository.sumarValores();
	}

	public Mensaje generarLinkPago(Venta venta) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Object> requestEntity = new HttpEntity<>(venta, headers);

		ResponseEntity<ReturnModelCreate> checkResponse = restTemplate.exchange(
				"http://localhost:8098/pago/crear",
				HttpMethod.POST,
				requestEntity,
				ReturnModelCreate.class);
		Mensaje msg = new Mensaje();
		msg.setMensaje(checkResponse.getBody().getItem().getBackUrl());
		msg.setStatus(200);
		msg.setOrderCode(checkResponse.getBody().getItem().getOrderCode());
		return msg;
	}

	public ResponseEntity<?> transaccionVenta(Venta venta) {
		Mensaje msg = new Mensaje();

		try {
			List<DetalleVenta> listDetalleVenta = venta.getDetalleVentas();

			// Verificar si todos los IDs de productos son válidos antes de realizar alguna
			// actualización
			for (DetalleVenta detalle : listDetalleVenta) {
				ResponseEntity<Boolean> checkResponse = restTemplate
						.getForEntity("http://localhost:8094/producto/checkProductoPorIdProductoEstado/"
								+ detalle.getIdProducto(), Boolean.class);
				if (!checkResponse.getBody()) {
					msg.setStatus(HttpStatus.BAD_REQUEST.value());
					msg.setMensaje("Producto inexistente");
					return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
				}
			}
			// verifica la cantidad de los produtos
			for (DetalleVenta detalle : listDetalleVenta) {
				ResponseEntity<Boolean> checkResponse = restTemplate
						.getForEntity("http://localhost:8094/producto/checkCantidadProducto/"
								+ detalle.getIdProducto() + "/" + detalle.getCantidad(), Boolean.class);
				if (!checkResponse.getBody()) {
					msg.setStatus(HttpStatus.BAD_REQUEST.value());
					msg.setMensaje("Stock insuficiente");
					return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
				}
			}

			String msgSend = "";
			FacturaBDD facturaBDD = obtenerAtributo(Facturar(venta));
			facturaBDD.setIdOrganizacion(venta.getIdOrganizacion());
			facturaBDDService.save(facturaBDD);
			venta.setFacturaBDD(facturaBDD);
			ventaRepository.save(venta);

			for (DetalleVenta elemento : listDetalleVenta) {
				// Crear un mensaje con los datos necesarios
				msgSend = elemento.getIdProducto() + ";" + elemento.getCantidad();

				// Enviar el mensaje al Azure Service Bus
				jmsTemplate.convertAndSend("pruebatransaccion", msgSend);
			}

			msg.setStatus(HttpStatus.CREATED.value());
			msg.setMensaje("Venta Exitosa");
			return new ResponseEntity<>(msg, HttpStatus.CREATED);
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Forzar rollback
			msg.setStatus(HttpStatus.BAD_REQUEST.value());
			msg.setMensaje(ex.getMessage());
			return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
		}
	}

	public String pruebaEnvioCola(Venta venta) {
		List<DetalleVenta> listDetalleVenta = venta.getDetalleVentas();
		String msgSend = "";
		for (DetalleVenta elemento : listDetalleVenta) {
			// Crear un mensaje con los datos necesarios
			msgSend = elemento.getIdProducto() + ";" + elemento.getCantidad();

			// Enviar el mensaje al Azure Service Bus
			jmsTemplate.convertAndSend("pruebatransaccion", msgSend);
			System.err.println("Enviando: " + msgSend);
		}
		return msgSend;

	}

	public String Facturar(Venta venta) {
		// URL de la API
		String apiUrl = "https://ns2-enterprise.eerpbo.com:6001/api_facturar";
		String productoSIN = "";
		String actividadSIN = "";
		// crear el objeto para mandar a la api
		Cliente cliente = new Cliente();
		cliente.setNit(5815709017L);
		cliente.setUsuario("user.mariafernanda");
		cliente.setPassword("123456");
		cliente.setCodigo_documentos_sector_id(1);
		cliente.setSucursal(0);
		cliente.setPos(0);
		cliente.setActividadEconomica("471900");
		cliente.setImeimac("56:3C:43:56:C7:B8");

		UUID uuid = UUID.randomUUID();

		Cabecera cabecera = new Cabecera();
		cabecera.setIdTransaccion("ID-" + uuid); // falta generar - OK
		cabecera.setNombreRazonSocial(venta.getRazonSocial()); // sacar del pago
		cabecera.setCorreoElectronico(venta.getCorreo()); // sacar del pago
		cabecera.setCodigoTipoDocumentoIdentidad(venta.getTipoDocumento()); // NIT Verificado
		cabecera.setNumeroDocumento(venta.getNit()); // sacar del pago
		cabecera.setComplemento("");
		cabecera.setCodigoCliente(venta.getIdUsuario() + ""); // ID usuario
		cabecera.setCodigoMetodoPago(81); // Pago online
		cabecera.setNumeroTarjeta(0); // D
		cabecera.setMontoTotal(venta.getTotal());// Total
		cabecera.setMontoTotalSujetoIva(venta.getTotal()); // D
		cabecera.setCodigoMoneda(1);// bolivianos
		cabecera.setTipoCambio(1);// 1Boiviano es 1boliviano
		cabecera.setMontoTotalMoneda(venta.getTotal());// aqui habia un 100
		cabecera.setMontoGiftCard(0);
		cabecera.setDescuentoAdicional(0);
		cabecera.setCodigoExcepcion(0);
		cabecera.setUsuario("factufacil.enterprise");
		cabecera.setCodigoDocumentoSector(1);
		cabecera.setTipoFacturaDocumento(1);

		// tiene que funcionar con un for de ventas

		List<DetalleVenta> detalleVentas = venta.getDetalleVentas();
		List<DetalleFactura> listDetalleFacturas = new ArrayList<>();

		// ObjectMapper de Jackson para parsear JSON
		ObjectMapper objectMapper = new ObjectMapper();
		for (DetalleVenta detalleVenta : detalleVentas) {
			try {
				ResponseEntity<String> response = restTemplate.exchange(
						"http://localhost:8094/producto/obtenerProductoPorId/" + detalleVenta.getIdProducto(),
						HttpMethod.GET,
						null,
						String.class);
				// Parsear el JSON a un objeto JsonNode
				JsonNode jsonNode = objectMapper.readTree(response.getBody());

				// Obtener el valor del json
				productoSIN = jsonNode
						.at("/codigoProductoSIN")
						.asText();

				String[] parts = productoSIN.split("\\|");

				if (parts.length == 2) {
					productoSIN = parts[0]; // Contiene "62145"
					actividadSIN = parts[1]; // Contiene "471900"
				} else {
					System.err.println("El formato del string no es válido.");
				}

				DetalleFactura detalleFactura = new DetalleFactura();
				detalleFactura.setActividadEconomica(actividadSIN); // api producto
				detalleFactura.setCodigoProductoSin(Integer.parseInt(productoSIN)); // api producto
				detalleFactura.setCodigoProducto(jsonNode
						.at("/codigo")
						.asText());// api producto
				detalleFactura.setDescripcion(jsonNode
						.at("/descripcion")
						.asText()); // api producto
				detalleFactura.setCantidad(detalleVenta.getCantidad());
				detalleFactura.setUnidadMedida(Integer.parseInt(jsonNode
						.at("/codigoUnidadSIN")
						.asText()));// api producto
				detalleFactura.setPrecioUnitario(detalleVenta.getPrecioUnitario());
				detalleFactura.setMontoDescuento(0);
				detalleFactura.setSubTotal(detalleVenta.getSubTotal());
				detalleFactura.setNumeroSerie("");
				detalleFactura.setNumeroImei("");
				listDetalleFacturas.add(detalleFactura);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		Factura factura = new Factura();
		factura.setCabecera(cabecera);
		factura.setDetalle(listDetalleFacturas);

		EnviarFactura enviarFactura = new EnviarFactura();
		enviarFactura.setCliente(cliente);
		enviarFactura.setFactura(factura);

		// Configura la cabecera
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// Configura los parámetros en un mapa
		TokenFac token = tokenService.verificarToken();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl)
				.queryParam("token", token.getToken());

		// Configura la solicitud con los parámetros en el cuerpo
		HttpEntity<EnviarFactura> requestEntity = new HttpEntity<>(enviarFactura, headers);

		// RestTemplate
		RestTemplate restTemplate = new RestTemplate();
		// Deshabilita la validación de SSL
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}

					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}
				}
		};

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
			// Realiza la solicitud POST
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					uriBuilder.toUriString(),
					HttpMethod.POST,
					requestEntity,
					String.class);
			// // Imprime la respuesta y devuelve el objeto
			System.out.println("Respuesta de la API: " + responseEntity.getBody());
			return responseEntity.getBody();

		} catch (Exception e) {
			// Maneja cualquier excepción
			e.printStackTrace();
			// Puedes decidir cómo manejar errores en tu aplicación
			return null;
		}
	}

	public FacturaBDD obtenerAtributo(String factura) {
		FacturaBDD facturaBDD = new FacturaBDD();
		// Tu JSON como una cadena
		String json = factura;

		// ObjectMapper de Jackson para parsear JSON
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			// Parsear el JSON a un objeto JsonNode
			JsonNode jsonNode = objectMapper.readTree(json);

			// Obtener el valor del json
			facturaBDD.setNumeroFactura(jsonNode
					.at("/facturaElectronicaCompraVenta/cabecera/numeroFactura")
					.asText());
			facturaBDD.setXml(jsonNode
					.at("/factura_xml_base64")
					.asText());
			facturaBDD.setRazonSocial(jsonNode
					.at("/facturaElectronicaCompraVenta/cabecera/nombreRazonSocial")
					.asText());
			facturaBDD.setNumeroDocumeto(jsonNode
					.at("/facturaElectronicaCompraVenta/cabecera/numeroDocumento")
					.asText());
			facturaBDD.setCuf(jsonNode
					.at("/facturaElectronicaCompraVenta/cabecera/cuf")
					.asText());
			facturaBDD.setTotal(jsonNode
					.at("/facturaElectronicaCompraVenta/cabecera/montoTotal")
					.asDouble());
			facturaBDD.setUrl(jsonNode
					.at("/url_qr")
					.asText());

			return facturaBDD;

		} catch (Exception e) {
			// Manejar cualquier excepción que pueda ocurrir durante el parseo
			e.printStackTrace();
			return null;
		}
	}

	public void sendEmailTemplate(Email email) {
		String userName = "";
		String emailTo = "";
		MimeMessage message = javaMailSender.createMimeMessage();
		// ObjectMapper de Jackson para parsear JSON
		ObjectMapper objectMapper = new ObjectMapper();
		try {

			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8095/usuario/UsuarioId/" + email.getIdUsuario(),
					HttpMethod.GET,
					null,
					String.class);
			// Parsear el JSON a un objeto JsonNode
			JsonNode jsonNode = objectMapper.readTree(response.getBody());

			// Obtener el valor del json
			userName = jsonNode
					.at("/nombreUsuario")
					.asText();
			emailTo = jsonNode
					.at("/email")
					.asText();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			org.thymeleaf.context.Context context = new org.thymeleaf.context.Context();
			Map<String, Object> model = new HashMap<>();
			model.put("titulo", email.getTitulo());
			model.put("userName", userName);
			model.put("urlImagen", email.getUrlImagen());
			model.put("mensaje", email.getMensaje());
			context.setVariables(model);
			String htmlText = templateEngine.process("notificacionTemplate", context);
			helper.setFrom(mailFrom);
			helper.setTo(emailTo);
			helper.setSubject(email.getSubject());
			helper.setText(htmlText, true);
			javaMailSender.send(message);

		} catch (MessagingException | JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}

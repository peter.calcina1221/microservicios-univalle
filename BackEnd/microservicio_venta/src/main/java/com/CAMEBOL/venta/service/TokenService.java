package com.CAMEBOL.venta.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.venta.entity.TokenFac;
import com.CAMEBOL.venta.entity.TokenRespuesta;
import com.CAMEBOL.venta.repository.TokenRespository;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@Service
@Transactional
public class TokenService {
	@Autowired
	TokenRespository tokenRespository;
	@Autowired
	private RestTemplate restTemplate;

	public TokenService(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	public TokenFac verificarToken() {
		TokenFac token = getById(2).get();
		try {
			LocalDateTime now = LocalDateTime.now();
			LocalDateTime fishingDataDateTime = token.getFechaActualizacion()
					.toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDateTime();

			Duration duration = Duration.between(fishingDataDateTime, now);
			long hoursDifference = duration.toHours();
			if (hoursDifference >= 23) {
				TokenRespuesta tokenRespuesta = generarToken();
				token.setToken(tokenRespuesta.getToken());
				save(token);
				TokenFac tokenNuevo = getById(2).get();
				creandoLog("Venta", "Actualizar", "Se actualizo el token para la facturacion", "Exito");
				return tokenNuevo;
			} else {
				return token;
			}
		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return token;
		}

	}

	public TokenRespuesta generarToken() {
		// URL de la API
		String apiUrl = "https://ns2-enterprise.eerpbo.com:6001/token";

		// JSON a enviar
		String jsonInput = "{\"cliente\":{\"nit\":\"5815709017\",\"usuario\":\"user.mariafernanda\",\"password\":\"123456\",\"imeimac\":\"56:3C:43:56:C7:B8\"}}";

		// Configura la cabecera
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// Configura la solicitud
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonInput, headers);

		// RestTemplate
		RestTemplate restTemplate = new RestTemplate();
		// Deshabilita la validación de SSL
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}

					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}
				}
		};

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
			// Realiza la solicitud POST
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Deserializa la respuesta JSON en un objeto TokenRespuesta
			ObjectMapper objectMapper = new ObjectMapper();
			TokenRespuesta tokenRespuesta = objectMapper.readValue(responseEntity.getBody(), TokenRespuesta.class);

			// Imprime la respuesta y devuelve el objeto TokenRespuesta
			System.out.println("Respuesta de la API: " + tokenRespuesta);
			return tokenRespuesta;
		} catch (Exception e) {
			// Maneja cualquier excepción
			e.printStackTrace();
			// Puedes decidir cómo manejar errores en tu aplicación
			return null;
		}
	}

	public List<TokenFac> getAll() {
		return tokenRespository.findAll();
	}

	public Optional<TokenFac> getById(int id) {
		return tokenRespository.findById(id);
	}

	public boolean existsById(Integer id) {
		return tokenRespository.existsById(id);
	}

	public void save(TokenFac tokenFac) {
		tokenRespository.save(tokenFac);
	}

	public void delete(int id) {
		tokenRespository.deleteById(id);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}

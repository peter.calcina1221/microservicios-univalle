package com.CAMEBOL.venta.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.venta.entity.DetalleVenta;
import com.CAMEBOL.venta.entity.Venta;

@Repository
public interface DetalleVentaRepository extends JpaRepository<DetalleVenta, Integer> {
	List<DetalleVenta> findByVenta(Venta venta);

	List<DetalleVenta> findByIdEmpresa(int idEmpresa);

	List<DetalleVenta> findByVentaAndEstado(Venta venta, String estado);

	List<DetalleVenta> findByIdEmpresaAndCodigo(int idEmpresa, String codigo);

	List<DetalleVenta> findByIdEmpresaAndEstado(int idEmpresa, String estado);

	@Query("SELECT dv FROM DetalleVenta dv WHERE DATE(dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin AND dv.idEmpresa=:idEmpresa")
	List<DetalleVenta> listarPorRangoFechaYEmpresa(@Param("fechaInicio") Date fechaInicio,
			@Param("fechaFin") Date fechaFin, @Param("idEmpresa") int idEmpresa);

	@Query("SELECT dv.idProducto, SUM(dv.cantidad) AS cantidad FROM com.CAMEBOL.venta.entity.DetalleVenta dv WHERE dv.idEmpresa= :idEmpresa GROUP BY dv.idProducto ORDER BY cantidad DESC")
	List<Object[]> listarProductoMasVendidoPorEmpresa(@Param("idEmpresa") int idEmpresa);

	@Query("SELECT dv.idProducto, SUM(dv.cantidad) AS cantidad FROM com.CAMEBOL.venta.entity.DetalleVenta dv WHERE dv.idEmpresa= :idEmpresa AND DATE(dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin GROUP BY dv.idProducto ORDER BY cantidad DESC")
	List<Object[]> listarProductoMasVendidoPorEmpresaYFecha(@Param("idEmpresa") int idEmpresa,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);

	@Query("SELECT dv.idProducto, SUM(dv.cantidad) AS cantidad FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion GROUP BY dv.idProducto ORDER BY cantidad DESC")
	List<Object[]> listarProductoMasVendidoPorOrganizacion(@Param("idOrganizacion") int idOrganizacion);

	@Query("SELECT dv.idProducto, COUNT(dv.idProducto) AS cantidad FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idUsuario = :idUsuario GROUP BY dv.idProducto ORDER BY cantidad DESC")
	List<Object[]> listarProductoMasCompradosPorusuario(@Param("idUsuario") int idUsuario);

	@Query("SELECT dv.idProducto, SUM(dv.subTotal) AS subTotal FROM com.CAMEBOL.venta.entity.DetalleVenta dv WHERE dv.idEmpresa= :idEmpresa GROUP BY dv.idProducto ORDER BY subTotal DESC")
	List<Object[]> listarSubTotalProductosPorEmpresa(@Param("idEmpresa") int idEmpresa);

	@Query("SELECT dv.idProducto, SUM(dv.subTotal) AS subTotal FROM com.CAMEBOL.venta.entity.DetalleVenta dv WHERE dv.idEmpresa= :idEmpresa AND DATE(dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin GROUP BY dv.idProducto ORDER BY subTotal DESC")
	List<Object[]> listarSubTotalProductosPorEmpresaYFecha(@Param("idEmpresa") int idEmpresa,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);

	@Query("SELECT dv.idEmpresa, COUNT(dv.idEmpresa) AS cantidad FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion GROUP BY dv.idEmpresa ORDER BY cantidad DESC")
	List<Object[]> listarEmpresasQueMasVenden(@Param("idOrganizacion") int idOrganizacion);

	@Query("SELECT dv.idEmpresa, COUNT(dv.idEmpresa) AS cantidad FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion AND DATE (dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin GROUP BY dv.idEmpresa ORDER BY cantidad DESC")
	List<Object[]> listarEmpresasQueMasVendenYFecha(@Param("idOrganizacion") int idOrganizacion,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);

	@Query("SELECT dv.idEmpresa, COUNT(dv.idEmpresa) AS cantidad FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idUsuario = :idUsuario GROUP BY dv.idEmpresa ORDER BY cantidad DESC")
	List<Object[]> listarEmpresasQueMasCompranPorUsuario(@Param("idUsuario") int idUsuario);

	@Query("SELECT dv FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idUsuario = :idUsuario AND dv.estado = :estado")
	List<DetalleVenta> ListarPorUsuarioYEstado(@Param("idUsuario") int idUsuario, @Param("estado") String estado);

	@Query("SELECT dv FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion")
	List<DetalleVenta> ListarPorIdOrganizacion(@Param("idOrganizacion") int idOrganizacion);

	@Query("SELECT dv FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion AND DATE(dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<DetalleVenta> ListarPorIdOrganizacionYFecha(@Param("idOrganizacion") int idOrganizacion,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
	@Query("SELECT dv FROM DetalleVenta dv INNER JOIN dv.venta v WHERE v.idOrganizacion = :idOrganizacion AND DATE(dv.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin AND dv.idEmpresa = :idEmpresa")
	List<DetalleVenta> ListarPorIdOrganizacionFechaYIdEmpresa(@Param("idOrganizacion") int idOrganizacion,
			@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin,@Param("idEmpresa") int idEmpresa);

}

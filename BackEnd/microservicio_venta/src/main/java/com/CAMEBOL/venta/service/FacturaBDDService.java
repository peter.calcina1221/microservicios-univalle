package com.CAMEBOL.venta.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CAMEBOL.venta.entity.FacturaBDD;
import com.CAMEBOL.venta.repository.FacturaBDDRepository;

@Service
public class FacturaBDDService {
    @Autowired
    FacturaBDDRepository facturaBDDRepository;

    public List<FacturaBDD> getAll() {
        return facturaBDDRepository.findAll();
    }

    public List<FacturaBDD> getAllByIdOrganizacion(int idOrganizacion) {
        return facturaBDDRepository.findByIdOrganizacion(idOrganizacion);
    }

    public Optional<FacturaBDD> getById(int id) {
        return facturaBDDRepository.findById(id);
    }

    public boolean existsById(Integer id) {
        return facturaBDDRepository.existsById(id);
    }

    public void save(FacturaBDD facturaBDD) {
        facturaBDDRepository.save(facturaBDD);
    }

    public void delete(int id) {
        facturaBDDRepository.deleteById(id);
    }
}

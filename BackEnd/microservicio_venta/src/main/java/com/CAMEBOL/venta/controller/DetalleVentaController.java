package com.CAMEBOL.venta.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.venta.entity.DetalleVenta;
import com.CAMEBOL.venta.entity.EmpresasVenden;
import com.CAMEBOL.venta.entity.Mensaje;
import com.CAMEBOL.venta.entity.ProductoCantidad;
import com.CAMEBOL.venta.entity.ProductoSubTotal;
import com.CAMEBOL.venta.entity.UsuarioProducto;
import com.CAMEBOL.venta.entity.Venta;
import com.CAMEBOL.venta.service.DetalleVentaService;
import com.CAMEBOL.venta.service.VentaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/detalleVenta")
@CrossOrigin(origins = "*")
public class DetalleVentaController {
	@Autowired
	private VentaService ventaService;
	@Autowired
	private DetalleVentaService detalleVentaService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<DetalleVenta>> getAll() {
		List<DetalleVenta> list = detalleVentaService.getAll();
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdVenta/{idVenta}")
	public ResponseEntity<List<DetalleVenta>> getAllByEstado(@PathVariable("idVenta") int idVenta) {
		if (!ventaService.existsById(idVenta)) {
			return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
		}
		Venta venta = ventaService.getById(idVenta).get();
		List<DetalleVenta> list = detalleVentaService.getAllByIdVenta(venta);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdVentaYEstado/{idVenta}/{estado}")
	public ResponseEntity<List<DetalleVenta>> getAllByEstado(@PathVariable("idVenta") int idVenta,
			@PathVariable("estado") String estado) {
		if (!ventaService.existsById(idVenta)) {
			return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
		}
		Venta venta = ventaService.getById(idVenta).get();
		List<DetalleVenta> list = detalleVentaService.getIdVentaAndEstado(venta, estado);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdEmpresa/{idEmpresa}")
	public ResponseEntity<List<DetalleVenta>> getByIdEmpresa(@PathVariable("idEmpresa") int idEmpresa) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.getByIdEmpresa(idEmpresa);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<DetalleVenta>> getByIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.listarPorIdOrganiacion(idOrganizacion);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdOrganizacionYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<DetalleVenta>> getByIdOrganizacionYFecha(
			@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.listarPorIdOrganiacionYFecha(idOrganizacion, fechaInicio, fechaFin);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}
	@GetMapping("/listaPorIdOrganizacionFechaYIdEmpresa/{idOrganizacion}/{fechaInicio}/{fechaFin}/{idEmpresa}")
	public ResponseEntity<List<DetalleVenta>> getByIdOrganizacionFechaYIdEmpresa(
			@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,@PathVariable("idEmpresa") int idEmpresa) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.listarPorIdOrganiacionFechaYIdEmpresa(idOrganizacion, fechaInicio, fechaFin,idEmpresa);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdEmpresaYCodigo/{idEmpresa}/{codigo}")
	public ResponseEntity<List<DetalleVenta>> getByIdEmpresaCodigo(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("codigo") String codigo) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.getByIdEmpresaCodigo(idEmpresa, codigo);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorIdEmpresaYEstado/{idEmpresa}/{estado}")
	public ResponseEntity<List<DetalleVenta>> getByIdEmpresaEstado(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("estado") String estado) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.getByIdEmpresaEstado(idEmpresa, estado);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaPorFechasYEmpresa/{fechaInicio}/{fechaFin}/{idEmpresa}")
	public ResponseEntity<List<DetalleVenta>> getByFechasYEmpresa(
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,
			@PathVariable("idEmpresa") int idEmpresa) {
		// realizar validaciones si la empresa existe
		List<DetalleVenta> list = detalleVentaService.getByFechasAndIdEmpresa(fechaInicio, fechaFin, idEmpresa);
		return new ResponseEntity<List<DetalleVenta>>(list, HttpStatus.OK);
	}

	@GetMapping("/listaProductoMasVendidoPorEmpresa/{idEmpresa}/{limit}")
	public List<ProductoCantidad> getBestSellingProductByEmpresa(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarProductoMasVendidoPorEmpresa(idEmpresa, limit);
	}

	@GetMapping("/listaProductoMasVendidoPorEmpresaYFecha/{idEmpresa}/{fechaInicio}/{fechaFin}/{limit}")
	public List<ProductoCantidad> getBestSellingProductByEmpresaAndDate(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarProductoMasVendidoPorEmpresaYFecha(idEmpresa, fechaInicio, fechaFin, limit);
	}

	@GetMapping("/listaProductoMasVendidoPorOrganizacion/{idOrganizacion}/{limit}")
	public List<ProductoCantidad> getBestSellingProductByOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarProductoMasVendidoPorOrganizacion(idOrganizacion, limit);
	}

	@GetMapping("/listarSubTotalProductosPorEmpresa/{idEmpresa}")
	public List<ProductoSubTotal> getSubTotalProductByEmpresa(@PathVariable("idEmpresa") int idEmpresa) {
		return detalleVentaService.listarSubTotalProductosPorEmpresa(idEmpresa);
	}

	@GetMapping("/listarSubTotalProductosPorEmpresaYFecha/{idEmpresa}/{fechaInicio}/{fechaFin}")
	public List<ProductoSubTotal> getSubTotalProductByEmpresaAndDate(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		return detalleVentaService.listarSubTotalProductosPorEmpresaYFecha(idEmpresa, fechaInicio, fechaFin);
	}

	@GetMapping("/ListaProductoCompradosPorUsuario/{idUsuario}")
	public List<UsuarioProducto> getProductoByUsuario(@PathVariable("idUsuario") int idUsuario) {
		return detalleVentaService.listarPorductoCompradosPorUsuarios(idUsuario);
	}

	@GetMapping("/ListarPorUsuarioYEstado/{idUsuario}/{estado}")
	public List<DetalleVenta> getDetalleVentaByUsuarioAndEstado(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("estado") String estado) {
		return detalleVentaService.ListarPorUsuarioYEstado(idUsuario, estado);
	}

	@GetMapping("/listarEmpresasQueMasVenden/{idOrganizacion}/{limit}")
	public List<EmpresasVenden> getBestSellingEmpresaByEmpresa(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarEmpresasQueMasVenden(idOrganizacion, limit);
	}

	@GetMapping("/listarEmpresasQueMasVendenYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}/{limit}")
	public List<EmpresasVenden> getBestSellingEmpresaByEmpresaYFecha(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarEmpresasQueMasVendenYFecha(idOrganizacion, fechaInicio, fechaFin, limit);
	}

	@GetMapping("/listarEmpresasQueMasCompranPorUsuario/{idUsuario}/{limit}")
	public List<EmpresasVenden> getBestSellingEmpresaByUsuario(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("limit") int limit) {
		return detalleVentaService.listarEmpresasQueMasCompranPorUsuario(idUsuario, limit);
	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody DetalleVenta detalleVenta, BindingResult bindingResult) {
		try {
			detalleVentaService.save(detalleVenta);
			creandoLog("Venta", "Insertar",
					"Se creo el detalle venta de la venta " + detalleVenta.getVenta().getCodigoVenta(), "Exito");
			return new ResponseEntity(new Mensaje("Detalle Venta exitoso"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizarEstado/{idDetalleVenta}/{estado}")
	public ResponseEntity<?> actualizarEstado(@PathVariable("idDetalleVenta") int idDetalleVenta,
			@PathVariable("estado") String estado) {
		try {
			DetalleVenta detalleVenta = detalleVentaService.getById(idDetalleVenta).get();
			detalleVenta.setEstado(estado);
			detalleVentaService.save(detalleVenta);
			creandoLog("Venta", "Actualizar", "Se actualizo el estado del detalle venta numero " + idDetalleVenta, "Exito");
			return new ResponseEntity(new Mensaje("Estado Actualizado"), HttpStatus.CREATED);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{idDetalleVenta}")
	public ResponseEntity<?> actualizar(@PathVariable("idDetalleVenta") int idDetalleVenta,
			@RequestBody DetalleVenta detalleVenta) {

		try {
			DetalleVenta detVenta = detalleVentaService.getById(idDetalleVenta).get();
			detVenta.setCantidad(detalleVenta.getCantidad());
			detVenta.setDescuento(detalleVenta.getDescuento());
			detVenta.setEstado(detalleVenta.getEstado());
			detVenta.setFechaRegistro(detalleVenta.getFechaRegistro());
			detVenta.setIdEmpresa(detalleVenta.getIdEmpresa());
			detVenta.setIdProducto(detalleVenta.getIdProducto());
			detVenta.setPrecioUnitario(detalleVenta.getPrecioUnitario());
			detVenta.setSubTotal(detalleVenta.getSubTotal());
			detVenta.setVenta(detalleVenta.getVenta());
			detVenta.setCodigo(detalleVenta.getCodigo());
			detalleVentaService.save(detVenta);
			creandoLog("Venta", "Actualizar", "Se actualizo el detalle venta numero " + idDetalleVenta, "Exito");
			return new ResponseEntity(new Mensaje("Detalle Venta Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Venta", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/elimnar/{idDetalleVenta}")
	public ResponseEntity<?> delete(@PathVariable("idDetalleVenta") int idDetalleVenta) {
		detalleVentaService.delete(idDetalleVenta);
		return new ResponseEntity(new Mensaje("Detalle Venta Eliminada"), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}

package com.CAMEBOL.compra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CAMEBOL.compra.entity.Departamento;


public interface DepartamentoRepository extends JpaRepository<Departamento, Integer> {

}

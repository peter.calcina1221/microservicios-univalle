package com.CAMEBOL.compra.repository;

import org.springframework.data.jpa.repository.JpaRepository;



public interface ProveedorRepository extends JpaRepository<com.CAMEBOL.compra.entity.Proveedor, Integer> {

}

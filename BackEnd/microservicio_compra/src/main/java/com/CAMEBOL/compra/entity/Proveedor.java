package com.CAMEBOL.compra.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "ProveedorRepository")
public class Proveedor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProveedor;
	private String nombre;
	private String email;
	private String celular;
	private String direccion;
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private String estado;
	
	 @OneToOne
	 @JoinColumn(name = "idDepartamento")
	 private Departamento departamento;
	 
	@JsonIgnoreProperties(value = { "proveedor" }, allowSetters = true)
	@OneToMany(mappedBy = "proveedor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Compra> compras;
}

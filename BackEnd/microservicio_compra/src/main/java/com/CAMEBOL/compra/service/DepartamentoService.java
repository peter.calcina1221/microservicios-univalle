package com.CAMEBOL.compra.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.compra.entity.Departamento;
import com.CAMEBOL.compra.repository.DepartamentoRepository;



@Service
@Transactional
public class DepartamentoService {
	@Autowired
	DepartamentoRepository departamentoRepository;
	 public List<Departamento> getAll(){
	    	
	    	return departamentoRepository.findAll();
	    }
	 public Optional<Departamento> getById(int id){
	        return departamentoRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return departamentoRepository.existsById(id);
	    }
	 public void save(Departamento categoria){
		 departamentoRepository.save(categoria);
	    }
	  public void delete(int id) {
		  departamentoRepository.deleteById(id);
	    }
}

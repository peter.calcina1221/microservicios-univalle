package com.CAMEBOL.compra.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name = "Departamento")
public class Departamento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDepartamento;
	private String nombre;
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private String estado;
	
	@OneToOne(mappedBy = "departamento")
	private Proveedor proveedor;
}

package com.CAMEBOL.compra.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.compra.entity.Compra;
import com.CAMEBOL.compra.service.CompraService;


@RestController
@RequestMapping("/compra")
@CrossOrigin(origins = "*")
public class CompraController {
	@Autowired
	private CompraService compraService;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Compra>> getAll(){
		List<Compra> list = compraService.getAll();
		return  new ResponseEntity<List<Compra>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Compra> getById(@PathVariable("id") int id){
		if(!compraService.existsById(id)) {
			return new ResponseEntity("El id no existe",HttpStatus.NOT_FOUND);
		}
		Compra organizacion=compraService.getById(id).get();
		return new ResponseEntity(organizacion,HttpStatus.OK);
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@RequestBody Compra organizacion, BindingResult bindingResult){
	
		compraService.save(organizacion);
        
		return new ResponseEntity("Organizacion creada",HttpStatus.CREATED);
    }

	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		compraService.delete(id);
		return new ResponseEntity("Organizacion Eliminada",HttpStatus.OK);
	}
}

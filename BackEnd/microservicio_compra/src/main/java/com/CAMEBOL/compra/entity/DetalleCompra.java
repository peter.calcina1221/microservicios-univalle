package com.CAMEBOL.compra.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "DetalleCompra")
public class DetalleCompra {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDetalleCompra;
	private int idProducto;
	private Double cantidad;
	private Double precioUnitario;
	private Double descuento;
	private Double subTotal;
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private String estado;
	
	@JsonIgnoreProperties(value = {"detalleCompras"} , allowSetters = true)
	@ManyToOne()
	@JoinColumn(name = "idCompra")
	 private Compra compra;
}

package com.CAMEBOL.compra.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.compra.entity.Proveedor;
import com.CAMEBOL.compra.repository.ProveedorRepository;


@Service
@Transactional
public class ProveedorService {
	@Autowired
	ProveedorRepository proveedorRepository;
	 public List<Proveedor> getAll(){
	    	
	    	return proveedorRepository.findAll();
	    }
	 public Optional<Proveedor> getById(int id){
	        return proveedorRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return proveedorRepository.existsById(id);
	    }
	 public void save(Proveedor categoria){
		 proveedorRepository.save(categoria);
	    }
	  public void delete(int id) {
		  proveedorRepository.deleteById(id);
	    }
}

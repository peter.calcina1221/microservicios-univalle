package com.CAMEBOL.compra.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.compra.entity.DetalleCompra;
import com.CAMEBOL.compra.repository.DetalleCompraRepository;


@Service
@Transactional
public class DetalleCompraService {
	@Autowired
	DetalleCompraRepository detalleCompraRepository;
	 public List<DetalleCompra> getAll(){
	    	
	    	return detalleCompraRepository.findAll();
	    }
	 public Optional<DetalleCompra> getById(int id){
	        return detalleCompraRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return detalleCompraRepository.existsById(id);
	    }
	 public void save(DetalleCompra categoria){
		 detalleCompraRepository.save(categoria);
	    }
	  public void delete(int id) {
		  detalleCompraRepository.deleteById(id);
	    }
}

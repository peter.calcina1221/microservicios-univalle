package com.CAMEBOL.compra.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.compra.entity.Compra;
import com.CAMEBOL.compra.repository.CompraRepository;


@Service
@Transactional
public class CompraService {
	@Autowired
	CompraRepository compraRepository;
	 public List<Compra> getAll(){
	    	
	    	return compraRepository.findAll();
	    }
	 public Optional<Compra> getById(int id){
	        return compraRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return compraRepository.existsById(id);
	    }
	 public void save(Compra categoria){
		 compraRepository.save(categoria);
	    }
	  public void delete(int id) {
		  compraRepository.deleteById(id);
	    }
}

package com.CAMEBOL.compra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CAMEBOL.compra.entity.DetalleCompra;



public interface DetalleCompraRepository extends JpaRepository<DetalleCompra, Integer> {

}

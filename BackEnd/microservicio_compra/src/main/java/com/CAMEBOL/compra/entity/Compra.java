package com.CAMEBOL.compra.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "Compra")
public class Compra {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCompra;
	private Date fechaCompra;
	private Date fechaPago;
	private Double descuento;
	private Double total;
	private int idUsuario;
	private int idEmpresa;
	private int idOrganizacion;
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private String estado;
	
	@JsonIgnoreProperties(value = {"compras"} , allowSetters = true)
	@ManyToOne()
	@JoinColumn(name = "idProveedor")
	 private Proveedor proveedor;
	
	@JsonIgnoreProperties(value = { "compra" }, allowSetters = true)
	@OneToMany(mappedBy = "compra", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<DetalleCompra> detalleCompras;
}

package com.CAMEBOL.organizacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.organizacion.entity.Organizacion;
import com.CAMEBOL.organizacion.repository.OrganizacionRepository;




@Service
@Transactional
public class OrganizacionService {
	@Autowired
	OrganizacionRepository organizacionRepository;
	 public List<Organizacion> getAll(){
	    	
	    	return organizacionRepository.findAll();
	    }
	 public List<Organizacion> getByIdOrganizacion(int idOrganizacion){
	    	
	    	return organizacionRepository.findByIdOrganizacion(idOrganizacion);
	    }
	 public Optional<Organizacion> getById(int id){
	        return organizacionRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return organizacionRepository.existsById(id);
	    }
	 public void save(Organizacion organizacion){
		 organizacionRepository.save(organizacion);
	    }
	  public void delete(int id) {
	    	organizacionRepository.deleteById(id);
	    }
	    
}

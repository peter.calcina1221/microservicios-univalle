package com.CAMEBOL.organizacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.organizacion.entity.Organizacion;

@Repository
public interface OrganizacionRepository extends JpaRepository<Organizacion, Integer>{
	List<Organizacion> findByIdOrganizacion(int idOrganizacion);
}

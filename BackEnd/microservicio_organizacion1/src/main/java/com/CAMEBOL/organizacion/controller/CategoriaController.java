package com.CAMEBOL.organizacion.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.organizacion.entity.Categoria;
import com.CAMEBOL.organizacion.entity.Mensaje;
import com.CAMEBOL.organizacion.service.CategoriaService;


@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins = "*")
public class CategoriaController {
	@Autowired
	private CategoriaService categoriaService;
	@GetMapping("/lista")
	public ResponseEntity<List<Categoria>> getAll(){
		List<Categoria> list = categoriaService.getAll();
		return  new ResponseEntity<List<Categoria>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Categoria> getById(@PathVariable("id") int id){
		if(!categoriaService.existsById(id)) {
			return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
		}
		Categoria categoria=categoriaService.getById(id).get();
		return new ResponseEntity(categoria,HttpStatus.OK);
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Categoria categoria, BindingResult bindingResult){
	
		categoriaService.save(categoria);
        
		return new ResponseEntity(new Mensaje("Categoria creada"),HttpStatus.CREATED);
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Categoria categoria){

			
		Categoria cat=categoriaService.getById(id).get();
		cat.setNombre(categoria.getNombre());
		cat.setDescripcion(categoria.getDescripcion());
		cat.setEstado(categoria.getEstado());
		// faltan actualizar las fechas 
		categoriaService.save(cat);
		return new ResponseEntity(new Mensaje("Organizacion Actualizada"),HttpStatus.OK);
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		categoriaService.delete(id);
		return new ResponseEntity(new Mensaje("Organizacion Eliminada"),HttpStatus.OK);
	}
	

}

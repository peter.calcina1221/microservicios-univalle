package com.CAMEBOL.organizacion.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.organizacion.entity.Categoria;
import com.CAMEBOL.organizacion.entity.Mensaje;
import com.CAMEBOL.organizacion.entity.Organizacion;
import com.CAMEBOL.organizacion.service.OrganizacionService;



@RestController
@RequestMapping("/organizacion")
@CrossOrigin(origins = "*")
public class OrganizacionController {
	@Autowired
	private OrganizacionService organizacionService;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Organizacion>> getAll(){
		List<Organizacion> list = organizacionService.getAll();
		return  new ResponseEntity<List<Organizacion>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<Organizacion>> getAll(@PathVariable("idOrganizacion") int idOrganizacion){
		List<Organizacion> list = organizacionService.getByIdOrganizacion(idOrganizacion);
		return  new ResponseEntity<List<Organizacion>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Organizacion> getById(@PathVariable("id") int id){
		if(!organizacionService.existsById(id)) {
			return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
		}
		Organizacion organizacion=organizacionService.getById(id).get();
		return new ResponseEntity(organizacion,HttpStatus.OK);
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Organizacion organizacion, BindingResult bindingResult){
	
		organizacionService.save(organizacion);
        
		return new ResponseEntity(new Mensaje("Organizacion creada"),HttpStatus.CREATED);
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Organizacion organizacion){

			
		Organizacion org=organizacionService.getById(id).get();
		org.setNombreOrganizacion(organizacion.getNombreOrganizacion());
		org.setRazonSocial(organizacion.getRazonSocial());
		org.setNit(organizacion.getNit());
		org.setEmail(organizacion.getEmail());
		org.setLatitud(organizacion.getLatitud());
		org.setLongitud(organizacion.getLongitud());
		org.setDescripcion(organizacion.getDescripcion());
		org.setNumeroCelular(organizacion.getNumeroCelular());
		org.setEstado(organizacion.getEstado());
		org.setCategoria(organizacion.getCategoria());
		// faltan actualizar las fechas 
		organizacionService.save(org);
		return new ResponseEntity(new Mensaje("Organizacion Actualizada"),HttpStatus.OK);
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		organizacionService.delete(id);
		return new ResponseEntity(new Mensaje("Organizacion Eliminada"),HttpStatus.OK);
	}

}

package com.CAMEBOl.usuario.security.controller;

import java.io.File;
import java.net.MalformedURLException;
//import java.net.http.HttpHeaders;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.CAMEBOl.usuario.security.dto.JwtDto;
import com.CAMEBOl.usuario.security.dto.LoginUsuario;
import com.CAMEBOl.usuario.security.entity.Mensaje;
import com.CAMEBOl.usuario.security.entity.Rol;
import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.enums.RolNombre;
import com.CAMEBOl.usuario.security.jwt.JwtProvider;
import com.CAMEBOl.usuario.security.service.RolService;
import com.CAMEBOl.usuario.security.service.UsuarioService;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import java.util.Date;
import org.springframework.http.HttpHeaders;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping("usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	RolService rolService;

	@Autowired
	JwtProvider jwtProvider;
	@Autowired
	private RestTemplate restTemplate;

	String mensaje = "";
	Integer estado = 0;
	String respuesta = "";
	String fallo = "";

	@GetMapping("/lista")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Usuario>> getAll() {
		try {
			List<Usuario> list = usuarioService.getAll();
			return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaId/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<Usuario> getById(@PathVariable("id") int id) {
		try {
			if (!usuarioService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Usuario usuario = usuarioService.getById(id).get();
			return new ResponseEntity(usuario, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Usuario>> getByIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion) {
		try {
			List<Usuario> list = usuarioService.getByIdOrganizacion(idOrganizacion);
			return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacionYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Usuario>> getByIdOrganizacionYFecha(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		try {
			List<Usuario> list = usuarioService.getByIdOrganizacionYFecha(idOrganizacion, fechaInicio, fechaFin);
			return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaEstado/{estado}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Usuario>> getByEstado(@PathVariable("estado") String estado) {
		try {
			List<Usuario> list = usuarioService.getByEstado(estado);
			return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/eliminarActivar/{id}/{estado}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @PathVariable("estado") String estado) {
		try {

			int idEmpresa = obtenerIdEmpresaPorIdUsuario(id);
			Usuario usuario2 = usuarioService.getById(id).get();
			usuario2.setEstado(estado);
			usuarioService.save(usuario2);
			if (idEmpresa > 0) {
				eliminarEmpresa(idEmpresa, estado);
				eliminarProducto(idEmpresa, estado);
			}

			creandoLog("Usuario", "Actualizar", "Se actualizo el usuario " + id + "", "Exito");
			return new ResponseEntity(new Mensaje("Usuario Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Usuario usuario) {
		try {
			Usuario usuario2 = usuarioService.getById(id).get();
			usuario2.setNombre(usuario.getNombre());
			usuario2.setEmail(usuario.getEmail());
			usuario2.setPrimerApellido(usuario.getPrimerApellido());
			usuario2.setSegundoApellido(usuario.getSegundoApellido());
			usuario2.setFechaNacimiento(usuario.getFechaNacimiento());
			usuario2.setNumeroTelefono(usuario.getNumeroTelefono());
			usuario2.setCi(usuario.getCi());
			usuario2.setEstado(usuario.getEstado());
			usuario2.setIdEmpresa(usuario.getIdEmpresa());
			usuario2.setIdOrganizacion(usuario.getIdOrganizacion());

			usuarioService.save(usuario2);
			creandoLog("Usuario", "Actualizar", "Se actualizo el usuario " + id + "", "Exito");
			return new ResponseEntity(new Mensaje("Usuario Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/subirFoto")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> subirFoto(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Integer id) {
		// @RequestParam("id") Integer id

		Map<String, Object> response = new HashMap<>();

		Usuario usuario = usuarioService.getById(id).get();

		if (!archivo.isEmpty()) {
			String nombreArchivo = UUID.randomUUID().toString() + "_" + archivo.getOriginalFilename().replace(" ", "");
			Path rutaArchivo = Paths.get("ImagenUsuario").resolve(nombreArchivo).toAbsolutePath();

			try {
				java.nio.file.Files.copy(archivo.getInputStream(), rutaArchivo);

			} catch (Exception e) {
				response.put("mensaje", "Error al subir la imagen");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			String nombreFotoAnterior = usuario.getImagen();
			if (nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {
				Path rutaFotoAnterior = Paths.get("ImagenUsuario").resolve(nombreFotoAnterior).toAbsolutePath();
				File fotoAnterior = rutaFotoAnterior.toFile();
				if (fotoAnterior.exists() && fotoAnterior.canRead()) {

					fotoAnterior.delete();
				}

			}
			usuario.setImagen(nombreArchivo);

			usuarioService.save(usuario);
			response.put("Usuario", usuario);
			response.put("mensaje", "Imagen subida con exito: " + nombreArchivo);
		}

		return new ResponseEntity(response, HttpStatus.CREATED);
	}

	@GetMapping("/img/{nombreFoto:.+}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto) {

		Path rutaArchivo = Paths.get("ImagenUsuario").resolve(nombreFoto).toAbsolutePath();
		Resource recurso = null;

		try {
			recurso = new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		if (!recurso.exists() && !recurso.isReadable()) {
			throw new RuntimeException("Error no se pudo cargar la imagen:");
		}
		org.springframework.http.HttpHeaders cabecera = new org.springframework.http.HttpHeaders();
		cabecera.add(org.springframework.http.HttpHeaders.CONTENT_DISPOSITION,
				"Attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}

	@GetMapping("/UsuarioId/{id}")
	@PermitAll
	public ResponseEntity<Usuario> getByID(@PathVariable("id") Integer id) {
		Usuario user = usuarioService.getById(id).get();
		return new ResponseEntity<Usuario>(user, HttpStatus.OK);
	}

	@PostMapping("/verficarNombreUsuario/{nombreUsuario}")
	@PermitAll
	public Boolean verificarNombreUsuario(@PathVariable("nombreUsuario") String nombreUsuario) {

		return usuarioService.existsByNombreUsuario(nombreUsuario) ? true : false;

	}

	@PostMapping("/verficarEmail/{email}/{idOrganizacion}")
	@PermitAll
	public Boolean verificarEmail(@PathVariable("email") String email,
			@PathVariable("idOrganizacion") int idOrganizacion) {

		return usuarioService.existsByEmailAndIdOrganizacion(email, idOrganizacion) ? true : false;

	}

	@PermitAll
	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody Usuario nuevoUsuario, BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors())
				return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
			if (usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
				return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
			if (usuarioService.existsByEmailAndIdOrganizacion(nuevoUsuario.getEmail(),
					nuevoUsuario.getIdOrganizacion()))
				return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);

			Set<Rol> roles = nuevoUsuario.getRoles();
			roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());

			nuevoUsuario.setRoles(roles);
			nuevoUsuario.setPassword(passwordEncoder.encode(nuevoUsuario.getPassword()));
			usuarioService.save(nuevoUsuario);
			creandoLog("Usuario", "Insertar", "Se inserto el usuario " + nuevoUsuario.getNombreUsuario()
					+ " de la organizacion " + nuevoUsuario.getIdOrganizacion(), "Exito");
			return new ResponseEntity(new Mensaje("Usuario creado"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/agregarRolEmprendedora/{id}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<?> agregarRolEmprendedora(@PathVariable("id") int id) {
		try {
			Usuario usuario2 = usuarioService.getById(id).get();
			Set<Rol> roles = usuario2.getRoles();
			roles.add(rolService.getByRolNombre(RolNombre.ROLE_EMPRENDEDORA).get());
			creandoLog("Usuario", "Actualizar",
					"Se inserto un nuevo rol para el usuario " + usuario2.getNombreUsuario(), "Exito");
			usuarioService.save(usuario2);
			return new ResponseEntity(new Mensaje("Usuario Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/verificarPassword")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Boolean verificarPassword(@RequestBody LoginUsuario usuario) {
		Usuario user = usuarioService.getById(usuario.getIdUsuario()).get();
		String hashAlmacenado = user.getPassword(); // Obtén el hash almacenado desde la base de datos

		// Compara la contraseña proporcionada con el hash almacenado
		boolean contraseñaCorrecta = passwordEncoder.matches(usuario.getPassword(), hashAlmacenado);

		if (contraseñaCorrecta) {
			return true;
		} else {
			return false;
		}
	}

	@PostMapping("/cambiarPassword")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> cambiarPassword(@RequestBody LoginUsuario usuario) {
		try {
			Usuario user = usuarioService.getById(usuario.getIdUsuario()).get();
			user.setPassword(passwordEncoder.encode(usuario.getPassword()));
			usuarioService.save(user);
			creandoLog("Usuario", "Actualiar", "Se actulizo el password del usuario " + user.getNombreUsuario(),
					"Exito");
			return new ResponseEntity(new Mensaje("Usuario actualizado"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PermitAll
	@PostMapping("/login")
	public ResponseEntity<?> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity("campos mal puestos", HttpStatus.BAD_REQUEST);
		try {
			Usuario user = usuarioService.getByNombreUsuarioOrEmailAndIdOrganizacion(loginUsuario.getNombreUsuario(),
					loginUsuario.getIdOrganizacion()).get();

			if (user != null && user.getEstado().equals("1")) {
				Authentication authentication = authenticationManager
						.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(),
								loginUsuario.getPassword()));
				SecurityContextHolder.getContext().setAuthentication(authentication);
				String jwt = jwtProvider.generateToken(authentication);
				UserDetails userDetails = (UserDetails) authentication.getPrincipal();
				JwtDto jwtDto = new JwtDto(jwt);

				creandoLog("Usuario", "Login", "El usuario " + loginUsuario.getNombreUsuario() + " ingreso al sistema",
						"Exito");
				return new ResponseEntity(jwtDto, HttpStatus.OK);

			} else {
				return new ResponseEntity<>(new Mensaje("Login incorrecto. Verifica tus credenciales."),
						HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity<>(new Mensaje("Login incorrecto. Verifica tus credenciales."),
					HttpStatus.UNAUTHORIZED);
		}

	}

	@PutMapping("/actualizarImgPerfil")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> actualizarFoto(@RequestBody Usuario usuario) {
		try {
			Usuario usuario2 = usuarioService.getById(usuario.getId()).get();
			usuario2.setImagen(usuario.getImagen());
			usuarioService.save(usuario2);
			creandoLog("Usuario", "Actualizar", "Se actulizo la imagen del usuario " + usuario2.getNombreUsuario(),
					"Exito");
			return new ResponseEntity(new Mensaje("Imagen Actualizada"), HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public Integer obtenerIdEmpresaPorIdUsuario(int idUsuario) {
		// ObjectMapper de Jackson para parsear JSON
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8091/empresa/empresaPorIdUsuario/" + idUsuario, HttpMethod.GET, null,
					String.class);
			// Parsear el JSON a un objeto JsonNode
			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			Integer idEmpresa = jsonNode.at("/idEmpresa").asInt();
			if (idEmpresa != null && idEmpresa > 0) {
				return idEmpresa;
			} else {
				return 0;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
	}

	public void eliminarEmpresa(int idEmpresa, String estado) {
		// ObjectMapper de Jackson para parsear JSON
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8091/empresa/eliminarActivar/" + idEmpresa + "/" + estado, HttpMethod.PUT, null,
					String.class);

		} catch (Exception e) {
			// TODO: handle exception

		}
	}

	public void eliminarProducto(int idEmpresa, String estado) {
		ObjectMapper objectMapper = new ObjectMapper();
		List<Integer> idProductos = null;

		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8094/producto/obtenerIdProductosPorIdEmpresa/" + idEmpresa,
					HttpMethod.GET,
					null,
					String.class);

			// Convertir la respuesta JSON a una lista de enteros
			idProductos = objectMapper.readValue(response.getBody(), new TypeReference<List<Integer>>() {
			});

			// Verificar si la lista NO está vacía
			if (!idProductos.isEmpty()) {
				for (Integer idProducto : idProductos) {
					// Realizar la solicitud para eliminar o activar cada producto
					ResponseEntity<String> response2 = restTemplate.exchange(
							"http://localhost:8094/producto/eliminarActivar/" + idProducto + "/" + estado,
							HttpMethod.PUT,
							null,
							String.class);
				}
			}

		} catch (Exception e) {
			// Manejar cualquier excepción que pueda ocurrir durante la solicitud
			e.printStackTrace();
		}
	}

}

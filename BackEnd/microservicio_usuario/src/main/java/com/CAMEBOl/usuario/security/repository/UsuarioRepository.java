package com.CAMEBOl.usuario.security.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOl.usuario.security.entity.Usuario;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    Optional<Usuario> findByNombreUsuario(String nombreUsuario);
    Optional<Usuario> findByNombreUsuarioOrEmail(String nombreUsuario,String email);
    Optional<Usuario> findByTokenPassword(String tokenPassword);
    boolean existsByNombreUsuario(String nombreUsuario);
    boolean existsByEmailAndIdOrganizacion(String email,int idOrganizacion);
//    @Query("selest u from usuario u where u.estado=?1")
    List<Usuario> getByEstado(String estado);
    List<Usuario> findByIdOrganizacion(int idOrganizacion);
    Optional<Usuario> findByIdOrganizacionAndNombreUsuarioOrEmail(int idOrganizacion,String nombreUsuario, String email);
   // Optional<Usuario> findByIdOrganizacionAndNombreUsuarioAndEstado(int idOrganizacion,String nombreUsuario, String estado);
   
    @Query("SELECT u FROM com.CAMEBOl.usuario.security.entity.Usuario u WHERE u.idOrganizacion= :idOrganizacion AND DATE (u.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<Usuario> findByIdOrganizacionYFecha(@Param("idOrganizacion") int idOrganizacion, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
    
}
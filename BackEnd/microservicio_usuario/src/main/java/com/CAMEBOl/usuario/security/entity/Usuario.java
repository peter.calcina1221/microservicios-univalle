package com.CAMEBOl.usuario.security.entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Usuario")
public class Usuario {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
  
    private String nombre;
    @NotNull
    @Column(unique = true)
    private String nombreUsuario;

    private String email;
 
    private String password;
    
    private String tokenPassword;
  
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"),
    inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles = new HashSet<>();
   
    private String primerApellido;
    private String segundoApellido;
  
    private Date fechaNacimiento;
    private String numeroTelefono;
    private String ci;
    private String genero;
   
    private int idEmpresa;
  
    private int idOrganizacion;

    private Date fechaRegistro;
    private Date fechaActualizacion;

    private String estado;
    

    private String imagen;
    public Usuario() {
    }
    @PrePersist
	public void prePersist() throws ParseException {

		Date dt = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF = formatter.format(dt);
		this.fechaRegistro = formatter.parse(fcF);
	}

	@PreUpdate
	public void preUpdate() throws ParseException {

		Date dt = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF = formatter.format(dt);
		this.fechaActualizacion = formatter.parse(fcF);
	}
//    public Usuario(@NotNull String nombre, @NotNull String nombreUsuario, @NotNull String email, @NotNull String password) {
//        this.nombre = nombre;
//        this.nombreUsuario = nombreUsuario;
//        this.email = email;
//        this.password = password;
//    }
//    
//
//	public Usuario(@NotNull String nombre, @NotNull String nombreUsuario, @NotNull String email,
//			@NotNull String password, @NotNull String primerApellido, @NotNull Date fechaNacimiento,
//			@NotNull int idEmpresa,@NotNull int idOrganizacion, @NotNull String estado) {
//		this.nombre = nombre;
//		this.nombreUsuario = nombreUsuario;
//		this.email = email;
//		this.password = password;
//		this.primerApellido = primerApellido;
//		this.fechaNacimiento = fechaNacimiento;
//		this.idEmpresa = idEmpresa;
//		this.idOrganizacion = idOrganizacion;
//		this.estado = estado;
//	}
//	
//	public Usuario(@NotNull String nombre, @NotNull String email, @NotNull String primerApellido,
//			String segundoApellido, @NotNull Date fechaNacimiento, String numeroTelefono, String ci,String estado) {
//		super();
//		this.nombre = nombre;
//		this.email = email;
//		this.primerApellido = primerApellido;
//		this.segundoApellido = segundoApellido;
//		this.fechaNacimiento = fechaNacimiento;
//		this.numeroTelefono = numeroTelefono;
//		this.ci = ci;
//		this.estado=estado;
//	}

	

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTokenPassword() {
		return tokenPassword;
	}

	public void setTokenPassword(String tokenPassword) {
		this.tokenPassword = tokenPassword;
	}

	public Set<Rol> getRoles() {
		return roles;
	}

	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getIdOrganizacion() {
		return idOrganizacion;
	}

	public void setIdOrganizacion(int idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}
	

}

package com.CAMEBOl.usuario.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.repository.UsuarioRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Transactional
public class UsuarioService {
	@Autowired
	UsuarioRepository usuarioRepository;

	public Optional<Usuario> getByNombreUsuario(String nombreUsuario) {
		return usuarioRepository.findByNombreUsuario(nombreUsuario);
	}

	public Optional<Usuario> getByNombreUsuarioOrEmail(String nombreUsuarioOrEmail) {
		return usuarioRepository.findByNombreUsuarioOrEmail(nombreUsuarioOrEmail, nombreUsuarioOrEmail);
	}
	public Optional<Usuario> getByNombreUsuarioOrEmailAndIdOrganizacion(String nombreUsuarioOrEmail,int idOrganizacion) {
		return usuarioRepository.findByIdOrganizacionAndNombreUsuarioOrEmail(idOrganizacion,nombreUsuarioOrEmail, nombreUsuarioOrEmail);
	}
	public Optional<Usuario> getByTokenPassword(String tokenPassword) {
		return usuarioRepository.findByTokenPassword(tokenPassword);
	}

	public boolean existsByNombreUsuario(String nombreUsuario) {
		return usuarioRepository.existsByNombreUsuario(nombreUsuario);
	}

	public boolean existsByEmailAndIdOrganizacion(String email , int idOrganizacion) {
		return usuarioRepository.existsByEmailAndIdOrganizacion(email,idOrganizacion);
	}

	public boolean existsById(Integer id) {
		return usuarioRepository.existsById(id);
	}

	public void save(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	public List<Usuario> getAll() {

		return usuarioRepository.findAll();
	}

	public List<Usuario> getByIdOrganizacion(int idOrganizacion) {

		return usuarioRepository.findByIdOrganizacion(idOrganizacion);
	}
	
	public List<Usuario> getByIdOrganizacionYFecha(int idOrganizacion,Date fechaInicio,Date fechaFin) {

		return usuarioRepository.findByIdOrganizacionYFecha(idOrganizacion,fechaInicio,fechaFin);
	}

	public List<Usuario> getByEstado(String estado) {

		return usuarioRepository.getByEstado(estado);
	}

	public Optional<Usuario> getById(int id) {
		return usuarioRepository.findById(id);
	}

	public void delete(int id) {
		usuarioRepository.deleteById(id);
	}

}

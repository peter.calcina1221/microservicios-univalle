package com.CAMEBOl.usuario.security.controller;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOl.usuario.security.dto.CambiarPassword;
import com.CAMEBOl.usuario.security.entity.Email;
import com.CAMEBOl.usuario.security.entity.Mensaje;
import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.jwt.JwtProvider;
import com.CAMEBOl.usuario.security.service.EmailService;
import com.CAMEBOl.usuario.security.service.UsuarioService;

@RestController
@RequestMapping("email")
@CrossOrigin(origins = "*")
public class EmailController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	EmailService emailService;

	@Autowired
	UsuarioService usuarioService;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Value("${spring.mail.username}")
	String mailFrom;

	@PostMapping("/enviar")
	@PermitAll
	public ResponseEntity<?> sendEmail(@RequestBody Email email) {
		java.util.Optional<Usuario> usuarioOptional = usuarioService.getByNombreUsuarioOrEmail(email.getMailTo());
		if (!usuarioOptional.isPresent())
			return new ResponseEntity(new Mensaje("No existe ningun usuario con esas credenciales"), HttpStatus.NOT_FOUND);
		Usuario usuario = usuarioOptional.get();
		email.setMailFrom(mailFrom);
		email.setSubject(usuario.getEmail());
		email.setSubject("Cambio de Contraseña");
		email.setUserName(usuario.getNombreUsuario());
		UUID uuid = UUID.randomUUID();
		String tokenPassword = uuid.toString();
		email.setTokenPassword(tokenPassword);
		usuario.setTokenPassword(tokenPassword);
		usuarioService.save(usuario);
		emailService.sendEmailTemplate(email);

		return new ResponseEntity(new Mensaje("Te hemos enviado un correo"), HttpStatus.OK);

	}

	@PostMapping("/verificar")
	public Boolean verificar(@RequestBody CambiarPassword cambiarPassword) {
		Optional<Usuario> usuOptional = usuarioService.getByTokenPassword(cambiarPassword.getTokenPassword());
		if (!usuOptional.isPresent())
			return false;
		return true;
	}

	@PostMapping("/cambiarPassword")
	public ResponseEntity<?> cambiarPassaword(@Valid @RequestBody CambiarPassword cambiarPassword,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity(new Mensaje("Campos masl puestos"), HttpStatus.BAD_REQUEST);
		}
		if (!cambiarPassword.getPassword().equals(cambiarPassword.getConfirmarPassword())) {
			return new ResponseEntity(new Mensaje("Las contraseñas no coinciden"), HttpStatus.BAD_REQUEST);
		}
		Optional<Usuario> usuOptional = usuarioService.getByTokenPassword(cambiarPassword.getTokenPassword());
		if (!usuOptional.isPresent())
			return new ResponseEntity(new Mensaje("No existe ningun usuario con esas credenciales"), HttpStatus.NOT_FOUND);
		Usuario usuario = usuOptional.get();
		String nuevoPassword = passwordEncoder.encode(cambiarPassword.getPassword());
		usuario.setPassword(nuevoPassword);
		usuario.setTokenPassword(null);
		usuarioService.save(usuario);
		return new ResponseEntity(new Mensaje("Contraseña actualizada"), HttpStatus.OK);
	}

}

package com.CAMEBOl.usuario.security.jwt;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.entity.UsuarioPrincipal;
import com.CAMEBOl.usuario.security.service.UsuarioService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;
import java.util.concurrent.Flow.Publisher;

@Component
public class JwtProvider {
	private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);
	@Autowired
    private RestTemplate restTemplate;
	
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;

    public String generateToken(Authentication authentication){
    	//No puedo usar la clase Usuario porque UsuarioPrincipal esta con los atributos mapeados con una clase de Authentication ya establecida
        UsuarioPrincipal usuarioPrincipal = (UsuarioPrincipal) authentication.getPrincipal();
        Integer idEmpresa=obtenerIdEmpresaPorIdUsuario(usuarioPrincipal.getId());
        String nombreEmpresa=obtenerNombreEmpresaPorIdUsuario(usuarioPrincipal.getId());
        return Jwts.builder()
        		.setSubject(usuarioPrincipal.getUsername())
        		.claim("idUsuario", usuarioPrincipal.getId())
        		.claim("rol", usuarioPrincipal.getAuthorities())
        		.claim("idEmpresa", idEmpresa)
        		// nombre de la empresa
        		.claim("nombreEmpresa", nombreEmpresa)
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + expiration * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    
    
    public Integer obtenerIdEmpresaPorIdUsuario(int idUsuario) {
    	// ObjectMapper de Jackson para parsear JSON
    			ObjectMapper objectMapper = new ObjectMapper();
    	try {
    		ResponseEntity<String> response = restTemplate.exchange(
		            "http://localhost:8091/empresa/empresaPorIdUsuario/"+idUsuario,
		            HttpMethod.GET,
		            null,
		            String.class
		        );
		   // Parsear el JSON a un objeto JsonNode
		    JsonNode jsonNode = objectMapper.readTree(response.getBody());
		    Integer idEmpresa=jsonNode
		            .at("/idEmpresa")  
		            .asInt();
		    if(idEmpresa !=null && idEmpresa>0) {
		    	return idEmpresa;
		    }
		    else {
				return 0;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return 0; 
		}
    }
    public String obtenerNombreEmpresaPorIdUsuario(int idUsuario) {
    	// ObjectMapper de Jackson para parsear JSON
    			ObjectMapper objectMapper = new ObjectMapper();
    	try {
    		ResponseEntity<String> response = restTemplate.exchange(
		            "http://localhost:8091/empresa/empresaPorIdUsuario/"+idUsuario,
		            HttpMethod.GET,
		            null,
		            String.class
		        );
		   // Parsear el JSON a un objeto JsonNode
		    JsonNode jsonNode = objectMapper.readTree(response.getBody());
		    String nombreEmpresa=jsonNode
		            .at("/nombreEmpresa")  
		            .asText();
		    if(nombreEmpresa !=null && nombreEmpresa!="") {
		    	return nombreEmpresa;
		    }
		    else {
				return "";
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ""; 
		}
    }
    

    public String getNombreUsuarioFromToken(String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateToken(String token){
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }catch (MalformedJwtException e){
            logger.error("token mal formado");
        }catch (UnsupportedJwtException e){
            logger.error("token no soportado");
        }catch (ExpiredJwtException e){
            logger.error("token expirado");
        }catch (IllegalArgumentException e){
            logger.error("token vacío");
        }catch (SignatureException e){
            logger.error("fail en la firma");
        }
        return false;
    }
}

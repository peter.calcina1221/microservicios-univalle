package com.CAMEBOL.log1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicioLog1Application {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioLog1Application.class, args);
	}

}

package com.CAMEBOL.log1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.log1.entity.Log;



@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {

}

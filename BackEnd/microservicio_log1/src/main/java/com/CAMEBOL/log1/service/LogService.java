package com.CAMEBOL.log1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.log1.entity.Log;
import com.CAMEBOL.log1.repository.LogRepository;


@Service
@Transactional
public class LogService {
	@Autowired
	LogRepository logRepository;
	 public void save(Log log){
		 logRepository.save(log);
	    }
	 public List<Log> getAll() {

			return logRepository.findAll();
		}

}

package com.CAMEBOL.log1.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.log1.entity.Log;
import com.CAMEBOL.log1.service.LogService;


@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "*")
public class LogController {
	@Autowired
	private LogService logService;
	@GetMapping("/lista")
	public ResponseEntity<List<Log>> getAll(){
			List<Log> list = logService.getAll();
			return  new ResponseEntity<List<Log>>(list,HttpStatus.OK);
	}
	
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@RequestBody Log log, BindingResult bindingResult){
	
		logService.save(log);
        
		return new ResponseEntity(HttpStatus.CREATED);
    }
}

package com.CAMEBOL.reserva.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.reserva.entity.Mensaje;
import com.CAMEBOL.reserva.entity.Reserva;
import com.CAMEBOL.reserva.service.ReservaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.CAMEBOL.reserva.entity.Email;

@RestController
@RequestMapping("/reserva")
@CrossOrigin(origins = "*")
public class ReservaController {
	@Autowired
	private ReservaService reservaService;
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Reserva>> getAll(){
		try {
			List<Reserva> list = reservaService.getAll();
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<Reserva>> getByIdOrganizacion(@PathVariable("idOrganizacion")int idOrganizacion){
		try {
			List<Reserva> list = reservaService.getByIdOrganizacion(idOrganizacion);
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/listaPorIdOrganizacionYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Reserva>> getByIdOrganizacionYFecha(@PathVariable("idOrganizacion")int idOrganizacion,@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio, @PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin){
		try {
			List<Reserva> list = reservaService.getByIdOrganizacionYFecha(idOrganizacion,fechaInicio,fechaFin);
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/listaPorIdOrganizacionFechaYIdEmpresa/{idOrganizacion}/{fechaInicio}/{fechaFin}/{idEmpresa}")
	public ResponseEntity<List<Reserva>> getByIdOrganizacionFechaYIdEmpresa(@PathVariable("idOrganizacion")int idOrganizacion,@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio, @PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,@PathVariable("idEmpresa")int idEmpresa){
		try {
			List<Reserva> list = reservaService.getByIdOrganizacionFechaYIdEmpresa(idOrganizacion,fechaInicio,fechaFin,idEmpresa);
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdUsuario/{idUsuario}")
	public ResponseEntity<List<Reserva>> getByIdUsuario(@PathVariable("idUsuario")int idUsuario){
		try {
			List<Reserva> list = reservaService.getByIdUsuario(idUsuario);
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdUsuarioYEstado/{idUsuario}/{estado}")
	public ResponseEntity<List<Reserva>> getByIdUsuarioAndEstado(@PathVariable("idUsuario")int idUsuario,@PathVariable("estado")String estado){
		try {
			List<Reserva> list = reservaService.getByIdUsuarioAndEstado(idUsuario,estado);
			return  new ResponseEntity<List<Reserva>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@RequestBody Reserva reserva){
		
		try {

			reservaService.save(reserva);
			creandoLog("Reserva","Insertar","Se registro la reserva "+reserva.getCodigoReserva()+"","Exito");
			return new ResponseEntity(new Mensaje("Reserva exitosa"),HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
	
    }
	@PutMapping("/actualizar/{idReserva}")
	public ResponseEntity<?> actualizar(@PathVariable("idReserva") int idReserva, @RequestBody Reserva reserva){

		try {
			Reserva reser=reservaService.getById(idReserva).get();
			reser.setIdUsuario(reserva.getIdUsuario());
			reser.setIdOrganizacion(reserva.getIdOrganizacion());
			reser.setCodigoReserva(reserva.getCodigoReserva());
			reser.setFechaSolicitud(reserva.getFechaSolicitud());
			reser.setFechaEntrega(reserva.getFechaEntrega());
			reser.setTotal(reserva.getTotal());
			reser.setFechaActualizacion(reserva.getFechaActualizacion());
			reser.setEstado(reserva.getEstado());
			reservaService.save(reser);
			creandoLog("Reserva","Actualizar","Se actualizo la reserva "+idReserva,"Exito");
			return new ResponseEntity(new Mensaje("Reserva Actualizada"),HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	
	}	
	@DeleteMapping("/elimnar/{idReserva}")
	public ResponseEntity<?> delete(@PathVariable("idReserva") int idReserva){
		reservaService.delete(idReserva);
		return new ResponseEntity(new Mensaje("Reserva Eliminada"),HttpStatus.OK);
	}
	
	@PostMapping("/enviarCorreo")
	@PermitAll
	public ResponseEntity<?> sendEmail(@RequestBody Email email){	
		
		try {
			reservaService.sendEmailTemplate(email);
			return new ResponseEntity(new Mensaje("Te hemos enviado un correo"),HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
	

		
		
	}
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
}

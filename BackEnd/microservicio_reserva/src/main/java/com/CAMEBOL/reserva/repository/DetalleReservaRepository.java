package com.CAMEBOL.reserva.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.reserva.entity.DetalleReserva;
import com.CAMEBOL.reserva.entity.Reserva;
@Repository
public interface DetalleReservaRepository extends JpaRepository<DetalleReserva, Integer> {
	List<DetalleReserva> findByIdEmpresa(int idEmpresa);
	List<DetalleReserva> findByIdEmpresaAndEstado(int idEmpresa,String estado);
	List<DetalleReserva> findByReserva(Reserva reserva);
	
	@Query("SELECT dr FROM DetalleReserva dr INNER JOIN dr.reserva r WHERE r.idOrganizacion = :idOrganizacion")
	List<DetalleReserva> ListarPorIdOrganizacion(@Param("idOrganizacion") int idOrganizacion);
	
	@Query("SELECT dr FROM DetalleReserva dr INNER JOIN dr.reserva r WHERE r.idOrganizacion = :idOrganizacion AND DATE(dr.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<DetalleReserva> ListarPorIdOrganizacionYFecha(@Param("idOrganizacion") int idOrganizacion, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
	@Query("SELECT dr FROM DetalleReserva dr WHERE dr.idEmpresa = :idEmpresa AND DATE(dr.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<DetalleReserva> ListarPorIdEmpresaYFecha(@Param("idEmpresa") int idEmpresa, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
	
	@Query("SELECT dr FROM DetalleReserva dr INNER JOIN dr.reserva r WHERE r.idUsuario = :idUsuario AND dr.estado = :estado")
	List<DetalleReserva> ListarPorUsuarioYEstado(@Param("idUsuario") int idUsuario, @Param("estado") String estado);
}

package com.CAMEBOL.reserva.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.reserva.entity.DetalleReserva;
import com.CAMEBOL.reserva.entity.Mensaje;
import com.CAMEBOL.reserva.entity.Reserva;
import com.CAMEBOL.reserva.service.DetalleReservaService;
import com.CAMEBOL.reserva.service.ReservaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/detalleReserva")
@CrossOrigin(origins = "*")
public class DetalleReservaController {
	@Autowired
	private DetalleReservaService detalleReservaService;
	@Autowired
	private ReservaService reservaService;
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/lista")
	public ResponseEntity<List<DetalleReserva>> getAll() {
		try {
			List<DetalleReserva> list = detalleReservaService.getAll();
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/listaPorIdEmpresa/{idEmpresa}")
	public ResponseEntity<List<DetalleReserva>> getByIdEmpresa(@PathVariable("idEmpresa") int idEmpresa) {
		try {
			// realizar validaciones si la empresa existe
			List<DetalleReserva> list = detalleReservaService.getByIdEmpresa(idEmpresa);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdEmpresaYFecha/{idEmpresa}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<DetalleReserva>> getByIdEmpresaYFecha(@PathVariable("idEmpresa") int idEmpresa,@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio, @PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		try {
			// realizar validaciones si la empresa existe
			List<DetalleReserva> list = detalleReservaService.getByIdEmpresaYFecha(idEmpresa,fechaInicio,fechaFin);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping("/listaPorIdEmpresaYEstado/{idEmpresa}/{esatdo}")
	public ResponseEntity<List<DetalleReserva>> getByIdEmpresaAndEstado(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("esatdo") String esatdo) {
		try {
			// realizar validaciones si la empresa existe
			List<DetalleReserva> list = detalleReservaService.getByIdEmpresaAndEstado(idEmpresa, esatdo);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping("/listaPorReserva/{idReserva}")
	public ResponseEntity<List<DetalleReserva>> getAllByEstado(@PathVariable("idReserva") int idReserva) {
		try {
			if (!reservaService.existsById(idReserva)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Reserva reserva = reservaService.getById(idReserva).get();
			List<DetalleReserva> list = detalleReservaService.getByReserva(reserva);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody DetalleReserva detalleReserva, BindingResult bindingResult) {

		try {
			detalleReservaService.save(detalleReserva);
			creandoLog("Reserva","Insertar","Se registro el detalle de la reserva reserva "+detalleReserva.getReserva().getIdReserva()+"","Exito");
			return new ResponseEntity(new Mensaje("DetalleReserva exitosa"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
		
	}

	@PutMapping("/actualizarEstado/{idDetalleReserva}/{estado}")
	public ResponseEntity<?> actualizarEstado(@PathVariable("idDetalleReserva") int idDetalleReserva,
			@PathVariable("estado") String estado) {

		try {
			DetalleReserva detalleReserva = detalleReservaService.getById(idDetalleReserva).get();
			detalleReserva.setEstado(estado);

			detalleReservaService.save(detalleReserva);
			creandoLog("Reserva","Actualizar","Se actualizo el estado del detalle reserva "+idDetalleReserva+" con el estado "+estado,"Exito");
			return new ResponseEntity(new Mensaje("Estado Actualizado"), HttpStatus.CREATED);
			
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/actualizar/{idDetalleReserva}")
	public ResponseEntity<?> actualizar(@PathVariable("idDetalleReserva") int idDetalleReserva,
			@RequestBody DetalleReserva detalleReserva) {

		try {
			DetalleReserva detReserva = detalleReservaService.getById(idDetalleReserva).get();
			detReserva.setReserva(detalleReserva.getReserva());
			detReserva.setIdProducto(detalleReserva.getIdProducto());
			detReserva.setIdEmpresa(detalleReserva.getIdEmpresa());
			detReserva.setCantidad(detalleReserva.getCantidad());
			detReserva.setPrecioUnitario(detalleReserva.getPrecioUnitario());
			detReserva.setDescuento(detalleReserva.getDescuento());
			detReserva.setFechaActualizacion(detalleReserva.getFechaActualizacion());
			detReserva.setEstado(detalleReserva.getEstado());
			detalleReservaService.save(detReserva);
			creandoLog("Reserva","Actualizar","Se actualizo  detalle reserva "+idDetalleReserva+"","Exito");
			return new ResponseEntity(new Mensaje("DetalleReserva Actualizada"), HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		

	}

	@DeleteMapping("/elimnar/{idDetalleReserva}")
	public ResponseEntity<?> delete(@PathVariable("idDetalleReserva") int idDetalleReserva) {
		detalleReservaService.delete(idDetalleReserva);
		return new ResponseEntity(new Mensaje("DetalleReserva Eliminada"), HttpStatus.OK);
	}

	@GetMapping("/listaPorIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<DetalleReserva>> getByIdOrganizacion(
			@PathVariable("idOrganizacion") int idOrganizacion) {
		try {
			// realizar validaciones si la empresa existe
			List<DetalleReserva> list = detalleReservaService.listarPorIdOrganiacion(idOrganizacion);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaPorIdOrganizacionYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<DetalleReserva>> getByIdOrganizacionYFecha(
			@PathVariable("idOrganizacion") int idOrganizacion,@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio, @PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		try {
			// realizar validaciones si la empresa existe
			List<DetalleReserva> list = detalleReservaService.listarPorIdOrganiacionYFecha(idOrganizacion,fechaInicio,fechaFin);
			return new ResponseEntity<List<DetalleReserva>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Reserva","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping("/ListarPorUsuarioYEstado/{idUsuario}/{estado}")
	public List<DetalleReserva> getDetalleVentaByUsuarioAndEstado(@PathVariable("idUsuario") int idUsuario,@PathVariable("estado") String estado) {
	
		return detalleReservaService.ListarPorUsuarioYEstado(idUsuario, estado);
	
	}
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

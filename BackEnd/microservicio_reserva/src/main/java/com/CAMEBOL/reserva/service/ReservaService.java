package com.CAMEBOL.reserva.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;

import com.CAMEBOL.reserva.entity.Reserva;
import com.CAMEBOL.reserva.repository.ReservaRepository;
import com.CAMEBOL.reserva.entity.Email;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
@Transactional
public class ReservaService {
	@Autowired
	ReservaRepository reservaRepository;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	JavaMailSender javaMailSender;
	@Autowired
	TemplateEngine templateEngine;
	@Value("${spring.mail.username}")
	String mailFrom;
	 public List<Reserva> getAll(){
	    	return reservaRepository.findAll();
	    }
	 public List<Reserva> getByIdOrganizacion(int idOrganizacion){
	    	return reservaRepository.findByIdOrganizacion(idOrganizacion);
	    }
	 
	 public List<Reserva> getByIdOrganizacionYFecha(int idOrganizacion,Date fechaInicio,Date fechaFin){
	    	return reservaRepository.findByIdOrganizacionYFecha(idOrganizacion,fechaInicio,fechaFin);
	    }
	 
	 
	 public List<Reserva> getByIdOrganizacionFechaYIdEmpresa(int idOrganizacion,Date fechaInicio,Date fechaFin,int idEmpresa){
	    	return reservaRepository.findByIdOrganizacionFechaYIdEmpresa(idOrganizacion,fechaInicio,fechaFin,idEmpresa);
	    }
	 
	 public List<Reserva> getByIdUsuario(int idUsuario){
	    	return reservaRepository.findByIdUsuario(idUsuario);
	    }
	    
	 public List<Reserva> getByIdUsuarioAndEstado(int idUsuario,String estado){
	    	return reservaRepository.findByIdUsuarioAndEstado(idUsuario,estado);
	    }
	    
	 public Optional<Reserva> getById(int id){
	        return reservaRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return reservaRepository.existsById(id);
	    }
	 public void save(Reserva reserva){
		 reservaRepository.save(reserva);
	    }
	  public void delete(int id) {
		  reservaRepository.deleteById(id);
	    }
	  public void sendEmailTemplate(Email email) {
			String userName="";
			String emailTo="";
			MimeMessage message=javaMailSender.createMimeMessage();
			// ObjectMapper de Jackson para parsear JSON
						ObjectMapper objectMapper = new ObjectMapper();
		    try {
		    	
					   ResponseEntity<String> response = restTemplate.exchange(
					            "http://localhost:8095/usuario/UsuarioId/"+email.getIdUsuario(),
					            HttpMethod.GET,
					            null,
					            String.class
					        );
					   // Parsear el JSON a un objeto JsonNode
					    JsonNode jsonNode = objectMapper.readTree(response.getBody());

					    // Obtener el valor del json
					    userName=jsonNode
					            .at("/nombreUsuario")  
					            .asText();
					    emailTo=jsonNode
					            .at("/email")  
					            .asText();
		    	
		    	
		    	
				MimeMessageHelper helper = new MimeMessageHelper(message,true);
				org.thymeleaf.context.Context context= new org.thymeleaf.context.Context();
				Map<String, Object> model = new HashMap<>();
				model.put("titulo",email.getTitulo());
				model.put("userName",userName);
				model.put("urlImagen",email.getUrlImagen());
				model.put("mensaje",email.getMensaje());
				context.setVariables(model);
				String htmlText=templateEngine.process("notificacionTemplate", context);
				helper.setFrom(mailFrom);
				helper.setTo(emailTo);
				helper.setSubject(email.getSubject());
				helper.setText(htmlText,true);
				javaMailSender.send(message);
				
			} catch (MessagingException | JsonProcessingException e) {
				e.printStackTrace();
			}
		}
}

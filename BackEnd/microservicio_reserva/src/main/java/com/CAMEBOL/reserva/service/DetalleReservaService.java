package com.CAMEBOL.reserva.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.reserva.entity.DetalleReserva;
import com.CAMEBOL.reserva.entity.Reserva;
import com.CAMEBOL.reserva.repository.DetalleReservaRepository;


@Service
@Transactional
public class DetalleReservaService {
	@Autowired
	DetalleReservaRepository detalleReservaRepository;

	public List<DetalleReserva> getAll() {
		return detalleReservaRepository.findAll();
	}

	public Optional<DetalleReserva> getById(int id) {
		return detalleReservaRepository.findById(id);
	}

	public boolean existsById(Integer id) {
		return detalleReservaRepository.existsById(id);
	}

	public void save(DetalleReserva detalleReserva) {
		detalleReservaRepository.save(detalleReserva);
	}

	public void delete(int id) {
		detalleReservaRepository.deleteById(id);
	}

	public List<DetalleReserva> getByIdEmpresa(int idEmpresa) {
		return detalleReservaRepository.findByIdEmpresa(idEmpresa);
	}
	public List<DetalleReserva> getByIdEmpresaYFecha(int idEmpresa,Date fechaInicio,Date fechaFin) {
		return detalleReservaRepository.ListarPorIdEmpresaYFecha(idEmpresa,fechaInicio,fechaFin);
	}

	public List<DetalleReserva> getByIdEmpresaAndEstado(int idEmpresa, String estado) {
		return detalleReservaRepository.findByIdEmpresaAndEstado(idEmpresa, estado);
	}

	public List<DetalleReserva> getByReserva(Reserva reserva) {
		return detalleReservaRepository.findByReserva(reserva);
	}

	public List<DetalleReserva> listarPorIdOrganiacion(int idOrganizacion) {
		return detalleReservaRepository.ListarPorIdOrganizacion(idOrganizacion);
	}
	
	public List<DetalleReserva> listarPorIdOrganiacionYFecha(int idOrganizacion,Date fechaInicio,Date fechaFin) {
		return detalleReservaRepository.ListarPorIdOrganizacionYFecha(idOrganizacion,fechaInicio,fechaFin);
	}

	public List<DetalleReserva> ListarPorUsuarioYEstado(int idUsuario, String estado) {
		List<DetalleReserva> resultados = detalleReservaRepository.ListarPorUsuarioYEstado(idUsuario, estado);
		return resultados;

	}

}

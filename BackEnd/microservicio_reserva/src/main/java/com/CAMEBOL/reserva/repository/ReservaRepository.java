package com.CAMEBOL.reserva.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.reserva.entity.Reserva;
@Repository
public interface ReservaRepository extends JpaRepository<Reserva, Integer> {
	List<Reserva> findByIdUsuario(int idUsuario);
	List<Reserva> findByIdUsuarioAndEstado(int idUsuario,String estado);
	List<Reserva> findByIdOrganizacion(int idOrganizacion);
	
	@Query("SELECT r FROM com.CAMEBOL.reserva.entity.Reserva r WHERE r.idOrganizacion= :idOrganizacion AND DATE (r.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	List<Reserva> findByIdOrganizacionYFecha(@Param("idOrganizacion") int idOrganizacion, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
	
	@Query("SELECT r FROM com.CAMEBOL.reserva.entity.Reserva r INNER JOIN r.detalleReservas dr WHERE r.idOrganizacion= :idOrganizacion AND DATE (r.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin AND dr.idEmpresa= :idEmpresa")
	List<Reserva> findByIdOrganizacionFechaYIdEmpresa(@Param("idOrganizacion") int idOrganizacion, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin,@Param("idEmpresa") int idEmpresa);
	
}

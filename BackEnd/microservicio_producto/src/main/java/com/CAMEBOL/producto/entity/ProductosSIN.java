package com.CAMEBOL.producto.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductosSIN {
	@JsonProperty("TRANSACCION")
    private boolean transaccion;
	@JsonProperty("PRODSIN")
    private String prodsin;

    // Getters y Setters (omitiendo para brevedad)

    @JsonProperty("TRANSACCION")
    public boolean isTransaccion() {
        return transaccion;
    }

    @JsonProperty("TRANSACCION")
    public void setTransaccion(boolean transaccion) {
        this.transaccion = transaccion;
    }

    
    public String getPRODSIN() {
		return prodsin;
	}

	public void setPRODSIN(String pRODSIN) {
		prodsin = pRODSIN;
	}

	@Override
    public String toString() {
        return "ProductosSIN{" +
                "TRANSACCION=" + transaccion +
                ", PRODSIN='" + prodsin + '\'' +
                '}';
    }
}

package com.CAMEBOL.producto.entity;

public class UnidadSINResultado {
	private String nombre;
    private String valor;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

//    @Override
//    public String toString() {
//        return "Unidad{" +
//                "nombre='" + nombre + '\'' +
//                ", valor='" + valor + '\'' +
//                '}';
//    }
}

package com.CAMEBOL.producto.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.producto.entity.ImagenProducto;
import com.CAMEBOL.producto.entity.Mensaje;
import com.CAMEBOL.producto.entity.Producto;
import com.CAMEBOL.producto.service.ImagenProductoService;
import com.CAMEBOL.producto.service.ProductoService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/imagenProducto")
@CrossOrigin(origins = "*")
public class ImagenProductoController {
	@Autowired
	private ImagenProductoService imagenProductoService;
	@Autowired
	private ProductoService productoService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<ImagenProducto>> getAll() {
		try {
			List<ImagenProducto> list = imagenProductoService.getAll();
			return new ResponseEntity<List<ImagenProducto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaIdProducto/{id}")
	public ResponseEntity<List<ImagenProducto>> getByIdProducto(@PathVariable("id") int id) {
		try {
			if (!productoService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Producto producto = productoService.getById(id).get();
			List<ImagenProducto> list = imagenProductoService.getByIdProducto(producto);
			return new ResponseEntity<List<ImagenProducto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaId/{id}")
	public ResponseEntity<ImagenProducto> getById(@PathVariable("id") int id) {
		try {
			if (!imagenProductoService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			ImagenProducto imagenProducto = imagenProductoService.getById(id).get();
			return new ResponseEntity(imagenProducto, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);// TODO: handle exception
		}

	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody ImagenProducto imagenProducto, BindingResult bindingResult) {

		try {
			imagenProductoService.save(imagenProducto);
			creandoLog("Producto", "Insertar", "Se creo la imagen producto " + imagenProducto.getImgUrl(), "Exito");
			return new ResponseEntity(new Mensaje("Imagen creada"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody ImagenProducto imagenProducto) {

		try {
			ImagenProducto imgPro = imagenProductoService.getById(id).get();
			imgPro.setImgUrl(imagenProducto.getImgUrl());
			imgPro.setPrincipal(imagenProducto.getPrincipal());
			imgPro.setProducto(imagenProducto.getProducto());
			imgPro.setEstado(imagenProducto.getEstado());
			imgPro.setFechaRegistro(imagenProducto.getFechaRegistro());
			imagenProductoService.save(imgPro);
			creandoLog("Producto", "Actualizar", "Se actualizo la imagen producto " + id, "Exito");
			return new ResponseEntity(new Mensaje("Imagen Actualizada"), HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		if (!imagenProductoService.existsById(id)) {
			return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
		}
		imagenProductoService.delete(id);
		return new ResponseEntity(new Mensaje("Imagen Eliminada"), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}

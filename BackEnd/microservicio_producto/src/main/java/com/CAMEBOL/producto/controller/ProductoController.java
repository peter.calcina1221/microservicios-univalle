package com.CAMEBOL.producto.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;

import com.CAMEBOL.producto.entity.CategoriaProducto;
import com.CAMEBOL.producto.entity.ImagenProducto;
import com.CAMEBOL.producto.entity.Mensaje;
import com.CAMEBOL.producto.entity.Producto;
import com.CAMEBOL.producto.entity.ProductoSINResultado;
import com.CAMEBOL.producto.entity.UnidadSINResultado;
import com.CAMEBOL.producto.service.CategoriaProductoService;
import com.CAMEBOL.producto.service.ProductoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/producto")
@CrossOrigin(origins = "*")
public class ProductoController {
	@Autowired
	private ProductoService productoService;
	@Autowired
	private CategoriaProductoService categoriaProductoService;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	JavaMailSender javaMailSender;
	@Value("${spring.mail.username}")
	String mailFrom;
	@Autowired
	TemplateEngine templateEngine;

	@GetMapping("/lista")
	public ResponseEntity<List<Producto>> getAll() {
		try {
			List<Producto> list = productoService.getAll();
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacionAndEstado/{idOrganizacion}/{estado}")
	public ResponseEntity<List<Producto>> getByIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("estado") String estado) {
		try {
			List<Producto> list = productoService.getByIdOrganizacionAndEstado(idOrganizacion, estado);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacionAndEstadoYFecha/{idOrganizacion}/{estado}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Producto>> getByIdOrganizacionYFecha(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("estado") String estado,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		try {
			List<Producto> list = productoService.getByIdOrganizacionAndEstadoYFecha(idOrganizacion, estado, fechaInicio,
					fechaFin);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}


	@GetMapping("/listaPorIdOrganizacionEstadoFechaYIdEmpresa/{idOrganizacion}/{estado}/{fechaInicio}/{fechaFin}/{idEmpresa}")
	public ResponseEntity<List<Producto>> getByIdOrganizacionEstadoFechaYIdEmpresa(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("estado") String estado,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,@PathVariable("idEmpresa") int idEmpresa) {
		try {
			List<Producto> list = productoService.getByIdOrganizacionEstadoFechaYIdEmpresa(idOrganizacion, estado, fechaInicio,
					fechaFin,idEmpresa);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}
	@GetMapping("/token")
	public ResponseEntity<String> getToken() {
		try {
			String token = productoService.obtenerToken();
			return new ResponseEntity<String>(token, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/productoSIN")
	public ResponseEntity<List<ProductoSINResultado>> getProductoSIN() {
		try {
			List<ProductoSINResultado> list = productoService.obtenerProductoSIN();
			return new ResponseEntity<List<ProductoSINResultado>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/unidadSIN")
	public ResponseEntity<List<UnidadSINResultado>> getUnidadSIN() {
		try {
			List<UnidadSINResultado> list = productoService.obtenerUnidadSIN();
			return new ResponseEntity<List<UnidadSINResultado>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductoPorId/{id}")
	public ResponseEntity<Producto> getProductoByIdProducto(@PathVariable("id") int id) {
		try {
			if (!productoService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Producto producto = productoService.getById(id).get();
			return new ResponseEntity(producto, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosPorIdEmpresaEstado/{id}/{estado}")
	public ResponseEntity<List<Producto>> getByidEmpresaEstado(@PathVariable("id") int idEmpresa,
			@PathVariable("estado") String estado) {

		try {
			if (!productoService.existsByIdEmpresa(idEmpresa)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			List<Producto> list = productoService.getByEmpresaAndEstado(idEmpresa, estado);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/obtenerProductosPorIdEmpresa/{id}")
	public ResponseEntity<List<Producto>> getByidEmpresa(@PathVariable("id") int idEmpresa) {

		try {
			if (!productoService.existsByIdEmpresa(idEmpresa)) {
				Mensaje msg = new Mensaje();
				msg.setMensaje("El id no existe");
				msg.setStatus(HttpStatus.NOT_FOUND.value());
				return new ResponseEntity(msg, HttpStatus.NOT_FOUND);
			}
			List<Producto> list = productoService.getByEmpresa(idEmpresa);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerIdProductosPorIdEmpresa/{id}")
	public ResponseEntity<List<Integer>> getIdProductoByidEmpresa(@PathVariable("id") int idEmpresa) {

		try {

			List<Integer> idProductos = new ArrayList<>();
			if (!productoService.existsByIdEmpresa(idEmpresa)) {
				Mensaje msg = new Mensaje();
				msg.setMensaje("El id no existe");
				msg.setStatus(HttpStatus.NOT_FOUND.value());
				return new ResponseEntity(msg, HttpStatus.NOT_FOUND);
			}

			List<Producto> list = productoService.getByEmpresa(idEmpresa);
			for (Producto producto : list) {
				idProductos.add(producto.getIdProducto());
			}
			return new ResponseEntity<List<Integer>>(idProductos, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosPorEstado/{estado}")
	public ResponseEntity<List<Producto>> getByEstado(@PathVariable("estado") String estado) {

		try {
			List<Producto> list = productoService.getByEstado(estado);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosCompradosPorUsuarios/{idUsuario}/{limit}")
	public ResponseEntity<List<Producto>> getProductosCompradosPorUsuario(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("limit") int limit) {
		try {
			List<Producto> list = productoService.getProductosCompradosPorUsuario(idUsuario, limit);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosMasVendidosPorEmpresa/{idEmpresa}/{limit}")
	public ResponseEntity<List<Producto>> getProductoCantidad(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("limit") int limit) {
		try {
			List<Producto> list = productoService.getProductoMasVendidoPorEmpresa(idEmpresa, limit);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosMasVendidosPorEmpresayFecha/{idEmpresa}/{fechaInicio}/{fechaFin}/{limit}")
	public ResponseEntity<List<Producto>> getProductoCantidadYFecha(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,
			@PathVariable("limit") int limit) {
		try {
			List<Producto> list = productoService.getProductoMasVendidoPorEmpresaYFecha(idEmpresa, fechaInicio, fechaFin,
					limit);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosMasVendidosPorOrganizacion/{idOrganizacion}/{limit}")
	public ResponseEntity<List<Producto>> getProductoMasVendidosPorOrganizacion(
			@PathVariable("idOrganizacion") int idOrganizacion, @PathVariable("limit") int limit) {
		try {
			List<Producto> list = productoService.getProductoMasVendidoPorOrganizacion(idOrganizacion, limit);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosSubTotalesPorEmpresa/{idEmpresa}")
	public ResponseEntity<List<Producto>> getProductoSubTotalesPorEmpresa(@PathVariable("idEmpresa") int idEmpresa) {

		try {
			List<Producto> list = productoService.getSubtotalProductoPorEmpresa(idEmpresa);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerProductosSubTotalesPorEmpresaYFecha/{idEmpresa}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Producto>> getProductoSubTotalesPorEmpresaYFecha(@PathVariable("idEmpresa") int idEmpresa,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {

		try {
			List<Producto> list = productoService.getSubtotalProductoPorEmpresaYFecha(idEmpresa, fechaInicio, fechaFin);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/ObtenerProductosPorIdCategoriaIdOrganizacionEstado/{idCategoria}/{idOrganizacion}/{estado}")
	public ResponseEntity<List<Producto>> getByCategoriaOrganizacionEstado(
			@PathVariable("idCategoria") Integer idCategoria, @PathVariable("idOrganizacion") Integer idOrganizacion,
			@PathVariable("estado") String estado) {

		try {
			if (!categoriaProductoService.existsById(idCategoria)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			CategoriaProducto categoriaProducto = categoriaProductoService.getById(idCategoria).get();
			List<Producto> list = productoService.getByCategoriaAndOrganizacionAndEstado(categoriaProducto,
					idOrganizacion, estado);
			return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody Producto producto, BindingResult bindingResult) {

		try {
			productoService.save(producto);
			Mensaje response = new Mensaje();
			response.setStatus(HttpStatus.CREATED.value());
			response.setMensaje("Producto creado exitosamente");
			creandoLog("Producto", "Insertar", "Se registro el producto " + producto.getNombre()
					+ " de la organizacion " + producto.getIdOrganizacion(), "Exito");
			return ResponseEntity.status(HttpStatus.CREATED).body(response);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/eliminarActivar/{id}/{estado}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @PathVariable("estado") String estado) {
		try {

			Producto producto = productoService.getById(id).get();
			producto.setEstado(estado);
			productoService.save(producto);

			creandoLog("Producto", "Actualizar", "Se actualizo el producto " + id + "", "Exito");
			return new ResponseEntity(new Mensaje("Producto Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Usuario", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Producto producto) {

		try {
			Producto prod = productoService.getById(id).get();
			prod.setCodigo(producto.getCodigo());
			prod.setDescripcion(producto.getDescripcion());
			prod.setIdEmpresa(producto.getIdEmpresa());
			prod.setIdOrganizacion(producto.getIdOrganizacion());
			prod.setNombre(producto.getNombre());
			prod.setPrecioCosto(producto.getPrecioCosto());
			prod.setPrecioVenta(producto.getPrecioVenta());
			prod.setStockTotal(producto.getStockTotal());
			prod.setStockRestante(producto.getStockRestante());
			prod.setStockMinimo(producto.getStockMinimo());
			prod.setCategoriaProducto(producto.getCategoriaProducto());
			prod.setEstado(producto.getEstado());
			prod.setCodigoUnidadSIN(producto.getCodigoUnidadSIN());
			prod.setCodigoProductoSIN(producto.getCodigoProductoSIN());
			prod.setFechaRegistro(producto.getFechaRegistro());
			prod.setCaracteristicaProductos(producto.getCaracteristicaProductos());
			prod.setDetalleLotes(producto.getDetalleLotes());
			prod.setImagenesProducto(producto.getImagenesProducto());
			productoService.save(prod);
			creandoLog("Producto", "Actualizar", "Se actualizo el producto " + id, "Exito");
			return new ResponseEntity(new Mensaje("Producto Actualizado"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizarStock/{id}/{cantidad}")
	public ResponseEntity<?> actualizarStock(@PathVariable("id") int id, @PathVariable("cantidad") Double cantidad) {
		try {
			String mensaje = "";
			String emailTo = "";
			String urlImagen = "";
			Double stockMinimo = 0.0;
			Double stockActual = 0.0;
			String nombreProducto = "";
			Optional<Producto> optionalProducto = productoService.getById(id);
			if (optionalProducto.isPresent()) {
				Producto prod = optionalProducto.get();

				if (prod.getStockRestante() < cantidad) {
					return new ResponseEntity(new Mensaje("Stock Insuficiente"), HttpStatus.BAD_REQUEST);
				}

				double stockNuevo = prod.getStockRestante() - cantidad;

				prod.setStockRestante(stockNuevo);
				productoService.save(prod);

				// Para mandar el correo de bajo stock
				if (stockNuevo <= prod.getStockMinimo()) {
					List<ImagenProducto> imagenesProducto = prod.getImagenesProducto();

					if (!imagenesProducto.isEmpty()) {
						ImagenProducto primeraImagen = imagenesProducto.get(0);

						urlImagen = primeraImagen.getImgUrl();

					} else {
						urlImagen = "https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Productos_Image%2F19197374.jpg?alt=media&token=1c53622e-6dc9-49a4-8994-2df2d93ac975";
					}

					mensaje = "Se detectó que solo te quedan pocas unidades del producto " + prod.getNombre()
							+ " le recomendamos recargar su stock";
					emailTo = obtenerEmailEmprendedora(obtenerIdUsuarioPorIdEmpresa(prod.getIdEmpresa()));
					stockMinimo = prod.getStockMinimo();
					stockActual = prod.getStockRestante();
					nombreProducto = prod.getNombre();
					sendEmailTemplate(emailTo, mensaje, urlImagen, prod.getIdEmpresa(), stockMinimo, stockActual, nombreProducto);
				}

				creandoLog("Producto", "Actualizar", "Se actualizo el stock del producto " + id, "Exito");
				return new ResponseEntity(new Mensaje("Stock Actualizado"), HttpStatus.OK);
			} else {
				creandoLog("Producto", "Actualizar", "El " + id + " del producto no exite", "Error");
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	// Existe el producto y es valido su estado es 1
	@GetMapping("/checkProductoPorIdProductoEstado/{idProducto}")
	public boolean checkExisteProductoEstadoValido(@PathVariable("idProducto") int idProducto) {
		try {
			boolean existe = productoService.existsByIdAndEstado(idProducto, "1");
			if (existe) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return false;
		}

	}

	// Existe el elcodigo registrado por una empresa
	@GetMapping("/checkExisteCodigoPorEmpresa/{codigo}/{idEmpresa}")
	public boolean checkExisteCodigoPorEmpresa(@PathVariable("codigo") String codigo,
			@PathVariable("idEmpresa") int idEmpresa) {
		try {
			boolean existe = productoService.existsBycodigoAndIdEmpresa(codigo, idEmpresa);
			if (existe) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return false;
		}

	}

	// Cuenta con la cantidad necesaria para hacer la compra
	@GetMapping("/checkCantidadProducto/{idProducto}/{cantidad}")
	public boolean checkCantidadProducto(@PathVariable("idProducto") int idProducto,
			@PathVariable("cantidad") Double cantidad) {

		try {
			Optional<Producto> productoOptional = productoService.getById(idProducto);

			if (productoOptional.isPresent()) {
				if (productoOptional.get().getStockRestante() >= cantidad && cantidad > 0) {
					return true;
				} else {

					return false;
				}

			} else {

				return false;
			}
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return false;
		}

	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@PostMapping("/probarEmail")
	public String probar() {

		String emString = "";
		emString = obtenerEmailEmprendedora(obtenerIdUsuarioPorIdEmpresa(22));

		return emString;
	}

	public void sendEmailTemplate(String emailTo, String mensaje, String urlImagen, int idEmpresa, Double stockMinimo,
			Double stockActual, String nombreProducto) {

		MimeMessage message = javaMailSender.createMimeMessage();

		try {

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			org.thymeleaf.context.Context context = new org.thymeleaf.context.Context();
			Map<String, Object> model = new HashMap<>();
			model.put("titulo", "Hola " + obtenerNombreEmpresaIdEmpresa(idEmpresa) + " aviso de stock mínimo");
			model.put("urlImagen", urlImagen);
			model.put("mensaje", mensaje);
			model.put("stockMinimo", stockMinimo);
			model.put("stockActual", stockActual);
			model.put("nombreProducto", nombreProducto);
			context.setVariables(model);
			String htmlText = templateEngine.process("StockMinimo", context);
			helper.setFrom(mailFrom);
			helper.setTo(emailTo);
			helper.setSubject("Stock mínimo");
			helper.setText(htmlText, true);
			javaMailSender.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public int obtenerIdUsuarioPorIdEmpresa(int idEmpresa) {
		int idUsuario = 0; // Valor predeterminado o valor de error, ajusta según tu lógica

		ObjectMapper objectMapper = new ObjectMapper();

		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8091/empresa/obtenerEmpresaPorId/" + idEmpresa, HttpMethod.GET, null,
					String.class);

			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			idUsuario = jsonNode.at("/idUsuario").asInt();
		} catch (Exception e) {
			// Capturar excepciones más generales, puedes imprimir o manejar el error según
			// tu necesidad.
			e.printStackTrace();
		}

		return idUsuario;
	}

	public String obtenerNombreEmpresaIdEmpresa(int idEmpresa) {

		String nombreEmpresa = ""; // Valor predeterminado o valor de error, ajusta según tu lógica

		ObjectMapper objectMapper = new ObjectMapper();

		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8091/empresa/obtenerEmpresaPorId/" + idEmpresa, HttpMethod.GET, null,
					String.class);

			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			nombreEmpresa = jsonNode.at("/nombreEmpresa").asText();
		} catch (Exception e) {
			// Capturar excepciones más generales, puedes imprimir o manejar el error según
			// tu necesidad.
			e.printStackTrace();
		}

		return nombreEmpresa;
	}

	public String obtenerEmailEmprendedora(int idUsuario) {
		String email = ""; // Valor predeterminado o valor de error

		ObjectMapper objectMapper = new ObjectMapper();

		try {
			ResponseEntity<String> response = restTemplate.exchange(
					"http://localhost:8095/usuario/listaId/" + idUsuario, HttpMethod.GET, null,
					String.class);

			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			email = jsonNode.at("/email").asText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
	}
	@GetMapping("/obtenerNombreUnidadSIN/{codigo}")
	public String obtenerUnidadMedida(@PathVariable("codigo") String codigo) {
	    String jsonUnidades;
	    try {
	        jsonUnidades = productoService.unidadSIN().getUmedida();
	    } catch (Exception e) {
	        e.printStackTrace();
	        return "{\"error\":\"Error al obtener las unidades de medida\"}";
	    }

	    ObjectMapper objectMapper = new ObjectMapper();
	    Map<String, String> unidadesMap;
	    try {
	        unidadesMap = objectMapper.readValue(jsonUnidades, new TypeReference<Map<String, String>>() {});
	    } catch (IOException e) {
	        e.printStackTrace();
	        return "{\"error\":\"Error al parsear el JSON de unidades\"}";
	    }

	    String nombreUnidad = unidadesMap.entrySet()
	            .stream()
	            .filter(entry -> codigo.equals(entry.getValue()))
	            .map(Map.Entry::getKey)
	            .findFirst()
	            .orElse("Código no encontrado");

	    // Crear un objeto JSON con la propiedad "nombre"
	    ObjectNode nombreJson = objectMapper.createObjectNode().put("nombre", nombreUnidad);

	    try {
	        // Convertir el objeto JSON a una cadena JSON y devolverla
	        return objectMapper.writeValueAsString(nombreJson);
	    } catch (JsonProcessingException e) {
	        e.printStackTrace();
	        return "{\"error\":\"Error al generar el JSON de respuesta\"}";
	    }
	}
	
	
}

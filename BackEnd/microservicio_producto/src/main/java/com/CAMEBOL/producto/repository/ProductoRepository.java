package com.CAMEBOL.producto.repository;

import java.util.Date;
import java.util.List;

import javax.swing.Spring;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.producto.entity.CategoriaProducto;
import com.CAMEBOL.producto.entity.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer> {
		List<Producto> findByIdEmpresaAndEstado(Integer idEmpresa, String estado);
		List<Producto> findByIdEmpresa(Integer idEmpresa);
		List<Producto> findByCategoriaProductoAndIdOrganizacionAndEstado(CategoriaProducto categoriaProducto,Integer idOrganizacion, String estado);
		Boolean existsByIdEmpresa(Integer idEmpresa);
		Boolean existsByIdProductoAndEstado(Integer idEmpresa,String estado);
		Boolean existsByCodigoAndIdEmpresa(String codigo,int idEmpresa);
		List<Producto> findByEstado(String estado);
		List<Producto> findByIdOrganizacionAndEstado(int idOrganizacion,String estado);
		
		@Query("SELECT p FROM Producto p INNER JOIN p.categoriaProducto c WHERE c.idCategoriaProducto IN :listaIdCategorias")
	    List<Producto> findByCategoriaIds(List<Integer> listaIdCategorias);
		
		@Query("SELECT p FROM Producto p WHERE p.idOrganizacion = :idOrganizacion AND p.estado = :estado AND DATE(p.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
	    List<Producto> findByIdOrganizacionAndEstadoYFecha(int idOrganizacion,String estado, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
		
		
		@Query("SELECT p FROM Producto p WHERE p.idOrganizacion = :idOrganizacion AND p.estado = :estado AND DATE(p.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin AND p.idEmpresa= :idEmpresa")
	    List<Producto> findByIdOrganizacionEstadoFechaYIdEmpresa(int idOrganizacion,String estado, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin,int idEmpresa);
}

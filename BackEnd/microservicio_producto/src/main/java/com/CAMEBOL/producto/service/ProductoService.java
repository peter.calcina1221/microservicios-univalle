package com.CAMEBOL.producto.service;

import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.Spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.TemplateEngine;

import com.CAMEBOL.producto.entity.CategoriaProducto;
import com.CAMEBOL.producto.entity.ImagenProducto;
import com.CAMEBOL.producto.entity.Producto;
import com.CAMEBOL.producto.entity.ProductoCantidad;
import com.CAMEBOL.producto.entity.ProductoSINResultado;
import com.CAMEBOL.producto.entity.ProductoSubTotal;
import com.CAMEBOL.producto.entity.ProductosSIN;
import com.CAMEBOL.producto.entity.TokenFac;
import com.CAMEBOL.producto.entity.TransaccionVenta;
import com.CAMEBOL.producto.entity.UnidadSIN;
import com.CAMEBOL.producto.entity.UnidadSINResultado;
import com.CAMEBOL.producto.repository.ProductoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bytebuddy.asm.Advice.Return;

import org.springframework.util.MultiValueMap;
import org.springframework.util.LinkedMultiValueMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
@Component
public class ProductoService {
	@Autowired
	ProductoRepository productoRepository;
	@Autowired
    private RestTemplate restTemplate;
	@Autowired
	JavaMailSender javaMailSender;
	@Value("${spring.mail.username}")
	String mailFrom;
	@Autowired
	TemplateEngine templateEngine;


	 public List<Producto> getAll(){
	    	
	    	return productoRepository.findAll();
	    }
	 public List<Producto> getByEmpresaAndEstado(int idEmpresa, String estado){
	    	
	    	return productoRepository.findByIdEmpresaAndEstado(idEmpresa, estado);
	    }
	 public List<Producto> getByIdOrganizacionAndEstado(int idOrganizacion,String estado){
	    	
	    	return productoRepository.findByIdOrganizacionAndEstado(idOrganizacion,estado);
	    }
	 public List<Producto> getByIdOrganizacionAndEstadoYFecha(int idOrganizacion,String estado,Date fechaInicio,Date fechaFin){
	    	
	    	return productoRepository.findByIdOrganizacionAndEstadoYFecha(idOrganizacion,estado,fechaInicio,fechaFin);
	    }
	 
	 public List<Producto> getByIdOrganizacionEstadoFechaYIdEmpresa(int idOrganizacion,String estado,Date fechaInicio,Date fechaFin,int idEmpresa){
	    	
	    	return productoRepository.findByIdOrganizacionEstadoFechaYIdEmpresa(idOrganizacion,estado,fechaInicio,fechaFin,idEmpresa);
	    }
	 public List<Producto> getByEmpresa(int idEmpresa){
	    	
	    	return productoRepository.findByIdEmpresa(idEmpresa);
	    }
	 public List<Producto> getByCategoriaAndOrganizacionAndEstado(CategoriaProducto categoriaProducto,int idOrganizacion, String estado){
	    	
	    	return productoRepository.findByCategoriaProductoAndIdOrganizacionAndEstado(categoriaProducto,idOrganizacion, estado);
	    }
	 public List<Producto> getByEstado(String estado){
	    	
	    	return productoRepository.findByEstado(estado);
	    }
	 public Optional<Producto> getById(int id){
	        return productoRepository.findById(id);
	    }
	 public boolean existsById(Integer id){
	        return productoRepository.existsById(id);
	    }
	 public boolean existsByIdAndEstado(Integer id,String estado){
	        return productoRepository.existsByIdProductoAndEstado(id,estado);
	    }
	 public boolean existsBycodigoAndIdEmpresa(String codigo,int idEmrpesa){
	        return productoRepository.existsByCodigoAndIdEmpresa(codigo,idEmrpesa);
	    }
	 public boolean existsByIdEmpresa(Integer idEmpresa){
	        return productoRepository.existsByIdEmpresa(idEmpresa);
	    }
	 @Transactional
	 public void save(Producto producto){
		 productoRepository.save(producto);
	    }
	 
	  public void delete(int id) {
		  productoRepository.deleteById(id);
	    }
	  
	  @JmsListener(destination = "pruebatransaccion", containerFactory = "jmsListenerContainerFactory")
	  @Transactional
	  public void actualizarStock(String mensaje) {
	      Logger logger = LoggerFactory.getLogger(ProductoService.class);
	      String input = mensaje;
	      String msg="";
	      String emailTo="";
	      String urlImagen="";
	      Double stockMinimo=0.0;
	      Double stockActual=0.0;
	      String nombreProducto="";  
	        // Divide la cadena utilizando el carácter ';'
	        String[] parts = input.split(";");
	        
	        // Asigna las partes a variables individuales
	        String idString = parts[0]; // "hola"
	        String cantidadString = parts[1]; // "mundo"
	      
	        System.err.println("Mensaje recibido desde la cola: " + idString+" "+cantidadString);

	      int idProducto = Integer.parseInt(idString);
	      double cantidad = Double.parseDouble(cantidadString);

	      Optional<Producto> optionalProducto = getById(idProducto);
	      if (optionalProducto.isPresent()) {
	          Producto prod = optionalProducto.get();
	          double stockNuevo = prod.getStockRestante() - cantidad;
	          prod.setStockRestante(stockNuevo);
	          save(prod);
	          
	          //Para mandar el correo de bajo stock
	          if(stockNuevo<=prod.getStockMinimo()) {
					List<ImagenProducto> imagenesProducto = prod.getImagenesProducto();

					if (!imagenesProducto.isEmpty()) {
					    ImagenProducto primeraImagen = imagenesProducto.get(0);

					    urlImagen=primeraImagen.getImgUrl();
						
					}
					else {
						urlImagen="https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Productos_Image%2F19197374.jpg?alt=media&token=1c53622e-6dc9-49a4-8994-2df2d93ac975";
					}
					
					msg="Se detectó que solo te quedan pocas unidades del producto " + prod.getNombre() + " le recomendamos recargar su stock.";
					emailTo=obtenerEmailEmprendedora(obtenerIdUsuarioPorIdEmpresa(prod.getIdEmpresa()));
					stockMinimo=prod.getStockMinimo();
					stockActual=prod.getStockRestante();
					nombreProducto=prod.getNombre();
					sendEmailTemplate(emailTo, msg, urlImagen,prod.getIdEmpresa(),stockMinimo,stockActual,nombreProducto);
				}
	          
	      } else {
	          // Manejar el caso en que no se encuentra el producto
	          logger.error("Producto no encontrado con ID: " + idProducto);
	      }
	  }
	
	  public List<Producto> getProductoMasVendidoPorEmpresa(int idEmpresa,int limit){
		  ResponseEntity<List<ProductoCantidad>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listaProductoMasVendidoPorEmpresa/"+idEmpresa+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<ProductoCantidad>>() {}
		        );
		  
		  List<ProductoCantidad> productoCant=new ArrayList<>();
		  List<Producto> productos=new ArrayList<>();
		  productoCant=response.getBody();
		  Producto producto=new Producto();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (ProductoCantidad PC : productoCant) {
						 producto=getById(PC.getIdProducto()).get();
						 producto.setCantidadVendida(PC.getCantidad());
						 productos.add(producto);
					}
		        	
		        	
		            return productos;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	    }
	  
	  public List<Producto> getProductoMasVendidoPorEmpresaYFecha(int idEmpresa,Date fechaInicio,Date fechaFin, int limit){
		  ResponseEntity<List<ProductoCantidad>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listaProductoMasVendidoPorEmpresaYFecha/"+idEmpresa+"/"+fechaInicio+"/"+fechaFin+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<ProductoCantidad>>() {}
		        );
		  
		  List<ProductoCantidad> productoCant=new ArrayList<>();
		  List<Producto> productos=new ArrayList<>();
		  productoCant=response.getBody();
		  Producto producto=new Producto();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (ProductoCantidad PC : productoCant) {
						 producto=getById(PC.getIdProducto()).get();
						 producto.setCantidadVendida(PC.getCantidad());
						 productos.add(producto);
					}
		        	
		        	
		            return productos;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	    }
	  
	  public List<Producto> getProductosCompradosPorUsuario(int idUsuario, int limit){
		// ObjectMapper de Jackson para parsear JSON
			ObjectMapper objectMapper = new ObjectMapper();
		  try {
			   ResponseEntity<String> response = restTemplate.exchange(
			            "http://localhost:8096/detalleVenta/ListaProductoCompradosPorUsuario/"+idUsuario,
			            HttpMethod.GET,
			            null,
			            String.class
			        );
			   // Parsear el JSON a un array de JsonNode
			    JsonNode[] jsonNodes = objectMapper.readValue(response.getBody(), JsonNode[].class);

			    List<Integer> idProductos = new ArrayList<>();

			    // Iterar sobre los elementos y extraer los IDs de producto
			    for (JsonNode jsonNode : jsonNodes) {
			        int idProducto = jsonNode.get("idProducto").intValue();
			        idProductos.add(idProducto);
			    }
			    
			    
			    List<Integer> idCategorias = new ArrayList<>();
			    //Obtener una lista de categorias
			    for (Integer idProducto : idProductos) {
			    	Producto producto=getById(idProducto).get();
			    	CategoriaProducto categoriaProducto = producto.getCategoriaProducto();
			    	if (idCategorias.contains(categoriaProducto.getIdCategoriaProducto())) {
			    	    
			    	} else {
			    	   idCategorias.add(categoriaProducto.getIdCategoriaProducto());
			    	}
			    }
			    
			    
			    List<Producto> productos = productoRepository.findByCategoriaIds(idCategorias);
			    //Desordena la lista productos
			    Collections.shuffle(productos);
			    List<Producto> mostrar=new ArrayList<>();  
			    
			    if(productos.size()>=limit) {
			    	for (int i = 0; i < limit; i++) {
			    		mostrar.add(productos.get(i));
					    }
			    }
			  //Desordena la lista mostrar
			    Collections.shuffle(mostrar);
			   
			    return mostrar;
		   } 
		  catch (Exception e) {
			// TODO: handle exception
			  return null;
		}
	    }
	  public String obtenerToken() {
		  ResponseEntity<TokenFac> checkResponse = restTemplate
					.getForEntity("http://localhost:8096/token/verificarToken", TokenFac.class);
		  TokenFac tokenFac=checkResponse.getBody();
		  String token=tokenFac.getToken();
		  return token;
	  }

	  public List<ProductoSINResultado> obtenerProductoSIN()
	  {
		  ProductosSIN productosSIN=productosSIN();
		   String jsonString = productosSIN.getPRODSIN(); // Reemplaza con el string que proporcionaste

			// Inicializar ObjectMapper
	        ObjectMapper objectMapper = new ObjectMapper();

	        try {
	            // Deserializar el JSON en un objeto JsonNode
	            JsonNode jsonNode = objectMapper.readTree(jsonString);
	            List<ProductoSINResultado> categorias = new ArrayList<>();
	            // Iterar sobre cada par "nombre" y "valor"
	            jsonNode.fields().forEachRemaining(entry -> {
	                String nombre = entry.getKey();
	                String valor = entry.getValue().asText();
	                ProductoSINResultado categoria = new ProductoSINResultado();
	                categoria.setNombre(nombre);
	                categoria.setValor(valor);
	                categorias.add(categoria);
	            });

		        return categorias;


	        } catch (Exception e) {
	            System.err.println("Error al procesar el JSON: " + e.getMessage());
	            return null;
	        }
		   
		   
	     	   
	  }
	  public ProductosSIN productosSIN() {
		    // URL de la API
		    String apiUrl = "https://ns2-enterprise.eerpbo.com:6001/api_consulta_productos_sin";
		    String jsonInput = "{ \"cliente\": { \"nit\": \"5815709017\" } }";
		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Configura los parámetros en un mapa
		    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl)
		            .queryParam("token", obtenerToken());
		   
		    // Configura la solicitud con los parámetros en el cuerpo
		    HttpEntity<String> requestEntity = new HttpEntity<>(jsonInput,headers);

		    // RestTemplate
		    RestTemplate restTemplate = new RestTemplate();

		 // Deshabilita la validación de SSL
	        TrustManager[] trustAllCerts = new TrustManager[] {
	            new X509TrustManager() {
	                public X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	                public void checkClientTrusted(X509Certificate[] certs, String authType) {}
	                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
	            }
	        };

		    try {
		    	SSLContext sc = SSLContext.getInstance("SSL");
	            sc.init(null, trustAllCerts, new java.security.SecureRandom());
	            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	            HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
		        // Realiza la solicitud POST
		        ResponseEntity<String> responseEntity = restTemplate.exchange(
		                uriBuilder.toUriString(),
		                HttpMethod.POST,
		                requestEntity,
		                String.class
		        );
		     // Deserializa la respuesta JSON en un objeto TokenRespuesta
	            ObjectMapper objectMapper = new ObjectMapper();
	            ProductosSIN productosSIN = objectMapper.readValue(responseEntity.getBody(), ProductosSIN.class);
		        // Imprime la respuesta y devuelve el objeto
		        System.out.println("Respuesta de la API: " + responseEntity.getBody());
		        return productosSIN;
		    } catch (Exception e) {
		        // Maneja cualquier excepción
		        e.printStackTrace();
		        // Puedes decidir cómo manejar errores en tu aplicación
		        return null;
		    }
		}
	  public List<UnidadSINResultado> obtenerUnidadSIN()
	  {
		  UnidadSIN unidadSIN=unidadSIN();
		  String jsonString = unidadSIN.getUmedida(); // Reemplaza con el string que proporcionaste

			// Inicializar ObjectMapper
	        ObjectMapper objectMapper = new ObjectMapper();

	        try {
	            // Deserializar el JSON en un objeto JsonNode
	            JsonNode jsonNode = objectMapper.readTree(jsonString);
	            List<UnidadSINResultado> unidades = new ArrayList<>();
	            // Iterar sobre cada par "nombre" y "valor"
	            jsonNode.fields().forEachRemaining(entry -> {
	                String nombre = entry.getKey();
	                String valor = entry.getValue().asText();
	                UnidadSINResultado unidad = new UnidadSINResultado();
	            	unidad.setNombre(nombre);
	            	unidad.setValor(valor);
	            	unidades.add(unidad);
	            });

		        return unidades;


	        } catch (Exception e) {
	            System.err.println("Error al procesar el JSON: " + e.getMessage());
	            return null;
	        }
	  }
	  
	  public UnidadSIN unidadSIN() {
		    // URL de la API
		    String apiUrl = "https://ns2-enterprise.eerpbo.com:6001/api_consulta_unidad_medida";
		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Configura los parámetros en un mapa
		    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiUrl)
		            .queryParam("token", obtenerToken());
		   
		   

		    // RestTemplate
		    RestTemplate restTemplate = new RestTemplate();
		   
		 // Deshabilita la validación de SSL
	        TrustManager[] trustAllCerts = new TrustManager[] {
	            new X509TrustManager() {
	                public X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	                public void checkClientTrusted(X509Certificate[] certs, String authType) {}
	                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
	            }
	        };

		    try {
		    	SSLContext sc = SSLContext.getInstance("SSL");
	            sc.init(null, trustAllCerts, new java.security.SecureRandom());
	            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	            HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
	            // Realiza la solicitud GET y obtiene la respuesta en un ResponseEntity
		        ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.toUriString(), String.class);
		     // Deserializa la respuesta JSON en un objeto TokenRespuesta
	          ObjectMapper objectMapper = new ObjectMapper();
	          UnidadSIN unidadSIN = objectMapper.readValue(response.getBody(), UnidadSIN.class);
		        // Imprime la respuesta y devuelve el objeto
		        System.out.println("Respuesta de la API: " + response.getBody());
		        return unidadSIN;
		    } catch (Exception e) {
		        // Maneja cualquier excepción
		        e.printStackTrace();
		        // Puedes decidir cómo manejar errores en tu aplicación
		        return null;
		    }
		}
	  
	  
	  public List<Producto> getProductoMasVendidoPorOrganizacion(int idOrganizacion,int limit){
		  ResponseEntity<List<ProductoCantidad>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listaProductoMasVendidoPorOrganizacion/"+idOrganizacion+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<ProductoCantidad>>() {}
		        );
		  
		  List<ProductoCantidad> productoCant=new ArrayList<>();
		  List<Producto> productos=new ArrayList<>();
		  productoCant=response.getBody();
		  Producto producto=new Producto();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (ProductoCantidad PC : productoCant) {
						 producto=getById(PC.getIdProducto()).get();
						 producto.setCantidadVendida(PC.getCantidad());
						 productos.add(producto);
					}
		        	
		        	
		            return productos;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	    	
	    }
	  public List<Producto> getSubtotalProductoPorEmpresa(int idEmpresa){
		  ResponseEntity<List<ProductoSubTotal>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listarSubTotalProductosPorEmpresa/"+idEmpresa,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<ProductoSubTotal>>() {}
		        );
		  
		  List<ProductoSubTotal> productoSubtotal=new ArrayList<>();
		  List<Producto> productos=new ArrayList<>();
		  productoSubtotal=response.getBody();
		  Producto producto=new Producto();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (ProductoSubTotal PST : productoSubtotal) {
						 producto=getById(PST.getIdProducto()).get();
						 producto.setSubTotal(PST.getSubtotal());
						 productos.add(producto);
					}
		        	
		        	
		            return productos;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	    	
	    }
	  public List<Producto> getSubtotalProductoPorEmpresaYFecha(int idEmpresa, Date fechaInicio, Date fechaFin) {
		    ResponseEntity<List<ProductoSubTotal>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listarSubTotalProductosPorEmpresaYFecha/" + idEmpresa + "/"
		                    + formatDate(fechaInicio) + "/" + formatDate(fechaFin),
		            HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductoSubTotal>>() {});

		    List<ProductoSubTotal> productoSubtotal = response.getBody();
		    List<Producto> productos = new ArrayList<>();

		    if (response.getStatusCode() == HttpStatus.OK) {
		        for (ProductoSubTotal PST : productoSubtotal) {
		            Producto producto = getById(PST.getIdProducto()).orElse(new Producto());
		            producto.setSubTotal(PST.getSubtotal());
		            productos.add(producto);
		        }

		        return productos;
		    } else {
		        throw new RuntimeException("Error al obtener la lista desde el servicio.");
		    }
		}

		private String formatDate(Date date) {
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    return formatter.format(date);
		}
	  
		public void sendEmailTemplate(String emailTo,String mensaje,String urlImagen,int idEmpresa,Double stockMinimo,Double stockActual,String nombreProducto) {
			
			
			MimeMessage message = javaMailSender.createMimeMessage();

			try {

				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				org.thymeleaf.context.Context context = new org.thymeleaf.context.Context();
				Map<String, Object> model = new HashMap<>();
				model.put("titulo", "Hola "+obtenerNombreEmpresaIdEmpresa(idEmpresa)+" aviso de stock mínimo");
				model.put("urlImagen", urlImagen);
				model.put("mensaje", mensaje);
				model.put("stockMinimo", stockMinimo);
				model.put("stockActual", stockActual);
				model.put("nombreProducto", nombreProducto);
				context.setVariables(model);
				String htmlText = templateEngine.process("StockMinimo", context);
				helper.setFrom(mailFrom);
				helper.setTo(emailTo);
				helper.setSubject("Stock mínimo");
				helper.setText(htmlText, true);
				javaMailSender.send(message);

			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}

		public int obtenerIdUsuarioPorIdEmpresa(int idEmpresa) {
			 int idUsuario = 0; // Valor predeterminado o valor de error, ajusta según tu lógica

			    ObjectMapper objectMapper = new ObjectMapper();

			    try {
			        ResponseEntity<String> response = restTemplate.exchange(
			                "http://localhost:8091/empresa/obtenerEmpresaPorId/" + idEmpresa, HttpMethod.GET, null,
			                String.class);

			        JsonNode jsonNode = objectMapper.readTree(response.getBody());
			        idUsuario = jsonNode.at("/idUsuario").asInt();
			    } catch (Exception e) {
			        // Capturar excepciones más generales, puedes imprimir o manejar el error según tu necesidad.
			        e.printStackTrace();
			    }

			    return idUsuario;
		}
		public String obtenerNombreEmpresaIdEmpresa(int idEmpresa) {
				
				String nombreEmpresa = ""; // Valor predeterminado o valor de error, ajusta según tu lógica

			    ObjectMapper objectMapper = new ObjectMapper();

			    try {
			        ResponseEntity<String> response = restTemplate.exchange(
			                "http://localhost:8091/empresa/obtenerEmpresaPorId/" + idEmpresa, HttpMethod.GET, null,
			                String.class);

			        JsonNode jsonNode = objectMapper.readTree(response.getBody());
			        nombreEmpresa = jsonNode.at("/nombreEmpresa").asText();
			    } catch (Exception e) {
			        // Capturar excepciones más generales, puedes imprimir o manejar el error según tu necesidad.
			        e.printStackTrace();
			    }

			    return nombreEmpresa;
		}
		
		public String obtenerEmailEmprendedora(int idUsuario) {
			 String email = ""; // Valor predeterminado o valor de error, ajusta según tu lógica

			    ObjectMapper objectMapper = new ObjectMapper();

			    try {
			        ResponseEntity<String> response = restTemplate.exchange(
			                "http://localhost:8095/usuario/listaId/" + idUsuario, HttpMethod.GET, null,
			                String.class);

			        JsonNode jsonNode = objectMapper.readTree(response.getBody());
			        email = jsonNode.at("/email").asText();
			    } catch (Exception e) {
			        // Capturar excepciones más generales, puedes imprimir o manejar el error según tu necesidad.
			        e.printStackTrace();
			    }

			    return email;
		}

}

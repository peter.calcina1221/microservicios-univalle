package com.CAMEBOL.producto.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.producto.entity.CaracteristicaProducto;
import com.CAMEBOL.producto.entity.Mensaje;
import com.CAMEBOL.producto.entity.Producto;
import com.CAMEBOL.producto.service.CaracteristicaProductoService;
import com.CAMEBOL.producto.service.ProductoService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/caracteristicaProducto")
@CrossOrigin(origins = "*")
public class CaracteristicaProductoController {
	@Autowired
	private CaracteristicaProductoService caracteristicaProductoService;
	@Autowired
	private ProductoService productoService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<CaracteristicaProducto>> getAll() {
		try {
			List<CaracteristicaProducto> list = caracteristicaProductoService.getAll();
			return new ResponseEntity<List<CaracteristicaProducto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/listaPorIdProducto/{idProducto}")
	public ResponseEntity<List<CaracteristicaProducto>> getByIdProducto(@PathVariable("idProducto") int idProducto) {

		try {
			if (!productoService.existsById(idProducto)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Producto producto = productoService.getById(idProducto).get();
			List<CaracteristicaProducto> list = caracteristicaProductoService.getByIdProducto(producto);
			return new ResponseEntity<List<CaracteristicaProducto>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerPorIdCaracteristicaProducto/{idCaracteristicaProducto}")
	public ResponseEntity<CaracteristicaProducto> getById(
			@PathVariable("idCaracteristicaProducto") int idCaracteristicaProducto) {
		try {
			if (!caracteristicaProductoService.existsByIdCaracteristicaProducto(idCaracteristicaProducto)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			CaracteristicaProducto caracteristicaProducto = caracteristicaProductoService
					.getByIdCaracteristicaProducto(idCaracteristicaProducto).get();
			return new ResponseEntity(caracteristicaProducto, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody CaracteristicaProducto caracteristicaProducto,
			BindingResult bindingResult) {

		try {
			caracteristicaProductoService.save(caracteristicaProducto);
			creandoLog("Producto", "Insertar",
					"Se creo la caracteristica producto " + caracteristicaProducto.getIdCaracteristicaProducto(), "Exito");
			return new ResponseEntity(new Mensaje("Caracteristica Producto creada"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{idCaracteristicaProducto}")
	public ResponseEntity<?> actualizar(@PathVariable("idCaracteristicaProducto") int idCaracteristicaProducto,
			@RequestBody CaracteristicaProducto caracteristicaProducto) {

		try {
			CaracteristicaProducto carctProd = caracteristicaProductoService
					.getByIdCaracteristicaProducto(idCaracteristicaProducto).get();
			carctProd.setValor(caracteristicaProducto.getValor());
			carctProd.setCaracteristica(caracteristicaProducto.getCaracteristica());
			carctProd.setProducto(caracteristicaProducto.getProducto());
			caracteristicaProductoService.save(carctProd);
			creandoLog("Producto", "Actualizar", "Se actualizo la caracteristica producto " + idCaracteristicaProducto,
					"Exito");
			return new ResponseEntity(new Mensaje("Imagen Actualizada"), HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/elimnar/{idCaracteristicaProducto}")
	public ResponseEntity<?> delete(@PathVariable("idCaracteristicaProducto") int idCaracteristicaProducto) {
		if (!caracteristicaProductoService.existsByIdCaracteristicaProducto(idCaracteristicaProducto)) {
			return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
		}
		caracteristicaProductoService.delete(idCaracteristicaProducto);
		return new ResponseEntity(new Mensaje("Caracteristica producto Eliminada"), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}

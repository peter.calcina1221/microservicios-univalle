package com.CAMEBOL.producto.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.producto.entity.Caracteristica;
import com.CAMEBOL.producto.entity.Mensaje;
import com.CAMEBOL.producto.service.CaracteristicaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/caracteristica")
@CrossOrigin(origins = "*")
public class CaracateristicaController {
	@Autowired
	private CaracteristicaService caracteristicaService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<Caracteristica>> getAll() {
		try {
			List<Caracteristica> list = caracteristicaService.getAll();
			return new ResponseEntity<List<Caracteristica>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorEstado/{estado}")
	public ResponseEntity<List<Caracteristica>> getAllByEstado(@PathVariable("estado") String estado) {
		try {
			List<Caracteristica> list = caracteristicaService.getAllByEstado(estado);
			return new ResponseEntity<List<Caracteristica>>(list, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerPorIdCaracteristica/{idCaracteristica}")
	public ResponseEntity<Caracteristica> getById(@PathVariable("idCaracteristica") int idCaracteristica) {
		try {
			if (!caracteristicaService.existsById(idCaracteristica)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Caracteristica caracteristica = caracteristicaService.getById(idCaracteristica).get();
			return new ResponseEntity(caracteristica, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody Caracteristica caracteristica, BindingResult bindingResult) {
		try {
			caracteristicaService.save(caracteristica);
			creandoLog("Producto", "Insertar", "Se creo la caracteristica producto " + caracteristica.getNombre(), "Exito");
			return new ResponseEntity(new Mensaje("Caracteristica creada"), HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{idCaracteristica}")
	public ResponseEntity<?> actualizar(@PathVariable("idCaracteristica") int idCaracteristica,
			@RequestBody Caracteristica caracteristica) {

		try {
			Caracteristica carac = caracteristicaService.getById(idCaracteristica).get();
			carac.setNombre(caracteristica.getNombre());
			carac.setDescripcion(caracteristica.getDescripcion());
			carac.setEstado(caracteristica.getEstado());
			carac.setFechaRegistro(caracteristica.getFechaRegistro());
			carac.setCaracteristicaProductos(caracteristica.getCaracteristicaProductos());
			carac.setIdOrganizacion(caracteristica.getIdOrganizacion());
			caracteristicaService.save(carac);
			creandoLog("Producto", "Actualizar", "Se actualizo la caracteristica producto " + idCaracteristica, "Exito");
			return new ResponseEntity(new Mensaje("Caracteristica Actualizada"), HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Producto", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping("/elimnar/{idCaracteristica}")
	public ResponseEntity<?> delete(@PathVariable("idCaracteristica") int idCaracteristica) {
		caracteristicaService.delete(idCaracteristica);
		return new ResponseEntity(new Mensaje("Caracteristica Eliminada"), HttpStatus.OK);
	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			// Puedes manejar la respuesta según sea necesario
			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}

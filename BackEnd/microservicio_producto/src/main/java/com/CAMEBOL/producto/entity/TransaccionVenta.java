package com.CAMEBOL.producto.entity;

import java.io.Serializable;

public class TransaccionVenta implements Serializable {
	 private static final long serialVersionUID = -295422703255886286L;
	 private int idProducto;
	 private Double cantidad;
	 
	public TransaccionVenta() {
		super();
	}
	
	public TransaccionVenta(int idProducto, double cantidad) {
		super();
		this.idProducto = idProducto;
		this.cantidad = cantidad;
	}

	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}
	 
}

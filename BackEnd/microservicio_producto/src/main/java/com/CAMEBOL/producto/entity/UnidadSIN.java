package com.CAMEBOL.producto.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnidadSIN {
	@JsonProperty("TRANSACCION")
    private boolean transaccion;
	@JsonProperty("UMEDIDA")
    private String umedida;
	public UnidadSIN() {
		super();
	}
	public boolean isTransaccion() {
		return transaccion;
	}
	public void setTransaccion(boolean transaccion) {
		this.transaccion = transaccion;
	}
	public String getUmedida() {
		return umedida;
	}
	public void setUmedida(String umedida) {
		this.umedida = umedida;
	}
	
}

package com.CAMEBOL.pago1.entity;

import com.CAMEBOL.pago1.entity.OrderPaymentStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnModelInquiryPayment {
	 private int Status;
		@JsonProperty("OrderPaymentStatus")
		 private OrderPaymentStatus OrderPaymentStatus; 
		 private String Message;
		
		public ReturnModelInquiryPayment() {
			super();
		}
		public int getStatus() {
			return Status;
		}
		public void setStatus(int status) {
			Status = status;
		}
		public OrderPaymentStatus getOrderPaymentStatus() {
			return OrderPaymentStatus;
		}
		public void setOrderPaymentStatus(OrderPaymentStatus orderPaymentStatus) {
			OrderPaymentStatus = orderPaymentStatus;
		}
		public String getMessage() {
			return Message;
		}
		public void setMessage(String message) {
			Message = message;
		}
		 
		 
}

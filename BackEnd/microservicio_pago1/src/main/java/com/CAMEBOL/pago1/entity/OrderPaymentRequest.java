package com.CAMEBOL.pago1.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderPaymentRequest {
	@JsonProperty("Credentials")
    private String Credentials;
	@JsonProperty("OrderCode")
    private String OrderCode;
	public OrderPaymentRequest() {
		super();
	}
	
	public String getCredentials() {
		return Credentials;
	}
	public void setCredentials(String credentials) {
		Credentials = credentials;
	}
	public String getOrderCode() {
		return OrderCode;
	}
	public void setOrderCode(String orderCode) {
		OrderCode = orderCode;
	}
}

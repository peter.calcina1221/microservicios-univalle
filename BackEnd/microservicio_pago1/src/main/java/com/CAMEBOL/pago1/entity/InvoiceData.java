package com.CAMEBOL.pago1.entity;

import java.util.List;

import com.CAMEBOL.pago1.entity.InvoiceDetail;

public class InvoiceData {
	private List<InvoiceDetail> InvoiceDetails; //Información relacionada al detalle de la factura a generar.

	public InvoiceData() {
		super();
	}

	public List<InvoiceDetail> getInvoiceDetails() {
		return InvoiceDetails;
	}

	public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
		InvoiceDetails = invoiceDetails;
	}
}

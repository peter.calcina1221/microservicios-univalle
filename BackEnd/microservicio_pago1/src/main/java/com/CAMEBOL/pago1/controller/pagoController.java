package com.CAMEBOL.pago1.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.pago1.entity.OrderPaymentRequest;
import com.CAMEBOL.pago1.entity.PaymentRequest;
import com.CAMEBOL.pago1.entity.ReturnModelCreate;
import com.CAMEBOL.pago1.entity.ReturnModelInquiryPayment;
import com.CAMEBOL.pago1.entity.Venta;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping("/pago")
@CrossOrigin(origins = "*")
public class pagoController {
	@Autowired
	private RestTemplate restTemplate;
	@Value("${credenciales}")
	private String credenciales;
	private  List<String[]> iData = new ArrayList<>();
	
	@PostMapping("/crear")
    public ResponseEntity<?> nuevo(@RequestBody Venta venta){
		try {
			LocalDateTime now = LocalDateTime.now();
			UUID uuid = UUID.randomUUID();
			String uniqueId = now.toString() + "-" + uuid.toString();
			String[] array1 = {"client_phone_number", "+59177178368"};
			String[] array2 = {"customer_document_number", "5811832"};
			iData.add(array1);
			iData.add(array2);
			PaymentRequest paymentRequest=new PaymentRequest();
			int total=(int)(venta.getTotal()*100);
			String[] formOfPayments = null;
			paymentRequest.setAmount(total);
			paymentRequest.setCurrencyCode("BOB");
	        paymentRequest.setCredentials(credenciales);
	        paymentRequest.setSystemTraceAuditNumber(uniqueId);
	        paymentRequest.setEmailShooper("e.crespoparada@gmail.com");// hay que cambiar 
	        // adicional data
	        paymentRequest.setIData(iData);
	        paymentRequest.setFormOfPayments(formOfPayments);
	        paymentRequest.setRequireNIT(false);
	        paymentRequest.setLinklifetimeMinutes(5);
	        
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);

	        HttpEntity<PaymentRequest> requestEntity = new HttpEntity<>(paymentRequest, headers);
	        
	        
	        ResponseEntity<ReturnModelCreate> responseEntity = restTemplate.exchange(
	                "http://devkqtest.eastus2.cloudapp.azure.com/KXPaymentTR/HostedService.svc/CreateOrder",
	                HttpMethod.POST,
	                requestEntity,
	                ReturnModelCreate.class
	            );
	    	creandoLog("Pago","Insertar","Se creo el pago","Exito");
			return new ResponseEntity(responseEntity.getBody(),HttpStatus.CREATED);
			
		} catch (Exception e) {
			creandoLog("Pago","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		}
		
    }
	@PostMapping("/confirmar")
    public ResponseEntity<?> confirmar(@RequestBody OrderPaymentRequest orderPaymentRequest){
		try {
			OrderPaymentRequest orderPaymentRequ=new OrderPaymentRequest();
			orderPaymentRequ.setCredentials(credenciales);
			orderPaymentRequ.setOrderCode(orderPaymentRequest.getOrderCode());
			 HttpHeaders headers = new HttpHeaders();
		        headers.setContentType(MediaType.APPLICATION_JSON);

		        HttpEntity<OrderPaymentRequest> requestEntity = new HttpEntity<>(orderPaymentRequ, headers);
		        
		        
		        ResponseEntity<ReturnModelInquiryPayment> responseEntity = restTemplate.exchange(
		                "http://devkqtest.eastus2.cloudapp.azure.com/KXPaymentTR/HostedService.svc/InquiryPayment",
		                HttpMethod.POST,
		                requestEntity,
		                ReturnModelInquiryPayment.class
		            );
		     creandoLog("Pago","Confirmar","Se confirmo el pago","Exito");     
			return new ResponseEntity(responseEntity.getBody(),HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Pago","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		}
		
    }
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

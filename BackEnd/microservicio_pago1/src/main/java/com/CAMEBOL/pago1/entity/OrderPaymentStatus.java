package com.CAMEBOL.pago1.entity;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderPaymentStatus {
	@JsonProperty("AuthorizationCode")
    private String authorizationCode;

    @JsonProperty("AuthorizationDate")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date authorizationDate;

    @JsonProperty("FormOfPayment")
    private String formOfPayment;

    @JsonProperty("OrderCode")
    private String orderCode;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("StatusCode")
    private int statusCode;

    @JsonProperty("SystemTraceAuditNumber")
    private String systemTraceAuditNumber;

    @JsonProperty("TransactionDate")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date transactionDate;
    
	public OrderPaymentStatus() {
		super();
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public Date getAuthorizationDate() {
		return authorizationDate;
	}

	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	public String getFormOfPayment() {
		return formOfPayment;
	}

	public void setFormOfPayment(String formOfPayment) {
		this.formOfPayment = formOfPayment;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}

	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
    
}

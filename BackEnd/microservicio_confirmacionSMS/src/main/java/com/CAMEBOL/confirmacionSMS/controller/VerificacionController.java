package com.CAMEBOL.confirmacionSMS.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.confirmacionSMS.service.VerificacionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.CAMEBOL.confirmacionSMS.entity.Mensaje;
import com.CAMEBOL.confirmacionSMS.entity.MensajeVerificacion;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/verificacion")
public class VerificacionController {
	@Autowired
	private VerificacionService verificacionService;
	String codigoAleatorio;
	@Autowired
	private RestTemplate restTemplate;
	    @PostMapping("/envioSMS")
	    public ResponseEntity<?> sendVerification(@RequestBody MensajeVerificacion mensajeVerificacion) {
	    	try {
	    		int longitudCodigo = 6;
	   	      	codigoAleatorio = generarCodigoAleatorio(longitudCodigo);
	   	      	mensajeVerificacion.setCodigo(codigoAleatorio);
	   	      	verificacionService.enviarSMS(mensajeVerificacion);  
	   	     creandoLog("SMS","Enviar","Se envio un codigo de verificacion al numero "+mensajeVerificacion.getNumero(),"Exito");
	   	      	return new ResponseEntity(new Mensaje("Mensaje Enviado"),HttpStatus.CREATED);
				
			} catch (Exception e) {
				creandoLog("SMS","Excepcion",e.getMessage(),"Error");
				return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
			}
	     
	    }
	    
	    @PostMapping("/verificarCodigo")
	    public ResponseEntity<?> verificarCodigo(@RequestBody MensajeVerificacion mensajeVerificacion) {
	    	try {
	    		if (mensajeVerificacion.getCodigoVerificacion().equals(codigoAleatorio)) {
		        	codigoAleatorio="";
		        	creandoLog("SMS","Verificar","El codigo de SMS es correcto","Exito");
		        	 return new ResponseEntity(new Mensaje("El código es válido."),HttpStatus.CREATED);
		           
		            
		        } else {
		        	creandoLog("SMS","Verificar","El codigo es incorrecto","Error");
		        	 return new ResponseEntity(new Mensaje("El código no es válido."),HttpStatus.CREATED);
		        }
				
			} catch (Exception e) {
				creandoLog("SMS","Excepcion",e.getMessage(),"Error");
				return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
			}
	        
	    }
	    
	    public  String generarCodigoAleatorio(int longitud) {
	    	try {
	    		 Random random = new Random();
	 	        StringBuilder codigo = new StringBuilder();

	 	        for (int i = 0; i < longitud; i++) {
	 	            int numeroAleatorio = random.nextInt(10); // Genera un número aleatorio entre 0 y 9
	 	            codigo.append(numeroAleatorio);
	 	        }

	 	        return codigo.toString();
			} 
	    	catch (Exception e) {
				creandoLog("SMS","Excepcion",e.getMessage(),"Error");
				return "";
			}
	       
	    }
	    
	    public void creandoLog(String ms,String accion, String descripcion,String estado) {
			try {
				 // URL de la API
			    String apiUrl = "http://localhost:8101/log/nuevo";

			    // Configura la cabecera
			    HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_JSON);

			    // Crea un objeto Java y conviértelo a JSON
			    Map<String, String> requestBody = new HashMap<>();
			    requestBody.put("ms", ms);
			    requestBody.put("accion", accion);
			    requestBody.put("descripcion", descripcion);
			    requestBody.put("estado", estado);
			    String json = new ObjectMapper().writeValueAsString(requestBody);

			    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
			    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			    // Realiza la solicitud
			    ResponseEntity<String> responseEntity = restTemplate.exchange(
			        apiUrl,
			        HttpMethod.POST,
			        requestEntity,
			        String.class
			    );

			    // Puedes manejar la respuesta según sea necesario
			    String responseBody = responseEntity.getBody();
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
}

package com.CAMEBOL.confirmacionSMS;

import java.util.HashMap;
import java.util.Map;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroservicioConfirmacionSmsApplication {
	@Autowired
	private RestTemplate restTemplate;
	public static void main(String[] args) {
		SpringApplication.run(MicroservicioConfirmacionSmsApplication.class, args);
	}
	 @PostConstruct
	    public void init() {
		 creandoLog("SMS","Iniciar","Se inicio el microservicio SMS","Exito");
	    }
	 @PreDestroy
	    public void shutdown() {
		 creandoLog("SMS","Apagar","Se apago el microservicio SMS","Exito");
	    }
	 
	 public void creandoLog(String ms,String accion, String descripcion,String estado) {
			try {
				 // URL de la API
			    String apiUrl = "http://localhost:8101/log/nuevo";

			    // Configura la cabecera
			    HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_JSON);

			    // Crea un objeto Java y conviértelo a JSON
			    Map<String, String> requestBody = new HashMap<>();
			    requestBody.put("ms", ms);
			    requestBody.put("accion", accion);
			    requestBody.put("descripcion", descripcion);
			    requestBody.put("estado", estado);
			    String json = new ObjectMapper().writeValueAsString(requestBody);

			    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
			    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			    // Realiza la solicitud
			    ResponseEntity<String> responseEntity = restTemplate.exchange(
			        apiUrl,
			        HttpMethod.POST,
			        requestEntity,
			        String.class
			    );

			    // Puedes manejar la respuesta según sea necesario
			    String responseBody = responseEntity.getBody();
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}

}

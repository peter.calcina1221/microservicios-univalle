package com.CAMEBOL.confirmacionSMS.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.CAMEBOL.confirmacionSMS.entity.MensajeVerificacion;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class VerificacionService {
	 @Value("${twilio.accountSid}")
	    private String accountSid;

	    @Value("${twilio.authToken}")
	    private String authToken;

	    @Value("${twilio.messagingServiceSid}")
	    private String messagingServiceSid;

	    @Value("${twilio.phone-number}")
	    private String twilioPhoneNumber;

	    public void enviarSMS(MensajeVerificacion mensajeVerificacion) {
	    	 Twilio.init(accountSid, authToken);

		        // Enviar el mensaje SMS
		        Message message = Message.creator(
		            new PhoneNumber(mensajeVerificacion.getNumero()),
		            new PhoneNumber(twilioPhoneNumber),
		            "Tu código de verificación es: " + mensajeVerificacion.getCodigo()
		        ).create();

	    }
}

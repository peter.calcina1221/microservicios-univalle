package com.CAMEBOL.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.CodigoEmpresa;
import com.google.common.base.Optional;

@Repository
public interface CodigoEmpresaRepository extends JpaRepository<CodigoEmpresa, Integer> {
	CodigoEmpresa findByNumeroCodigo(String numeroCodigo);
}

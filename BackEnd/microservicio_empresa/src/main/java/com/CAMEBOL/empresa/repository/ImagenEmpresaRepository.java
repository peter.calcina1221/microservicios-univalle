package com.CAMEBOL.empresa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.Empresa;
import com.CAMEBOL.empresa.entity.ImagenEmpresa;


@Repository
public interface ImagenEmpresaRepository extends JpaRepository<ImagenEmpresa, Integer>  {
	List<ImagenEmpresa> findByEmpresa(Empresa empresa);
}
package com.CAMEBOL.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.Sucursal;


@Repository
public interface SucursalRepository extends JpaRepository<Sucursal, Integer> {

}

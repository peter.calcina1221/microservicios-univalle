package com.CAMEBOL.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.empresa.entity.Almacen;
import com.CAMEBOL.empresa.repository.AlmacenRepository;


@Service
@Transactional
public class AlmacenService {
	@Autowired
	AlmacenRepository almacenRepository;


	public List<Almacen> getAll(){
    	
    	return almacenRepository.findAll();
    }
	public Optional<Almacen> getById(int id){
        return almacenRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return almacenRepository.existsById(id);
    }
	public void save(Almacen almacen){
		almacenRepository.save(almacen);
    }
	public void delete(int id) {
		almacenRepository.deleteById(id);
    }	

}

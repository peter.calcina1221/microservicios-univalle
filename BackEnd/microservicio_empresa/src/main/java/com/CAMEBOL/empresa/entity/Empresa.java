package com.CAMEBOL.empresa.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Empresa")
public class Empresa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int idEmpresa;
	 private String nombreEmpresa;
	 private String numeroCelular;
	 private String latitud;
	 private String longitud;
	 private String direccion;
	 private String descripcion;
	 private String nit;
	 private String razonSocial;
	 private int idUsuario;
	 private int idOrganizacion;
	 @Transient
	 private int cantidadVentas;
	 
	 @JsonIgnoreProperties(value = {"empresas"} ,allowSetters = true)
	 @ManyToOne()
	 @JoinColumn(name = "idDepartamento")
	 private Departamento departamento;
	 
	 @JsonIgnoreProperties(value = {"empresas"} ,allowSetters = true)
	 @ManyToOne()
	 @JoinColumn(name = "idCategoriaEmpresa")
	 private CategoriaEmpresa categoriaEmpresa;
	 
	 private Date fechaRegistro;
	 private Date fechaActualizacion;
	 private String estado;
	 
	
	 
	 @JsonIgnoreProperties(value = {"empresa"} , allowSetters = true)
	 @OneToMany(mappedBy = "empresa" , cascade = CascadeType.ALL  , orphanRemoval = true)
	 private List<Sucursal> sucursales;
	 
	 
	 @JsonIgnoreProperties(value = {"empresa"} , allowSetters = true)
	 @OneToMany(mappedBy = "empresa" , cascade = CascadeType.ALL , fetch = FetchType.LAZY , orphanRemoval = true)
	 private List<ImagenEmpresa> imagenesEmpresa;
	 
	 
	public Empresa() {
		this.imagenesEmpresa= new ArrayList<>();
		this.sucursales= new ArrayList<>();
	}
	
	@PrePersist
	public void prePersist() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaRegistro=formatter.parse(fcF);
	}
	@PreUpdate
	public void preUpdate() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaActualizacion=formatter.parse(fcF);
	}
	 public List<ImagenEmpresa> getImagenesEmpresa() {
		return imagenesEmpresa;
	}


	public void setImagenesEmpresa(List<ImagenEmpresa> imagenEmpresas) {
		this.imagenesEmpresa.clear();
		imagenEmpresas.forEach(p-> this.addImagen(p));
	}
	public void addImagen(ImagenEmpresa imagenEmpresa) {
		this.imagenesEmpresa.add(imagenEmpresa);
		imagenEmpresa.setEmpresa(this);
	}


	public int getIdOrganizacion() {
		return idOrganizacion;
	}
	public void setIdOrganizacion(int idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getNumeroCelular() {
		return numeroCelular;
	}
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public CategoriaEmpresa getCategoriaEmpresa() {
		return categoriaEmpresa;
	}
	public void setCategoriaEmpresa(CategoriaEmpresa categoriaEmpresa) {
		this.categoriaEmpresa = categoriaEmpresa;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales.clear();
		sucursales.forEach(p-> this.addSucursal(p));
	}
	public void addSucursal(Sucursal sucurales) {
		this.sucursales.add(sucurales);
		sucurales.setEmpresa(this);
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getCantidadVentas() {
		return cantidadVentas;
	}

	public void setCantidadVentas(int cantidadVentas) {
		this.cantidadVentas = cantidadVentas;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}



	 
	 

}

package com.CAMEBOL.empresa.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.Empresa;
import com.CAMEBOL.empresa.entity.EmpresasVenden;
import com.CAMEBOL.empresa.repository.EmpresaRepository;

@Service
@Transactional
public class EmpresaService {
	@Autowired
	EmpresaRepository empresaRepository;
	@Autowired
    private RestTemplate restTemplate;

	public List<Empresa> getAll(){
    	
    	return empresaRepository.findAll();
    }
	public List<Empresa> getByIdOrganizacionAndEstado(int idOrganizacion,String estado){
    	
    	return empresaRepository.findByIdOrganizacionAndEstado(idOrganizacion,estado);
    }
	public List<Empresa> getByIdOrganizacionAndEstadoYFecha(int idOrganizacion,String estado,Date fechaInicio,Date fechaFin){
	
	return empresaRepository.findByIdOrganizacionAndEstadoYFecha(idOrganizacion,estado,fechaInicio,fechaFin);
}
	public Optional<Empresa> getById(int id){
        return empresaRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return empresaRepository.existsById(id);
    }
	public boolean existsByIdUsuario(Integer id){
        return empresaRepository.existsByIdUsuario(id);
    }
	public void save(Empresa empresa){
		empresaRepository.save(empresa);
    }
	public void delete(int id) {
		empresaRepository.deleteById(id);
    }
	public Empresa getEmpresaByIdEmpresa(Integer idEmpresa){
	        return empresaRepository.findByIdEmpresa(idEmpresa);
	    }
	public Empresa getEmpresaByIdUsuario(Integer idUsuairo){
        return empresaRepository.findByIdUsuario(idUsuairo);
    }
	
	public List<Empresa> getEmpresaQueMasVende(int idOrganizacion,int limit){
		  ResponseEntity<List<EmpresasVenden>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listarEmpresasQueMasVenden/"+idOrganizacion+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<EmpresasVenden>>() {}
		        );
		  
		  List<EmpresasVenden> empresasVenden=new ArrayList<>();
		  List<Empresa> empresas=new ArrayList<>();
		  empresasVenden=response.getBody();
		  Empresa empresa=new Empresa();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (EmpresasVenden EV : empresasVenden) {
		        		empresa=getById(EV.getIdEmpresa()).get();
						 empresa.setCantidadVentas(EV.getCantidad());
						 empresas.add(empresa);
					}
		        	
		        	
		            return empresas;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	}
	
	public List<Empresa> getEmpresaQueMasVendeYFecha(int idOrganizacion,Date fechaInicio,Date fechaFin,int limit){
		  ResponseEntity<List<EmpresasVenden>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listarEmpresasQueMasVendenYFecha/"+idOrganizacion+"/"+formatDate(fechaInicio)+"/"+formatDate(fechaFin)+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<EmpresasVenden>>() {}
		        );
		  
		  List<EmpresasVenden> empresasVenden=new ArrayList<>();
		  List<Empresa> empresas=new ArrayList<>();
		  empresasVenden=response.getBody();
		  Empresa empresa=new Empresa();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (EmpresasVenden EV : empresasVenden) {
		        		empresa=getById(EV.getIdEmpresa()).get();
						 empresa.setCantidadVentas(EV.getCantidad());
						 empresas.add(empresa);
					}
		        	
		        	
		            return empresas;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	}
	private String formatDate(Date date) {
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    return formatter.format(date);
	}
  
	public List<Empresa> getEmpresaQueMasCompranPorUsuario(int idUsuario,int limit){
		  ResponseEntity<List<EmpresasVenden>> response = restTemplate.exchange(
		            "http://localhost:8096/detalleVenta/listarEmpresasQueMasCompranPorUsuario/"+idUsuario+"/"+limit,
		            HttpMethod.GET,
		            null,
		            new ParameterizedTypeReference<List<EmpresasVenden>>() {}
		        );
		  
		  List<EmpresasVenden> empresasVenden=new ArrayList<>();
		  List<Empresa> empresas=new ArrayList<>();
		  empresasVenden=response.getBody();
		  Empresa empresa=new Empresa();
		        if (response.getStatusCode() == HttpStatus.OK) {
		        	for (EmpresasVenden EV : empresasVenden) {
		        		empresa=getById(EV.getIdEmpresa()).get();
						 empresa.setCantidadVentas(EV.getCantidad());
						 empresas.add(empresa);
					}
		        	
		        	
		            return empresas;
		        } else {
		            throw new RuntimeException("Error al obtener la lista desde el servicio.");
		        }
	}
}

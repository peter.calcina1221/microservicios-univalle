package com.CAMEBOL.empresa.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.Empresa;
import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.service.EmpresaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/empresa")
@CrossOrigin(origins = "*")
public class EmpresaController {
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/lista")
	public ResponseEntity<List<Empresa>> getAll() {
		try {
			List<Empresa> list = empresaService.getAll();
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacionAndEstado/{idOrganizacion}/{estado}")
	public ResponseEntity<List<Empresa>> getByIdOrganizacionAndEstado(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("estado") String estado) {
		try {
			List<Empresa> list = empresaService.getByIdOrganizacionAndEstado(idOrganizacion, estado);
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaPorIdOrganizacionAndEstadoYFecha/{idOrganizacion}/{estado}/{fechaInicio}/{fechaFin}")
	public ResponseEntity<List<Empresa>> getByIdOrganizacionAndEstadoYFecha(
			@PathVariable("idOrganizacion") int idOrganizacion, @PathVariable("estado") String estado,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin) {
		try {
			List<Empresa> list = empresaService.getByIdOrganizacionAndEstadoYFecha(idOrganizacion, estado, fechaInicio,
					fechaFin);
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaEmpresasQueMasVenden/{idOrganizacion}/{limit}")
	public ResponseEntity<List<Empresa>> getEmpresaMasVenden(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("limit") int limit) {
		try {
			List<Empresa> list = empresaService.getEmpresaQueMasVende(idOrganizacion, limit);
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaEmpresasQueMasVendenYFecha/{idOrganizacion}/{fechaInicio}/{fechaFin}/{limit}")
	public ResponseEntity<List<Empresa>> getEmpresaMasVendenYFecha(@PathVariable("idOrganizacion") int idOrganizacion,
			@PathVariable("fechaInicio") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
			@PathVariable("fechaFin") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin,
			@PathVariable("limit") int limit) {
		try {
			List<Empresa> list = empresaService.getEmpresaQueMasVendeYFecha(idOrganizacion, fechaInicio, fechaFin, limit);
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/listaEmpresasQueMasCompranPorUsuario/{idUsuario}/{limit}")
	public ResponseEntity<List<Empresa>> getEmpresaMasCompranPorUsuario(@PathVariable("idUsuario") int idUsuario,
			@PathVariable("limit") int limit) {

		try {
			List<Empresa> list = empresaService.getEmpresaQueMasCompranPorUsuario(idUsuario, limit);
			return new ResponseEntity<List<Empresa>>(list, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/obtenerEmpresaPorId/{id}")
	public ResponseEntity<Empresa> getById(@PathVariable("id") int id) {

		try {
			if (!empresaService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Empresa empresa = empresaService.getById(id).get();
			return new ResponseEntity(empresa, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/existeEmpresa/{idEmpresa}")
	public ResponseEntity<?> existe(@PathVariable("idEmpresa") int idEmpresa) {

		try {
			if (!empresaService.existsById(idEmpresa)) {
				return new ResponseEntity(false, HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity(true, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping("/nuevo")
	public ResponseEntity<?> nuevo(@Valid @RequestBody Empresa empresa, BindingResult bindingResult) {

		try {
			empresaService.save(empresa);
			creandoLog("Empresa", "Insertar", "Se inserto la empresa " + empresa.getNombreEmpresa(), "Exito");
			return new ResponseEntity(new Mensaje("Empresa creada"), HttpStatus.CREATED);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Empresa empresa) {

		try {
			Empresa emp = empresaService.getById(id).get();
			emp.setNombreEmpresa(empresa.getNombreEmpresa());
			emp.setNumeroCelular(empresa.getRazonSocial());
			emp.setRazonSocial(empresa.getRazonSocial());
			emp.setIdUsuario(empresa.getIdUsuario());
			emp.setLatitud(empresa.getLatitud());
			emp.setLongitud(empresa.getLongitud());
			emp.setDescripcion(empresa.getDescripcion());
			emp.setNumeroCelular(empresa.getNumeroCelular());
			emp.setEstado(empresa.getEstado());
			emp.setCategoriaEmpresa(empresa.getCategoriaEmpresa());
			emp.setIdOrganizacion(empresa.getIdOrganizacion());
			emp.setImagenesEmpresa(empresa.getImagenesEmpresa());
			emp.setCategoriaEmpresa(empresa.getCategoriaEmpresa());
			emp.setFechaRegistro(empresa.getFechaRegistro());
			emp.setSucursales(empresa.getSucursales());
			emp.setDireccion(empresa.getDireccion());
			emp.setNit(empresa.getNit());
			empresaService.save(emp);
			creandoLog("Empresa", "Actualizar", "Se actualizo la empresa " + id, "Exito");
			return new ResponseEntity(new Mensaje("Empresa Actualizada"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping("/eliminarActivar/{id}/{estado}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @PathVariable("estado") String estado) {
		try {
			Empresa emp = empresaService.getById(id).get();
			emp.setEstado(estado);
			empresaService.save(emp);
			creandoLog("Empresa", "Actualizar", "Se actualizo la empresa " + id, "Exito");
			return new ResponseEntity(new Mensaje("Empresa Actualizada"), HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/empresaPorId/{id}")
	public ResponseEntity<Empresa> getEmpresaId(@PathVariable("id") int id) {
		try {
			if (!empresaService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Empresa empresa = empresaService.getEmpresaByIdEmpresa(id);
			return new ResponseEntity(empresa, HttpStatus.OK);

		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/empresaPorIdUsuario/{id}")
	public ResponseEntity<Empresa> getEmpresaIdUsuario(@PathVariable("id") int id) {
		try {
			if (!empresaService.existsByIdUsuario(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"), HttpStatus.NOT_FOUND);
			}
			Empresa empresa = empresaService.getEmpresaByIdUsuario(id);
			return new ResponseEntity(empresa, HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa", "Excepcion", e.getMessage(), "Error");
			return new ResponseEntity(new Mensaje(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	public void creandoLog(String ms, String accion, String descripcion, String estado) {
		try {
			// URL de la API
			String apiUrl = "http://localhost:8101/log/nuevo";

			// Configura la cabecera
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			// Crea un objeto Java y conviértelo a JSON
			Map<String, String> requestBody = new HashMap<>();
			requestBody.put("ms", ms);
			requestBody.put("accion", accion);
			requestBody.put("descripcion", descripcion);
			requestBody.put("estado", estado);
			String json = new ObjectMapper().writeValueAsString(requestBody);

			// Configura la solicitud con el JSON en el cuerpo y las cabeceras
			HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

			// Realiza la solicitud
			ResponseEntity<String> responseEntity = restTemplate.exchange(
					apiUrl,
					HttpMethod.POST,
					requestEntity,
					String.class);

			String responseBody = responseEntity.getBody();

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}

package com.CAMEBOL.empresa.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.boot.jaxb.hbm.spi.PluralAttributeInfoPrimitiveArrayAdapter;
@Entity
@Table(name="CodigoEmpresa")
public class CodigoEmpresa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int idCodigo;
	 private String numeroCodigo;
	 private int idOrganizacion;
	 private int idUsuario;
	 private Date fechaRegistro;
	 private Date fechaActualizacion;
	 private String estado;
	 
	 
	public CodigoEmpresa() {
	
	}
	@PrePersist
	public void prePersist() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaRegistro=formatter.parse(fcF);
	}
	@PreUpdate
	public void preUpdate() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaActualizacion=formatter.parse(fcF);
	}
	 public int getIdOrganizacion() {
		return idOrganizacion;
	}
	public void setIdOrganizacion(int idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}
	public int getIdCodigo() {
		return idCodigo;
	}
	public void setIdCodigo(int idCodigo) {
		this.idCodigo = idCodigo;
	}
	public String getNumeroCodigo() {
		return numeroCodigo;
	}
	public void setNumeroCodigo(String numeroCodigo) {
		this.numeroCodigo = numeroCodigo;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	 
	 
}

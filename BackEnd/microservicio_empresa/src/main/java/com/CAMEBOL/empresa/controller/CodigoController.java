package com.CAMEBOL.empresa.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.CodigoEmpresa;
import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.service.CodigoEmpresaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.common.base.Optional;

@RestController
@RequestMapping("/codigo")
@CrossOrigin(origins = "*")
public class CodigoController {
	@Autowired
	private CodigoEmpresaService codigoEmpresaService;
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/lista")
	public ResponseEntity<List<CodigoEmpresa>> getAll(){
		try {
			List<CodigoEmpresa> list = codigoEmpresaService.getAll();
			return  new ResponseEntity<List<CodigoEmpresa>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<CodigoEmpresa> getById(@PathVariable("id") int id){
		try {
			if(!codigoEmpresaService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
			}
			CodigoEmpresa codEmpresa=codigoEmpresaService.getById(id).get();
			return new ResponseEntity(codEmpresa,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody CodigoEmpresa codigoEmpresa, BindingResult bindingResult){
		try {
			CodigoEmpresa codig=codigoEmpresaService.getBynumeroCodigo(codigoEmpresa.getNumeroCodigo());
			if(codig != null) {
				creandoLog("Empresa","Insertar","El codigo que se intenta insertar ya existe","Error");
				return new ResponseEntity(new Mensaje("El codigo ya existe"),HttpStatus.CREATED);
		    }
			else {
				codigoEmpresaService.save(codigoEmpresa);
				creandoLog("Empresa","Insertar","Se registro un nuevo codigo de empresa "+codigoEmpresa,"Exito");
				return new ResponseEntity(new Mensaje("Codigo creado con exito"),HttpStatus.CREATED);
			}
			
		}
		catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
    }
	@PostMapping("/verificar/{numeroCodigo}")
    public ResponseEntity<?> verificar(@PathVariable("numeroCodigo") String numeroCodigo){
	
		try {
			CodigoEmpresa codigoEmpresa=codigoEmpresaService.getBynumeroCodigo(numeroCodigo);
			
			 if (codigoEmpresa != null && "1".equals(codigoEmpresa.getEstado())) {
			        return new ResponseEntity<>(true, HttpStatus.CREATED);
			    } else {
			        return new ResponseEntity<>(false, HttpStatus.CREATED);
			    }
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
        
		
	@PutMapping("/actualizar/{numeroCodigo}")
	public ResponseEntity<?> actualizar(@PathVariable("numeroCodigo") String numeroCodigo, @RequestBody CodigoEmpresa codigoEmpresa){

		try {
			CodigoEmpresa codEmp=codigoEmpresaService.getBynumeroCodigo(numeroCodigo);
			codEmp.setNumeroCodigo(codigoEmpresa.getNumeroCodigo());
			codEmp.setIdUsuario(codigoEmpresa.getIdUsuario());
			codEmp.setIdOrganizacion(codigoEmpresa.getIdOrganizacion());
			codEmp.setEstado(codigoEmpresa.getEstado());
			
			codigoEmpresaService.save(codEmp);
			creandoLog("Empresa","Actualizar","Se actualizo el codigo "+numeroCodigo,"Exito");
			return new ResponseEntity(new Mensaje("Codigo Actualizado"),HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		codigoEmpresaService.delete(id);
		return new ResponseEntity(new Mensaje("Codigo Eliminado"),HttpStatus.OK);
	}
	
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

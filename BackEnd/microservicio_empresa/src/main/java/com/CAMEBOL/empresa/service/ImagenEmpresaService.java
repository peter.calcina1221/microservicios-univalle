package com.CAMEBOL.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.empresa.entity.Empresa;
import com.CAMEBOL.empresa.entity.ImagenEmpresa;
import com.CAMEBOL.empresa.repository.ImagenEmpresaRepository;

@Service
@Transactional
public class ImagenEmpresaService {
	@Autowired
	ImagenEmpresaRepository imagenEmpresaRepository;


	public List<ImagenEmpresa> getAll(){
    	
    	return imagenEmpresaRepository.findAll();
    }
	public List<ImagenEmpresa> getByIdEmpresa(Empresa empresa){
    	
    	return imagenEmpresaRepository.findByEmpresa(empresa);
    }
	public Optional<ImagenEmpresa> getById(int id){
        return imagenEmpresaRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return imagenEmpresaRepository.existsById(id);
    }
	public void save(ImagenEmpresa imagenEmpresa){
		imagenEmpresaRepository.save(imagenEmpresa);
    }
	public void delete(int id) {
		imagenEmpresaRepository.deleteById(id);
    }	

}

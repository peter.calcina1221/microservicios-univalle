package com.CAMEBOL.empresa.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.CategoriaEmpresa;
import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.service.CategoriaEmpresaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/categoriaEmpresa")
@CrossOrigin(origins = "*")
public class CategoriaController {
	@Autowired
	private CategoriaEmpresaService categoriaEmpresaService;
	@Autowired
	private RestTemplate restTemplate;
	@GetMapping("/lista")
	public ResponseEntity<List<CategoriaEmpresa>> getAll(){
		try {
			List<CategoriaEmpresa> list = categoriaEmpresaService.getAll();
			return  new ResponseEntity<List<CategoriaEmpresa>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaIdOrganizacion/{idOrganizacion}")
	public ResponseEntity<List<CategoriaEmpresa>> getbyIdOrganizacion(@PathVariable("idOrganizacion") int idOrganizacion){
		try {
			List<CategoriaEmpresa> list = categoriaEmpresaService.getByIdOrganizacion(idOrganizacion);
			return  new ResponseEntity<List<CategoriaEmpresa>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaIdOrganizacionAndEstado/{idOrganizacion}/{estado}")
	public ResponseEntity<List<CategoriaEmpresa>> getbyIdOrganizacionAndEstado(@PathVariable("idOrganizacion") int idOrganizacion,@PathVariable("estado") String estado){
		try {
			List<CategoriaEmpresa> list = categoriaEmpresaService.getByIdOrganizacionAndEstado(idOrganizacion,estado);
			return  new ResponseEntity<List<CategoriaEmpresa>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/listaId/{id}")
	public ResponseEntity<CategoriaEmpresa> getById(@PathVariable("id") int id){
		try {
			if(!categoriaEmpresaService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
			}
			CategoriaEmpresa catEmpresa=categoriaEmpresaService.getById(id).get();
			return new ResponseEntity(catEmpresa,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody CategoriaEmpresa categoriaEmpresa, BindingResult bindingResult){
	
		try {
			categoriaEmpresaService.save(categoriaEmpresa);
			creandoLog("Empresa","Insertar","Se registro una categoria de la organizacion "+categoriaEmpresa.getIdOrganizacion(),"Exito");
			return new ResponseEntity(new Mensaje("Categoria creada"),HttpStatus.CREATED);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody CategoriaEmpresa categoriaEmpresa){

		try {
			CategoriaEmpresa catEmp=categoriaEmpresaService.getById(id).get();
			catEmp.setNombre(categoriaEmpresa.getNombre());
			catEmp.setDescripcion(categoriaEmpresa.getDescripcion());
			catEmp.setIdOrganizacion(categoriaEmpresa.getIdOrganizacion());
			catEmp.setEmpresas(categoriaEmpresa.getEmpresas());
			catEmp.setFechaRegistro(categoriaEmpresa.getFechaRegistro());
			catEmp.setEstado(categoriaEmpresa.getEstado());
			categoriaEmpresaService.save(catEmp);
			creandoLog("Empresa","Actualizar","Se actualizo la categoria  "+id,"Exito");
			return new ResponseEntity(new Mensaje("Categoria Actualizada"),HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}	
		
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		categoriaEmpresaService.delete(id);
		return new ResponseEntity(new Mensaje("Categoria Eliminada"),HttpStatus.OK);
	}
	
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

package com.CAMEBOL.empresa.entity;

public class EmpresasVenden {
	private int idEmpresa;
	private int cantidad;
	public EmpresasVenden() {
		super();
	}
	
	public EmpresasVenden(int idEmpresa, int cantidad) {
		super();
		this.idEmpresa = idEmpresa;
		this.cantidad = cantidad;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}

package com.CAMEBOL.empresa.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.Almacen;
import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.service.AlmacenService;
import com.fasterxml.jackson.databind.ObjectMapper;



@RestController
@RequestMapping("/almacen")
@CrossOrigin(origins = "*")
public class AlmacenController {
	@Autowired
	private AlmacenService almacenService;
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Almacen>> getAll(){
		try {
			List<Almacen> list = almacenService.getAll();
			return  new ResponseEntity<List<Almacen>>(list,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Almacen> getById(@PathVariable("id") int id){
		try {
			if(!almacenService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
			}
			Almacen almacen=almacenService.getById(id).get();
			return new ResponseEntity(almacen,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Almacen almacen, BindingResult bindingResult){
	
		try {
			almacenService.save(almacen);
			creandoLog("Empresa","Insertar","Se registro un almacen de la sucursal "+almacen.getSucursal(),"Exito");
			return new ResponseEntity(new Mensaje("Almacen creado"),HttpStatus.CREATED);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Almacen almacen){

		try {
			
			Almacen alm=almacenService.getById(id).get();
			alm.setNombre(almacen.getNombre());
			alm.setSucursal(almacen.getSucursal());
			alm.setIdUsuario(almacen.getIdUsuario());
			alm.setEstado(almacen.getEstado());
			alm.setSucursal(almacen.getSucursal());
			alm.setFechaRegistro(almacen.getFechaRegistro());
			almacenService.save(alm);
			creandoLog("Empresa","Actualizar","Se actualizo el almacen  "+id,"Exito");
			return new ResponseEntity(new Mensaje("Almacen Actualizado"),HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		almacenService.delete(id);
		return new ResponseEntity(new Mensaje("Almacen Eliminada"),HttpStatus.OK);
	}
	
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

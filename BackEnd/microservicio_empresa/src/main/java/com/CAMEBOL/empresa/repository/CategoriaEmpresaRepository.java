package com.CAMEBOL.empresa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.CategoriaEmpresa;


@Repository
public interface CategoriaEmpresaRepository extends JpaRepository<CategoriaEmpresa, Integer> {
	List<CategoriaEmpresa>findByIdOrganizacion(int idOrganizacion);
	List<CategoriaEmpresa>findByIdOrganizacionAndEstado(int idOrganizacion,String estado);
}

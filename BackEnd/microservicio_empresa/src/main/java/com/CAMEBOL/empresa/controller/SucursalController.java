package com.CAMEBOL.empresa.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.entity.Sucursal;
import com.CAMEBOL.empresa.service.SucursalService;


@RestController
@RequestMapping("/sucursal")
@CrossOrigin(origins = "*")
public class SucursalController {
	@Autowired
	private SucursalService sucursalService;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Sucursal>> getAll(){
		List<Sucursal> list = sucursalService.getAll();
		return  new ResponseEntity<List<Sucursal>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Sucursal> getById(@PathVariable("id") int id){
		if(!sucursalService.existsById(id)) {
			return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
		}
		Sucursal sucursal=sucursalService.getById(id).get();
		return new ResponseEntity(sucursal,HttpStatus.OK);
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Sucursal sucursal, BindingResult bindingResult){
	
		sucursalService.save(sucursal);
        
		return new ResponseEntity(new Mensaje("Sucursal creada"),HttpStatus.CREATED);
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Sucursal sucursal){

			
		Sucursal suc=sucursalService.getById(id).get();
		suc.setNombre(sucursal.getNombre());
		suc.setDireccion(sucursal.getDireccion());
		suc.setIdUsuario(sucursal.getIdUsuario());
		suc.setLatitud(sucursal.getLatitud());
		suc.setLongitud(sucursal.getLongitud());
		suc.setEmpresa(sucursal.getEmpresa());
		suc.setDepartamento(sucursal.getDepartamento());
		suc.setAlmacenes(sucursal.getAlmacenes());
		suc.setEstado(sucursal.getEstado());
		
		// faltan actualizar las fechas 
		sucursalService.save(suc);
		return new ResponseEntity(new Mensaje("Sucursal Actualizada"),HttpStatus.OK);
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		sucursalService.delete(id);
		return new ResponseEntity(new Mensaje("Sucursal Eliminada"),HttpStatus.OK);
	}
}

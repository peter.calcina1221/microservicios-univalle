package com.CAMEBOL.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.empresa.entity.Sucursal;
import com.CAMEBOL.empresa.repository.SucursalRepository;


@Service
@Transactional
public class SucursalService {
	@Autowired
	SucursalRepository sucursalRepository;


	public List<Sucursal> getAll(){
    	
    	return sucursalRepository.findAll();
    }
	public Optional<Sucursal> getById(int id){
        return sucursalRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return sucursalRepository.existsById(id);
    }
	public void save(Sucursal sucursal){
		sucursalRepository.save(sucursal);
    }
	public void delete(int id) {
		sucursalRepository.deleteById(id);
    }	


}

package com.CAMEBOL.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.empresa.entity.CodigoEmpresa;
import com.CAMEBOL.empresa.repository.CodigoEmpresaRepository;



@Service
@Transactional
public class CodigoEmpresaService {
	@Autowired
	CodigoEmpresaRepository codigoEmpresaRepository;


	public List<CodigoEmpresa> getAll(){
    	
    	return codigoEmpresaRepository.findAll();
    }
	public Optional<CodigoEmpresa> getById(int id){
        return codigoEmpresaRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return codigoEmpresaRepository.existsById(id);
    }
	public void save(CodigoEmpresa codigoEmpresa){
		codigoEmpresaRepository.save(codigoEmpresa);
    }
	public void delete(int id) {
		codigoEmpresaRepository.deleteById(id);
    }	
	public CodigoEmpresa getBynumeroCodigo(String numeroCodigo) {
	
      return codigoEmpresaRepository.findByNumeroCodigo(numeroCodigo);  
    }
}

package com.CAMEBOL.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.Departamento;


@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, Integer> {

}

package com.CAMEBOL.empresa.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="CategoriaEmpresa")
public class CategoriaEmpresa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int idCategoriaEmpresa;
	 private String nombre;
	 private String descripcion;
	 private int idOrganizacion;
	 private Date fechaRegistro;
	 private Date fechaActualizacion;
	 private String estado;
	 
	
	 @JsonIgnoreProperties(value = {"categoriaEmpresa"} , allowSetters = true)
	 @OneToMany(mappedBy = "categoriaEmpresa" , cascade = CascadeType.ALL  , orphanRemoval = true)
	 private List<Empresa> empresas;
	 
	 
	public CategoriaEmpresa() {
		this.empresas= new ArrayList<>();
	}
	@PrePersist
	public void prePersist() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaRegistro=formatter.parse(fcF);
	}
	@PreUpdate
	public void preUpdate() throws ParseException {
		
		Date dt= new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String fcF=formatter.format(dt);
		this.fechaActualizacion=formatter.parse(fcF);
	}
	
	public CategoriaEmpresa(int idCategoriaEmpresa, String nombre) {
		super();
		this.idCategoriaEmpresa = idCategoriaEmpresa;
		this.nombre = nombre;
	}

	public int getIdCategoriaEmpresa() {
		return idCategoriaEmpresa;
	}
	public void setIdCategoriaEmpresa(int idCategoriaEmpresa) {
		this.idCategoriaEmpresa = idCategoriaEmpresa;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getIdOrganizacion() {
		return idOrganizacion;
	}
	public void setIdOrganizacion(int idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}
	public List<Empresa> getEmpresas() {
		return empresas;
	}
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas.clear();
		empresas.forEach(p-> this.addEmpresas(p));
	}
	public void addEmpresas(Empresa empresa) {
		this.empresas.add(empresa);
		empresa.setCategoriaEmpresa(this);
	}



	
	 

}

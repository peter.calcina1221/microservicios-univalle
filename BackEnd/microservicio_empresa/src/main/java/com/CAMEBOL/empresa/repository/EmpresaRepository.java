package com.CAMEBOL.empresa.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.Empresa;


@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {
		Empresa findByIdEmpresa(Integer idEmpresa);
		Empresa findByIdUsuario(Integer idUsuario);
		Boolean existsByIdUsuario(Integer idUsuario);
		List<Empresa> findByIdOrganizacionAndEstado(int idOrganizacion,String estado);
		
		@Query("SELECT e FROM com.CAMEBOL.empresa.entity.Empresa e WHERE e.idOrganizacion= :idOrganizacion AND e.estado= :estado AND DATE (e.fechaRegistro) BETWEEN :fechaInicio AND :fechaFin")
		List<Empresa> findByIdOrganizacionAndEstadoYFecha(@Param("idOrganizacion") int idOrganizacion,@Param("estado") String estado, @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);
		
}

package com.CAMEBOL.empresa.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.CAMEBOL.empresa.entity.Departamento;
import com.CAMEBOL.empresa.entity.Mensaje;
import com.CAMEBOL.empresa.service.DepartamentoService;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping("/departamento")
@CrossOrigin(origins = "*")
public class DepartamentoController {
	@Autowired
	private DepartamentoService departamentoService;
	@Autowired
	private RestTemplate restTemplate;
	@GetMapping("/lista")
	public ResponseEntity<List<Departamento>> getAll(){
		try {
			List<Departamento> list = departamentoService.getAll();
			return  new ResponseEntity<List<Departamento>>(list,HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Departamento> getById(@PathVariable("id") int id){
		try {
			if(!departamentoService.existsById(id)) {
				return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
			}
			Departamento depar=departamentoService.getById(id).get();
			return new ResponseEntity(depar,HttpStatus.OK);
			
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
	}
	@PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody Departamento departamento, BindingResult bindingResult){
	
		try {
			departamentoService.save(departamento);
			creandoLog("Empresa","Insertar","Se inserto el departamento "+departamento.getNombre(),"Exito");
			return new ResponseEntity(new Mensaje("Departamento Creado"),HttpStatus.CREATED);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
		
    }
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Departamento departamento){
		
		try {
			Departamento depar=departamentoService.getById(id).get();
			depar.setNombre(departamento.getNombre());
			depar.setEstado(departamento.getEstado());
			depar.setSucursales(departamento.getSucursales());
			depar.setFechaRegistro(departamento.getFechaRegistro());
			departamentoService.save(depar);
			creandoLog("Empresa","Actualizar","Se actualizo el departamento "+id,"Exito");
			return new ResponseEntity(new Mensaje("Departamento Actualizado"),HttpStatus.OK);
		} catch (Exception e) {
			creandoLog("Empresa","Excepcion",e.getMessage(),"Error");
			return new ResponseEntity(new Mensaje(e.getMessage()),HttpStatus.NOT_FOUND);
		}
			
		
	
	}	
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		departamentoService.delete(id);
		return new ResponseEntity(new Mensaje("Departamento Eliminado"),HttpStatus.OK);
	}
	public void creandoLog(String ms,String accion, String descripcion,String estado) {
		try {
			 // URL de la API
		    String apiUrl = "http://localhost:8101/log/nuevo";

		    // Configura la cabecera
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);

		    // Crea un objeto Java y conviértelo a JSON
		    Map<String, String> requestBody = new HashMap<>();
		    requestBody.put("ms", ms);
		    requestBody.put("accion", accion);
		    requestBody.put("descripcion", descripcion);
		    requestBody.put("estado", estado);
		    String json = new ObjectMapper().writeValueAsString(requestBody);

		    // Configura la solicitud con el JSON en el cuerpo y las cabeceras
		    HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

		    // Realiza la solicitud
		    ResponseEntity<String> responseEntity = restTemplate.exchange(
		        apiUrl,
		        HttpMethod.POST,
		        requestEntity,
		        String.class
		    );

		    // Puedes manejar la respuesta según sea necesario
		    String responseBody = responseEntity.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}

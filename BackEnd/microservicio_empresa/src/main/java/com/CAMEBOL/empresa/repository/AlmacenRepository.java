package com.CAMEBOL.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CAMEBOL.empresa.entity.Almacen;


@Repository
public interface AlmacenRepository extends JpaRepository<Almacen, Integer>{

}

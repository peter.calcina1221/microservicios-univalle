package com.CAMEBOL.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOL.empresa.entity.CategoriaEmpresa;
import com.CAMEBOL.empresa.repository.CategoriaEmpresaRepository;


@Service
@Transactional
public class CategoriaEmpresaService {
	@Autowired
	CategoriaEmpresaRepository categoriaEmpresaRepository;


	public List<CategoriaEmpresa> getAll(){
    	
    	return categoriaEmpresaRepository.findAll();
    }
	public List<CategoriaEmpresa> getByIdOrganizacion(int idOrganizacion){
    	
    	return categoriaEmpresaRepository.findByIdOrganizacion(idOrganizacion);
    }
	public List<CategoriaEmpresa> getByIdOrganizacionAndEstado(int idOrganizacion,String estado){
    	
    	return categoriaEmpresaRepository.findByIdOrganizacionAndEstado(idOrganizacion,estado);
    }
	public Optional<CategoriaEmpresa> getById(int id){
        return categoriaEmpresaRepository.findById(id);
    }
	public boolean existsById(Integer id){
        return categoriaEmpresaRepository.existsById(id);
    }
	public void save(CategoriaEmpresa categoriaEmpresa){
		categoriaEmpresaRepository.save(categoriaEmpresa);
    }
	public void delete(int id) {
		categoriaEmpresaRepository.deleteById(id);
    }	

}

const urls = {
  usuarioService:[
    'http://localhost:8095/usuario/',
  ],
  ventaService: [
    'http://localhost:8096/venta/',
    'http://localhost:8096/factura/',
    'http://localhost:8096/detalleVenta/'
  ],
  reservaService: [
    'http://localhost:8099/reserva/',
    'http://localhost:8099/detalleReserva/'
  ],
  empresaService: [
    'http://localhost:8091/empresa/',
    'http://localhost:8091/categoriaEmpresa/'
  ],
  producoService: [
    'http://localhost:8094/producto/',
    'http://localhost:8094/categoriaProducto/'
  ],
  calificacionService: [
    'http://localhost:8090/calificacion/'
  ],
}

export default urls;
import { useEffect, useState } from 'react';
import { Chart } from 'primereact/chart';
import { ServicioReportes } from '../services/ServicioReportes';
import { ServicioProducto } from '../services/ServicioProducto';

export function ReporteVentasCategoria() {
  const [chartDataVenta, setChartDataVenta] = useState({});
  const [chartOptionsVenta, setChartOptionsVenta] = useState({});
  const [labels, setLabels] = useState([]);
  const servicioReportes = new ServicioReportes();
  const servicioProductos = new ServicioProducto();
  const documentStyle = getComputedStyle(document.documentElement);

  useEffect(() => {
    servicioProductos.obtenerCategoriaProducto().then(categorias => {
      setLabels(categorias.map(categoria => categoria.nombre));
    });
  }, []);

  useEffect(() => {
    servicioReportes.obtenerVentasRegistradas().then(ventas => {
      let categorias = Array(labels.length).fill(0);
  
      Promise.all(
        ventas.map(venta =>
          Promise.all(
            venta.detalleVentas.map(detalleVenta =>
              servicioProductos.obtenerProductoPorId(detalleVenta.idProducto).then(producto => {
                const categoriaProducto = producto.categoriaProducto.nombre;
                const index = labels.indexOf(categoriaProducto);
  
                if (index !== -1) {
                  categorias[index] += 1;
                }
              })
            )
          )
        )
      ).then(() => {
        const data = {
          labels: labels,
          datasets: [
            {
              label: 'Ventas: ',
              data: categorias,
              backgroundColor: [
                documentStyle.getPropertyValue('--blue-500'),
                documentStyle.getPropertyValue('--yellow-500'),
                documentStyle.getPropertyValue('--green-500'),
                documentStyle.getPropertyValue('--red-500'),
                documentStyle.getPropertyValue('--purple-500'),
                documentStyle.getPropertyValue('--orange-500'),
                documentStyle.getPropertyValue('--indigo-500'),
              ],
              hoverBackgroundColor: [
                documentStyle.getPropertyValue('--blue-400'),
                documentStyle.getPropertyValue('--yellow-400'),
                documentStyle.getPropertyValue('--green-400'),
                documentStyle.getPropertyValue('--red-400'),
                documentStyle.getPropertyValue('--purple-400'),
                documentStyle.getPropertyValue('--orange-400'),
                documentStyle.getPropertyValue('--indigo-400'),
              ]
            }
          ]
        }
  
        const options = {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        };
  
        setChartDataVenta(data);
        setChartOptionsVenta(options);
      });
    });
  }, [labels]);
  

  return (
    <Chart type="bar" data={chartDataVenta} options={chartOptionsVenta} class="chart_size" />
  );
}
import { useEffect, useState } from 'react';
import { Chart } from 'primereact/chart';
import { ServicioReportes } from '../services/ServicioReportes';

export function ReporteReservasTotales() {
  const [chartDataReserva, setChartDataReserva] = useState({});
  const [chartOptionsReserva, setChartOptionsReserva] = useState({});
  const servicioReportes = new ServicioReportes();
  const documentStyle = getComputedStyle(document.documentElement);

  useEffect(() => {
    servicioReportes.obtenerReservasRegistradas().then(reservas => {
      let estadoEnProceso = 0;
      let estadoConfirmado = 0;
      let estadoRechazado = 0;
      let estadoListoParaEntregar = 0;
      let estadoEntregado = 0;
      //Recorrer las reservas
      reservas.forEach(reserva => {
        reserva.detalleReservas.forEach(detalleReserva => {
          detalleReserva.estado === '1' ? estadoEnProceso++ :
            detalleReserva.estado === '2' ? estadoConfirmado++ :
              detalleReserva.estado === '3' ? estadoRechazado++ :
                detalleReserva.estado === '4' ? estadoListoParaEntregar++ : estadoEntregado++;
        });
      });

      const data = {
        labels: ['Entregado', 'En proceso', 'Listo para entregar', 'Confirmado', 'Rechazado'],
        datasets: [
          {
            data: [estadoEntregado, estadoEnProceso, estadoListoParaEntregar, estadoConfirmado, estadoRechazado],
            backgroundColor: [
              documentStyle.getPropertyValue('--blue-500'),
              documentStyle.getPropertyValue('--yellow-500'),
              documentStyle.getPropertyValue('--green-500'),
              documentStyle.getPropertyValue('--orange-500'),
              documentStyle.getPropertyValue('--red-500'),
            ],
            hoverBackgroundColor: [
              documentStyle.getPropertyValue('--blue-400'),
              documentStyle.getPropertyValue('--yellow-400'),
              documentStyle.getPropertyValue('--green-400'),
              documentStyle.getPropertyValue('--orange-400'),
              documentStyle.getPropertyValue('--red-400'),
            ]
          }
        ]
      }
      const options = {
        cutout: '60%',
        plugins: {
          legend: {
            labels: {
              usePointStyle: true
            }
          }
        }
      };

      setChartDataReserva(data);
      setChartOptionsReserva(options);
    });
  }, []);

  return (
    <div className='chart_class'>
      <Chart type="doughnut" data={chartDataReserva} options={chartOptionsReserva} className="w-full md:w-30rem" />
    </div>
  );
}
import { useEffect, useState } from 'react';
import { Chart } from 'primereact/chart';
import { ServicioReportes } from '../services/ServicioReportes';

export function ReporteVentasTotales() {
  const [chartDataVenta, setChartDataVenta] = useState({});
  const [chartOptionsVenta, setChartOptionsVenta] = useState({});
  const servicioReportes = new ServicioReportes();
  const documentStyle = getComputedStyle(document.documentElement);

  useEffect(() => {
    servicioReportes.obtenerVentasRegistradas().then(ventas => {
      let estadoEntregado = 0;
      let estadoEnProceso = 0;
      let estadoListoParaEntregar = 0;
      //Recorrer las ventas
      ventas.forEach(venta => {
        venta.detalleVentas.forEach(detalleVenta => {
          detalleVenta.estado === '1' ? estadoEnProceso++ : detalleVenta.estado === '2' ? estadoListoParaEntregar++ : estadoEntregado++;
        });
      });

      const data = {
        labels: ['Entregado', 'En proceso', 'Listo para entregar'],
        datasets: [
          {
            data: [estadoEntregado, estadoEnProceso, estadoListoParaEntregar],
            backgroundColor: [
              documentStyle.getPropertyValue('--blue-500'),
              documentStyle.getPropertyValue('--yellow-500'),
              documentStyle.getPropertyValue('--green-500'),
            ],
            hoverBackgroundColor: [
              documentStyle.getPropertyValue('--blue-400'),
              documentStyle.getPropertyValue('--yellow-400'),
              documentStyle.getPropertyValue('--green-400')
            ]
          }
        ]
      }
      const options = {
        plugins: {
          legend: {
            labels: {
              usePointStyle: true
            }
          }
        }
      };

      setChartDataVenta(data);
      setChartOptionsVenta(options);
    });
  }, []);

  return (
    <div className='chart_class'>
      <Chart type="pie" data={chartDataVenta} options={chartOptionsVenta} className="w-full md:w-30rem" />
    </div>
  );
}
import React, { useState, useEffect, useRef } from 'react'
import classNames from 'classnames'
import { BrowserRouter } from 'react-router-dom'

/* IMPORTACIONES DE LOS COMPONENTES */
import { TopBar } from './TopBar'
import { Footer } from './Footer'
import { Menu } from './MenuAdministrador'
/* --- FIN DE LAS IMPORTACIONES DE LOS COMPONENTES --- */

/* IMPORTACION DE LAS RUTAS */
import { Rutas } from '../../../routers/routes'

/* IMPORTACION DE ESTILOS */
import PrimeReact from 'primereact/api'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'
import '/node_modules/primeflex/primeflex.css'
import '../style/Administrador.css'

const Administrador = () => {

  const [menuAbierto, setMenuAbierto] = useState(true)

  return (
    <div className='body'>
      <BrowserRouter>
        <div className={`container ${menuAbierto ? 'estado-menu activo' : 'estado-menu'}`}>
          <Menu menuAbierto={menuAbierto} setMenuAbierto={setMenuAbierto} />
          <div className="content">
            <TopBar />
            <Rutas />
          </div>
        </div>
      </BrowserRouter >
    </div >
  );

}

export default Administrador;

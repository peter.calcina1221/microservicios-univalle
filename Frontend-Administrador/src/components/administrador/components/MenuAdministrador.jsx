import { NavLink } from 'react-router-dom'
import { Divider } from 'primereact/divider';

import '../style/MenuAdministrador.css'
import logo from '../../../assets/images/logo.png'

export function Menu({ menuAbierto, setMenuAbierto }) {

  const ModMenuAbierto = () => {
    setMenuAbierto(!menuAbierto);
  }

  const paginas = [
    { label: 'Reportes', icon: 'pi pi-fw pi-user', to: '/reporte' },
    { label: 'Descargar reportes', icon: 'pi pi-fw pi-download', to: '/descargarReportes' },
    { label: 'Agregar administrador', icon: 'pi pi-fw pi-user-plus', to: '/agregarAdministrador' },
    { label: 'Lista de usuarios', icon: 'pi pi-fw pi-history', to: '/listaUsuarios' },
    { label: 'Lista de emprendimientos', icon: 'pi pi-fw pi-users', to: '/listaEmprendimientos' },
    { label: 'Configuraciones', icon: 'pi pi-fw pi-table', to: '/configuraciones' },
    { label: 'Facturas', icon: 'pi pi-fw pi-file', to: '/facturas' },
    { label: 'Cerrar sesión', icon: 'pi pi-fw pi-sign-out', to: '/logout' },
  ];

  return (
    <div className='contenedor-menu' menuAbierto={menuAbierto}>
      <button className={`boton-menu ${menuAbierto ? 'rotar-boton' : 'normal'}`} onClick={ModMenuAbierto} title='Colapsar'>
        <i className='pi pi-angle-left'></i>
      </button>
      <div className='logo'>
        <div className='img-contenedor'>
          <img src={logo} alt='Logo Camebol' />
        </div>
      </div>
      <Divider />
      {
        paginas.map(({ label, icon, to }) => (
          <NavLink key={label} to={to} className={({ isActive }) => `enlace${isActive ? ' activo' : ''}`} >
            <i className={icon} />
            {menuAbierto && (<span>{label}</span>)}
          </NavLink>
        ))
      }
    </div>
  );
}
import React, { useEffect, useState } from 'react';
import { decodeToken } from 'react-jwt';
import { ServicioUsuarios } from '../services/ServicioUsuarios';
import '../style/TopBar.css'

export function TopBar() {
  const token = sessionStorage.getItem('token');
  const decodedToken = decodeToken(token);
  const servicioUsuario = new ServicioUsuarios();
  const [usuario, setUsuario] = useState('');
  const [foto, setFoto] = useState('');

  useEffect(() => {
    servicioUsuario.obtenerUsuarioPorId(decodedToken.idUsuario).then(data => {
      setUsuario(data.nombre + ' ' + data.primerApellido + ' ' + data.segundoApellido);
      setFoto(data.imagen);
    }).catch(error => {
      console.log(error.message)
    });
  }, [])

  return (
    <div className="layout-topbar">
      <div className="layout-topbar-photo">
        <span>{usuario}</span>
        <img src={foto} alt="Foto de perfil" id='foto' />
      </div>
    </div>
  );
}

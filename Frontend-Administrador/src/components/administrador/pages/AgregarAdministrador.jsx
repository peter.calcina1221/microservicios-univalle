import React, { useState, useEffect } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { Password } from 'primereact/password';
import { GeneralUtils } from '../../../util/GeneralUtils';
import { ServicioUsuarios } from '../services/ServicioUsuarios';
import { useToast } from '../../../provider/ToastContext';
import '../style/AgregarAdministrador.css'

export function AgregarAdministrador() {
  let usuarioVacio = {
    id: 0,
    idOrganizacion: '1',
    idEmpresa: '0',
    nombre: '',
    primerApellido: '',
    segundoApellido: '',
    ci: '',
    imagen: 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Usuarios_Image%2F9a75bead3057213ff4647a8a508c9daf1c763715616f79aeeeea076f0a6d1046.jfif?alt=media&token=dd0d5749-9f6e-4a9b-8bfa-3aac9a85a909',
    fechaNacimiento: '',
    numeroTelefono: '',
    nombreUsuario: '',
    genero: '',
    email: '',
    password: '',
    roles: [
      {
        id: '1',
      }
    ],
    estado: '1'
  }

  const { showToast } = useToast();
  const [usuario, setUsuario] = useState(usuarioVacio);
  const [password, setPassword] = useState('');
  const [fechaNacimiento, establecerFechaNacimiento] = useState(null);
  const [genero, setGenero] = useState(null);
  const generos = [
    { name: 'Masculino', code: 'M' },
    { name: 'Femenino', code: 'F' }
  ];

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUsuario({
      ...usuario,
      [name]: value
    });
  }

  const registrarUsuario = () => {
    const camposVacios = ['nombre', 'primerApellido', 'fechaNacimiento', 'ci', 'email', 'password', 'genero', 'numeroTelefono', 'nombreUsuario'];
    usuario.fechaNacimiento = fechaNacimiento || '';
    usuario.genero = genero.code || '';

    if (!GeneralUtils.validateEmptyFields(usuario, camposVacios))
      showToast('error', 'Error', 'Llene los campos obligatorios', 3000);
    else if (password !== usuario.password)
      showToast('error', 'Error', 'Las contraseñas no coinciden', 3000);
    else {
      const servicioUsuarios = new ServicioUsuarios();
      servicioUsuarios.nuevoUsuario(usuario).then(data => {
        console.log(data);
        if (data.status === 400) showToast('error', 'Error', 'Ha ocurrido un error al registrar al usuario, intente nuevamente.', 3000);
        else if (data.status === 201) {
          showToast('success', 'Éxito', 'Se registró el usuario correctamente', 3000);
          setUsuario(usuarioVacio);
          setPassword('');
          establecerFechaNacimiento(null);
          setGenero(null);
        }
      }).catch(error => {
        showToast('error', 'Error', error.message, 3000);
      });
    }
  }

  return (
    <div className="container_add_admin">
      <h3 className='h3-style h-shadow'>Nuevo administrador</h3>
      <div className="forms_form">
        <div className='field'>
          <div className="forms_field">
            <InputText name='nombre' type="text" placeholder='Nombre' className="inpt_form" autoFocus value={usuario.nombre} onChange={handleInputChange} />
          </div>

          <div className="forms_field form_group">
            <InputText name='primerApellido' type="text" placeholder='Primer apellido' className="inpt_form" value={usuario.primerApellido} onChange={handleInputChange} />
            <InputText name='segundoApellido' type="text" placeholder='Segundo apellido' className="inpt_form" value={usuario.segundoApellido} onChange={handleInputChange} />
          </div>

          <div className="forms_field form_group">
            <InputText name='ci' type="text" placeholder='CI' className="inpt_form" value={usuario.ci} onChange={handleInputChange} />
            <Calendar value={fechaNacimiento} className="inpt_form" placeholder='Fecha de nacimiento' onChange={(e) => establecerFechaNacimiento(e.value)} />
            <Dropdown value={genero} onChange={(e) => setGenero(e.value)} options={generos} optionLabel="name"
              placeholder="Selecciona un género" className="inpt_form" />
          </div>

          <div className="forms_field form_group">
            <InputText name='numeroTelefono' type="text" placeholder='Número de celular' className="inpt_form" value={usuario.numeroTelefono} onChange={handleInputChange} />
          </div>

          <div className="forms_field form_group">
            <InputText name='nombreUsuario' type="text" placeholder='Nombre de usuario' className="inpt_form" value={usuario.nombreUsuario} onChange={handleInputChange} />
            <InputText name='email' type="text" placeholder='Correo' className="inpt_form" value={usuario.email} onChange={handleInputChange} />
          </div>

          <div className="forms_field form_group">
            <Password name='password' placeholder='Contraseña' className="inpt_form" value={usuario.password} onChange={handleInputChange} />
            <Password placeholder='Repita contraseña' className="inpt_form" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
        </div>
        <div className="forms_buttons form_group">
          <Button label="CREAR CUENTA" className='inpt_form forms_buttons-action' onClick={registrarUsuario}></Button>
        </div>
      </div>
    </div>
  );
}
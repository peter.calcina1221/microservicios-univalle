import { useEffect, useState } from 'react';
import { ServicioReportes } from '../services/ServicioReportes';
import { ReporteVentasTotales } from '../reports/ReporteVentasTotales';
import { ReporteReservasTotales } from '../reports/ReporteReservasTotales';
import { ReporteVentasCategoria } from '../reports/ReporteVentasCategoria';
import { ReporteReservasCategoria } from '../reports/ReporteReservasCategoria';
import { EmprendimientosQueMasVenden } from '../reports/EmprendimientosQueMasVenden';
import '../style/Reportes.css'

export function Reportes() {
  const [usuarios, setUsuarios] = useState(0);
  const [emprendimientos, setEmprendimientos] = useState(0);
  const [ventas, setVentas] = useState(0);
  const [reservas, setReservas] = useState(0);
  const servicioReportes = new ServicioReportes();

  useEffect(() => {
    servicioReportes.obtenerUsuarios().then(data => {
      const intervalo = setTimeout(() => {
        usuarios < data.length ? setUsuarios(usuarios + 1) : clearTimeout(intervalo);
      }, 10);
    });
  }, [usuarios]);

  useEffect(() => {
    servicioReportes.obtenerEmprendimientosRegistrados().then(data => {
      const intervalo = setTimeout(() => {
        emprendimientos < data.length ? setEmprendimientos(emprendimientos + 1) : clearTimeout(intervalo);
      }, 10);
    });
  }, [emprendimientos]);

  useEffect(() => {
    servicioReportes.obtenerVentasRegistradas().then(data => {
      const intervalo = setTimeout(() => {
        ventas < data.length ? setVentas(ventas + 1) : clearTimeout(intervalo);
      }, 10);
    });
  }, [ventas]);

  useEffect(() => {
    servicioReportes.obtenerReservasRegistradas().then(data => {
      const intervalo = setTimeout(() => {
        reservas < data.length ? setReservas(reservas + 1) : clearTimeout(intervalo);
      }, 10);
    });
  }, [reservas]);

  return (
    <div className="contenedor">
      <label className='h-shadow'>Informe de usuarios y actividades</label>
      <div className="informe_container">
        <div className="informe">
          <span className='span_title'>usuarios activos</span>
          <label className='lbl h-shadow'>{usuarios}</label>
        </div>
        <div className="informe">
          <span className='span_title'>Emprendimientos activos</span>
          <label className='lbl h-shadow'>{emprendimientos}</label>
        </div>
        <div className="informe">
          <span className='span_title'>Ventas</span>
          <label className='lbl h-shadow'>{ventas}</label>
        </div>
        <div className="informe">
          <span className='span_title'>Reservas</span>
          <label className='lbl h-shadow'>{reservas}</label>
        </div>
      </div>

      <label className='h-shadow'>Informe de las ventas y reservas</label>
      <div className="contenedor-charts">
        <div className="chart_container">
          <h2>Estado de los productos vendidos</h2>
          <ReporteVentasTotales />
        </div>

        <div className="chart_container">
          <h2>Estado de las reservas</h2>
          <ReporteReservasTotales />
        </div>
      </div>

      <div className="contenedor-charts">
        <div className="chart_container">
          <h2>Ventas por categoría</h2>
          <ReporteVentasCategoria />
        </div>

        <div className="chart_container">
          <h2>Reservas por categoría</h2>
          <ReporteReservasCategoria />
        </div>
      </div>

      <div className="contenedor-charts">
        <div className="datatable_container">
          <h2>Emprendimientos que más venden</h2>
          <EmprendimientosQueMasVenden />
        </div>
      </div>
    </div>
  );
}
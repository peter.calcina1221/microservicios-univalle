import { useEffect, useState, useRef } from 'react';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { Divider } from 'primereact/divider';
import { Button } from 'primereact/button';
import { ServicioReportes } from '../services/ServicioReportes';
import { ServicioEmprendimientos } from '../services/ServicioEmprendimiento';
import { ServicioProducto } from '../services/ServicioProducto';
import { ServicioVentas } from '../services/ServicioVentas';
import { ServicioReservas } from '../services/ServicioReservas';
import { ServicioCalificacion } from '../services/ServicioCalificacion';
import '../style/DescargarReportes.css';

export function DescargarReportes() {
  const componenteAImprimir = useRef();

  //variables para el filtro
  const [fechaVentas, setFechaVentas] = useState(null);
  const [fechaReservas, setFechaReservas] = useState(null);
  const [fechaGeneral, setFechaGeneral] = useState(null);
  const [fechaActividades, setFechaActividades] = useState(null);
  const [fechaCalificacion, setFechaCalificacion] = useState(null);
  const [fechaInicio, setFechaInicio] = useState(null);
  const [fechaFin, setFechaFin] = useState(null);
  const [emprendedoraSeleccionada, setEmprendedoraSeleccionada] = useState(null);
  const [nombreEmprendimiento, setNombreEmprendimiento] = useState(null);

  //variables para los reportes
  const [usuarios, setUsuarios] = useState(0);
  const [emprendimientos, setEmprendimientos] = useState([]);
  const [emprendimientosDropdown, setEmprendimientosDropdown] = useState([]);
  const [emprendimientosQueMasVenden, setEmprendimientosQueMasVenden] = useState([]);
  const [emprendimientosQueMasReserva, setEmprendimientosQueMasReserva] = useState([]);
  const [ventas, setVentas] = useState([]);
  const [estadoVentas, setEstadoVentas] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [totalVentaCategoria, setTotalVentaCategoria] = useState([]);
  const [productos, setProductos] = useState([]);
  const [reservas, setReservas] = useState([]);
  const [estadoReservas, setEstadoReservas] = useState([]);
  const [totalReservaCategoria, setTotalReservaCategoria] = useState([]);
  //Emprendedora seleccionada
  const [productosQueMasVende, setProductosQueMasVende] = useState([]);
  const [productosQueMasReserva, setProductosQueMasReserva] = useState([]);
  const [gananciaVenta, setGananciaVenta] = useState([]);
  const [gananciaReserva, setGananciaReserva] = useState([]);
  const [calificacion, setCalificacion] = useState([]);
  const [desgloceCalificacion, setDesgloceCalificacion] = useState([]);

  //Servicios
  const servicioReportes = new ServicioReportes();
  const servicioProducto = new ServicioProducto();
  const servicioVentas = new ServicioVentas();
  const servicioEmprendimiento = new ServicioEmprendimientos();
  const servicioReservas = new ServicioReservas();
  const servicioCalificacion = new ServicioCalificacion();

  useEffect(() => {
    setFechaInicio()
  })

  useEffect(() => {
    servicioReportes.obtenerEmprendimientosRegistrados().then(data => {
      let emprendimientosDropdown = data.map(emprendimiento => ({
        name: emprendimiento.nombreEmpresa,
        id: emprendimiento.idEmpresa
      }));

      setEmprendimientosDropdown(emprendimientosDropdown);
    });

  }, []);

  useEffect(() => {
    const manejarFechas = async () => {
      if (fechaGeneral !== null) {
        if (fechaGeneral[1] !== null) {
          let fechaInicioLocal = fechaGeneral[0].toISOString().split('T')[0];
          let fechaFinLocal = fechaGeneral[1].toISOString().split('T')[0];

          !emprendedoraSeleccionada ?
            await ejecutarReportesGenerales(fechaInicioLocal, fechaFinLocal) :
            await ejecutarReportesGeneralesPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
        }
      }
    }

    manejarFechas();
  }, [fechaGeneral, emprendedoraSeleccionada]);

  useEffect(() => {
    const manejarFechas = async () => {
      if (fechaActividades !== null) {
        if (fechaActividades[1] !== null) {
          let fechaInicioLocal = fechaActividades[0].toISOString().split('T')[0];
          let fechaFinLocal = fechaActividades[1].toISOString().split('T')[0];

          if (!emprendedoraSeleccionada) {
            await obtenerUsuariosPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerEmprendimientosRegistradosPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerVentasRegistradasPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerReservasRegistradasPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerProductosRegistradosPorFecha(fechaInicioLocal, fechaFinLocal);
          } else {
            await obtenerVentasRegistradasPorFechaPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerReservasRegistradasPorFechaPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerProductosRegistradosPorFechaPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerCalificacionPorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
          }
        }
      }
    }

    manejarFechas();
  }, [fechaActividades, emprendedoraSeleccionada]);

  useEffect(() => {
    const manejarFechas = async () => {
      if (fechaVentas !== null) {
        if (fechaVentas[1] !== null) {
          let fechaInicioLocal = fechaVentas[0].toISOString().split('T')[0];
          let fechaFinLocal = fechaVentas[1].toISOString().split('T')[0];

          if (!emprendedoraSeleccionada) {
            await obtenerEstadoVentasPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerTotalVentaCategoriaPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerEmprendimientosQueMasVendenPorFecha(fechaInicioLocal, fechaFinLocal);
          } else {
            await obtenerEstadoVentasPorFechaPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerProductosQueMasVendePorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerGananciaDetalleVentaPorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
          }
        }
      }
    }

    manejarFechas();
  }, [fechaVentas, emprendedoraSeleccionada]);

  useEffect(() => {
    const manejarFechas = async () => {
      if (fechaReservas !== null) {
        if (fechaReservas[1] !== null) {
          let fechaInicioLocal = fechaReservas[0].toISOString().split('T')[0];
          let fechaFinLocal = fechaReservas[1].toISOString().split('T')[0];

          if (!emprendedoraSeleccionada) {
            await obtenerEstadoReservasPorFecha(fechaInicioLocal, fechaFinLocal);
            await obtenerTotalReservaCategoriaPorFecha(fechaInicioLocal, fechaFinLocal);
          } else {
            await obtenerEstadoReservasPorFechaPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerProductosQueMasReservaPorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
            await obtenerGananciaReservaPorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
          }
        }
      }
    }

    manejarFechas();
  }, [fechaReservas, emprendedoraSeleccionada]);

  useEffect(() => {
    const manejarFechas = async () => {
      let fechaInicioLocal, fechaFinLocal;

      if (!fechaGeneral && !fechaActividades && !fechaVentas && !fechaReservas && !fechaCalificacion) {

        let fechaActual = new Date();
        fechaActual.setMonth(fechaActual.getMonth() - 3);

        fechaInicioLocal = fechaActual.toISOString().split('T')[0];
        fechaFinLocal = new Date().toISOString().split('T')[0];

        if (fechaInicioLocal && fechaFinLocal) {
          !emprendedoraSeleccionada ?
            await ejecutarReportesGenerales(fechaInicioLocal, fechaFinLocal) :
            await ejecutarReportesGeneralesPorEmprendimiento(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
        }
      }
    };

    manejarFechas();
  }, [fechaInicio, fechaFin, emprendedoraSeleccionada]);

  useEffect(() => {
    const cargarDatosEmprendedora = async () => {
      if (fechaCalificacion !== null) {
        if (fechaCalificacion[1] === null) {
          let fechaInicioLocal = fechaCalificacion[0].toISOString().split('T')[0];
          let fechaFinLocal = fechaCalificacion[1].toISOString().split('T')[0];

          await obtenerCalificacionPorFecha(emprendedoraSeleccionada, fechaInicioLocal, fechaFinLocal);
        }
      }
    };

    cargarDatosEmprendedora();
  }, [emprendedoraSeleccionada, fechaCalificacion]);

  const ejecutarReportesGenerales = async (fechaInicioLocal, fechaFinLocal) => {
    await obtenerUsuariosPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerVentasRegistradasPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerEstadoVentasPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerTotalVentaCategoriaPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerProductosRegistradosPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerReservasRegistradasPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerEmprendimientosQueMasVendenPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerEmprendimientosRegistradosPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerEstadoReservasPorFecha(fechaInicioLocal, fechaFinLocal);
    await obtenerTotalReservaCategoriaPorFecha(fechaInicioLocal, fechaFinLocal);
  }

  const ejecutarReportesGeneralesPorEmprendimiento = async (idEmprendimiento, fechaInicioLocal, fechaFinLocal) => {
    await obtenerEstadoReservasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerProductosQueMasReservaPorFecha(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerGananciaReservaPorFecha(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerEstadoVentasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerProductosQueMasVendePorFecha(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerGananciaDetalleVentaPorFecha(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerVentasRegistradasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerReservasRegistradasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerProductosRegistradosPorFechaPorEmprendimiento(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
    await obtenerCalificacionPorFecha(idEmprendimiento, fechaInicioLocal, fechaFinLocal);
  }

  const obtenerUsuariosPorFecha = async (fechaInicio, fechaFin) => {
    servicioReportes.obtenerUsuariosPorFecha(fechaInicio, fechaFin).then(data => {
      setUsuarios(data.length);
    });
  };

  const obtenerEmprendimientosRegistradosPorFecha = async (fechaInicio, fechaFin) => {
    servicioReportes.obtenerEmprendimientosRegistradosPorFecha(fechaInicio, fechaFin).then(data => {
      setEmprendimientos(data);
    });
  };

  const obtenerVentasRegistradasPorFecha = async (fechaInicio, fechaFin) => {
    servicioReportes.obtenerVentasRegistradasPorFecha(fechaInicio, fechaFin).then(data => {
      setVentas(data);
    });
  };

  const obtenerProductosRegistradosPorFecha = async (fechaInicio, fechaFin) => {
    servicioProducto.obtenerProductosRegistradosPorFecha(fechaInicio, fechaFin).then(data => {
      setProductos(data);
    });
  };

  const obtenerReservasRegistradasPorFecha = async (fechaInicio, fechaFin) => {
    servicioReportes.obtenerReservasRegistradasPorFecha(fechaInicio, fechaFin).then(data => {
      setReservas(data);
      return data;
    }).then(reservas => {
      let emprendimientos = {};

      Promise.all(
        reservas.map(reserva =>
          Promise.all(
            reserva.detalleReservas.map(detalleReserva =>
              servicioEmprendimiento.obtenerEmprendimientoPorId(detalleReserva.idEmpresa).then(emprendimiento => {
                const nombreEmprendimiento = emprendimiento.nombreEmpresa;

                if (emprendimientos[nombreEmprendimiento]) {
                  emprendimientos[nombreEmprendimiento].cantidad++;
                } else {
                  emprendimientos[nombreEmprendimiento] = {
                    nombre: nombreEmprendimiento,
                    cantidad: 1
                  }
                }
              })
            )
          )
        )
      ).then(() => {
        let emprendimientosQueMasReserva = Object.values(emprendimientos).sort((a, b) => b.cantidad - a.cantidad).slice(0, 10);
        setEmprendimientosQueMasReserva(emprendimientosQueMasReserva);
      });
    });
  };

  const obtenerEmprendimientosQueMasVendenPorFecha = async (fechaInicio, fechaFin) => {
    servicioEmprendimiento.obtenerEmprendimientosQueMasVendenPorFecha(fechaInicio, fechaFin).then((datos) => {
      setEmprendimientosQueMasVenden(datos);
    });
  };

  const obtenerEstadoReservasPorFecha = async (fechaInicio, fechaFin) => {
    servicioReservas.obtenerDetalleReservaPorFecha(fechaInicio, fechaFin).then(data => {
      let reservaPorConfirmar = 0;
      let reservaConfirmada = 0;
      let reservaCancelada = 0;
      let reservaListaParaEntregar = 0;
      let reservaFinalizada = 0;

      data.forEach(detalleReserva => {
        switch (detalleReserva.estado) {
          case '1':
            reservaPorConfirmar++;
            break;
          case '2':
            reservaConfirmada++;
            break;
          case '3':
            reservaCancelada++;
            break;
          case '4':
            reservaListaParaEntregar++;
            break;
          case '5':
          case '6':
            reservaFinalizada++;
            break;
        }
      });

      setEstadoReservas({
        reservaPorConfirmar: reservaPorConfirmar,
        reservaConfirmada: reservaConfirmada,
        reservaCancelada: reservaCancelada,
        reservaListaParaEntregar: reservaListaParaEntregar,
        reservaFinalizada: reservaFinalizada
      });
    });
  };

  const obtenerEstadoVentasPorFecha = async (fechaInicio, fechaFin) => {
    servicioVentas.obtenerDetalleVentaPorFecha(fechaInicio, fechaFin).then(data => {
      let ventasEnProceso = 0;
      let ventasListasParaEntregar = 0;
      let ventasFinalizadas = 0;

      data.forEach(detalleVenta => {
        detalleVenta.estado === '1' ? ventasEnProceso++ : detalleVenta.estado === '2' ? ventasListasParaEntregar++ : ventasFinalizadas++;
      });

      setEstadoVentas({
        ventasEnProceso: ventasEnProceso,
        ventasListosParaEntregar: ventasListasParaEntregar,
        ventasFinalizadas: ventasFinalizadas
      });
    });
  };

  const obtenerTotalReservaCategoriaPorFecha = async (fechaInicio, fechaFin) => {
    servicioProducto.obtenerCategoriaProducto().then(categorias => {
      setCategorias(categorias.map(categoria => categoria.nombre));
      return categorias;
    }).then(categoriasProducto => {
      servicioReportes.obtenerReservasRegistradasPorFecha(fechaInicio, fechaFin).then(reservas => {
        let total = Array(categoriasProducto.length).fill(0);

        Promise.all(
          reservas.map(reserva =>
            Promise.all(
              reserva.detalleReservas.map(detalleReserva =>
                servicioProducto.obtenerProductoPorId(detalleReserva.idProducto).then(producto => {
                  const categoriaProducto = producto.categoriaProducto.nombre;
                  const index = categoriasProducto.findIndex(categoria => categoria.nombre === categoriaProducto);

                  if (index !== -1) {
                    total[index] += 1;
                  }
                })
              )
            )
          )
        ).then(() => {
          setTotalReservaCategoria(total);
        });
      });
    });
  };

  const obtenerTotalVentaCategoriaPorFecha = async (fechaInicio, fechaFin) => {
    servicioProducto.obtenerCategoriaProducto().then(categorias => {
      setCategorias(categorias.map(categoria => categoria.nombre));
      return categorias;
    }).then(categoriasProducto => {
      servicioReportes.obtenerVentasRegistradasPorFecha(fechaInicio, fechaFin).then(ventas => {
        let total = Array(categoriasProducto.length).fill(0);

        Promise.all(
          ventas.map(venta =>
            Promise.all(
              venta.detalleVentas.map(detalleVenta =>
                servicioProducto.obtenerProductoPorId(detalleVenta.idProducto).then(producto => {
                  const categoriaProducto = producto.categoriaProducto.nombre;
                  const index = categoriasProducto.findIndex(categoria => categoria.nombre === categoriaProducto);

                  if (index !== -1) {
                    total[index] += 1;
                  }
                })
              )
            )
          )
        ).then(() => {
          setTotalVentaCategoria(total);
        });
      });
    });
  };

  //Reportes para emprendedora
  const obtenerVentasRegistradasPorFechaPorEmprendimiento = async (idEmpresa, fechaInicio, fechaFin) => {
    servicioReportes.obtenerVentasRegistradasPorFechaPorEmprendimiento(idEmpresa, fechaInicio, fechaFin).then(data => {
      setVentas(data);
    });
  };

  const obtenerReservasRegistradasPorFechaPorEmprendimiento = async (idEmpresa, fechaInicio, fechaFin) => {
    servicioReportes.obtenerReservasRegistradasPorFechaPorEmprendimiento(idEmpresa, fechaInicio, fechaFin).then(data => {
      setReservas(data);
    });
  };

  const obtenerProductosRegistradosPorFechaPorEmprendimiento = async (idEmpresa, fechaInicio, fechaFin) => {
    servicioProducto.obtenerProductosRegistradosPorFechaPorEmprendimiento(idEmpresa, fechaInicio, fechaFin).then(data => {
      setProductos(data);
    });
  };

  const obtenerEstadoVentasPorFechaPorEmprendimiento = async (idEmpresa, fechaInicio, fechaFin) => {
    servicioVentas.obtenerDetalleVentaPorFechaPorEmprendimiento(idEmpresa, fechaInicio, fechaFin).then(data => {
      let ventasEnProceso = 0;
      let ventasListasParaEntregar = 0;
      let ventasFinalizadas = 0;

      data.forEach(detalleVenta => {
        detalleVenta.estado === '1' ? ventasEnProceso++ : detalleVenta.estado === '2' ? ventasListasParaEntregar++ : ventasFinalizadas++;
      });

      setEstadoVentas({
        ventasEnProceso: ventasEnProceso,
        ventasListosParaEntregar: ventasListasParaEntregar,
        ventasFinalizadas: ventasFinalizadas
      });
    });
  };

  const obtenerEstadoReservasPorFechaPorEmprendimiento = async (idEmpresa, fechaInicio, fechaFin) => {
    servicioReservas.obtenerDetalleReservaPorEmpresaYFecha(idEmpresa, fechaInicio, fechaFin).then(data => {
      let reservaPorConfirmar = 0;
      let reservaConfirmada = 0;
      let reservaCancelada = 0;
      let reservaListaParaEntregar = 0;
      let reservaFinalizada = 0;

      data.forEach(detalleReserva => {
        switch (detalleReserva.estado) {
          case '1':
            reservaPorConfirmar++;
            break;
          case '2':
            reservaConfirmada++;
            break;
          case '3':
            reservaCancelada++;
            break;
          case '4':
            reservaListaParaEntregar++;
            break;
          case '5':
          case '6':
            reservaFinalizada++;
            break;
        }
      });

      setEstadoReservas({
        reservaPorConfirmar: reservaPorConfirmar,
        reservaConfirmada: reservaConfirmada,
        reservaCancelada: reservaCancelada,
        reservaListaParaEntregar: reservaListaParaEntregar,
        reservaFinalizada: reservaFinalizada
      });
    });
  };

  const obtenerProductosQueMasVendePorFecha = async (idEmprendimiento, fechaIncio, fechaFin) => {
    servicioProducto.obtenerProductosQueMasVendePorFecha(idEmprendimiento, fechaIncio, fechaFin).then(data => {
      let productosQueMasVende = data.map(producto => (
        {
          nombre: producto.nombre,
          cantidad: producto.subTotal
        }));

      setProductosQueMasVende(productosQueMasVende);
    });
  };

  const obtenerProductosQueMasReservaPorFecha = async (idEmprendimiento, fechaInicio, fechaFin) => {
    try {
      const [detalleReserva, productos] = await Promise.all([
        servicioReservas.obtenerDetalleReservaPorEmpresaYFecha(idEmprendimiento, fechaInicio, fechaFin),
        servicioProducto.obtenerProductoPorIdEmpresa(idEmprendimiento),
      ]);

      const productosConCantidad = {};

      detalleReserva.forEach((detalleVenta) => {
        const { idProducto, subTotal } = detalleVenta;
        const producto = productos.find((p) => p.idProducto === idProducto);

        if (producto && producto.nombre) {
          if (productosConCantidad[idProducto]) {
            productosConCantidad[idProducto].cantidad += subTotal;
          } else {
            productosConCantidad[idProducto] = {
              nombre: producto.nombre,
              cantidad: subTotal,
            };
          }
        }
      });

      const resultado = Object.values(productosConCantidad);
      setProductosQueMasReserva(resultado);
    } catch (error) {
      console.error('Error al procesar detalles de ventas', error);
    }
  };

  const obtenerGananciaDetalleVentaPorFecha = async (idEmprendimiento, fechaInicio, fechaFin) => {
    servicioVentas.obtenerGananciaDetalleVentaPorFecha(idEmprendimiento, fechaInicio, fechaFin).then(data => {
      let gananciaVenta = 0;
      data.forEach(detalleVenta => {
        gananciaVenta += detalleVenta.subTotal;
      });

      setGananciaVenta(gananciaVenta);
    });
  };

  const obtenerGananciaReservaPorFecha = async (idEmprendimiento, fechaInicio, fechaFin) => {
    servicioReservas.obtenerDetalleReservaPorEmpresaYFecha(idEmprendimiento, fechaInicio, fechaFin).then(data => {
      let gananciaReserva = 0;
      data.forEach(detalleReserva => {
        gananciaReserva += detalleReserva.subTotal;
      });

      setGananciaReserva(gananciaReserva);
    })
  };

  const obtenerCalificacionPorFecha = async (idEmprendimiento, fechaInicio, fechaFin) => {
    servicioCalificacion.obtenerCalificacionPorFecha(idEmprendimiento, fechaInicio, fechaFin).then(data => {
      let calificacionPromedio = 0;
      let desgloceCalificacion = {
        CiencoEstrellas: 0,
        CuatroEstrellas: 0,
        TresEstrellas: 0,
        DosEstrellas: 0,
        UnaEstrella: 0
      };

      data.forEach(calificacion => {
        calificacionPromedio += calificacion.puntuacion;
        switch (calificacion.puntuacion) {
          case 5:
            desgloceCalificacion.CiencoEstrellas++;
            break;
          case 4:
            desgloceCalificacion.CuatroEstrellas++;
            break;
          case 3:
            desgloceCalificacion.TresEstrellas++;
            break;
          case 2:
            desgloceCalificacion.DosEstrellas++;
            break;
          case 1:
            desgloceCalificacion.UnaEstrella++;
            break;
        }
      });

      calificacionPromedio = parseFloat(calificacionPromedio / data.length).toFixed(1);
      setCalificacion(isNaN(calificacionPromedio) ? 0 : calificacionPromedio);
      setDesgloceCalificacion(desgloceCalificacion);
    });
  };

  const emprendedoraSeleccionadaTemplate = (option, props) => {
    if (option) {
      setNombreEmprendimiento(option.name);
      return (
        <div className="flex align-items-center">
          <div>{option.name}</div>
        </div>
      );
    }
    return <span>{props.placeholder}</span>;
  };

  const opcionEmprendedoraTemplate = (option) => {
    return (
      <div className="flex align-items-center">
        <div>{option.name}</div>
      </div>
    );
  };

  const handleFechaGeneralChange = (value) => {
    const selectedDates = value;
    if (selectedDates) {
      setFechaGeneral(selectedDates);
      setFechaActividades(selectedDates);
      setFechaVentas(selectedDates);
      setFechaReservas(selectedDates);
      setFechaCalificacion(selectedDates);
    }
  };

  const handleFechaEspecificaChange = (value, tipo) => {
    const selectedDates = value;

    if (fechaGeneral) setFechaGeneral(null);

    if (selectedDates) {
      switch (tipo) {
        case 'actividades':
          setFechaActividades(selectedDates);
          break;
        case 'ventas':
          setFechaVentas(selectedDates);
          break;
        case 'reservas':
          setFechaReservas(selectedDates);
          break;
        default:
          setFechaCalificacion(selectedDates);
          break;
      }
    }
  };

  const descargarReporte = async () => {
    //Descargar como PDF
    const doc = new jsPDF();
    const contenido = componenteAImprimir.current;

    const canvas = await html2canvas(contenido);
    const imgData = canvas.toDataURL('image/png');
    const imgAltura = canvas.height * 208 / canvas.width;
    doc.addImage(imgData, 0, 0, 208, imgAltura);
    doc.save('reporte.pdf');
  }

  const imprimirReporte = async () => {
    const canvas = await html2canvas(componenteAImprimir.current);
    const imgData = canvas.toDataURL('image/png');

    const newWin = window.open('', '_blank');
    if (newWin !== null) {
      const page = newWin.document.open();
      page.write(`<html><head><title>Reporte</title></head><body><img src="${imgData}" style="width:100%;height:auto;"/></body></html>`);
      page.close();

      newWin.onload = function () {
        newWin.print();
      };
    } else {
      console.error('No se pudo abrir la ventana de impresión');
    }
  };

  const limpiarFiltros = () => {
    setFechaGeneral(null);
    setFechaActividades(null);
    setFechaVentas(null);
    setFechaReservas(null);
    setFechaCalificacion(null);
    setEmprendedoraSeleccionada(null);
  };

  return (
    <div className="contenedor-descargarReportes">
      <label className='h-shadow'>Descargar o Imprimir Reporte</label>
      <div className="filtros">
        <div className="filtro-emprendedora">
          <label className='h-shadow'>Filtrar por emprendedora</label>
          <Dropdown value={emprendedoraSeleccionada} onChange={(e) => setEmprendedoraSeleccionada(e.value)} options={emprendimientosDropdown} optionLabel="name" optionValue='id' placeholder="Selecciona una emprendedora"
            filter valueTemplate={emprendedoraSeleccionadaTemplate} itemTemplate={opcionEmprendedoraTemplate} className="w-full md:w-20rem" showClear="true" />
        </div>
        <Divider />
        <div className="filtro-fecha-general">
          <label className='h-shadow'>Filtrar por fecha general (Para todos los reportes)</label>
          <Calendar value={fechaGeneral} onChange={(e) => handleFechaGeneralChange(e.value)} selectionMode="range" readOnlyInput className='w-full md:w-20rem' />
        </div>
        <Divider />
        <label className='h-shadow'>Filtrar por fechas especificas (Para cada reporte)</label>
        <div className="filtro-fechas-especificas">
          <div className="filtro-informe-actividades">
            <label className='h-shadow'>Actividades</label>
            <Calendar value={fechaActividades} onChange={(e) => handleFechaEspecificaChange(e.value, 'actividades')} selectionMode="range" readOnlyInput />
          </div>

          <div className="filtro-informe-ventas">
            <label className='h-shadow'>Ventas</label>
            <Calendar value={fechaVentas} onChange={(e) => handleFechaEspecificaChange(e.value, 'ventas')} selectionMode="range" readOnlyInput />
          </div>

          <div className="filtro-informe-reservas">
            <label className='h-shadow'>Reservas</label>
            <Calendar value={fechaReservas} onChange={(e) => handleFechaEspecificaChange(e.value, 'reservas')} selectionMode="range" readOnlyInput />
          </div>

          {emprendedoraSeleccionada && (
            <div className="filtro-informe-calificacion">
              <label className='h-shadow'>Calificación</label>
              <Calendar value={fechaCalificacion} onChange={(e) => handleFechaEspecificaChange(e.value, 'calificacion')} selectionMode="range" readOnlyInput />
            </div>
          )}
        </div>
      </div>
      <Divider />

      <div className="botones">
        <Button label="Descargar" icon="pi pi-download" className="p-button-raised" onClick={descargarReporte} />
        <Button label="Imprimir" icon="pi pi-print" className="p-button-raised" onClick={imprimirReporte} />
        <Button label="Limpiar filtros" icon="pi pi-trash" className="p-button-raised" onClick={limpiarFiltros} />
      </div>

      <Divider />

      <div id='reporte' className="contenedor-reporte" ref={componenteAImprimir}>
        {!emprendedoraSeleccionada ? (
          <>
            <label className='h-shadow'>REPORTE GLOBAL DE LA APLICACIÓN</label>

            <div className="informe_container">
              <div className="titulo titulo_principal">
                <span className='h-shadow'>Reporte de actividad </span>
              </div>
              <div className="contenido">
                <span>Usuarios registrados: <strong>{usuarios} </strong></span>
                <span>Emprendimientos registrados: <strong>{emprendimientos.length}</strong></span>
                <span>Ventas registradas: <strong>{ventas.length}</strong></span>
                <span>Reservas registradas: <strong>{reservas.length}</strong></span>
                <span>Productos registrados: <strong>{productos.length}</strong></span>
              </div>
            </div>

            <div className="informe_container">
              <div className="titulo">
                <span className='h-shadow'>Estado de las ventas</span>
                <span className='h-shadow'>Ventas por categoría</span>
                <span className='h-shadow'>Cantidad de ventas</span>
              </div>
              <div className="contenido-ventas">
                <div className="ventas">
                  <span>Ventas en proceso: <strong>{estadoVentas.ventasEnProceso}</strong></span>
                  <span>Ventas listos para entregar: <strong>{estadoVentas.ventasListosParaEntregar}</strong></span>
                  <span>Ventas finalizadas: <strong>{estadoVentas.ventasFinalizadas}</strong></span>
                </div>

                <div className="ventas">
                  {categorias && categorias.length > 0 ? (
                    categorias.map((categoria, index) => (
                      <span key={index} >{categoria}: <strong>{totalVentaCategoria[index]}</strong></span>
                    ))
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>

                <div className="ventas">
                  {emprendimientosQueMasVenden && emprendimientosQueMasVenden.length > 0 ? (
                    emprendimientosQueMasVenden.map((emprendimiento, index) => (
                      <span key={index}>{emprendimiento.nombreEmpresa}: <strong>{emprendimiento.cantidadVentas}</strong></span>
                    ))
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>

              </div>
            </div>

            <div className="informe_container">
              <div className="titulo">
                <span className='h-shadow'>Estado de las reservas</span>
                <span className='h-shadow'>Reservas por categoría</span>
                <span className='h-shadow'>Cantidad de reservas</span>
              </div>
              <div className="contenido-reservas">
                <div className="reservas">
                  <span>Reservas por confirmar: <strong>{estadoReservas.reservaPorConfirmar}</strong> </span>
                  <span>Reservas confirmadas: <strong>{estadoReservas.reservaConfirmada}</strong> </span>
                  <span>Reservas canceladas: <strong>{estadoReservas.reservaCancelada}</strong> </span>
                  <span>Reservas listas para entregar: <strong>{estadoReservas.reservaListaParaEntregar}</strong> </span>
                  <span>Reservas finalizadas: <strong>{estadoReservas.reservaFinalizada}</strong> </span>
                </div>

                <div className="reservas">
                  {categorias.map((categoria, index) => (
                    <span key={index} >{categoria}: <strong>{totalReservaCategoria[index]}</strong></span>
                  ))}
                </div>

                <div className="reservas">
                  {emprendimientosQueMasReserva && emprendimientosQueMasReserva.length > 0 ? (
                    emprendimientosQueMasReserva.map((emprendimiento, index) => (
                      <span key={index}>{emprendimiento.nombre}: <strong>{emprendimiento.cantidad}</strong></span>
                    ))
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <label className='h-shadow'>REPORTE DE LA EMPRENDEDORA:  {nombreEmprendimiento}</label>

            <div className="informe_container">
              <div className="titulo">
                <span className='h-shadow'>Reporte de actividad</span>
              </div>
              <div className="contenido">
                <span>Ventas registradas: <strong>{ventas.length}</strong></span>
                <span>Reservas registradas: <strong>{reservas.length}</strong></span>
                <span>Productos registrados: <strong>{productos.length}</strong></span>
              </div>
            </div>

            <div className="informe_container">
              <div className="titulo">
                <span className='h-shadow'>Estado de las ventas</span>
                <span className='h-shadow'>Estado de las reservas</span>
                <span className='h-shadow'>Calificación</span>
              </div>

              <div className="contenido-ventas_reservas">
                <div className="ventas">
                  <span>Ventas en proceso: <strong>{estadoVentas.ventasEnProceso}</strong></span>
                  <span>Ventas listos para entregar: <strong>{estadoVentas.ventasListosParaEntregar}</strong></span>
                  <span>Ventas finalizadas: <strong>{estadoVentas.ventasFinalizadas}</strong></span>
                </div>

                <div className="reservas">
                  <span>Reservas por confirmar: <strong>{estadoReservas.reservaPorConfirmar}</strong> </span>
                  <span>Reservas confirmadas: <strong>{estadoReservas.reservaConfirmada}</strong> </span>
                  <span>Reservas canceladas: <strong>{estadoReservas.reservaCancelada}</strong> </span>
                  <span>Reservas listas para entregar: <strong>{estadoReservas.reservaListaParaEntregar}</strong> </span>
                  <span>Reservas finalizadas: <strong>{estadoReservas.reservaFinalizada}</strong> </span>
                </div>

                <div className="calificacion">
                  <span>Calificación promedio: <strong>{calificacion}</strong></span>
                  <span>5 estrellas: <strong>{desgloceCalificacion.CiencoEstrellas}</strong></span>
                  <span>4 estrellas: <strong>{desgloceCalificacion.CuatroEstrellas}</strong></span>
                  <span>3 estrellas: <strong>{desgloceCalificacion.TresEstrellas}</strong></span>
                  <span>2 estrellas: <strong>{desgloceCalificacion.DosEstrellas}</strong></span>
                  <span>1 estrellas: <strong>{desgloceCalificacion.UnaEstrella}</strong></span>

                </div>
              </div>
            </div>

            <div className="informe_container">
              <div className="titulo">
                <span className='h-shadow'>Ingresos por productos vendidos</span>
                <span className='h-shadow'>Ingresos por productos reservados</span>
                <span className='h-shadow'>Ganancias</span>
              </div>

              <div className="contenido-ventas_reservas">
                <div className="ventas">
                  {productosQueMasVende && productosQueMasVende.length > 0 ? (
                    productosQueMasVende.map((producto, index) => (
                      <span key={index}>{producto.nombre}: <strong>{producto.cantidad}Bs.</strong></span>
                    ))
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>

                <div className="reservas">
                  {productosQueMasReserva && productosQueMasReserva.length > 0 ? (
                    productosQueMasReserva.map((producto, index) => (
                      <span key={index}>{producto.nombre}: <strong>{producto.cantidad}Bs.</strong></span>
                    ))
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>

                <div className="ganancia">
                  {gananciaVenta && gananciaVenta > 0 ? (
                    <span>Total recaudado con ventas: <strong>{gananciaVenta}Bs. </strong></span>
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}

                  {gananciaReserva && gananciaReserva > 0 ? (
                    <span>Total recaudado con reservas: <strong>{gananciaReserva}Bs. </strong></span>
                  ) : (
                    <span>No hay datos disponibles.</span>
                  )}
                </div>
              </div>
            </div>
          </>
        )
        }
      </div>
    </div>
  );
}
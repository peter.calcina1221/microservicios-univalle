import React, { useState, useEffect } from 'react';
import { FilterMatchMode, FilterOperator } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { useToast } from '../../../provider/ToastContext';
import { ServicioFactura } from '../services/ServicioFactura';
import '../style/Facturas.css'

export function Facturas() {
  const { showToast } = useToast();
  const [facturas, setFacturas] = useState([]);
  const servicioFactura = new ServicioFactura();
  const [loading, setLoading] = useState(true);
  const [filters, setFilters] = useState({
    numeroFactura: { value: null, matchMode: FilterMatchMode.CONTAINS },
    numeroDocumeto: { value: null, matchMode: FilterMatchMode.CONTAINS },
    razonSocial: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });

  useEffect(() => {
    servicioFactura.obtenerFacturasPorIdOrganizacion()
      .then(data => {
        setFacturas(data);
        setLoading(false);
      })
      .catch(error => {
        showToast('error', 'Error', 'No se pudieron cargar las facturas correctamente.', 3000);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const fechaTemplate = (rowData) => {
    return (
      <div className='flex justify-content-center'>
        {new Date(rowData.fechaRegistro).toLocaleDateString('es-PE')}
      </div>
    );
  }

  const urlTemplate = (rowData) => {
    return (
      <div className='flex justify-content-center'>
        <a href={rowData.url} target='_blank' rel='noreferrer' className='link_factura'>
          <i className='pi pi-eye' />
        </a>
      </div>
    );
  };

  return (
    <div className='container_factura'>
      <DataTable value={facturas} paginator showGridlines rows={10} dataKey='id' filters={filters} filterDisplay='row' loading={loading}
        globalFilterFields={['numeroFactura', 'razonSocial', 'numeroDocumeto']} emptyMessage='No se encontraron facturas.'>
        <Column field='numeroFactura' header='Número de factura' showFilterMenu={false} filter filterPlaceholder='Buscar' style={{ maxWidth: '10rem' }}  />
        <Column field='razonSocial' header='Razón Social' showFilterMenu={false} filter filterPlaceholder='Buscar' style={{ maxWidth: '10rem' }}  />
        <Column field='numeroDocumeto' header='Número de documento' showFilterMenu={false} filter filterPlaceholder='Buscar' style={{ maxWidth: '10rem' }} />
        <Column header='Fecha de registro' body={fechaTemplate}  />
        <Column header='Factura' body={urlTemplate} />
      </DataTable>
    </div>
  );
}
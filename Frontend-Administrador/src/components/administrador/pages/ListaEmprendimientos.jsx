import React, { useState, useEffect, useRef } from 'react'
import { FilterMatchMode } from 'primereact/api'
import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { InputText } from 'primereact/inputtext'

import { ServicioEmprendimientos } from '../services/ServicioEmprendimiento';
import { ServicioUsuarios } from '../services/ServicioUsuarios';
import '../style/ListaEmprendimiento.css'

export function ListaEmprendimientos() {
  const [emprendimientos, setEmprendimientos] = useState([]);
  const [propietarios, setPropietarios] = useState([]);

  const [filtros, setFiltros] = useState(null);
  const [valorFiltroGlobal, setValorFiltroGlobal] = useState('')
  const toast = useRef(null);

  const servicioEmprendimiento = new ServicioEmprendimientos();
  const servicioUsuario = new ServicioUsuarios();

  useEffect(() => {
    servicioEmprendimiento.obtenerEmprendimientos().then((datos) => {
      setEmprendimientos(datos);
      const promises = datos
        .filter(emp => emp.idUsuario)
        .map(emp => {
          return servicioUsuario.obtenerUsuarioPorId(emp.idUsuario);
        });

      Promise.all(promises).then(usuarios => {
        const propietariosAux = [];
        usuarios.forEach((usuario, index) => {
          propietariosAux[datos[index].idUsuario] = `${usuario.nombre} ${usuario.primerApellido} ${usuario.segundoApellido}`;
        });
        setPropietarios(propietariosAux);
      });
    });
    filtrosIniciales();
  }, []);


  const onCambioFiltroGlobal = (e) => {
    let value = e.target.value;
    let _filtros = { ...filtros };

    _filtros['global'].value = value;

    setFiltros(_filtros);
    setValorFiltroGlobal(value);
  };

  const filtrosIniciales = () => {
    setFiltros({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS }
    });
    setValorFiltroGlobal('');
  };

  const renderHeader = () => {
    return (
      <div className="flex justify-content-between">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText value={valorFiltroGlobal} onChange={onCambioFiltroGlobal} placeholder="Buscar palabra clave" />
        </span>
      </div>
    );
  };

  const formatoCeldaNombre = (rowData) => {
    const emprendimiento = rowData;

    return (
      <div className="flex align-items-center gap-2">
        <span>{emprendimiento.nombreEmpresa}</span>
      </div>
    );
  };

  const formatoCeldaUsuario = (rowData) => {
    const emprendimiento = rowData;
    const propietario = propietarios[emprendimiento.idUsuario];

    return (
      <div className="flex align-items-center gap-2">
        <span>{propietario}</span>
      </div>
    );
  };

  const formatoCeldaCelular = (rowData) => {
    const emprendimiento = rowData;

    return (
      <div className="flex align-items-center gap-2">
        <span>{emprendimiento.numeroCelular}</span>
      </div>
    );
  };

  const formatoCeladaCategoria = (rowData) => {
    const emprendimiento = rowData;

    return (
      <div className="flex align-items-center gap-2">
        <span>{emprendimiento.categoriaEmpresa.nombre}</span>
      </div>
    );
  };

  const formatoCeldaDireccion = (rowData) => {
    const emprendimiento = rowData;

    return (
      <div className="flex align-items-center gap-2">
        <span>{emprendimiento.direccion}</span>
      </div>
    )
  };

  const header = renderHeader();
  return (
    <div className="user-list">
      <DataTable value={emprendimientos} paginator showGridlines rows={7} dataKey="id"
        filters={filtros} globalFilterFields={['nombreEmpresa', 'propietario', 'numeroCelular', 'categoriaEmpresa.nombre']} header={header}
        emptyMessage="No se encontraron emprendimientos.">
        <Column header="EMPRENDIMIENTO" filterField="nombreEmpresa" body={formatoCeldaNombre} />
        <Column header="PROPIETARIO" filterField="propietario" body={formatoCeldaUsuario} />
        <Column header="CELULAR" filterField="numeroCelular" body={formatoCeldaCelular} />
        <Column header="DIRECCIÓN" body={formatoCeldaDireccion} />
        <Column header="CATEGORÍA" filterField='categoriaEmpresa.nombre' body={formatoCeladaCategoria} />
      </DataTable>
    </div>
  );
}
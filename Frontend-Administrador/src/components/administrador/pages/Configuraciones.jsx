import React, { useEffect, useState, useRef } from 'react';
import { classNames } from 'primereact/utils';
import { Divider } from 'primereact/divider';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { ServicioProducto } from '../services/ServicioProducto';
import { ServicioEmprendimientos } from '../services/ServicioEmprendimiento';
import { useToast } from '../../../provider/ToastContext';
import '../style/Configuracion.css';

export function Configuraciones() {
  let categoriaEmpresaVacia = {
    idCategoriaEmpresa: null,
    idOrganizacion: 1,
    nombre: '',
    descripcion: '',
    estado: '1'
  };

  let categoriaProductoVacia = {
    idCategoriaProducto: null,
    idOrganizacion: 1,
    nombre: '',
    descripcion: '',
    estado: '1'
  };

  const [categoria, setCategoria] = useState(categoriaEmpresaVacia);
  const [categoriaProducto, setCategoriaProducto] = useState(categoriaProductoVacia);
  const [categoriaDialog, setCategoriaDialog] = useState(false);
  const [deleteCategoriaDialog, setDeleteCategoriaDialog] = useState(false);
  const [activeCategoriaDialog, setActiveCategoriaDialog] = useState(false);
  const [categorias, setCategorias] = useState([]);
  const [categoriasProducto, setCategoriasProducto] = useState([]);
  const servicioProducto = new ServicioProducto();
  const servicioEmprendimiento = new ServicioEmprendimientos();
  const [submitted, setSubmitted] = useState(false);
  const [dialogCategoriaProductoIsOpen, setDialogCategoriaIsOpen] = useState(false);
  const [deleteCategoriaProducto, setDeleteCategoriaProducto] = useState(false);
  const [activeCategoriaProducto, setActiveCategoriaProducto] = useState(false);
  const { showToast } = useToast();

  const openDialogCategoria = (categoriaProducto) => {
    if (categoriaProducto) {
      setCategoria(categoriaProductoVacia);
      setDialogCategoriaIsOpen(true);
    } else {
      setCategoria(categoriaEmpresaVacia);
      setDialogCategoriaIsOpen(false);
    }

    setSubmitted(false);
    setCategoriaDialog(true);
  };

  const onInputChange = (e, name) => {
    const val = (e.target && e.target.value) || '';
    let _categoria = dialogCategoriaProductoIsOpen ? { ...categoriaProducto } : { ...categoria };

    _categoria[`${name}`] = val;

    dialogCategoriaProductoIsOpen ? setCategoriaProducto(_categoria) : setCategoria(_categoria);
  };

  const hideDialog = () => {
    setSubmitted(false);
    setCategoriaDialog(false);
    setCategoria(categoriaEmpresaVacia);
    setCategoriaProducto(categoriaProductoVacia);
  };

  const hideDeleteProductDialog = () => {
    setDeleteCategoriaDialog(false);
  };

  const hideActiveProductDialog = () => {
    setActiveCategoriaDialog(false);
  };

  const guardarCategoria = async (categoria, servicio, id, setCategoria, categoriaVacia, cargarCategoria) => {
    setSubmitted(true);

    if (id) {
      await servicio.actualizarCategoriaPorId(categoria).then((datos) => {
        showToast('success', 'Éxito', 'Categoría actualizada correctamente.', 3000);
      });
    } else {
      if (categoria.nombre.trim() && categoria.descripcion.trim()) {
        await servicio.agregarCategoria(categoria).then((datos) => {
          showToast('success', 'Éxito', 'Categoría agregada correctamente.', 3000);
        });
      }
    }

    setCategoriaDialog(false);
    setCategoria(categoriaVacia);
    await cargarCategoria();
  };

  const guardar = async () => {
    if (dialogCategoriaProductoIsOpen) {
      await guardarCategoria(categoriaProducto, servicioProducto, categoriaProducto.idCategoriaProducto, setCategoriaProducto, categoriaProductoVacia, cargarCategoriaProducto);
    } else {
      await guardarCategoria(categoria, servicioEmprendimiento, categoria.idCategoriaEmpresa, setCategoria, categoriaEmpresaVacia, cargarCategoriaEmpresa);
    }
  };


  const editarCategoria = (categoria) => {
    setDialogCategoriaIsOpen(false);
    setCategoria({ ...categoria });
    setCategoriaDialog(true);
  };

  const editarCategoriaProducto = (categoria) => {
    setDialogCategoriaIsOpen(true);
    setCategoriaProducto({ ...categoria });
    setCategoriaDialog(true);
  };

  const confirmarEliminarCategoria = (categoria, tipo) => {
    if (tipo === 'Empresa') {
      setCategoria(categoria);
      setDeleteCategoriaProducto(false);
    } else {
      setCategoriaProducto(categoria);
      setDeleteCategoriaProducto(true);
    }
    setDeleteCategoriaDialog(true);
  };

  const eliminarCategoria = () => {
    if (deleteCategoriaProducto) {
      categoriaProducto.estado = '0';
      servicioProducto.actualizarCategoriaPorId(categoriaProducto).then((datos) => {
        showToast('success', 'Éxito', 'Categoría eliminada correctamente.', 3000);
        cargarCategoriaProducto();
      });
    } else {
      categoria.estado = '0';
      servicioEmprendimiento.actualizarCategoriaPorId(categoria).then((datos) => {
        console.log(datos);
        showToast('success', 'Éxito', 'Categoría eliminada correctamente.', 3000);
        cargarCategoriaEmpresa();
      });
    }

    setDeleteCategoriaDialog(false);
    setCategoria(categoriaEmpresaVacia);
    setCategoriaProducto(categoriaProductoVacia);
  };

  const confirmarActivarCategoria = (categoria, tipo) => {
    if (tipo === 'Empresa') {
      setCategoria(categoria);
      setActiveCategoriaProducto(false);
    } else {
      setCategoriaProducto(categoria);
      setActiveCategoriaProducto(true);
    }
    setActiveCategoriaDialog(true);
  };

  const activarCategoria = () => {
    if (activeCategoriaProducto) {
      categoriaProducto.estado = '1';
      servicioProducto.actualizarCategoriaPorId(categoriaProducto).then((datos) => {
        showToast('success', 'Éxito', 'Categoría activada correctamente.', 3000);
        cargarCategoriaProducto();
      });
    } else {
      categoria.estado = '1';
      servicioEmprendimiento.actualizarCategoriaPorId(categoria).then((datos) => {
        showToast('success', 'Éxito', 'Categoría activada correctamente.', 3000);
        cargarCategoriaEmpresa();
      });
    }

    setActiveCategoriaDialog(false);
    setCategoria(categoriaEmpresaVacia);
    setCategoriaProducto(categoriaProductoVacia);
  }

  const productDialogFooter = (
    <React.Fragment>
      <Button label="Cancelar" icon="pi pi-times" outlined onClick={hideDialog} />
      <Button label="Guardar" icon="pi pi-check" onClick={guardar} />
    </React.Fragment>
  );

  const deleteCategoriaDialogFooter = (
    <React.Fragment>
      <Button label="No" icon="pi pi-times" outlined onClick={hideDeleteProductDialog} />
      <Button label="Sí" icon="pi pi-check" onClick={eliminarCategoria} /> 
    </React.Fragment>
  );

  const activeCategoriaDialogFooter = (
    <React.Fragment>
      <Button label="No" icon="pi pi-times" outlined onClick={hideActiveProductDialog} />
      <Button label="Sí" icon="pi pi-check" onClick={activarCategoria} />
    </React.Fragment>
  );

  useEffect(() => {
    cargarCategoriaProducto();
    cargarCategoriaEmpresa();
  }, []);

  const cargarCategoriaProducto = () => {
    servicioProducto.obtenerCategoriaProducto().then((datos) => {
      setCategoriasProducto(datos);
    });
  };

  const cargarCategoriaEmpresa = () => {
    servicioEmprendimiento.obtenerCategoriaEmpresa().then((datos) => {
      setCategorias(datos);
    });
  };

  const renderHeader = () => <Button label='Agregar nueva categoría' icon='pi pi-plus' onClick={() => openDialogCategoria(false)} />;
  const renderHeaderCategoriaProducto = () => <Button label='Agregar nueva categoría' icon='pi pi-plus' onClick={() => openDialogCategoria(true)} />;

  const formatoCeldaCategoria = (rowData) => <span>{rowData.nombre}</span>;
  const formatoCeldaDescripcion = (rowData) => <span>{rowData.descripcion}</span>;
  const formatoCeldaEstado = (rowData) => {
    const categoria = obtenerEstado(rowData.estado)
    return (<span className={categoria}>{categoria}</span>);
  }

  const formatoCeldaAcciones = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <Button icon='pi pi-pencil' onClick={() => editarCategoria(rowData)} />
        {rowData.estado === '1' ?
          <Button icon='pi pi-trash' onClick={() => confirmarEliminarCategoria(rowData, 'Empresa')} /> :
          <Button icon='pi pi-check' onClick={() => confirmarActivarCategoria(rowData, 'Empresa')} />
        }
      </div>
    );
  }

  const formatoCeldaCategoriaProducto = (rowData) => <span>{rowData.nombre}</span>;
  const formatoCeldaDescripcionProducto = (rowData) => <span>{rowData.descripcion}</span>;
  const formatoCeldaEstadoProducto = (rowData) => {
    const categoria = obtenerEstado(rowData.estado)
    return <span className={categoria}>{categoria}</span>;
  }

  const formatoCeldaAccionesProducto = (rowData) => {
    return (
      <div className="flex align-items-center gap-2">
        <Button icon='pi pi-pencil' onClick={() => editarCategoriaProducto(rowData)} />
        {rowData.estado === '1' ?
          <Button icon='pi pi-trash' onClick={() => confirmarEliminarCategoria(rowData, 'Producto')} /> :
          <Button icon='pi pi-check' onClick={() => confirmarActivarCategoria(rowData, 'Producto')} />
        }
      </div>
    );
  }

  const obtenerEstado = (estado) => estado === '1' ? 'Activo' : 'Inactivo';

  const header = renderHeader();
  const headerCategoriaProducto = renderHeaderCategoriaProducto();
  return (
    <div className="container_configuracion">
      <h3 className='h3-style h-shadow'>Configuración de categorías</h3>

      <Divider align="center">
        <span className="p-tag">Categorias para emprendimientos</span>
      </Divider>

      <DataTable value={categorias} paginator showGridlines rows={7} dataKey="id" header={header}
        emptyMessage="No se encontraron emprendimientos.">
        <Column header="CATEGORÍA" body={formatoCeldaCategoria} />
        <Column header="DESCRIPCIÓN" body={formatoCeldaDescripcion} />
        <Column header="ESTADO" body={formatoCeldaEstado} />
        <Column header="" body={formatoCeldaAcciones} />
      </DataTable>

      <Divider align="center">
        <span className="p-tag">Categorias para productos</span>
      </Divider>

      <DataTable value={categoriasProducto} paginator showGridlines rows={7} dataKey="id" header={headerCategoriaProducto}
        emptyMessage="No se encontraron emprendimientos.">
        <Column header="CATEGORÍA" body={formatoCeldaCategoriaProducto} />
        <Column header="DESCRIPCIÓN" body={formatoCeldaDescripcionProducto} />
        <Column header="ESTADO" body={formatoCeldaEstadoProducto} />
        <Column header="" body={formatoCeldaAccionesProducto} />
      </DataTable>

      <Dialog visible={categoriaDialog} style={{ width: '32rem' }} breakpoints={{ '960px': '75vw', '641px': '90vw' }} header="CATEGORÍA" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
        <div className="field">
          <label htmlFor="nombre" className="font-bold">
            Nombre de la categoría
          </label>
          {dialogCategoriaProductoIsOpen ?
            <>
              <InputText id="nombre" value={categoriaProducto.nombre} onChange={(e) => onInputChange(e, 'nombre')} required autoFocus className={classNames({ 'p-invalid': submitted && !categoriaProducto.nombre })} />
              {submitted && !categoriaProducto.nombre && <small className="p-error">El nombre de la categoría es requerido.</small>}
            </> :
            <>
              <InputText id="nombre" value={categoria.nombre} onChange={(e) => onInputChange(e, 'nombre')} required autoFocus className={classNames({ 'p-invalid': submitted && !categoria.nombre })} />
              {submitted && !categoria.nombre && <small className="p-error">El nombre de la categoría es requerido.</small>}
            </>
          }
        </div>
        <div className="field">
          <label htmlFor="descripcion" className="font-bold">
            Descripción
          </label>
          {dialogCategoriaProductoIsOpen ?
            <>
              <InputTextarea id="descripcion" value={categoriaProducto.descripcion} onChange={(e) => onInputChange(e, 'descripcion')} required rows={3} cols={20} />
              {submitted && !categoriaProducto.descripcion && <small className="p-error">La descripción es requerida.</small>}
            </> :
            <>
              <InputTextarea id="descripcion" value={categoria.descripcion} onChange={(e) => onInputChange(e, 'descripcion')} required rows={3} cols={20} />
              {submitted && !categoria.descripcion && <small className="p-error">La descripción es requerida.</small>}
            </>
          }
        </div>
      </Dialog>

      <Dialog visible={deleteCategoriaDialog} style={{ width: '32rem' }} breakpoints={{ '960px': '75vw', '641px': '90vw' }} header="ELIMINAR CATEGORÍA" modal footer={deleteCategoriaDialogFooter} onHide={hideDeleteProductDialog}>
        <div className="confirmation-content">
          <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
          {deleteCategoriaProducto ?
            <span>¿Estás seguro que deseas eliminar la categoría: <b>{categoriaProducto.nombre}</b>?</span> :
            <span>¿Estás seguro que deseas eliminar la categoría: <b>{categoria.nombre}</b>?</span>
          }
        </div>
      </Dialog>

      <Dialog visible={activeCategoriaDialog} style={{ width: '32rem' }} breakpoints={{ '960px': '75vw', '641px': '90vw' }} header="ACTIVAR CATEGORÍA" modal footer={activeCategoriaDialogFooter} onHide={hideActiveProductDialog}>
        <div className="confirmation-content">
          <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
          {activeCategoriaProducto ?
            <span>¿Estás seguro que deseas activar la categoría: <b>{categoriaProducto.nombre}</b>?</span> :
            <span>¿Estás seguro que deseas activar la categoría: <b>{categoria.nombre}</b>?</span>
          }
        </div>
      </Dialog>
    </div>
  );
}
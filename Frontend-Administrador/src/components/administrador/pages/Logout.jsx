export function Logout() {
  //Eliminamos el token de la cookie segura
  document.cookie = 'jwt=; Max-Age=-99999999;';
  sessionStorage.clear();
  window.history.replaceState({}, document.title, "/");
  window.location.reload();
}
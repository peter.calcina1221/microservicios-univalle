import urls from "../../../config/urls";

export class ServicioUsuarios {
  _token = sessionStorage.getItem('token');
  _urlUsuario = urls.usuarioService[0];

  async obtenerUsuarios() {
    const res = await fetch(`${this._urlUsuario}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this._token}`
        }

      });
    const data = await res.json();
    return data;
  }

  async obtenerUsuarioPorId(idUsuario) {
    const res = await fetch(`${this._urlUsuario}listaId/${idUsuario}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this._token}`
        }
      });
    const data = await res.json();
    return data;
  }

  nuevoUsuario(usuario) {
    return fetch(`${this._urlUsuario}nuevo`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(usuario)
    });
  }

  async actualizarUsuario(usuario) {
    return await fetch(`${this._urlUsuario}actualizar/${usuario.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`
      },
      body: JSON.stringify(usuario)
    })
  }

  async eliminarActivarUsuario(idUsuario, estado) {
    return await fetch(`${this._urlUsuario}eliminarActivar/${idUsuario}/${estado}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`
      },
      body: JSON.stringify(idUsuario)
    })
  }
}

import urls from "../../../config/urls";

export class ServicioReservas {
  _token = sessionStorage.getItem('token');
  _urlReserva = urls.reservaService[1];

  async obtenerDetalleReserva() {
    const res = await fetch(`${this._urlReserva}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerDetalleReservaPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlReserva}listaPorIdOrganizacionYFecha/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerDetalleReservaPorEmpresaYFecha(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlReserva}listaPorIdEmpresaYFecha/${idEmprendimiento}/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }
}

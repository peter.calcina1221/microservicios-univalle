import urls from "../../../config/urls";

export class ServicioCalificacion {
  _token = sessionStorage.getItem('token');
  _urlCalificacion = urls.calificacionService[0];

  async obtenerCalificacionPorFecha(idEmpresa, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlCalificacion}listaPorIdOrganizacionIdElementoTipoElementoYFecha/1/${idEmpresa}/Empresa/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }
}

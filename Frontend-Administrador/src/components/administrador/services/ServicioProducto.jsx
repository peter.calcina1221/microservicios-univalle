import urls from "../../../config/urls";

export class ServicioProducto {
  _urlProducto = urls.producoService[0];
  _urlCategoria = urls.producoService[1];

  async obtenerProductosRegistrados() {
    const res = await fetch(`${this._urlProducto}listaPorIdOrganizacionAndEstado/1/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerProductosRegistradosPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlProducto}listaPorIdOrganizacionAndEstadoYFecha/1/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerProductosRegistradosPorFechaPorEmprendimiento(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlProducto}listaPorIdOrganizacionEstadoFechaYIdEmpresa/1/1/${fechaInicio}/${fechaFin}/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerProductoPorId(idProducto) {
    const res = await fetch(`${this._urlProducto}obtenerProductoPorId/${idProducto}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerCategoriaProducto() {
    const res = await fetch(`${this._urlCategoria}listaIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerProductosQueMasVendePorFecha(idEmpresa, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlProducto}obtenerProductosSubTotalesPorEmpresaYFecha/${idEmpresa}/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async agregarCategoria(categoriaProducto) {
    const res = await fetch(`${this._urlCategoria}nuevo`,
      {
        method: 'POST',
        body: JSON.stringify(categoriaProducto),
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async actualizarCategoriaPorId(categoriaProducto) {
    const res = await fetch(`${this._urlCategoria}actualizar/${categoriaProducto.idCategoriaProducto}`,
      {
        method: 'PUT',
        body: JSON.stringify(categoriaProducto),
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerProductoPorIdEmpresa(idEmpresa) {
    const res = await fetch(`${this._urlProducto}obtenerProductosPorIdEmpresa/${idEmpresa}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;

  }
}

import urls from "../../../config/urls";

export class ServicioReportes {
  _token = sessionStorage.getItem('token');
  _urlUsuario = urls.usuarioService[0];
  _urlEmprendimiento = urls.empresaService[0];
  _urlVenta = urls.ventaService[0];
  _urlReserva = urls.reservaService[0];

  async obtenerUsuarios() {
    const res = await fetch(`${this._urlUsuario}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerUsuariosPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlUsuario}listaPorIdOrganizacionYFecha/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerEmprendimientosRegistrados() {
    const res = await fetch(`${this._urlEmprendimiento}listaPorIdOrganizacionAndEstado/1/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerEmprendimientosRegistradosPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlEmprendimiento}listaPorIdOrganizacionAndEstadoYFecha/1/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerVentasRegistradas() {
    const res = await fetch(`${this._urlVenta}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerReservasRegistradas() {
    const res = await fetch(`${this._urlReserva}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerVentasRegistradasPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlVenta}listaPorIdOrganizacionYFecha/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerVentasRegistradasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlVenta}listaPorIdOrganizacionFechaYIdEmpresa/1/${fechaInicio}/${fechaFin}/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerReservasRegistradasPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlReserva}listaPorIdOrganizacionYFecha/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerReservasRegistradasPorFechaPorEmprendimiento(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlReserva}listaPorIdOrganizacionFechaYIdEmpresa/1/${fechaInicio}/${fechaFin}/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }
}

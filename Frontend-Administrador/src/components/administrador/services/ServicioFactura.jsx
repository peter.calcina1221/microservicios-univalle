import urls from "../../../config/urls";

export class ServicioFactura {
  _urlFactura = urls.ventaService[1];
  
  async obtenerFacturasPorIdOrganizacion() {
    const res = await fetch(`${this._urlFactura}listaByIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }
}

import urls from "../../../config/urls";

export class ServicioVentas {
  _token = sessionStorage.getItem('token');
  _urlDetalleVenta = urls.ventaService[2];

  async obtenerDetalleVenta() {
    const res = await fetch(`${this._urlDetalleVenta}listaPorIdOrganizacion/1`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerDetalleVentaPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlDetalleVenta}listaPorIdOrganizacionYFecha/1/${fechaInicio}/${fechaFin}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerDetalleVentaPorFechaPorEmprendimiento(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlDetalleVenta}listaPorIdOrganizacionFechaYIdEmpresa/1/${fechaInicio}/${fechaFin}/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }

  async obtenerGananciaDetalleVentaPorFecha(idEmprendimiento, fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlDetalleVenta}listaPorFechasYEmpresa/${fechaInicio}/${fechaFin}/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': `Bearer ${this._token}`  }
      });
    const data = await res.json();
    return data;
  }
}

import urls from "../../../config/urls";

export class ServicioEmprendimientos {
  _urlEmprendimiento = urls.empresaService[0];
  _urlCategoriaEmprendimiento = urls.empresaService[1];

  async obtenerEmprendimientos() {
    const res = await fetch(`${this._urlEmprendimiento}lista`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerEmprendimientoPorId(idEmprendimiento) {
    const res = await fetch(`${this._urlEmprendimiento}obtenerEmpresaPorId/${idEmprendimiento}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();

    return data;
  }

  async obtenerEmprendimientosQueMasVenden() {
    const res = await fetch(`${this._urlEmprendimiento}listaEmpresasQueMasVenden/1/1000`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerEmprendimientosQueMasVendenPorFecha(fechaInicio, fechaFin) {
    const res = await fetch(`${this._urlEmprendimiento}listaEmpresasQueMasVendenYFecha/1/${fechaInicio}/${fechaFin}/100`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerCategoriaEmpresaPorId(idCategoriaEmpresa) {
    const res = await fetch(`${this._urlEmprendimiento}listaId/${idCategoriaEmpresa}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async obtenerCategoriaEmpresa() {
    const res = await fetch(`${this._urlCategoriaEmprendimiento}lista`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async agregarCategoria(categoriaEmpresa) {
    const res = await fetch(`${this._urlCategoriaEmprendimiento}nuevo`,
      {
        method: 'POST',
        body: JSON.stringify(categoriaEmpresa),
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }

  async actualizarCategoriaPorId(categoriaEmpresa) {
    const res = await fetch(`${this._urlCategoriaEmprendimiento}actualizar/${categoriaEmpresa.idCategoriaEmpresa}`,
      {
        method: 'PUT',
        body: JSON.stringify(categoriaEmpresa),
        headers: { 'Content-Type': 'application/json' }
      });
    const data = await res.json();
    return data;
  }
}

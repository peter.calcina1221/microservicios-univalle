import React, { useState, useEffect } from 'react';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useToast } from '../../provider/ToastContext';
import { InicioSesionServicio } from '../inicio-sesion/ServicioInicioSesion';
import '../inicio-sesion/inicioSesion.css';

function InicioSesion({ onLogin }) {

  let usuarioVacio = {
    nombreUsuario: '',
    password: '',
    idOrganizacion: 1
  }

  const [usuarioIngresado, establecerUsuarioIngresado] = useState(usuarioVacio);

  const esteblecerValorUsuario = (e, usuario) => {
    let _usuario = { ...usuarioIngresado };
    _usuario[usuario] = e.target.value;
    establecerUsuarioIngresado(_usuario);
  }

  const { showToast } = useToast();
  const login = () => {
    const servicioInicioSesion = new InicioSesionServicio();
    servicioInicioSesion.login(usuarioIngresado).then(data => {
      document.cookie = `jwt=${data.token}; Secure; SameSite=Strict`;
      sessionStorage.setItem('token', data.token);
      showToast('success', 'Bienvenido', 'Iniciaste sesión correctamente', 3000);
      onLogin(true);
    }).catch(error => {
      console.log(error.message)
      showToast('error', 'Error', 'Usuario o contraseña incorrectos', 3000);
    });
  };


  useEffect(() => {
    const loginButton = document.getElementById('login-button'),
      userForms = document.getElementById('user_options-forms')

    loginButton.addEventListener('click', () => {
      userForms.classList.remove('bounceLeft')
      userForms.classList.add('bounceRight')
      userForms.classList.remove('new')
    }, false);
  })

  return (
    <section className="user">
      <div className="user_options-container">
        <div className="user_options-text">
          <div className="user_options-unregistered">
            <h2 className="user_unregistered-title">¿No tienes una cuenta?</h2>
            <p className="user_unregistered-text">
              Contacta con el administrador para que te proporcione una cuenta de usuario y puedas gestionar el sitio web.
            </p>

          </div>

          <div className="user_options-registered">
            <h2 className="user_registered-title">¿Ya tienes una cuenta?</h2>
            <p className="user_registered-text">Inicia sesión para empezar a gestionar el sitio web con facilidad y eficiencia.</p>
            <button className="user_registered-login" id="login-button">Iniciar Sesión</button>
          </div>
        </div>

        <div className="user_options-forms" id="user_options-forms">
          <div className="user_forms-login">
            <h2 className="forms_title">Iniciar Sesión</h2>
            <div className="forms_form">
              <div className='field'>
                <div className="forms_field">
                  <InputText type="text" id="usuario" onChange={(e) => { esteblecerValorUsuario(e, "nombreUsuario") }} placeholder='Usuario' className="p-invalid" required autoFocus />
                </div>
                <div className="forms_field">
                  <Password inputId="password" onChange={(e) => { esteblecerValorUsuario(e, "password") }} placeholder='Contraseña' className="p-invalid" feedback={false} required />
                </div>
              </div>
              <div className="forms_buttons">
                {/* <button type="button" className="forms_buttons-forgot">¿Olvidaste tu contraseña?</button> */}
                <Button label="INGRESAR" className='forms_buttons-action' onClick={login} ></Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default InicioSesion;
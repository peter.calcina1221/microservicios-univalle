import urls from "../../config/urls";

export class InicioSesionServicio {
  _urlUsuario = urls.usuarioService[0];

  async login(usuario) {
    const res = await fetch(`${this._urlUsuario}login`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(usuario)
      });
    const data_1 = await (res.ok ? res.json() : res.json().then(data => {
      throw new Error(data.message);
    }));
    return data_1;
  }
}
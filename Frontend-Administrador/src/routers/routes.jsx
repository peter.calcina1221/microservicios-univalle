import { Navigate, Routes, Route } from "react-router-dom"

/* IMPORTACIONES DE LAS PÁGINAS */
import { AgregarAdministrador } from '../components/administrador/pages/AgregarAdministrador'
import { ListaUsuarios } from '../components/administrador/pages/ListaUsuarios'
import { ListaEmprendimientos } from '../components/administrador/pages/ListaEmprendimientos'
import { Reportes } from '../components/administrador/pages/Reportes'
import { Configuraciones } from '../components/administrador/pages/Configuraciones'
import { Logout } from '../components/administrador/pages/Logout'
import { Facturas } from "../components/administrador/pages/Facturas"
import { DescargarReportes } from "../components/administrador/pages/DescargarReportes"
/* --- FIN DE LAS IMPORTACIONES DE LAS PÁGINAS --- */

export function Rutas() {
  return (
    <Routes>
      <Route path="/" element={<Navigate to='reporte' replace />} />
      <Route path="/agregarAdministrador" element={<AgregarAdministrador />} />
      <Route path="/listaUsuarios" element={<ListaUsuarios />} />
      <Route path="/listaEmprendimientos" element={<ListaEmprendimientos />} />
      <Route path="/reporte" element={<Reportes />} />
      <Route path="/descargarReportes" element={<DescargarReportes />} />
      <Route path="/configuraciones" element={<Configuraciones />} />
      <Route path="/facturas" element={<Facturas />} />
      <Route path="/logout" element={<Logout />} />
      
    </Routes>
  )
}
import { useState } from 'react'
import Administrador from './components/administrador/components/Administrador'
import InicioSesion from './components/inicio-sesion/inicioSesion'
import { ToastProvider } from './provider/ToastContext'

function App() {
  const [data, setData] = useState(false)
  const login = (data) => {
    setData(data)
  }

  if (sessionStorage.getItem('token') || data) {
    return (
      <ToastProvider>
        <div className='log-app'>
          <Administrador />
        </div>
      </ToastProvider>
    )
  } else {
    return (
      <ToastProvider>
        <div className='log-app'>
          <InicioSesion onLogin={login} />
        </div>
      </ToastProvider>
    )
  }
}

export default App

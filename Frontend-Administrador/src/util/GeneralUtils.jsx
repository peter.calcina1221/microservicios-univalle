export class GeneralUtils {
  static validateEmptyFields(object, validateField) {
    let isValid = true;
    validateField.forEach(field => {
      if (object[field] == null || object[field].toString() == '') {
        isValid = false;
      }
    });
    return isValid;
  }
}
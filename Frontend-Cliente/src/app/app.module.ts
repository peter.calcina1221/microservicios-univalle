import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AsyncPipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { provideStorage, getStorage } from '@angular/fire/storage';
import { JwtModule } from '@auth0/angular-jwt';
import { APP_INITIALIZER } from '@angular/core';
import { TokenService } from './services/Usuario/token.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
    provideStorage(() => getStorage()),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => localStorage.getItem('token')
      }
    })
  ],
  providers: [
    AsyncPipe,
    {
      provide: APP_INITIALIZER,
      useFactory: decryptJWT,
      deps: [TokenService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function decryptJWT(tokenService: TokenService) {
  return () => tokenService.DecryptJTW();
}

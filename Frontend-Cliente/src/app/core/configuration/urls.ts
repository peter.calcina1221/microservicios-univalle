const gatewayUrl = 'http://localhost:8080/api/';
export const urls = {
  // * URLs para el microservicio Usuario
  usuarioUrl: 'http://localhost:8095/usuario/',
  emailUrl: 'http://localhost:8095/email/',

  // * URLs para el microservicio Empresa
  empresaUrl: `http://localhost:8091/empresa/`,
  departamentoUrl: `http://localhost:8091/departamento/`,
  categoriaEmpresaUrl: `http://localhost:8091/categoriaEmpresa/`,
  codigoUrl: `http://localhost:8091/codigo/`,

  // * URLs para el microservicio Producto
  productoUrl: `http://localhost:8094/producto/`,
  categoriaProductoUrl: `http://localhost:8094/categoriaProducto/`,
  caracteristicaProductoUrl: `http://localhost:8094/caracteristicaProducto/`,

  // * URLs para el microservicio Venta
  ventaUrl: `http://localhost:8096/venta/`,
  detalleVentaUrl: `http://localhost:8096/detalleVenta/`,

  // * URLs para el microservicio Calificacion
  calificacionUrl: `http://localhost:8090/calificacion/`,
  estadisticaUrl: `http://localhost:8090/estadistica/`,

  // * URLs para el microservicio Reserva
  reservaUrl: `http://localhost:8099/reserva/`,
  detalleReservaUrl: `http://localhost:8099/detalleReserva/`,

  // * URLs para el microservicio Pago
  pagoUrl: `http://localhost:8098/pago/`,

  //* URL para la verificacion de SMS
  verificacionSMSUrl: `http://localhost:8097/verificacion/`,
};

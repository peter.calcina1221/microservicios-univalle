import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';


export const codigoEmprendimientoGuard: CanActivateFn = () => {
  if(sessionStorage.getItem('codigoValidado') !== 'true') {
    const router = inject(Router);
    router.navigate(['/home']);
    return false;
  }

  return true;
};


import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { TokenService } from 'src/app/services/Usuario/token.service';


export const emprendimientoGuard: CanActivateFn = () => {
  const authService = inject(TokenService);
  if(authService.getIdEmprendimiento() > 0) {
    const router = inject(Router);
    router.navigate(['/home']);
    return false;
  }

  return true;
};


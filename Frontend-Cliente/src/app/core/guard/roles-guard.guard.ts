import { CanActivateFn, Router } from '@angular/router';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { inject } from '@angular/core';

export const rolesGuard: CanActivateFn = () => {
  const roleUser = inject(TokenService);
  if (roleUser.getAuthorities().includes('ROLE_EMPRENDEDORA')) {
    return true;
  } else {
    const router = inject(Router);
    router.navigate(['/home']);
    return false;
  }
};

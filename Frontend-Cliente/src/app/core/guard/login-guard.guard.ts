import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { TokenService } from 'src/app/services/Usuario/token.service';


export const loginGuard: CanActivateFn = (route, state) => {
  const authService = inject(TokenService);

  if (!authService.getToken()) {
    const router = inject(Router);
    router.navigate(['']);
    return false;
  }

  return true;
};


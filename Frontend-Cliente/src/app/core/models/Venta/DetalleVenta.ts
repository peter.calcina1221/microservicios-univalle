import { Venta } from './Venta';

export class DetalleVenta {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idDetalleVenta: number = 0;
  private idProducto: number = 0;
  private idEmpresa: number = 0;
  private cantidad: number = 0;
  private codigo: string = '';
  private precioUnitario: number = 0;
  private descuento: number = 0;
  private subTotal: number = 0;
  private venta: Venta = new Venta();
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(detalleVenta?: DetalleVenta) {
    if (detalleVenta) {
      this.idDetalleVenta = detalleVenta.idDetalleVenta;
      this.idProducto = detalleVenta.idProducto;
      this.idEmpresa = detalleVenta.idEmpresa;
      this.cantidad = detalleVenta.cantidad;
      this.codigo = detalleVenta.codigo;
      this.precioUnitario = detalleVenta.precioUnitario;
      this.descuento = detalleVenta.descuento;
      this.subTotal = detalleVenta.subTotal;
      this.venta = new Venta(detalleVenta.venta);
      this.fechaRegistro = detalleVenta.fechaRegistro;
      this.fechaActualizacion = detalleVenta.fechaActualizacion;
      this.estado = detalleVenta.estado;
    }
  }

  get IdDetalleVenta(): number {
    return this.idDetalleVenta;
  }

  set IdDetalleVenta(value: number) {
    this.idDetalleVenta = value;
  }

  get IdProducto(): number {
    return this.idProducto;
  }

  set IdProducto(value: number) {
    this.idProducto = value;
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(value: number) {
    this.idEmpresa = value;
  }

  get Cantidad(): number {
    return this.cantidad;
  }

  set Cantidad(value: number) {
    this.cantidad = value;
  }

  get Codigo(): string {
    return this.codigo;
  }

  set Codigo(value: string) {
    this.codigo = value;
  }

  get PrecioUnitario(): number {
    return this.precioUnitario;
  }

  set PrecioUnitario(value: number) {
    this.precioUnitario = value;
  }

  get Descuento(): number {
    return this.descuento;
  }

  set Descuento(value: number) {
    this.descuento = value;
  }

  get SubTotal(): number {
    return this.subTotal;
  }

  set SubTotal(value: number) {
    this.subTotal = value;
  }

  get Venta(): Venta {
    return this.venta;
  }

  set Venta(value: Venta) {
    this.venta = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

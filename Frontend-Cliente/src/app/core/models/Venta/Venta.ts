import { DetalleVenta } from "./DetalleVenta";
import { Factura } from './Factura';


export class Venta {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idVenta: number = 0;
  private idUsuario: number = 0;
  private idOrganizacion: number = 1;
  private codigoVenta: string = '';
  private total: number = 0;
  private orderCode: String = '';
  private factura: Factura = new Factura();
  private detalleVentas: DetalleVenta[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  //Atributos para la factura
  private razonSocial: string = '';
  private nit: string = '';
  private correo: string = '';
  private tipoDocumento: number = 0;

  constructor(venta?: Venta) {
    if (venta) {
      this.idVenta = venta.idVenta;
      this.idUsuario = venta.idUsuario;
      this.idOrganizacion = venta.idOrganizacion;
      this.codigoVenta = venta.codigoVenta;
      this.total = venta.total;
      this.orderCode = venta.orderCode;
      this.factura = new Factura(venta.factura);
      if (venta.detalleVentas)
        for (let i = 0; i < venta.detalleVentas.length; i++) {
          this.detalleVentas.push(new DetalleVenta(venta.detalleVentas[i]));
        }
      this.fechaRegistro = venta.fechaRegistro;
      this.fechaActualizacion = venta.fechaActualizacion;
      this.estado = venta.estado;

      this.razonSocial = venta.razonSocial;
      this.nit = venta.nit;
      this.correo = venta.correo;
      this.tipoDocumento = venta.tipoDocumento;
    }
  }

  get IdVenta(): number {
    return this.idVenta;
  }

  set IdVenta(value: number) {
    this.idVenta = value;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  get CodigoVenta(): string {
    return this.codigoVenta;
  }

  set CodigoVenta(value: string) {
    this.codigoVenta = value;
  }

  get Total(): number {
    return this.total;
  }

  set Total(value: number) {
    this.total = value;
  }

  get OrderCode(): String {
    return this.orderCode;
  }

  set OrderCode(value: String) {
    this.orderCode = value;
  }

  get Factura(): Factura {
    return this.factura;
  }

  set Factura(value: Factura) {
    this.factura = value;
  }

  get DetalleVentas(): DetalleVenta[] {
    return this.detalleVentas;
  }

  set DetalleVentas(value: DetalleVenta[]) {
    this.detalleVentas = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get RazonSocial(): string {
    return this.razonSocial;
  }

  set RazonSocial(value: string) {
    this.razonSocial = value;
  }

  get Nit(): string {
    return this.nit;
  }

  set Nit(value: string) {
    this.nit = value;
  }

  get Correo(): string {
    return this.correo;
  }

  set Correo(value: string) {
    this.correo = value;
  }

  get TipoDocumento(): number {
    return this.tipoDocumento;
  }

  set TipoDocumento(value: number) {
    this.tipoDocumento = value;
  }

  //Método para generar el código de la venta
  generarCodigoVenta(): string {
    let numeroAleatorio = Math.floor(Math.random() * (9999 - 0)) + 0;
    return this.codigoVenta = 'V' + this.idUsuario + '-' + this.fechaRegistro.getFullYear() + '-' + (this.fechaRegistro.getMonth() + 1) + '-' + this.fechaRegistro.getDate() + '-' + numeroAleatorio;
  }
}

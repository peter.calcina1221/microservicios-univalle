import { Venta } from "./Venta";

export class Factura {
  [key: string]: any;
  private idFactura: number = 0;
  private numeroFactura: string = '';
  private xml: string = '';
  private razonSocial: string = '';
  private numeroDocumento: string = '';
  private cuf: string = '';
  private total: number = 0;
  private url: string = '';
  private estadoImpuestos: string = '';
  private idOrganizacion: number = 1;
  //private venta: Venta = new Venta();
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(factura?: Factura) {
    if (factura) {
      this.idFactura = factura.idFactura;
      this.numeroFactura = factura.numeroFactura;
      this.xml = factura.xml;
      this.razonSocial = factura.razonSocial;
      this.numeroDocumento = factura.numeroDocumento;
      this.cuf = factura.cuf;
      this.total = factura.total;
      this.url = factura.url;
      this.estadoImpuestos = factura.estadoImpuestos;
      this.idOrganizacion = factura.idOrganizacion;
      //this.venta = new Venta(factura.venta);
      this.fechaRegistro = factura.fechaRegistro;
      this.fechaActualizacion = factura.fechaActualizacion;
      this.estado = factura.estado;
    }
  }

  get IdFactura(): number {
    return this.idFactura;
  }

  set IdFactura(value: number) {
    this.idFactura = value;
  }

  get NumeroFactura(): string {
    return this.numeroFactura;
  }

  set NumeroFactura(value: string) {
    this.numeroFactura = value;
  }

  get Xml(): string {
    return this.xml;
  }

  set Xml(value: string) {
    this.xml = value;
  }

  get RazonSocial(): string {
    return this.razonSocial;
  }

  set RazonSocial(value: string) {
    this.razonSocial = value;
  }

  get NumeroDocumento(): string {
    return this.numeroDocumento;
  }

  set NumeroDocumento(value: string) {
    this.numeroDocumento = value;
  }

  get Cuf(): string {
    return this.cuf;
  }

  set Cuf(value: string) {
    this.cuf = value;
  }

  get Total(): number {
    return this.total;
  }

  set Total(value: number) {
    this.total = value;
  }

  get Url(): string {
    return this.url;
  }

  set Url(value: string) {
    this.url = value;
  }

  get EstadoImpuestos(): string {
    return this.estadoImpuestos;
  }

  set EstadoImpuestos(value: string) {
    this.estadoImpuestos = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  // get Venta(): Venta {
  //   return this.venta;
  // }

  // set Venta(value: Venta) {
  //   this.venta = value;
  // }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

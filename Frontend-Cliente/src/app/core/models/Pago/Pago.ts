export class Pago {
  private orderCode: string = '';

  constructor(pago?: Pago) {
    if(pago) {
      this.orderCode = pago.orderCode;
    }
  }

  get OrderCode(): string {
    return this.orderCode;
  }

  set OrderCode(value: string) {
    this.orderCode = value;
  }
}

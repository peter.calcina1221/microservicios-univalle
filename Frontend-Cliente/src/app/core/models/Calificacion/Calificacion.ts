export class Calificacion {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idCalificacion: number = 0;
  private idUsuario: number = 0;
  private idOrganizacion: number = 1;
  private idElemento: number = 0;
  private tipoElemento: string = '';
  private puntuacion: number = 0;
  private comentario: string = '';
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(calificacion? : Calificacion) {
    if (calificacion) {
      this.idCalificacion = calificacion.idCalificacion;
      this.idUsuario = calificacion.idUsuario;
      this.idOrganizacion = calificacion.idOrganizacion;
      this.idElemento = calificacion.idElemento;
      this.tipoElemento = calificacion.tipoElemento;
      this.puntuacion = calificacion.puntuacion;
      this.comentario = calificacion.comentario;
      this.fechaRegistro = calificacion.fechaRegistro;
      this.fechaActualizacion = calificacion.fechaActualizacion;
      this.estado = calificacion.estado;
    }
  }

  get IdCalificacion(): number {
    return this.idCalificacion;
  }

  set IdCalificacion(idCalificacion: number) {
    this.idCalificacion = idCalificacion;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(idUsuario: number) {
    this.idUsuario = idUsuario;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(idOrganizacion: number) {
    this.idOrganizacion = idOrganizacion;
  }

  get IdElemento(): number {
    return this.idElemento;
  }

  set IdElemento(idElemento: number) {
    this.idElemento = idElemento;
  }

  get TipoElemento(): string {
    return this.tipoElemento;
  }

  set TipoElemento(tipoElemento: string) {
    this.tipoElemento = tipoElemento;
  }

  get Puntuacion(): number {
    return this.puntuacion;
  }

  set Puntuacion(puntuacion: number) {
    this.puntuacion = puntuacion;
  }

  get Comentario(): string {
    return this.comentario;
  }

  set Comentario(comentario: string) {
    this.comentario = comentario;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(fechaRegistro: Date) {
    this.fechaRegistro = fechaRegistro;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(fechaActualizacion: Date) {
    this.fechaActualizacion = fechaActualizacion;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(estado: string) {
    this.estado = estado;
  }
}

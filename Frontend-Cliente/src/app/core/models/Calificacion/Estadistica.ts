export class Estadistica {
  [key: string]: any;
  private idEstadistica: number = 0;
  private idOrganizacion: number = 0;
  private idElemento: number = 0;
  private tipoElemento: string = '';
  private promedio: number = 0;
  private cantidadCalificaciones: number = 0;
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(estadistica? : Estadistica) {
    if (estadistica) {
      this.idEstadistica = estadistica.idEstadistica;
      this.idOrganizacion = estadistica.idOrganizacion;
      this.idElemento = estadistica.idElemento;
      this.tipoElemento = estadistica.tipoElemento;
      this.promedio = estadistica.promedio;
      this.cantidadCalificaciones = estadistica.cantidadCalificaciones;
      this.fechaRegistro = estadistica.fechaRegistro;
      this.fechaActualizacion = estadistica.fechaActualizacion;
      this.estado = estadistica.estado;
    }
  }

  get IdEstadistica(): number {
    return this.idEstadistica;
  }

  set IdEstadistica(idEstadistica: number) {
    this.idEstadistica = idEstadistica;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(idOrganizacion: number) {
    this.idOrganizacion = idOrganizacion;
  }

  get IdElemento(): number {
    return this.idElemento;
  }

  set IdElemento(idElemento: number) {
    this.idElemento = idElemento;
  }

  get TipoElemento(): string {
    return this.tipoElemento;
  }

  set TipoElemento(tipoElemento: string) {
    this.tipoElemento = tipoElemento;
  }

  get Promedio(): number {
    return this.promedio;
  }

  set Promedio(promedio: number) {
    this.promedio = promedio;
  }

  get CantidadCalificaciones(): number {
    return this.cantidadCalificaciones;
  }

  set CantidadCalificaciones(cantidadCalificaciones: number) {
    this.cantidadCalificaciones = cantidadCalificaciones;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(fechaRegistro: Date) {
    this.fechaRegistro = fechaRegistro;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(fechaActualizacion: Date) {
    this.fechaActualizacion = fechaActualizacion;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(estado: string) {
    this.estado = estado;
  }
}

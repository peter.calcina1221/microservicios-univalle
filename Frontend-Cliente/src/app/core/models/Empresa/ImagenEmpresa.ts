import { Emprendimiento } from './Emprendimiento';

export class ImagenEmpresa {
  private idImagenEmpresa: number = 0;
  private empresa: Emprendimiento = new Emprendimiento();
  private imgUrl: string = '';
  private principal: boolean = false;
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(imagenEmpresa?: ImagenEmpresa) {
    if (imagenEmpresa) {
      this.idImagenEmpresa = imagenEmpresa.idImagenEmpresa;
      this.empresa = imagenEmpresa.empresa;
      this.imgUrl = imagenEmpresa.imgUrl;
      this.principal = imagenEmpresa.principal;
      this.fechaRegistro = imagenEmpresa.fechaRegistro;
      this.fechaActualizacion = imagenEmpresa.fechaActualizacion;
      this.estado = imagenEmpresa.estado;
    }
  }

  get IdImagenEmpresa(): number {
    return this.idImagenEmpresa;
  }

  set IdImagenEmpresa(value: number) {
    this.idImagenEmpresa = value;
  }

  get Empresa(): Emprendimiento {
    return this.empresa;
  }

  set Empresa(value: Emprendimiento) {
    this.empresa = value;
  }

  get ImgUrl(): string {
    return this.imgUrl;
  }

  set ImgUrl(value: string) {
    this.imgUrl = value;
  }

  get Principal(): boolean {
    return this.principal;
  }

  set Principal(value: boolean) {
    this.principal = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

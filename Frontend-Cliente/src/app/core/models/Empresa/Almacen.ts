import { Sucursal } from "./Sucursal";

export class Almacen {
  private idAlmacen: number = 0;
  private nombre: string = 'Principal';
  private idUsuario: number = 0;
  private sucursal: Sucursal = new Sucursal();
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(almacen?: Almacen) {
    if (almacen) {
      this.idAlmacen = almacen.idAlmacen;
      this.nombre = almacen.nombre;
      this.idUsuario = almacen.idUsuario;
      this.sucursal = almacen.sucursal;
      this.fechaRegistro = almacen.fechaRegistro;
      this.fechaActualizacion = almacen.fechaActualizacion;
      this.estado = almacen.estado;
    }
  }

  get IdAlmacen(): number {
    return this.idAlmacen;
  }

  set IdAlmacen(value: number) {
    this.idAlmacen = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get Sucursal(): Sucursal {
    return this.sucursal;
  }

  set Sucursal(value: Sucursal) {
    this.sucursal = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

import { Emprendimiento } from "./Emprendimiento";

export class Categoria {
  private idCategoriaEmpresa: number = 0;
  private nombre: string = '';
  private descripcion: string = '';
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';
  private empresas: Emprendimiento[] = [];

  constructor(categoria?: Categoria) {
    if (categoria) {
      this.idCategoriaEmpresa = categoria.idCategoriaEmpresa;
      this.nombre = categoria.nombre;
      this.descripcion = categoria.descripcion;
      this.fechaRegistro = categoria.fechaRegistro;
      this.fechaActualizacion = categoria.fechaActualizacion;
      this.estado = categoria.estado;
      this.empresas = categoria.empresas;
    }
  }

  get IdCategoriaEmpresa(): number {
    return this.idCategoriaEmpresa;
  }

  set IdCategoriaEmpresa(value: number) {
    this.idCategoriaEmpresa = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get Descripcion(): string {
    return this.descripcion;
  }

  set Descripcion(value: string) {
    this.descripcion = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get Empresas(): Emprendimiento[] {
    return this.empresas;
  }

  set Empresas(value: Emprendimiento[]) {
    this.empresas = value;
  }
}

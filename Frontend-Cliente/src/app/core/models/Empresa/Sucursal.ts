import { Departamento } from "./Departamento";
import { Emprendimiento } from "./Emprendimiento";
import { Almacen } from './Almacen';

export class Sucursal {
  private idSucursal: number = 0;
  private nombre: string = 'Principal';
  private direccion: string = '';
  private latitud: number = 0;
  private longitud: number = 0;
  private idUsuario: number = 0;
  private empresa: Emprendimiento = new Emprendimiento();
  private departamento: Departamento = new Departamento();
  private almacen: Almacen[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(sucursal?: Sucursal) {
    if (sucursal) {
      this.idSucursal = sucursal.idSucursal;
      this.nombre = sucursal.nombre;
      this.direccion = sucursal.direccion;
      this.latitud = sucursal.latitud;
      this.longitud = sucursal.longitud;
      this.idUsuario = sucursal.idUsuario;
      this.empresa = sucursal.empresa;
      this.departamento = sucursal.departamento;
      this.almacen = sucursal.almacen;
      this.fechaRegistro = sucursal.fechaRegistro;
      this.fechaActualizacion = sucursal.fechaActualizacion;
      this.estado = sucursal.estado;
    }
  }

  get IdSucursal(): number {
    return this.idSucursal;
  }

  set IdSucursal(value: number) {
    this.idSucursal = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get Direccion(): string {
    return this.direccion;
  }

  set Direccion(value: string) {
    this.direccion = value;
  }

  get Latitud(): number {
    return this.latitud;
  }

  set Latitud(value: number) {
    this.latitud = value;
  }

  get Longitud(): number {
    return this.longitud;
  }

  set Longitud(value: number) {
    this.longitud = value;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get Empresa(): Emprendimiento {
    return this.empresa;
  }

  set Empresa(value: Emprendimiento) {
    this.empresa = value;
  }

  get Departamento(): Departamento {
    return this.departamento;
  }

  set Departamento(value: Departamento) {
    this.departamento = value;
  }

  get Almacen(): Almacen[] {
    return this.almacen;
  }

  set Almacen(value: Almacen[]) {
    this.almacen = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

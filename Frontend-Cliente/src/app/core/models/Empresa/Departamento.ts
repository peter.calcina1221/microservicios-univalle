import { Emprendimiento } from "./Emprendimiento";
import { Sucursal } from "./Sucursal";

export class Departamento {
  private idDepartamento: number = 0;
  private nombre: string = '';
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';
  private empresas: Emprendimiento[] = [];
  private sucursal: Sucursal[] = [];

  constructor(departamento?: Departamento) {
    if (departamento) {
      this.idDepartamento = departamento.idDepartamento;
      this.nombre = departamento.nombre;
      this.fechaRegistro = departamento.fechaRegistro;
      this.fechaActualizacion = departamento.fechaActualizacion;
      this.estado = departamento.estado;
      if (departamento.empresas != null)
        for (let i = 0; i < departamento.empresas.length; i++) {
          this.empresas.push(new Emprendimiento(departamento.empresas[i]));
        }

      if (departamento.sucursal != null)
        for (let i = 0; i < departamento.sucursal.length; i++) {
          this.sucursal.push(new Sucursal(departamento.sucursal[i]));
        }
    }
  }

  get IdDepartamento(): number {
    return this.idDepartamento;
  }

  set IdDepartamento(value: number) {
    this.idDepartamento = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get Sucursal(): Sucursal[] {
    return this.sucursal;
  }

  set Sucursal(value: Sucursal[]) {
    this.sucursal = value;
  }

  get Empresas(): Emprendimiento[] {
    return this.empresas;
  }

  set Empresas(value: Emprendimiento[]) {
    this.empresas = value;
  }
}

export class CodigoEmpresa {
  private idCodigo: number = 0;
  private idOrganizacion: number = 1;
  private idUsuario: number = 0;
  private numeroCodigo: string = '';
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(codigoEmpresa?: CodigoEmpresa) {
    if (codigoEmpresa) {
      this.idCodigo = codigoEmpresa.idCodigo;
      this.idOrganizacion = codigoEmpresa.idOrganizacion;
      this.idUsuario = codigoEmpresa.idUsuario;
      this.numeroCodigo = codigoEmpresa.numeroCodigo;
      this.fechaRegistro = codigoEmpresa.fechaRegistro;
      this.fechaActualizacion = codigoEmpresa.fechaActualizacion;
      this.estado = codigoEmpresa.estado;
    }
  }

  get IdCodigo(): number {
    return this.idCodigo;
  }

  set IdCodigo(value: number) {
    this.idCodigo = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get NumeroCodigo(): string {
    return this.numeroCodigo;
  }

  set NumeroCodigo(value: string) {
    this.numeroCodigo = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

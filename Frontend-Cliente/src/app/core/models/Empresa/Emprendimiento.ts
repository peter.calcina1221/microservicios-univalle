import { Categoria } from "./Categoria";
import { ImagenEmpresa } from './ImagenEmpresa';
import { Sucursal } from "./Sucursal";
import { Departamento } from './Departamento';

export class Emprendimiento {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idEmpresa: number = 0;
  private nombreEmpresa: string = '';
  private numeroCelular!: string;
  private razonSocial: string = '';
  private nit!: number;
  private descripcion: string = '';
  private latitud: number = 0;
  private longitud: number = 0;
  private direccion: string = '';
  private idUsuario: number = 0;
  private idOrganizacion: number = 1;
  private categoriaEmpresa: Categoria = new Categoria();
  private departamento: Departamento = new Departamento();
  private sucursal: Sucursal[] = [];
  private imagenesEmpresa: ImagenEmpresa[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';
  private cantidadVentas: number = 0;

  constructor(emprendimiento?: Emprendimiento) {
    if (emprendimiento) {
      this.idEmpresa = emprendimiento.idEmpresa;
      this.nombreEmpresa = emprendimiento.nombreEmpresa;
      this.numeroCelular = emprendimiento.numeroCelular;
      this.razonSocial = emprendimiento.razonSocial;
      this.nit = emprendimiento.nit;
      this.descripcion = emprendimiento.descripcion;
      this.latitud = emprendimiento.latitud;
      this.longitud = emprendimiento.longitud;
      this.direccion = emprendimiento.direccion;
      this.idUsuario = emprendimiento.idUsuario;
      this.idOrganizacion = emprendimiento.idOrganizacion;
      this.categoriaEmpresa = new Categoria(emprendimiento.categoriaEmpresa);
      this.departamento = new Departamento(emprendimiento.departamento);
      if (emprendimiento.sucursal != null) {
        for (let i = 0; i < emprendimiento.sucursal.length; i++) {
          this.sucursal.push(new Sucursal(emprendimiento.sucursal[i]));
        }
      }
      if (emprendimiento.imagenesEmpresa != null) {
        for (let i = 0; i < emprendimiento.imagenesEmpresa.length; i++) {
          this.imagenesEmpresa.push(new ImagenEmpresa(emprendimiento.imagenesEmpresa[i]));
        }
      }
      this.fechaRegistro = emprendimiento.fechaRegistro;
      this.fechaActualizacion = emprendimiento.fechaActualizacion;
      this.estado = emprendimiento.estado;
      this.cantidadVentas = emprendimiento.cantidadVentas;
    }
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(value: number) {
    this.idEmpresa = value;
  }

  get NombreEmpresa(): string {
    return this.nombreEmpresa;
  }

  set NombreEmpresa(value: string) {
    this.nombreEmpresa = value;
  }

  get RazonSocial(): string {
    return this.razonSocial;
  }

  set RazonSocial(value: string) {
    this.razonSocial = value;
  }

  get NIT(): number | undefined {
    return this.nit !== undefined ? this.nit : undefined;
  }

  set NIT(value: number) {
    this.nit = value;
  }

  get Descripcion(): string {
    return this.descripcion;
  }

  set Descripcion(value: string) {
    this.descripcion = value;
  }

  get NumeroCelular(): string {
    return this.numeroCelular;
  }

  set NumeroCelular(value: string) {
    this.numeroCelular = value;
  }

  get Latitud(): number {
    return this.latitud;
  }

  set Latitud(value: number) {
    this.latitud = value;
  }

  get Longitud(): number {
    return this.longitud;
  }

  set Longitud(value: number) {
    this.longitud = value;
  }

  get Direccion(): string {
    return this.direccion;
  }

  set Direccion(value: string) {
    this.direccion = value;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  get CategoriaEmpresa(): Categoria {
    return this.categoriaEmpresa;
  }

  set CategoriaEmpresa(value: Categoria) {
    this.categoriaEmpresa = value;
  }

  get Departamento(): Departamento {
    return this.departamento;
  }

  set Departamento(value: Departamento) {
    this.departamento = value;
  }

  get Sucursal(): Sucursal[] {
    return this.sucursal;
  }

  set Sucursal(value: Sucursal[]) {
    this.sucursal = value;
  }

  get ImagenesEmpresa(): ImagenEmpresa[] {
    return this.imagenesEmpresa;
  }

  set ImagenesEmpresa(value: ImagenEmpresa[]) {
    this.imagenesEmpresa = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get CantidadVentas(): number {
    return this.cantidadVentas;
  }

  set CantidadVentas(value: number) {
    this.cantidadVentas = value;
  }
}

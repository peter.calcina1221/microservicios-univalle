
export class JwtDto {
    token:string='';
    type: string='';
    nombreUsuario='';
    authorities:string[]=[];
    idUsuario:number=0;
}

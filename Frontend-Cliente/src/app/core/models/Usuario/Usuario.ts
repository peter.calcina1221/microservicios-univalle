export class Usuario {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private id: number = 0;
  private nombre: string = '';
  private primerApellido: string = '';
  private segundoApellido: string = '';
  private fechaNacimiento: Date = new Date();
  private imagen: string = '';
  private email: string = '';
  private numeroTelefono: string = '';
  private genero: string = '';
  private nombreUsuario: string = '';
  private password: string = '';
  private roles: string[] = [];
  private idEmpresa: number = 0;
  private idOrganizacion: number = 1;
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';
  private ci: string = '';

  constructor(usuario?: Usuario) {
    if (usuario) {
      this.id = usuario.id;
      this.nombre = usuario.nombre;
      this.primerApellido = usuario.primerApellido;
      this.segundoApellido = usuario.segundoApellido;
      this.fechaNacimiento = usuario.fechaNacimiento;
      this.imagen = usuario.imagen;
      this.email = usuario.email;
      this.numeroTelefono = usuario.numeroTelefono;
      this.genero = usuario.genero;
      this.nombreUsuario = usuario.nombreUsuario;
      this.password = usuario.password;
      this.roles = usuario.roles;
      this.idEmpresa = usuario.idEmpresa;
      this.idOrganizacion = usuario.idOrganizacion;
      this.fechaRegistro = usuario.fechaRegistro;
      this.fechaActualizacion = usuario.fechaActualizacion;
      this.estado = usuario.estado;
      this.ci = usuario.ci;
    }
  }

  get IdUsuario(): number {
    return this.id;
  }

  set IdUsuario(value: number) {
    this.id = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get PrimerApellido(): string {
    return this.primerApellido;
  }

  set PrimerApellido(value: string) {
    this.primerApellido = value;
  }

  get SegundoApellido(): string {
    return this.segundoApellido;
  }

  set SegundoApellido(value: string) {
    this.segundoApellido = value;
  }

  get FechaNacimiento(): Date {
    return this.fechaNacimiento;
  }

  set FechaNacimiento(value: Date) {
    this.fechaNacimiento = value;
  }

  get ImgUrl(): string {
    return this.imagen;
  }

  set ImgUrl(value: string) {
    this.imagen = value;
  }

  get Email(): string {
    return this.email;
  }

  set Email(value: string) {
    this.email = value;
  }

  get NumeroTelefono(): string {
    return this.numeroTelefono;
  }

  set NumeroTelefono(value: string) {
    this.numeroTelefono = value;
  }

  get Genero(): string {
    return this.genero;
  }

  set Genero(value: string) {
    this.genero = value;
  }

  get NombreUsuario(): string {
    return this.nombreUsuario;
  }

  set NombreUsuario(value: string) {
    this.nombreUsuario = value;
  }

  get Password(): string {
    return this.password;
  }

  set Password(value: string) {
    this.password = value;
  }

  get Roles(): string[] {
    return this.roles;
  }

  set Roles(value: string[]) {
    this.roles = value;
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(value: number) {
    this.idEmpresa = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get Ci(): string {
    return this.ci;
  }

  set Ci(value: string) {
    this.ci = value;
  }
}

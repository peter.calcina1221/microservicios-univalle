export class Email {
  mailTo: string = '';

  constructor(mailTo: string) {
    this.mailTo = mailTo;
  }

  validarCorreo(): boolean {
    const patron = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return patron.test(this.mailTo);
  }
}



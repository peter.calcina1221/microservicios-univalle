export class LoginUsuario {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  nombreUsuario: string = '';
  password: string = '';
  idUsuario: number = 0;
  idOrganizacion: number = 1;

  constructor(loginUsuario?: LoginUsuario) {
    if (loginUsuario) {
      this.nombreUsuario = loginUsuario.nombreUsuario;
      this.password = loginUsuario.password;
      this.idUsuario = loginUsuario.idUsuario;
      this.idOrganizacion = loginUsuario.idOrganizacion;
    }
  }

  get NombreUsuario(): string {
    return this.nombreUsuario;
  }

  set NombreUsuario(nombreUsuario: string) {
    this.nombreUsuario = nombreUsuario;
  }

  get Password(): string {
    return this.password;
  }

  set Password(password: string) {
    this.password = password;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(idUsuario: number) {
    this.idUsuario = idUsuario;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(idOrganizacion: number) {
    this.idOrganizacion = idOrganizacion;
  }
}

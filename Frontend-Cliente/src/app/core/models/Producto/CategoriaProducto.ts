import { Producto } from "./Producto";

export class CategoriaProducto {
  private idCategoriaProducto: number = 0;
  private nombre: string = '';
  private descripcion: string = '';
  private productos: Producto[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(categoriaProducto?: CategoriaProducto) {
    if (categoriaProducto) {
      this.idCategoriaProducto = categoriaProducto.idCategoriaProducto;
      this.nombre = categoriaProducto.nombre;
      this.descripcion = categoriaProducto.descripcion;
      if (categoriaProducto.productos != null) {
        for (let i = 0; i < categoriaProducto.productos.length; i++) {
          this.productos.push(new Producto(categoriaProducto.productos[i]));
        }
      }
      this.fechaRegistro = categoriaProducto.fechaRegistro;
      this.fechaActualizacion = categoriaProducto.fechaActualizacion;
      this.estado = categoriaProducto.estado;
    }
  }

  get IdCategoria(): number {
    return this.idCategoriaProducto;
  }

  set IdCategoria(value: number) {
    this.idCategoriaProducto = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get Descripcion(): string {
    return this.descripcion;
  }

  set Descripcion(value: string) {
    this.descripcion = value;
  }

  get Productos(): Producto[] {
    return this.productos;
  }

  set Productos(value: Producto[]) {
    this.productos = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

import { CaracteristicaProducto } from "./CaracteristicaProducto";

export class Caracteristica {
  private idCaracteristica: number = 0;
  private nombre: string = '';
  private descripcion: string = '';
  private caracteristicaProductos: CaracteristicaProducto[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(caracteristica?: Caracteristica) {
    if (caracteristica) {
      this.idCaracteristica = caracteristica.idCaracteristica;
      this.nombre = caracteristica.nombre;
      this.descripcion = caracteristica.descripcion;
      if (caracteristica.caracteristicaProductos != null)
        for (let i = 0; i < caracteristica.caracteristicaProductos.length; i++) {
          this.caracteristicaProductos.push(new CaracteristicaProducto(caracteristica.caracteristicaProductos[i]));
        }
      this.fechaRegistro = caracteristica.fechaRegistro;
      this.fechaActualizacion = caracteristica.fechaActualizacion;
      this.estado = caracteristica.estado;
    }
  }

  get IdCaracteristica(): number {
    return this.idCaracteristica;
  }

  set IdCaracteristica(value: number) {
    this.idCaracteristica = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get Descripcion(): string {
    return this.descripcion;
  }

  set Descripcion(value: string) {
    this.descripcion = value;
  }

  get CaracteristicaProductos(): CaracteristicaProducto[] {
    return this.caracteristicaProductos;
  }

  set CaracteristicaProductos(value: CaracteristicaProducto[]) {
    this.caracteristicaProductos = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

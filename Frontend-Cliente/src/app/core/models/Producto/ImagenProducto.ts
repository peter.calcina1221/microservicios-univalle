import { Producto } from "./Producto";

export class ImagenProducto {
  private idImagenProducto: number = 0;
  private producto: Producto = new Producto();
  private imgUrl: string = '';
  private principal: boolean = false;
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(imagenProducto?: ImagenProducto) {
    if (imagenProducto) {
      this.idImagenProducto = imagenProducto.idImagenProducto;
      this.imgUrl = imagenProducto.imgUrl;
      this.principal = imagenProducto.principal;
      this.producto = imagenProducto.producto;
      this.fechaRegistro = imagenProducto.fechaRegistro;
      this.fechaActualizacion = imagenProducto.fechaActualizacion;
      this.estado = imagenProducto.estado;
    }
  }

  get IdImagenProducto(): number {
    return this.idImagenProducto;
  }

  set IdImagenProducto(value: number) {
    this.idImagenProducto = value;
  }

  get Producto(): Producto {
    return this.producto;
  }

  set Producto(value: Producto) {
    this.producto = value;
  }

  get ImgUrl(): string {
    return this.imgUrl;
  }

  set ImgUrl(value: string) {
    this.imgUrl = value;
  }

  get Principal(): boolean {
    return this.principal;
  }

  set Principal(value: boolean) {
    this.principal = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

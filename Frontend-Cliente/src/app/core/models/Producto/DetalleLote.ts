import { Lote } from "./Lote";
import { Producto } from "./Producto";

export class DetalleLote {
  private idDetalleLote: number = 0;
  private idAlmacen: number = 0;
  private fechaIngreso: Date = new Date();
  private cantidad: number = 0;
  private precioCompra: number = 0;
  private precioVenta: number = 0;
  private porcentajeGanancia: number = 0;
  private porcentajeDescuento: number = 0;
  private producto: Producto = new Producto();
  private lote: Lote = new Lote();
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(detalleLote?: DetalleLote) {
    if(detalleLote) {
      this.idDetalleLote = detalleLote.idDetalleLote;
      this.idAlmacen = detalleLote.idAlmacen;
      this.fechaIngreso = detalleLote.fechaIngreso;
      this.cantidad = detalleLote.cantidad;
      this.precioCompra = detalleLote.precioCompra;
      this.precioVenta = detalleLote.precioVenta;
      this.porcentajeGanancia = detalleLote.porcentajeGanancia;
      this.porcentajeDescuento = detalleLote.porcentajeDescuento;
      this.producto = new Producto(detalleLote.producto);
      this.lote = new Lote(detalleLote.lote);
      this.fechaRegistro = detalleLote.fechaRegistro;
      this.fechaActualizacion = detalleLote.fechaActualizacion;
      this.estado = detalleLote.estado;
    }
  }

  get IdDetalleLote(): number {
    return this.idDetalleLote;
  }

  set IdDetalleLote(value: number) {
    this.idDetalleLote = value;
  }

  get IdAlmacen(): number {
    return this.idAlmacen;
  }

  set IdAlmacen(value: number) {
    this.idAlmacen = value;
  }

  get FechaIngreso(): Date {
    return this.fechaIngreso;
  }

  set FechaIngreso(value: Date) {
    this.fechaIngreso = value;
  }

  get Cantidad(): number {
    return this.cantidad;
  }

  set Cantidad(value: number) {
    this.cantidad = value;
  }

  get PrecioCompra(): number {
    return this.precioCompra;
  }

  set PrecioCompra(value: number) {
    this.precioCompra = value;
  }

  get PrecioVenta(): number {
    return this.precioVenta;
  }

  set PrecioVenta(value: number) {
    this.precioVenta = value;
  }

  get PorcentajeGanancia(): number {
    return this.porcentajeGanancia;
  }

  set PorcentajeGanancia(value: number) {
    this.porcentajeGanancia = value;
  }

  get PorcentajeDescuento(): number {
    return this.porcentajeDescuento;
  }

  set PorcentajeDescuento(value: number) {
    this.porcentajeDescuento = value;
  }

  get Producto(): Producto {
    return this.producto;
  }

  set Producto(value: Producto) {
    this.producto = value;
  }

  get Lote(): Lote {
    return this.lote;
  }

  set Lote(value: Lote) {
    this.lote = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

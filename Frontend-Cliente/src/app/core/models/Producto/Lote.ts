import { DetalleLote } from "./DetalleLote";

export class Lote {
  private idLote: number = 0;
  private idEmpresa: number = 0;
  private codigoLote: string = '';
  private cantidadProductos: number = 0;
  private fechaFabricacion: Date = new Date();
  private fechaVencimiento: Date = new Date();
  private detalleLotes: DetalleLote[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '';

  constructor(lote?: Lote) {
    if (lote) {
      this.idLote = lote.idLote;
      this.idEmpresa = lote.idEmpresa;
      this.codigoLote = lote.codigoLote;
      this.cantidadProductos = lote.cantidadProductos;
      this.fechaFabricacion = lote.fechaFabricacion;
      this.fechaVencimiento = lote.fechaVencimiento;
      if (lote.detalleLotes != null)
        for (let i = 0; i < lote.detalleLotes.length; i++) {
          this.detalleLotes.push(new DetalleLote(lote.detalleLotes[i]));
        }
      this.fechaRegistro = lote.fechaRegistro;
      this.fechaActualizacion = lote.fechaActualizacion;
      this.estado = lote.estado;
    }
  }

  get IdLote(): number {
    return this.idLote;
  }

  set IdLote(value: number) {
    this.idLote = value;
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(value: number) {
    this.idEmpresa = value;
  }

  get CodigoLote(): string {
    return this.codigoLote;
  }

  set CodigoLote(value: string) {
    this.codigoLote = value;
  }

  get CantidadProductos(): number {
    return this.cantidadProductos;
  }

  set CantidadProductos(value: number) {
    this.cantidadProductos = value;
  }

  get FechaFabricacion(): Date {
    return this.fechaFabricacion;
  }

  set FechaFabricacion(value: Date) {
    this.fechaFabricacion = value;
  }

  get FechaVencimiento(): Date {
    return this.fechaVencimiento;
  }

  set FechaVencimiento(value: Date) {
    this.fechaVencimiento = value;
  }

  get DetalleLotes(): DetalleLote[] {
    return this.detalleLotes;
  }

  set DetalleLotes(value: DetalleLote[]) {
    this.detalleLotes = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }
}

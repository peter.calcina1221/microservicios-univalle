import { ImagenProducto } from "./ImagenProducto";
import { CategoriaProducto } from './CategoriaProducto';
import { CaracteristicaProducto } from "./CaracteristicaProducto";
import { DetalleLote } from "./DetalleLote";

export class Producto {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idProducto: number = 0;
  private nombre: string = '';
  private descripcion: string = '';
  private codigo: string = '';
  private precioCosto: number = 0;
  private precioVenta: number = 0;
  private stockMinimo: number = 0;
  private stockRestante: number = 0;
  private stockTotal: number = 0;
  private codigoUnidadSIN: string = '';
  private codigoProductoSIN: string = '';
  private idOrganizacion: number = 1;
  private idEmpresa: number = 0;
  private imagenesProducto: ImagenProducto[] = [];
  private categoriaProducto: CategoriaProducto = new CategoriaProducto();
  private caracteristicaProductos: CaracteristicaProducto[] = [];
  private detalleLotes: DetalleLote[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';
  private cantidadVendida: number = 0;
  private subTotal: number = 0;

  constructor(producto?: Producto) {
    if (producto) {
      this.idProducto = producto.idProducto;
      this.nombre = producto.nombre;
      this.descripcion = producto.descripcion;
      this.codigo = producto.codigo;
      this.precioCosto = producto.precioCosto;
      this.precioVenta = producto.precioVenta;
      this.stockMinimo = producto.stockMinimo;
      this.stockRestante = producto.stockRestante;
      this.stockTotal = producto.stockTotal;
      this.codigoUnidadSIN = producto.codigoUnidadSIN;
      this.codigoProductoSIN = producto.codigoProductoSIN;
      this.idOrganizacion = producto.idOrganizacion;
      this.idEmpresa = producto.idEmpresa;
      this.categoriaProducto = new CategoriaProducto(producto.categoriaProducto);
      if (producto.imagenesProducto != null)
        for (let i = 0; i < producto.imagenesProducto.length; i++) {
          this.imagenesProducto.push(new ImagenProducto(producto.imagenesProducto[i]));
        }
      if (producto.caracteristicaProductos != null)
        for (let i = 0; i < producto.caracteristicaProductos.length; i++) {
          this.caracteristicaProductos.push(new CaracteristicaProducto(producto.caracteristicaProductos[i]));
        }
      if (producto.detalleLotes != null)
        for (let i = 0; i < producto.detalleLotes.length; i++) {
          this.detalleLotes.push(new DetalleLote(producto.detalleLotes[i]));
        }
      this.fechaRegistro = producto.fechaRegistro;
      this.fechaActualizacion = producto.fechaActualizacion;
      this.estado = producto.estado;
      this.cantidadVendida = producto.cantidadVendida;
      this.subTotal = producto.subTotal;
    }
  }

  get IdProducto(): number {
    return this.idProducto;
  }

  set IdProducto(value: number) {
    this.idProducto = value;
  }

  get Nombre(): string {
    return this.nombre;
  }

  set Nombre(value: string) {
    this.nombre = value;
  }

  get Descripcion(): string {
    return this.descripcion;
  }

  set Descripcion(value: string) {
    this.descripcion = value;
  }

  get Codigo(): string {
    return this.codigo;
  }

  set Codigo(value: string) {
    this.codigo = value;
  }

  get PrecioCosto(): number {
    return this.precioCosto;
  }

  set PrecioCosto(value: number) {
    this.precioCosto = value;
  }

  get PrecioVenta(): number {
    return this.precioVenta;
  }

  set PrecioVenta(value: number) {
    this.precioVenta = value;
  }

  get StockMinimo(): number {
    return this.stockMinimo;
  }

  set StockMinimo(value: number) {
    this.stockMinimo = value;
  }

  get StockRestante(): number {
    return this.stockRestante;
  }

  set StockRestante(value: number) {
    this.stockRestante = value;
  }

  get StockTotal(): number {
    return this.stockTotal;
  }

  set StockTotal(value: number) {
    this.stockTotal = value;
  }

  get CodigoUnidadSIN(): string {
    return this.codigoUnidadSIN;
  }

  set CodigoUnidadSIN(value: string) {
    this.codigoUnidadSIN = value;
  }

  get CodigoProductoSIN(): string {
    return this.codigoProductoSIN;
  }

  set CodigoProductoSIN(value: string) {
    this.codigoProductoSIN = value;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(value: number) {
    this.idOrganizacion = value;
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(value: number) {
    this.idEmpresa = value;
  }

  get ImagenesProducto(): ImagenProducto[] {
    return this.imagenesProducto;
  }

  set ImagenesProducto(value: ImagenProducto[]) {
    this.imagenesProducto = value;
  }

  get CategoriaProducto(): CategoriaProducto {
    return this.categoriaProducto;
  }

  set CategoriaProducto(value: CategoriaProducto) {
    this.categoriaProducto = value;
  }

  get CaracteristicaProductos(): CaracteristicaProducto[] {
    return this.caracteristicaProductos;
  }

  set CaracteristicaProductos(value: CaracteristicaProducto[]) {
    this.caracteristicaProductos = value;
  }

  get DetalleLotes(): DetalleLote[] {
    return this.detalleLotes;
  }

  set DetalleLotes(value: DetalleLote[]) {
    this.detalleLotes = value;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(value: Date) {
    this.fechaRegistro = value;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(value: Date) {
    this.fechaActualizacion = value;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(value: string) {
    this.estado = value;
  }

  get CantidadVendida(): number {
    return this.cantidadVendida;
  }

  set CantidadVendida(value: number) {
    this.cantidadVendida = value;
  }

  get SubTotal(): number {
    return this.subTotal;
  }

  set SubTotal(value: number) {
    this.subTotal = value;
  }

  obtenerEstadoPorStock(stockRestante: number, stockTotal: number, unidadMedida: string): string {
    if (stockRestante > stockTotal * 0.5) {
      return 'Stock alto: ' + stockRestante + ' ' + unidadMedida;
    }
    if (stockRestante > stockTotal * 0.25) {
      return 'Stock medio: ' + stockRestante + ' ' + unidadMedida;
    }
    if (stockRestante > 0) {
      return 'Stock bajo: ' + stockRestante + ' ' + unidadMedida;
    }

    return 'Agotado';
  }

  obtenerClasePorStock(stockRestante: number, stockTotal: number): string {
    if (stockRestante > stockTotal * 0.5) {
      return 'bg-success';
    }
    if (stockRestante > stockTotal * 0.25) {
      return 'bg-warning';
    }
    if (stockRestante > 0) {
      return 'bg-danger';
    }

    return 'bg-dark';
  }
}

import { Caracteristica } from './Caracteristica';
import { Producto } from './Producto';
export class CaracteristicaProducto {
  private idCarecteristicaProducto: number = 0;
  private producto: Producto = new Producto();
  private caracteristica: Caracteristica = new Caracteristica();
  private valor: string = '';

  constructor(caracteristicaProducto?: CaracteristicaProducto) {
    if (caracteristicaProducto) {
      this.idCarecteristicaProducto = caracteristicaProducto.idCarecteristicaProducto;
      this.producto = new Producto(caracteristicaProducto.producto);
      this.caracteristica = new Caracteristica(caracteristicaProducto.caracteristica);
      this.valor = caracteristicaProducto.valor;
    }
  }

  get IdCarecteristicaProducto(): number {
    return this.idCarecteristicaProducto;
  }

  set IdCarecteristicaProducto(value: number) {
    this.idCarecteristicaProducto = value;
  }

  get Producto(): Producto {
    return this.producto;
  }

  set Producto(value: Producto) {
    this.producto = value;
  }

  get Caracteristica(): Caracteristica {
    return this.caracteristica;
  }

  set Caracteristica(value: Caracteristica) {
    this.caracteristica = value;
  }

  get Valor(): string {
    return this.valor;
  }

  set Valor(value: string) {
    this.valor = value;
  }
}

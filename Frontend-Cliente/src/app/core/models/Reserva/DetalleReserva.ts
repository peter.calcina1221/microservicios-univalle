import { Reserva } from "./Reserva";

export class DetalleReserva {
  [key: string]: any;  //Usamos la key para identificar los atributos del objeto
  private idDetalleReserva: number = 0;
  private idProducto: number = 0;
  private idEmpresa: number = 0;
  private cantidad: number = 0;
  private precioUnitario: number = 0;
  private descuento: number = 0;
  private subTotal: number = 0;
  private codigoReserva: string = '';
  private reserva: Reserva = new Reserva();
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(detalleReserva?: DetalleReserva) {
    if (detalleReserva) {
      this.idDetalleReserva = detalleReserva.idDetalleReserva;
      this.idProducto = detalleReserva.idProducto;
      this.idEmpresa = detalleReserva.idEmpresa;
      this.cantidad = detalleReserva.cantidad;
      this.precioUnitario = detalleReserva.precioUnitario;
      this.descuento = detalleReserva.descuento;
      this.subTotal = detalleReserva.subTotal;
      this.codigoReserva = detalleReserva.codigoReserva;
      this.reserva = new Reserva(detalleReserva.reserva);
      this.fechaRegistro = detalleReserva.fechaRegistro;
      this.fechaActualizacion = detalleReserva.fechaActualizacion;
      this.estado = detalleReserva.estado;
    }
  }

  get IdDetalleReserva(): number {
    return this.idDetalleReserva;
  }

  set IdDetalleReserva(idDetalleReserva: number) {
    this.idDetalleReserva = idDetalleReserva;
  }

  get IdProducto(): number {
    return this.idProducto;
  }

  set IdProducto(idProducto: number) {
    this.idProducto = idProducto;
  }

  get IdEmpresa(): number {
    return this.idEmpresa;
  }

  set IdEmpresa(idEmpresa: number) {
    this.idEmpresa = idEmpresa;
  }

  get Cantidad(): number {
    return this.cantidad;
  }

  set Cantidad(cantidad: number) {
    this.cantidad = cantidad;
  }

  get PrecioUnitario(): number {
    return this.precioUnitario;
  }

  set PrecioUnitario(precioUnitario: number) {
    this.precioUnitario = precioUnitario;
  }

  get Descuento(): number {
    return this.descuento;
  }

  set Descuento(descuento: number) {
    this.descuento = descuento;
  }

  get SubTotal(): number {
    return this.subTotal;
  }

  set SubTotal(subTotal: number) {
    this.subTotal = subTotal;
  }

  get CodigoReserva(): string {
    return this.codigoReserva;
  }

  set CodigoReserva(codigoReserva: string) {
    this.codigoReserva = codigoReserva;
  }

  get Reserva(): Reserva {
    return this.reserva;
  }

  set Reserva(reserva: Reserva) {
    this.reserva = reserva;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(fechaRegistro: Date) {
    this.fechaRegistro = fechaRegistro;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(fechaActualizacion: Date) {
    this.fechaActualizacion = fechaActualizacion;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(estado: string) {
    this.estado = estado;
  }
}

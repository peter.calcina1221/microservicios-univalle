import { DetalleReserva } from "./DetalleReserva";

export class Reserva {
  private idReserva: number = 0;
  private idUsuario: number = 0;
  private idOrganizacion: number = 1;
  private codigoReserva: string = '';
  private fechaSolicitud: Date = new Date();
  private fechaEntrega: Date = new Date();
  private total: number = 0;
  private detalleReservas: DetalleReserva[] = [];
  private fechaRegistro: Date = new Date();
  private fechaActualizacion: Date = new Date();
  private estado: string = '1';

  constructor(reserva?: Reserva) {
    if (reserva) {
      this.idReserva = reserva.idReserva;
      this.idUsuario = reserva.idUsuario;
      this.idOrganizacion = reserva.idOrganizacion;
      this.codigoReserva = reserva.codigoReserva;
      this.fechaSolicitud = reserva.fechaSolicitud;
      this.fechaEntrega = reserva.fechaEntrega;
      this.total = reserva.total;
      if (reserva.detalleReservas)
        for (let i = 0; i < reserva.detalleReservas.length; i++) {
          this.detalleReservas.push(new DetalleReserva(reserva.detalleReservas[i]));
        }
      this.fechaRegistro = reserva.fechaRegistro;
      this.fechaActualizacion = reserva.fechaActualizacion;
      this.estado = reserva.estado;
    }
  }

  get IdReserva(): number {
    return this.idReserva;
  }

  set IdReserva(idReserva: number) {
    this.idReserva = idReserva;
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(idUsuario: number) {
    this.idUsuario = idUsuario;
  }

  get IdOrganizacion(): number {
    return this.idOrganizacion;
  }

  set IdOrganizacion(idOrganizacion: number) {
    this.idOrganizacion = idOrganizacion;
  }

  get CodigoReserva(): string {
    return this.codigoReserva;
  }

  set CodigoReserva(codigoReserva: string) {
    this.codigoReserva = codigoReserva;
  }

  get FechaSolicitud(): Date {
    return this.fechaSolicitud;
  }

  set FechaSolicitud(fechaSolicitud: Date) {
    this.fechaSolicitud = fechaSolicitud;
  }

  get FechaEntrega(): Date {
    return this.fechaEntrega;
  }

  set FechaEntrega(fechaEntrega: Date) {
    this.fechaEntrega = fechaEntrega;
  }

  get Total(): number {
    return this.total;
  }

  set Total(total: number) {
    this.total = total;
  }

  get DetalleReservas(): DetalleReserva[] {
    return this.detalleReservas;
  }

  set DetalleReservas(detalleReservas: DetalleReserva[]) {
    this.detalleReservas = detalleReservas;
  }

  get FechaRegistro(): Date {
    return this.fechaRegistro;
  }

  set FechaRegistro(fechaRegistro: Date) {
    this.fechaRegistro = fechaRegistro;
  }

  get FechaActualizacion(): Date {
    return this.fechaActualizacion;
  }

  set FechaActualizacion(fechaActualizacion: Date) {
    this.fechaActualizacion = fechaActualizacion;
  }

  get Estado(): string {
    return this.estado;
  }

  set Estado(estado: string) {
    this.estado = estado;
  }

  //Método para generar el código de la reserva
  generarCodigoReserva(): string {
    let numeroAleatorio = Math.floor(Math.random() * (9999 - 0)) + 0;
    return this.codigoReserva = 'R' + this.idUsuario + '-' + this.fechaRegistro.getFullYear() + '-' + (this.fechaRegistro.getMonth() + 1) + '-' + this.fechaRegistro.getDate() + '-' + numeroAleatorio;
  }

  obtenerEstadoReserva(estadoActivo: number): string {
    switch (estadoActivo) {
      case 0:
        return 'Esperando confirmación';
      case 1:
        return 'Confirmado';
      case 2:
        return 'Listo para recoger';
      case 3:
        return 'Entregado';
      default:
        return 'Esperando confirmación';
    }
  }
}

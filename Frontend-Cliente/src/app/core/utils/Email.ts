export class Email {
  private idUsuario: number = 0;
  private titulo: string = '';
  private mailTo: string = '';
  private subject: string = '';
  private urlImagen: string = '';
  private mensaje: string = '';

  constructor(email?: Email) {
    if(email) {
      this.idUsuario = email.idUsuario;
      this.titulo = email.titulo;
      this.mailTo = email.mailTo;
      this.subject = email.subject;
      this.urlImagen = email.urlImagen;
      this.mensaje = email.mensaje;
    }
  }

  get IdUsuario(): number {
    return this.idUsuario;
  }

  set IdUsuario(value: number) {
    this.idUsuario = value;
  }

  get Titulo(): string {
    return this.titulo;
  }

  set Titulo(value: string) {
    this.titulo = value;
  }

  get MailTo(): string {
    return this.mailTo;
  }

  set MailTo(value: string) {
    this.mailTo = value;
  }

  get Subject(): string {
    return this.subject;
  }

  set Subject(value: string) {
    this.subject = value;
  }

  get UrlImagen(): string {
    return this.urlImagen;
  }

  set UrlImagen(value: string) {
    this.urlImagen = value;
  }

  get Mensaje(): string {
    return this.mensaje;
  }

  set Mensaje(value: string) {
    this.mensaje = value;
  }
}

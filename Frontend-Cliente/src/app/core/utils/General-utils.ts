import * as CryptoJS from 'crypto-js';
import { KeyService } from '../../services/other/key.service';

export class GeneralUtils {
  private secretKey = '';

  constructor(
    private secretKeyService?: KeyService
  ) { }

  //Método para encriptar el nombre de la imagen
  static encryptFileName(fileName: string): string {
    return CryptoJS.SHA256(fileName).toString();
  }

  //Método para validar campos vacíos
  static validateEmptyFields(object: any, validateField: any[]): boolean {
    let isValid = true;
    validateField.forEach(field => {
      if (object[field] == null || object[field] == '') {
        isValid = false;
      }
    });

    return isValid;
  }

  //Método para validar número de celular con 8 dígitos
  static validateCellphone(cellphone: string): boolean {
    return cellphone.toString().length == 8;
  }

  //Método para validar email con expresión regular
  static validateEmail(email: string): boolean {
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return emailRegex.test(email);
  }

  static validateLongitudLatitud(longitud: number, latitud: number): boolean {
    return longitud != 0 && latitud != 0;
  }

  // Encriptar el ID utilizando AES
  encryptId(id: any): string {
    this.secretKeyService?.getApiKey().subscribe(key => this.secretKey = key.secretKey);
    const ciphertext = CryptoJS.AES.encrypt(id.toString(), this.secretKey).toString();
    return encodeURIComponent(ciphertext); // Codificar el resultado para que sea URL-safe
  }

  // Desencriptar el ID utilizando AES
  decryptId(encryptedId: any): any {
    try {
      const ciphertext = decodeURIComponent(encryptedId);
      const bytes = CryptoJS.AES.decrypt(ciphertext, this.secretKey);
      const originalText = bytes.toString(CryptoJS.enc.Utf8);
      return parseInt(originalText, 10);
    } catch (error) {
      console.error('Error al desencriptar el ID:', error);
      return 0;
    }
  }

  static validateSpecialCharacters(event: string): boolean {
    const regex = /^[a-zA-Z0-9.,\-_\s:;áéíóúÁÉÍÓÚüÜñÑ´]*$/;
    return regex.test(event);
  }
}


import { Component, EventEmitter, Input, OnChanges, Output, ViewEncapsulation } from '@angular/core';
import { EmprendimientoService } from '../../../../services/Empresa/emprendimiento.service';
import { CategoriaService } from 'src/app/services/Empresa/categoria.service';
import { Emprendimiento } from '../../../models/Empresa/Emprendimiento';
import { Categoria } from '../../../models/Empresa/Categoria';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { switchMap } from 'rxjs/operators';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { Router } from '@angular/router';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { ToastService } from 'src/app/services/other/toastService.service';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';
import { Estadistica } from 'src/app/core/models/Calificacion/Estadistica';

interface DatosCambiados {
  cantidad: number;
  producto: Producto;
  imgUrl: string;
}

@Component({
  selector: 'app-tarjeta-informacion',
  templateUrl: './tarjeta-informacion.component.html',
  styleUrls: ['./tarjeta-informacion.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class TarjetaInformacionComponent implements OnChanges {
  @Input() datos: { id: number, show: boolean, isProduct: boolean } = { id: 0, show: false, isProduct: true };
  @Output() cerrarDialogInformacion = new EventEmitter<void>();
  @Output() datosCambiados = new EventEmitter<DatosCambiados>();

  //Instanciamos los objetos que vamos a utilizar.
  emprendimiento: Emprendimiento;
  categoria: Categoria;
  producto: Producto;
  general: GeneralUtils = new GeneralUtils();

  listaImagenes: string[] = [];
  imagenPrincipal: string = '';
  cantidad: number = 1;

  calificacion: number = 0;

  //Nombre de la unidad SIN
  nombreUnidadSIN: string = '';

  constructor(
    private emprendimientoService: EmprendimientoService,
    private productoService: ProductoService,
    private toastService: ToastService,
    private estadisticaService: EstadisticaService,
    private router: Router
  ) {
    this.emprendimiento = new Emprendimiento();
    this.categoria = new Categoria();
    this.producto = new Producto();
  }

  ngOnChanges(): void {
    this.obtenerDatos();
  }

  cerrarDialog() {
    this.cerrarDialogInformacion.emit();
  }

  obtenerDatos() {
    if (this.datos && this.datos.id) {
      if (this.datos.isProduct) {
        this.obtenerDatosProductoPorId(this.datos.id);
      } else {
        this.obtenerDatosEmprendimientoPorId(this.datos.id);
      }
    }
  }

  obtenerNombreUnidadSIN() {
    this.productoService.obtenerNombreUnidadSIN(this.producto.CodigoUnidadSIN).subscribe({
      next: (unidadSIN: any) => this.nombreUnidadSIN = unidadSIN.nombre,
      error: () => this.toastService.mensajeError('Error', 'Ocurrio un error al obtener el nombre de la unidad', 5000)
    })
  }

  obtenerDatosProductoPorId(idProducto: number) {
    this.listaImagenes = [];

    this.productoService.obtenerProductoPorId(idProducto).subscribe({
      next: (producto) => {
        this.producto = new Producto(producto);
        this.obtenerNombreUnidadSIN();
        this.imagenPrincipal = this.producto.ImagenesProducto.sort((a) => a.Principal ? -1 : 1)[0].ImgUrl;
        this.listaImagenes = this.producto.ImagenesProducto.map(item => item.ImgUrl);
      }
    });
  }

  obtenerDatosEmprendimientoPorId(idEmprendimiento: number) {
    this.emprendimientoService.obtenerEmprendimientoPorId(idEmprendimiento).subscribe({
      next: (emprendimiento) => {
        this.emprendimiento = new Emprendimiento(emprendimiento);
        this.imagenPrincipal = this.emprendimiento.ImagenesEmpresa[0].ImgUrl;
        this.listaImagenes = this.emprendimiento.ImagenesEmpresa.map(item => item.ImgUrl);
        this.estadisticaService.obtenerCalificacion(idEmprendimiento).subscribe({
          next: (estadistica) => {
            this.calificacion = new Estadistica(estadistica).Promedio;
          },
          error: (error) => {
            console.error('Error al obtener la estadistica del emprendimiento:', error);
            this.toastService.mensajeError('Error', 'Error al obtener la calificación del emprendimiento', 3000);
          }
        });
      },
      error: (error) => {
        console.error('Error al obtener los datos de la emprendedora:', error);
        this.toastService.mensajeError('Error', 'Error al obtener los datos del emprendimiento', 3000);
      }
    });
  }

  verEmprendimiento(idEmprendimiento: number) {
    // Redirigir a la página de vistaEmprendimiento
    const newId = this.general.encryptId(idEmprendimiento);
    this.router.navigate(['/vistaEmprendimiento', newId]);
  }

  /**
   * Cambia la imagen más grande por cualquiera de las imagenes pequeñas seleccionables
   * @param { string } imagen URL de la imagen a cambiar.
   * @returns Cambia la imagen grande por la seleccionada.
   */
  cambiarImagenPrincipal(imagen: string) {
    this.imagenPrincipal = imagen;
  }
}

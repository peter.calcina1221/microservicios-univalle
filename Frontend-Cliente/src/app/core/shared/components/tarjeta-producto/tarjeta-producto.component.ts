import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { CarritoService } from 'src/app/services/other/carritoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { ConfirmationService } from 'primeng/api';
import { ReservaService } from 'src/app/services/Reserva/reserva.service';
import { Reserva } from 'src/app/core/models/Reserva/Reserva';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { Email } from 'src/app/core/utils/Email';
import { ProductoService } from 'src/app/services/Producto/producto.service';

@Component({
  selector: 'app-tarjeta-producto',
  templateUrl: './tarjeta-producto.component.html',
  styleUrls: ['./tarjeta-producto.component.css'],
  providers: [ConfirmationService],
  encapsulation: ViewEncapsulation.None,
})

export class TarjetaProductoComponent implements OnInit {
  @Input() producto: Producto = new Producto();
  imagenProductoUrl: string = '';
  isHover: boolean = false;

  @Output() enviarDatos = new EventEmitter<{ id: number; show: boolean; isProduct: boolean }>();

  //Datos para añadir el producto al carrito
  productoSeleccionado: Producto = new Producto();
  imgUrl: string = '';
  cantidad: number = 0;

  //Reserva de producto
  reserva: Reserva = new Reserva();
  detalleReserva: DetalleReserva = new DetalleReserva();
  mostrarDialogReserva: boolean = false;
  fechaRecojo: Date = new Date();
  fechaReserva: Date = new Date();
  cantidadReserva: number = 1;

  //Unidad SIN
  nombreUnidadSIN: string = '';

  isNotResponsive = true;

  constructor(
    private toastService: ToastService,
    private carritoService: CarritoService,
    private confirmationService: ConfirmationService,
    private reservaService: ReservaService,
    private productoService: ProductoService,
    private tokenService: TokenService,
  ) { }

  ngOnInit() {
    this.obtenerUnidadSIN();
    this.obtenerImagenProducto();

    //Comprobamos si no esta responsivo, si esta cambiamos a false
    this.isNotResponsive = window.innerWidth < 750 ? false : true;
  }

  obtenerUnidadSIN() {
    this.productoService.obtenerNombreUnidadSIN(this.producto.CodigoUnidadSIN).subscribe({
      next: (unidadSIN: any) => this.nombreUnidadSIN = unidadSIN.nombre,
      error: (e) => this.toastService.mensajeError('Error al obtener unidad SIN', 'Ocurrió un error al obtener la unidad SIN del producto.', 4000)
    })
  }

  obtenerImagenProducto() {
    this.imagenProductoUrl = this.producto.ImagenesProducto.sort((a) => {
      return a.Principal ? -1 : 1;
    })[0].ImgUrl;
  }

  agregarCarrito(producto: Producto, imgUrl: string) {
    let nuevoProducto = {
      IdProducto: producto.IdProducto,
      IdEmpresa: producto.IdEmpresa,
      Nombre: producto.Nombre,
      ImgUrl: imgUrl,
      Categoria: producto.CategoriaProducto.Nombre,
      PrecioVenta: producto.PrecioVenta,
      Stock: producto.StockRestante,
      Cantidad: 1
    }

    this.carritoService.agregarProducto(nuevoProducto).then((result: any) => {
      if (result.success)
        this.toastService.mensajeExito('Producto agregado al carrito', 'El producto se agregó al carrito correctamente.', 4000);
      else
        this.toastService.mensajeError('Error al agregar producto al carrito', 'Ocurrió un error al agregar el producto al carrito.', 4000);
    }).catch((error: any) => {
      console.log('Error al agregar producto al carrito', error);
      this.toastService.mensajeError('Error al agregar producto al carrito', 'Ocurrió un error al agregar el producto al carrito.', 4000);
    }
    );
  }

  confirmarReserva(producto: Producto) {
    if (this.fechaRecojo < new Date()) {
      this.toastService.mensajeError('Error al realizar reserva', 'La fecha de recojo no puede ser menor a la fecha actual.', 4000);
    } else {
      this.confirmationService.confirm({
        message: `¿Está seguro que desea reservar este producto? <strong>${producto.Nombre}</strong>`,
        accept: () => {
          this.reservarProducto(producto);
        },
        reject: () => {
          this.toastService.mensajeError('Reserva cancelada', 'La reserva se canceló.', 4000);
        }
      });
    }
  }

  reservarProducto(producto: Producto) {
    this.reserva = new Reserva();
    this.detalleReserva = new DetalleReserva();
    this.reserva.IdUsuario = this.tokenService.getIdUsuario();
    this.reserva.FechaSolicitud = this.fechaReserva;
    this.reserva.FechaEntrega = this.fechaRecojo;
    this.reserva.Total = producto.PrecioVenta * this.cantidadReserva;
    this.reserva.FechaRegistro = new Date();
    this.reserva.FechaActualizacion = new Date();

    this.detalleReserva.Cantidad = this.cantidadReserva;
    this.detalleReserva.PrecioUnitario = producto.PrecioVenta;
    this.detalleReserva.SubTotal = producto.PrecioVenta * this.cantidadReserva;
    this.detalleReserva.IdEmpresa = producto.IdEmpresa;
    this.detalleReserva.IdProducto = producto.IdProducto;
    this.detalleReserva.FechaRegistro = new Date();
    this.detalleReserva.FechaActualizacion = new Date();

    this.reserva.CodigoReserva = this.detalleReserva.CodigoReserva = this.reserva.generarCodigoReserva();

    this.reserva.DetalleReservas.push(this.detalleReserva);

    this.reservaService.registrarReserva(this.reserva).subscribe({
      next: (result: any) => {
        console.log(result);
        this.toastService.mensajeExito('Reserva realizada', 'La solicitud de reserva se realizo correctamente.', 4000);
        this.enviarCorreo(this.reserva, producto.Nombre);
        this.limpiarReserva();
      },
      error: (error: any) => {
        console.log(error);
        this.toastService.mensajeError('Error al realizar reserva', 'Ocurrió un error al realizar la reserva.', 4000);
      }
    });
  }

  limpiarReserva() {
    this.reserva = new Reserva();
    this.detalleReserva = new DetalleReserva();
    this.mostrarDialogReserva = false;
    this.fechaRecojo = new Date();
    this.fechaReserva = new Date();
    this.cantidadReserva = 1;
  }

  mostrarInformacion(id: number): void {
    const datos = { id: id, show: true, isProduct: true };
    this.enviarDatos.emit(datos);
  }

  enviarCorreo(reserva: Reserva, producto: string) {
    let email = new Email();
    email.IdUsuario = reserva.IdUsuario;
    email.Subject = this.tokenService.getUserName();
    email.Titulo = this.tokenService.getNombreEmprendimiento();
    email.Mensaje = `
    ¡Gracias por tu reserva! ¡Estamos encantados de que hayas reservado nuestro producto! <br>
    Aquí tienes los detalles de tu reserva: <br>
    <strong>Producto:</strong> ${producto} <br>
    <strong>Fecha de reserva:</strong> ${reserva.FechaSolicitud} <br>
    <strong>Fecha de recojo:</strong> ${reserva.FechaEntrega} <br>
    <strong>Código de reserva:</strong> ${reserva.CodigoReserva} <br>
    <strong>Costo total:</strong> ${reserva.Total}Bs <br>
    <strong>Estado de reserva:</strong> <br>`;
    email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FRESERVA1.png?alt=media&token=d0ffedef-f7f0-48d9-b494-b9918c459be0';

    this.reservaService.enviarCorreo(email).subscribe({
      error: (error: any) => {
        console.log(error);
      }
    });
  }
}

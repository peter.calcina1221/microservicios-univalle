import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tarjeta-guiaPositivo',
  templateUrl: './tarjeta-guiaPositivo.component.html',
  styleUrls: ['./tarjeta-guiaPositivo.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TarjetaGuiaPositivoComponent implements OnInit {
 @Input() imgUrl: string = '';
 @Input() mensaje: string = '';

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { Venta } from 'src/app/core/models/Venta/Venta';
import { CarritoService } from 'src/app/services/other/carritoService.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buscador-carrito',
  templateUrl: './buscador-carrito.component.html',
  styleUrls: ['./buscador-carrito.component.css']
})
export class BuscadorCarritoComponent implements OnInit {
  dialogVisible: boolean = false;
  cantidadProductos: number = 0;
  total: number = 0;

  constructor(
    private carritoService: CarritoService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.carritoService.productos$.subscribe(productos => {
      this.cantidadProductos = productos.length;
      this.total = this.carritoService.obtenerCostoTotalProductos();
    });
  }

  get productos(): any[] {
    return this.carritoService.productos;
  }

  quitarProducto(producto: any) {
    this.carritoService.quitarProducto(producto);
  }

  vaciarCarrito() {
    this.carritoService.vaciarCarrito();
    this.dialogVisible = false;
  }

  menosProductos(producto: any) {
    this.carritoService.reducirCantidadProducto(producto);
    this.actualizarTotal();
  }

  masProductos(producto: any) {
    this.carritoService.aumentarCantidadProducto(producto);
    this.actualizarTotal();
  }

  actualizarTotal() {
    this.total = this.carritoService.obtenerCostoTotalProductos();
  }

  confirmarVenta() {
    this.router.navigate(['/confirmar-venta']);
  }
}

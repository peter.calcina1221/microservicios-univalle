import { Component, ViewEncapsulation, HostListener, ElementRef, Input, OnChanges } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class NavbarComponent implements OnChanges {
  @Input() idUsuario: number = 0;
  responsive = true;
  noEstaLogueado: boolean = true;
  mostrarMenuPerfil: boolean = false;
  usuario: Usuario = new Usuario();
  items: MenuItem[] = [];
  esEmprendedora: boolean = false;
  imgDefault: string = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Usuarios_Image%2F9a75bead3057213ff4647a8a508c9daf1c763715616f79aeeeea076f0a6d1046.jfif?alt=media&token=dd0d5749-9f6e-4a9b-8bfa-3aac9a85a909';

  hamburguerOpen = false;

  constructor(
    private elementRef: ElementRef,
    public tokenService: TokenService,
    public userService: UsuarioService
  ) { }

  ngOnChanges(): void {
    if (this.idUsuario != 0) {
      this.cargarDatosUsuario();

      this.items = [
        {
          label: 'Mi emprendimiento',
          icon: 'uil uil-shop',
          items: [
            {
              label: 'Perfil',
              icon: 'pi pi-user',
              routerLink: ['/mi-emprendimiento'],
              command: () => this.mostrarMenuPerfil = false
            },
            {
              label: 'Reservas',
              icon: 'pi pi-calendar',
              routerLink: ['/reservas'],
              command: () => this.mostrarMenuPerfil = false
            },
            {
              label: 'Ventas',
              icon: 'pi pi-shopping-cart',
              routerLink: ['/ventas'],
              command: () => this.mostrarMenuPerfil = false
            },
            {
              label: 'Calificaciones',
              icon: 'pi pi-star',
              routerLink: ['/calificaciones'],
              command: () => this.mostrarMenuPerfil = false
            },
            {
              label: 'Reportes',
              icon: 'pi pi-chart-bar',
              routerLink: ['/reportes'],
              command: () => this.mostrarMenuPerfil = false
            },
          ]
        }
      ];
    }
  }

  cargarDatosUsuario() {
    this.userService.obtenerUsuarioPorId(this.tokenService.getIdUsuario()).subscribe(
      (usuario) => {
        this.usuario = new Usuario(usuario);
        this.usuario.ImgUrl = this.usuario.ImgUrl ? this.usuario.ImgUrl : this.imgDefault;
      }
    );

    this.esEmprendedora = this.tokenService.getIdEmprendimiento() != 0;
  }

  cerrarSesion() {
    this.mostrarMenuPerfil = false;
    this.userService.logout();
    this.tokenService.logOut();
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent) {
    if (!this.elementRef.nativeElement.contains(event.target))
      this.mostrarMenuPerfil = false;
  }

  ocultar() {
    this.mostrarMenuPerfil = false;
    this.hamburguerOpen = false;
  }
}

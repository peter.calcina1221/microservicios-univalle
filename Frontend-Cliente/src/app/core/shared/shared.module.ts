import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

//Importación de componentes generales
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { BuscadorCarritoComponent } from './components/buscador-carrito/buscador-carrito.component';
import { TarjetaInformacionComponent } from './components/tarjeta-informacion/tarjeta-informacion.component';
import { TarjetaGuiaPositivoComponent } from './components/tarjeta-guiaPositivo/tarjeta-guiaPositivo.component';
import { TarjetaProductoComponent } from './components/tarjeta-producto/tarjeta-producto.component';
import { LoadingOverlayComponent } from './components/loading-overlay/loading-overlay.component';

//Importación de los componentes de PRIMENG
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { CarouselModule } from 'primeng/carousel';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PanelModule } from 'primeng/panel';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { ImageModule } from 'primeng/image';
import { FileUploadModule } from 'primeng/fileupload';
import { TooltipModule } from 'primeng/tooltip';
import { OrderListModule } from 'primeng/orderlist';
import { ToolbarModule } from 'primeng/toolbar';
import { TableModule } from 'primeng/table';
import { RatingModule } from 'primeng/rating';
import { CheckboxModule } from 'primeng/checkbox';
import { AccordionModule } from 'primeng/accordion';
import { ChipModule } from 'primeng/chip';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { BadgeModule } from 'primeng/badge';
import { DataViewModule, DataViewLayoutOptions } from 'primeng/dataview';
import { TagModule } from 'primeng/tag';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FieldsetModule } from 'primeng/fieldset';
import { TabViewModule } from 'primeng/tabview';
import { StepsModule } from 'primeng/steps';
import { ChartModule } from 'primeng/chart';

@NgModule({
  imports: [
    RouterModule,
    FormsModule,
    CommonModule,
    DropdownModule,
    DialogModule,
    DividerModule,
    CarouselModule,
    ButtonModule,
    ToastModule,
    CalendarModule,
    ConfirmDialogModule,
    PanelModule,
    InputTextareaModule,
    ImageModule,
    FileUploadModule,
    TooltipModule,
    OrderListModule,
    ToolbarModule,
    TableModule,
    RatingModule,
    CheckboxModule,
    AccordionModule,
    ChipModule,
    InputNumberModule,
    PanelMenuModule,
    ProgressSpinnerModule,
    BadgeModule,
    DataViewModule,
    TagModule,
    RadioButtonModule,
    FieldsetModule,
    TabViewModule,
    StepsModule,
    ChartModule
  ],

  declarations: [
    NotFoundComponent,
    NavbarComponent,
    FooterComponent,
    BuscadorCarritoComponent,
    TarjetaInformacionComponent,
    TarjetaGuiaPositivoComponent,
    TarjetaProductoComponent,
    LoadingOverlayComponent
  ],

  exports: [
    RouterModule,
    FormsModule,
    CommonModule,
    NotFoundComponent,
    NavbarComponent,
    FooterComponent,
    BuscadorCarritoComponent,
    TarjetaInformacionComponent,
    TarjetaGuiaPositivoComponent,
    TarjetaProductoComponent,
    LoadingOverlayComponent,
    DropdownModule,
    DialogModule,
    CardModule,
    DividerModule,
    CarouselModule,
    ButtonModule,
    ToastModule,
    CalendarModule,
    ConfirmDialogModule,
    PanelModule,
    InputTextareaModule,
    ImageModule,
    FileUploadModule,
    TooltipModule,
    OrderListModule,
    ToolbarModule,
    TableModule,
    RatingModule,
    CheckboxModule,
    AccordionModule,
    ChipModule,
    InputNumberModule,
    PanelMenuModule,
    ProgressSpinnerModule,
    BadgeModule,
    DataViewModule,
    DataViewLayoutOptions,
    TagModule,
    RadioButtonModule,
    FieldsetModule,
    TabViewModule,
    StepsModule,
    ChartModule
  ],
  providers: [

  ]
})

export class SharedModule {
  constructor() { }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { Venta } from 'src/app/core/models/Venta/Venta';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from '../../services/Venta/detalleVenta.service';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { Router } from '@angular/router';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { CalificacionService } from 'src/app/services/Calificacion/calificacion.service';
import { Calificacion } from 'src/app/core/models/Calificacion/Calificacion';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.component.html',
  styleUrls: ['./calificacion.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CalificacionComponent implements OnInit {
  productos: any[] = [];
  reservas: any[] = [];
  producto: any = {};
  ventaAux: Venta = new Venta();
  calificacion: number = 0;
  comentario: string = '';
  dialogCalificacion: boolean = false;
  general: GeneralUtils = new GeneralUtils();
  mostrarSpinner: boolean = false;

  constructor(
    private detalleVentaService: DetalleVentaService,
    private detalleReservaService: DetalleReservaService,
    private productoService: ProductoService,
    private emprendimientoService: EmprendimientoService,
    private calificacionService: CalificacionService,
    private toastService: ToastService,
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cargarProductosComprados();
    this.cargarReservasTerminadas();
  }

  cargarProductosComprados() {
    this.productos = [];

    this.detalleVentaService.obtenerVentasPorIdUsuarioYEstado(this.tokenService.getIdUsuario()).subscribe({
      next: (detalleVentas) => {
        let detalleVentasAux: DetalleVenta[] = detalleVentas;

        this.construirEstructuraProductos(detalleVentasAux).then((productosConstruidos) => {
          this.productos = productosConstruidos;
        });
      },
      error: (err: any) => {
        this.toastService.mensajeError('Error', 'No se pudieron cargar los productos comprados', 4000);
      }
    });
  }

  async construirEstructuraProductos(detalleVentas: DetalleVenta[]): Promise<any[]> {
    const productosConstruidos: any[] = [];

    for (const detalleVenta of detalleVentas) {
      const detalleVentaNuevo = new DetalleVenta(detalleVenta);
      try {
        const producto = new Producto(await lastValueFrom(this.productoService.obtenerProductoPorId(detalleVentaNuevo.IdProducto)));
        const emprendimiento = new Emprendimiento(await lastValueFrom(this.emprendimientoService.obtenerEmprendimientoPorId(producto.IdEmpresa)));

        productosConstruidos.push({
          idProducto: producto.IdProducto,
          idDetalleVenta: detalleVentaNuevo.IdDetalleVenta,
          idEmpresa: producto.IdEmpresa,
          Empresa: emprendimiento.NombreEmpresa,
          Codigo: detalleVentaNuevo.Codigo,
          Nombre: producto.Nombre,
          ImgUrl: producto.ImagenesProducto[0].ImgUrl,
          Categoria: producto.CategoriaProducto.Nombre,
          Cantidad: detalleVentaNuevo.Cantidad,
          PrecioVenta: producto.PrecioVenta,
          Total: detalleVentaNuevo.SubTotal,
          FechaCompra: detalleVentaNuevo.FechaRegistro,
        });
      } catch (err) {
        console.log(err);
        this.toastService.mensajeError('Error', 'No se pudieron cargar los productos comprados', 4000);
      }
    }

    return productosConstruidos;
  }

  cargarReservasTerminadas() {
    this.reservas = [];

    this.detalleReservaService.obtenerReservasPorIdUsuaioYEstado(this.tokenService.getIdUsuario(), 5).subscribe({
      next: (detalleReserva) => {
        let detalleReservaAux: DetalleReserva[] = detalleReserva;

        this.construirEstructuraReservas(detalleReservaAux).then((reservasConstruidas) => {
          this.reservas = reservasConstruidas;
        });
      },
      error: (err: any) => {
        this.toastService.mensajeError('Error', 'No se pudieron cargar las reservas', 4000);
      }
    });
  }

  async construirEstructuraReservas(detalleReservas: DetalleReserva[]): Promise<any[]> {
    const reservasConstruidas: any[] = [];

    for (const detalleReserva of detalleReservas) {
      const detalleReservaNuevo = new DetalleReserva(detalleReserva);
      try {
        const producto = new Producto(await lastValueFrom(this.productoService.obtenerProductoPorId(detalleReservaNuevo.IdProducto)));
        const emprendimiento = new Emprendimiento(await lastValueFrom(this.emprendimientoService.obtenerEmprendimientoPorId(producto.IdEmpresa)));

        reservasConstruidas.push({
          idProducto: producto.IdProducto,
          idDetalleReserva: detalleReservaNuevo.IdDetalleReserva,
          idEmpresa: producto.IdEmpresa,
          Empresa: emprendimiento.NombreEmpresa,
          Codigo: detalleReservaNuevo.CodigoReserva,
          Nombre: producto.Nombre,
          ImgUrl: producto.ImagenesProducto[0].ImgUrl,
          Categoria: producto.CategoriaProducto.Nombre,
          Cantidad: detalleReservaNuevo.Cantidad,
          PrecioVenta: producto.PrecioVenta,
          Total: detalleReservaNuevo.SubTotal,
          FechaCompra: detalleReservaNuevo.FechaRegistro,
        });
      } catch (err) {
        console.log(err);
        this.toastService.mensajeError('Error', 'No se pudieron cargar las reservas', 4000);
      }
    }

    return reservasConstruidas;
  }

  abrirDialogCalificacion(producto: any) {
    this.dialogCalificacion = true
    this.producto = producto;
  }

  obtenerFecha(fecha: Date) {
    return new Date(fecha).toLocaleDateString();
  }

  calificarVenta(producto: any) {
    if (this.calificacion === 0) {
      this.toastService.mensajeAdvertencia('Error', 'Debe seleccionar las estrellas antes de calificar.', 4000);
    } else {
      let calificacion = new Calificacion();
      calificacion.IdUsuario = this.tokenService.getIdUsuario();
      calificacion.IdElemento = producto.idEmpresa;
      calificacion.TipoElemento = 'Empresa';
      calificacion.Puntuacion = this.calificacion;
      calificacion.Comentario = this.comentario;
      this.mostrarSpinner = true;

      this.calificacionService.calificar(calificacion).subscribe({
        next: (calificacion) => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeExito('Calificación', 'Se calificó correctamente.', 4000);
            this.dialogCalificacion = false;
            this.calificacion = 0;

            if (producto.idDetalleReserva) {
              this.detalleReservaService.actualizarPorEstado(producto.idDetalleReserva, 6).subscribe({
                next: (detalleReserva) => {
                  this.cargarReservasTerminadas();
                },
                error: (err: any) => {
                  console.log(err);
                  this.toastService.mensajeError('Error', 'No se pudo actualizar la reserva.', 4000);
                }
              });
            } else {
              this.detalleVentaService.actualizarPorEstado(producto.idDetalleVenta, 4).subscribe({
                next: (detalleVenta) => {
                  this.cargarProductosComprados();
                },
                error: (err: any) => {
                  console.log(err);
                  this.toastService.mensajeError('Error', 'No se pudo actualizar la venta.', 4000);
                }
              });
            }
          }, 2000);
        },
        error: (err: any) => {
          console.log(err);
          this.toastService.mensajeError('Error', 'No se pudo calificar.', 4000);
        }
      });
    }
  }

  verEmprendimiento(idEmprendimiento: number) {
    const newId = this.general.encryptId(idEmprendimiento);
    this.router.navigate(['/vistaEmprendimiento', newId]);
  }
}

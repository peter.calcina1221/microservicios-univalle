import { Component, OnInit, ViewChild } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { MenuItem, SelectItem } from 'primeng/api';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { Venta } from 'src/app/core/models/Venta/Venta';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { VentaService } from 'src/app/services/Venta/venta.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { Router } from '@angular/router';
import { DataView } from 'primeng/dataview';

@Component({
  selector: 'app-mis-compras',
  templateUrl: './mis-compras.component.html',
  styleUrls: ['./mis-compras.component.css'],
})
export class MisComprasComponent implements OnInit {
  productos: any[] = [];
  ventaAux: Venta = new Venta();
  layout: 'list' | 'grid' = 'list';
  estadoCompra!: MenuItem[];
  general: GeneralUtils = new GeneralUtils();

  //Filtros
  opcionesFiltro: SelectItem[] = [];
  filtroSeleccionado: SelectItem | undefined;
  ordenar: number = 0;
  ordenarPor: string = '';
  productosOriginales: any[] = [];

  //Overlay Loading
  mostrarSpinner: boolean = false;

  //DataView
  @ViewChild('dv') dt!: DataView;

  constructor(
    private ventaService: VentaService,
    private productoService: ProductoService,
    private emprendimientoService: EmprendimientoService,
    private toastService: ToastService,
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cargarProductosComprados();
    this.cargarArraysIniciales();
  }

  cargarProductosComprados() {
    this.productos = [];
    this.mostrarSpinner = true;

    this.ventaService.obtenerVentasPorIdOrganizacionIdUsuario(this.tokenService.getIdUsuario()).subscribe({
      next: (ventas) => {
        let detalleVentas: DetalleVenta[] = [];
        ventas.forEach((venta) => {
          const ventaAux = new Venta(venta);
          detalleVentas = detalleVentas.concat(ventaAux.DetalleVentas);
        });

        this.construirEstructuraProductos(detalleVentas).
        then((productosConstruidos) => {
          this.productosOriginales = this.productos = productosConstruidos;
        }).then(() => {
          this.mostrarSpinner = false;
        });
      },
      error: (err: any) => {
        this.toastService.mensajeError('Error', 'No se pudieron cargar los productos comprados', 4000);
      }
    });
  }

  async construirEstructuraProductos(detalleVentas: DetalleVenta[]): Promise<any[]> {
    const productosConstruidos: any[] = [];

    for (const detalleVenta of detalleVentas) {
      const detalleVentaNuevo = new DetalleVenta(detalleVenta);
      try {
        const producto = new Producto(await lastValueFrom(this.productoService.obtenerProductoPorId(detalleVentaNuevo.IdProducto)));
        const emprendimiento = new Emprendimiento(await lastValueFrom(this.emprendimientoService.obtenerEmprendimientoPorId(producto.IdEmpresa)));

        const estadoActivo = detalleVenta.Estado === '1' ? 0 : detalleVenta.Estado === '2' ? 1 : 2;

        productosConstruidos.push({
          idProducto: producto.IdProducto,
          idDetalleVenta: detalleVentaNuevo.IdDetalleVenta,
          idEmpresa: producto.IdEmpresa,
          Empresa: emprendimiento.NombreEmpresa,
          Nombre: producto.Nombre,
          ImgUrl: producto.ImagenesProducto[0].ImgUrl,
          Categoria: producto.CategoriaProducto.Nombre,
          Cantidad: detalleVentaNuevo.Cantidad,
          PrecioVenta: producto.PrecioVenta,
          FechaCompra: new Date(detalleVentaNuevo.FechaRegistro).toLocaleDateString(),
          Estado: detalleVentaNuevo.Estado,
          EstadoActivo: estadoActivo,
        });
      } catch (err) {
        this.toastService.mensajeError('Error', 'No se pudieron cargar los productos comprados', 4000);
      }
    }

    return productosConstruidos;
  }

  cambioOrden(event: any) {
    const estadoProducto = event.value;
    const filtro = event.originalEvent.target.innerText;
    this.productos = this.productosOriginales;

    if (estadoProducto !== null) {
      const nuevoOrden = this.filtrarProductos(estadoProducto);
      if (nuevoOrden.length > 0) this.productos = nuevoOrden;
      else this.toastService.mensajeInfo('Información de productos', `No hay productos para mostrar con el estado: '${filtro}'.`, 4000);
      this.dt.first = 0;
    }
  }

  filtrarProductos = (estadoProducto: string) => this.productos.filter((producto) => producto.EstadoActivo === estadoProducto);

  obtenerEstado = (estadoActivo: number) => estadoActivo === 0 ? 'En preparación' : estadoActivo === 1 ? 'Listo para recoger' : 'Entregado';

  cargarArraysIniciales() {
    this.estadoCompra = [
      { label: 'En preparación' },
      { label: 'Listo para recoger' },
      { label: 'Entregado' }
    ];

    this.opcionesFiltro = [
      { label: 'En preparación', value: 0 },
      { label: 'Listo para recoger', value: 1 },
      { label: 'Entregado', value: 2 },
    ];
  }

  verEmprendimiento(idEmprendimiento: number) {
    const newId = this.general.encryptId(idEmprendimiento);
    this.router.navigate(['/vistaEmprendimiento', newId]);
  }
}

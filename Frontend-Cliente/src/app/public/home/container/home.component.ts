import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Observable, of, switchMap } from 'rxjs';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { Producto } from 'src/app/core/models/Producto/Producto';

import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { CategoriaParamService } from 'src/app/services/other/categoriaParamService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';
import { Estadistica } from 'src/app/core/models/Calificacion/Estadistica';

interface DatosCambiados {
  cantidad: number;
  producto: Producto;
  imgUrl: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  //Variables para el dialog de Detalle
  mostrarDialogDetalle: boolean = false;
  dialogId: number = 0;
  isProduct: boolean = true;

  //Listas de datos
  producto: Producto[] = [];
  productosMasVendidos: any[] = [];
  productosMasComprados: any[] = [];
  emprendimientosMasSolicitados: any[] = [];
  misCaseras: any[] = [];

  //Datos para añadir el producto al carrito
  productoSeleccionado: Producto = new Producto();
  imgUrl: string = '';
  cantidad: number = 0;

  constructor(
    private router: Router,
    private emprendimientoServicio: EmprendimientoService,
    private productoServicio: ProductoService,
    private categoriaParamService: CategoriaParamService,
    private estadisticaService: EstadisticaService,
    private tokenService: TokenService
  ) { }

  ngOnInit(): void {
    this.cargarDatosCarrusel();
  }

  recibirDatosDelCarrusel(datos: { id: number, show: boolean, isProduct: boolean }): void {
    this.dialogId = datos.id;
    this.mostrarDialogDetalle = datos.show;
    this.isProduct = datos.isProduct;
  }

  abrirListaCategoria(categoria: string): void {
    this.categoriaParamService.seleccionarCategoria(categoria);
    this.router.navigate(['listaproductos']);
  }

  cargarDatosCarrusel() {
    this.emprendimientoServicio.obtenerEmprendimientosQueMasVenden(1, 8).subscribe({
      next: (response) => {
        this.obtenerEmprendimientos(response, 'ES')
      }
    });

    this.emprendimientoServicio.obtenerEmprendimientosSolicitadosPorUsuario(this.tokenService.getIdUsuario(), 8).subscribe({
      next: (response) => this.obtenerEmprendimientos(response, 'ESU')
    })

    this.productoServicio.obtenerProductosMasVendidosPorIdOrganizacion(1, 8).subscribe({
      next: (response) => this.obtenerProductos(response, 'PV')
    });

    this.productoServicio.obtenerProductosMasCompradosPorIdUsuario(this.tokenService.getIdUsuario(), 10).subscribe({
      next: (response) => this.obtenerProductos(response, 'PC')
    })
  }

  /**
     * Método para la obtencion de los emprendimientos más solicitados
     * @param { Emprendimiento [] } datos Lista de los emprendimientos a mostrar.
     * @param { string } tipoSolicitud Para diferenciar el tipo de solicitud que hacemos:
     * - 'ES': Obtiene las Emprendimientos que más venden.
     * - 'ESU': Obtiene las Emprendimientos de la que más compra el usuario logueado.
     * @returns Retorna una lista de los emprendimientos segun el id
     */

  obtenerEmprendimientos(datos: Emprendimiento[], tipoSolicitud: string) {
    const tarjetasObservables = datos.map((emprendimiento) => {
      emprendimiento = new Emprendimiento(emprendimiento);

      const calificacionObservable = this.estadisticaService.obtenerCalificacion(emprendimiento.IdEmpresa);

      return calificacionObservable.pipe(
        switchMap(estadistica => {
          const tarjeta = {
            id: emprendimiento.IdEmpresa,
            titulo: emprendimiento.NombreEmpresa,
            descripcion: emprendimiento.Descripcion,
            precioCategoria: emprendimiento.CategoriaEmpresa ? emprendimiento.CategoriaEmpresa.Nombre : 'Sin categoria',
            imgUrl: emprendimiento.ImagenesEmpresa[0].ImgUrl,
            isProduct: false,
            calificacion: new Estadistica(estadistica).Promedio
          };

          return of(tarjeta);
        })
      );
    });

    forkJoin(tarjetasObservables).subscribe((tarjetas: any[]) => {
      // Validamos qué tipo de solicitud recibimos.
      if (tipoSolicitud === 'ES') {
        this.emprendimientosMasSolicitados = tarjetas;
      } else {
        this.misCaseras = tarjetas;
      }
    });
  }


  /**
      * Método para la obtencion de los productos que se solicitan
      * @param { Emprendimiento [] } datos Lista de IDs de productos a mostrar.
      * @param { string } tipoSolicitud Para diferenciar el tipo de solicitud que hacemos:
      * - 'PV': Obtiene los productos más comprados.
      * - 'PC': Obtiene los productos que más compra el usuario.
      * @returns Retorna una lista de los productos segun el id.
      */
  obtenerProductos(datos: Producto[], tipoSolicitud: string) {
    const tarjetasObservables = datos.map((producto: Producto) => {
      producto = new Producto(producto);

      const tarjeta = {
        id: producto.IdProducto,
        titulo: producto.Nombre,
        descripcion: producto.Descripcion,
        precioCategoria: producto.CategoriaProducto ? producto.CategoriaProducto.Nombre : 'Sin categoria',
        imgUrl: producto.ImagenesProducto[0].ImgUrl,
        isProduct: true
      };

      return of(tarjeta);
    });

    forkJoin(tarjetasObservables).subscribe((tarjetas: any[]) => {
      //Validamos que tipo de solicitud recibimos.
      if (tipoSolicitud === 'PV') {
        this.productosMasVendidos = tarjetas;
      } else {
        this.productosMasComprados = tarjetas;
      }
    });
  }

  recibirDatosDeProducto(datos: DatosCambiados) {
    this.productoSeleccionado = datos.producto;
    this.cantidad = datos.cantidad;
    this.imgUrl = datos.imgUrl;
  }
}

import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Estadistica } from 'src/app/core/models/Calificacion/Estadistica';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';

@Component({
  selector: 'app-carrusel',
  templateUrl: './carrusel.component.html',
  styleUrls: ['./carrusel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CarruselComponent implements OnInit {
  @Input() nombreSection: string = ''
  @Input() datos: any[] = [];

  calificacion: number = 0;
  responsiveOptions: any[] = [];

  @Output() enviarDatos = new EventEmitter<{ id: number; show: boolean; isProduct: boolean }>();

  constructor(
    private estadisticaService: EstadisticaService,
  ) { }

  ngOnInit() {
    this.responsiveOptions = [
      { breakpoint: '1199px', numVisible: 1, numScroll: 1 },
      { breakpoint: '991px', numVisible: 2, numScroll: 1 },
      { breakpoint: '365px', numVisible: 1, numScroll: 1 }
    ];
  }

  mostrarInformacion(id: number, isProduct: boolean): void {
    const datos = { id: id, show: true, isProduct: isProduct };
    this.enviarDatos.emit(datos);
  }

  obtenerCalificacion(idEmpresa: number): void {
    this.estadisticaService.obtenerCalificacion(idEmpresa).subscribe((estadistica) => { this.calificacion = new Estadistica(estadistica).Promedio; });
  }
}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tarjeta-categoria',
  templateUrl: './tarjeta-categoria.component.html',
  styleUrls: ['./tarjeta-categoria.component.css']
})
export class TarjetaCategoriaComponent {

  @Input() imageLink: string = '';
  @Input() nombreTarjeta: string = '';
}

import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';
import { ToastService } from '../services/other/toastService.service';
import { TokenService } from '../services/Usuario/token.service';
import { CarritoService } from '../services/other/carritoService.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css'],
  providers: [ToastService, MessageService, CarritoService]
})

export class PublicComponent {
  hideNavFooter: boolean = false;

  constructor(
    public tokenService: TokenService,
    private router: Router,
  ) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: any) => {
      const urlSegments = event.url.split('/');
      const path = urlSegments[1];

      this.hideNavFooter = path === 'confirmar-venta' || path === 'cambiar-contrasenia';
    });
  }
}

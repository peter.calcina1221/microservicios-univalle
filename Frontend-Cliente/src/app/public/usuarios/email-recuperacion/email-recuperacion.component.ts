import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Email } from 'src/app/core/models/Usuario/Email';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-email-recuperacion',
  templateUrl: './email-recuperacion.component.html',
  styleUrls: ['./email-recuperacion.component.css']
})
export class EmailRecuperacionComponent implements OnInit {
  @Input() emailRecuperarVisible: boolean = false;
  @Output() eventoCerrarModal = new EventEmitter();

  mostrarSpinner: boolean = false;
  correo: string = '';
  email?: Email;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private toastService: ToastService
    ) { }

  ngOnInit(): void {
  }

  Enviar(): void {
    this.mostrarSpinner = true;
    this.email = new Email(this.correo);

    if(this.correo === '' || !this.email.validarCorreo()) {
      this.mostrarSpinner = false;
      return this.toastService.mensajeAdvertencia('Correo inválido', 'Ingrese un correo electrónico válido.', 5000);
    }

    this.usuarioService.enviarEmailPassword(this.email).subscribe({
      next: () => {
        setTimeout(() => {
          this.mostrarSpinner = false;
          this.toastService.mensajeInfo('Correo enviado', 'Se ha enviado un correo electrónico a su bandeja de entrada, siga las instrucciones para recuperar su contraseña.', 5000);
          this.cerrarModal();
        }, 2000);
      },
      error: () => {
        this.mostrarSpinner = false;
        this.toastService.mensajeError('Error al enviar correo', 'No se pudo enviar el correo electrónico, intente nuevamente.', 5000);
      }
    });
  }

  cerrarModal(): void {
    this.eventoCerrarModal.emit();
  }
}

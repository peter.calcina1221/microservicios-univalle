import { Component, OnInit, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { LoginUsuario } from 'src/app/core/models/Usuario/login-usuario';
import { ToastService } from 'src/app/services/other/toastService.service';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { GeneralUtils } from 'src/app/core/utils/General-utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class LoginComponent implements OnInit {
  @Input() loginVisible: boolean = false;
  @Output() eventoCerrarModal = new EventEmitter();
  @Output() toggleModals = new EventEmitter();
  @Output() abrirCorreoRecuperacion = new EventEmitter();

  nuevoUsuarioVisible: boolean = false;

  isLogged = false;
  isLoginFail = false;
  loginUsuario: LoginUsuario = new LoginUsuario();
  errMsj: string = '';
  rolesw: string[] = [];
  roles: string[] = [];

  constructor(
    private tokenService: TokenService,
    private router: Router,
    private usuarioService: UsuarioService,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
  }

  autenticarUsuario(): void {
    if (GeneralUtils.validateEmptyFields(this.loginUsuario, ['nombreUsuario', 'password'])) {
      this.usuarioService.login(this.loginUsuario).subscribe({
        next: (respuesta) => {
          this.isLogged = true;
          this.isLoginFail = false;

          this.tokenService.setToken(respuesta.token);
          this.usuarioService.isLogin();

          this.toastService.mensajeInfo('¡BIENVENIDO!', 'Nos alegra tenerte aquí ' + this.tokenService.getUserName().toUpperCase() + ' ¡Disfruta de tu visita!', 5000);
          this.router.navigate(['/home']);
        },
        error: err => {
          this.isLogged = false;
          if (err.status === 401) {
            this.isLoginFail = true;
            this.toastService.mensajeError('Error', 'Credenciales inválidas, inténtelo nuevamente.', 5000);
          } else if (err.status === 403) {
            this.toastService.mensajeError('Error', 'No tienes permisos para acceder a este recurso.', 5000);
          } else if (err.status === 500) {
            this.toastService.mensajeError('Error', 'Ha ocurrido un error, por favor intenta más tarde.', 5000);
          } else {
            this.toastService.mensajeError('Error', 'Ha ocurrido un error, por favor intenta más tarde.', 5000);
          }
        }
      });
    } else {
      this.toastService.mensajeError('Campos vacios', 'Por favor ingrese su usuario y contraseña.', 5000);
    }
  }

  abrirRecuperarContrasenia() {
    this.abrirCorreoRecuperacion.emit();
  }

  cerrarModal() {
    this.eventoCerrarModal.emit();
  }

  mostrarModalNuevoUsuario() {
    this.toggleModals.emit();
  }
}

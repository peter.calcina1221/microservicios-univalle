import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { getDownloadURL, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { ConfirmationService } from 'primeng/api';
import { FileUploadHandlerEvent } from 'primeng/fileupload';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { verificacionSMSService } from 'src/app/services/verificacionSMS/verificacionSMS.service';

interface Genero {
  name: string;
  code: string;
}

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers: [ConfirmationService],
  encapsulation: ViewEncapsulation.None
})

export class PerfilComponent implements OnInit {
  /* Variables para los modals y los cambios de edicion */
  titulo: string = '';
  edicion: boolean = false;
  metodoEdicion: boolean = false;
  fechaNacimiento: string = '';
  fechaMinima: Date = new Date(1930, 0, 1);
  fechaMaxima: Date = new Date();
  imgDefault: string = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Usuarios_Image%2F9a75bead3057213ff4647a8a508c9daf1c763715616f79aeeeea076f0a6d1046.jfif?alt=media&token=dd0d5749-9f6e-4a9b-8bfa-3aac9a85a909';
  dialogSMS: boolean = false;
  codigoVerificacion: string = '';
  tiempoEspera: number = 60;
  mostrarSpinner: boolean = false;
  mensajeSpinner: string = 'Enviando código de verificación...';

  /* Objeto Usuario para la edición */
  usuario: Usuario = new Usuario();
  usuarioOrginal: Usuario = new Usuario();
  generos: Genero[] = [];

  constructor(
    private confirmationService: ConfirmationService,
    private usuarioServicio: UsuarioService,
    private tokenService: TokenService,
    private storage: Storage,
    private toastService: ToastService,
    private verficacionSMSService: verificacionSMSService
  ) { }

  ngOnInit() {
    this.generos = [
      { name: 'Masculino', code: 'M' },
      { name: 'Femenino', code: 'F' }
    ];

    this.usuario.FechaNacimiento = this.fechaMaxima = new Date(new Date().getFullYear() - 17, new Date().getMonth(), new Date().getDate());
    this.cargarDatosUsuario();
  }

  cargarNuevaImagen(event: FileUploadHandlerEvent) {
    const file = event.files[0];
    const fileName = GeneralUtils.encryptFileName(file.name) + '.' + file.name.split('.').pop();
    const imgRef = ref(this.storage, `Usuarios_Image/${fileName}`);

    uploadBytes(imgRef, file)
      .then(() => {
        getDownloadURL(imgRef).then((url) => {
          let usuario = new Usuario();
          usuario.IdUsuario = this.tokenService.getIdUsuario();
          usuario.ImgUrl = url;
          this.usuarioServicio.actualizarImagenPerfil(usuario).subscribe({
            next: () => {
              this.toastService.mensajeExito('Éxito', 'Foto de perfil actualizada correctamente.', 5000);
              this.cargarDatosUsuario();
            },
            error: () => this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba actualizar la imagen, intente nuevamente', 7000)
          });
        }).catch(
          (error) => console.log('Error al obtener la URL pública')
        );
      })
      .catch(() =>
        this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba actualizar la imagen, intente nuevamente', 7000));
  }

  cargarDatosUsuario() {
    this.usuarioServicio.obtenerUsuarioPorId(this.tokenService.getIdUsuario()).subscribe({
      next: (usuario) => {
        this.usuario = new Usuario(usuario);
        this.usuarioOrginal = new Usuario(usuario);
        this.fechaNacimiento = new Date(this.usuario.FechaNacimiento).toLocaleDateString();
        this.usuario.FechaNacimiento = new Date(this.usuario.FechaNacimiento);

        if (this.usuario.ImgUrl === null || this.usuario.ImgUrl === '') this.usuario.ImgUrl = this.imgDefault;
      },
      error: (err) => {
        console.log("Error al obtener el usuario", err);
        this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba cargar los datos del usuario, intente nuevamente', 7000);
      }
    });
  }

  /* Modal de confirmación para quitar la foto de perfil */
  confirmar() {
    this.titulo = 'Eliminar foto de perfil';
    this.metodoEdicion = false;

    this.confirmationService.confirm({
      message: '¿Estás seguro de eliminar tu foto de perfil?',
      icon: 'pi pi-exclamation-triangle'
    });
  }

  /* Quitamos la foto de perfil cuando se acepte */
  aceptar() {
    let usuario = new Usuario();
    usuario.IdUsuario = this.tokenService.getIdUsuario();
    usuario.ImgUrl = this.imgDefault;
    this.usuarioServicio.actualizarImagenPerfil(usuario).subscribe({
      next: () => {
        this.toastService.mensajeExito('Éxito', 'Foto de perfil eliminada correctamente.', 5000);
        this.confirmationService.close();
        this.cargarDatosUsuario();
      },
      error: () => this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba eliminar la imagen, intente nuevamente', 7000)
    });
  }

  /* Modal de confirmación para Editar un perfil */
  confirmarEdicion() {
    this.titulo = 'Actualizar perfil';
    this.metodoEdicion = true;

    this.confirmationService.confirm({
      message: '<strong>Advertencia:</strong> Al guardar los cambios, se actualizará la información de tu perfil. ¿Estás seguro/a de que deseas guardar los cambios?',
      icon: 'pi pi-exclamation-triangle',
    });
  }

  /* Guardamos cambios del nuevo perfil */
  async guardarCambios() {
    const camposRequeridos = ['nombre', 'primerApellido', 'numeroTelefono', 'fechaNacimiento'];
    if (!GeneralUtils.validateEmptyFields(this.usuario, camposRequeridos)) {
      this.toastService.mensajeAdvertencia('Advertencia', 'Por favor, complete todos los campos requeridos.', 7000);
    } else {
      this.confirmationService.close();
      if (this.usuario.NumeroTelefono !== this.usuarioOrginal.NumeroTelefono) {
        await this.verificarNumero()
      } else {
        this.actualizarPerfil();
      }
    }
  }

  actualizarPerfil() {
    this.usuarioServicio.actualizarUsuario(this.usuario).subscribe({
      next: (respuesta) => {
        this.edicion = false;
        this.toastService.mensajeExito('Éxito', 'Perfil actualizado correctamente.', 5000);
      },
      error: (error) => {
        console.log("Error al modificar el usuario", error);
        this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba actualizar el perfil, intente nuevamente', 7000);
      }
    });

    this.dialogSMS = false;
  }

  habilitarEdicion(isActive: boolean) {
    if (isActive) {
      this.usuarioOrginal = new Usuario(this.usuario);
      this.edicion = true;
    } else {
      this.edicion = false;
      this.usuario = new Usuario(this.usuarioOrginal);
    }
  }

  /* Cancelamos la acción de editar perfil */
  cancelar() {
    this.confirmationService.close();
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      this.usuario[propiedad] = texto.value;
    }
  }

  async verificarNumero() {
    this.mensajeSpinner = 'Enviando código de verificación...';
    this.mostrarSpinner = true;

    if (this.usuario.NumeroTelefono.toString().length !== 8)
      this.toastService.mensajeAdvertencia('Advertencia', 'El número de teléfono debe tener 8 dígitos.', 7000);
    else {
      this.verficacionSMSService.envioSMS(this.usuario.NumeroTelefono).subscribe({
        next: (respuesta) => {
          setTimeout(() => {
            this.dialogSMS = true;
            this.mostrarSpinner = false;
            this.toastService.mensajeInfo('SMS enviado', 'Se ha enviado un SMS a su número de teléfono con un código de verificación', 5000);
          }, 2000);

          this.tiempoEspera = 60;
          setInterval(() => {
            if (this.tiempoEspera > 0) this.tiempoEspera--;
          }, 1000);
        },
        error: (error) => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeError('Error al enviar SMS', 'Ha ocurrido un error al enviar el SMS', 5000);
          }, 2000);
        }
      });
    }
  }

  validarSMS(): void {
    this.verficacionSMSService.verificarCodigo(this.codigoVerificacion).subscribe({
      next: (response) => {
        response.mensaje === 'El código es válido.' ? this.actualizarPerfil() : this.toastService.mensajeError('Código inválido', 'El código ingresado no es válido', 5000);
      }, error: () => {
        this.toastService.mensajeError('Error de validación', 'Ha ocurrido un error al validar el código', 5000);
      }
    });
  }


}

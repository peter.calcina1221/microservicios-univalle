import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastService } from '../../../../services/other/toastService.service';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GeneralUtils } from 'src/app/core/utils/General-utils';

@Component({
  selector: 'app-seguridad',
  templateUrl: './seguridad.component.html',
  styleUrls: ['./seguridad.component.css']
})

export class SeguridadComponent implements OnInit {
  validacionCorreo: boolean = false;
  cambiarContrasenia: boolean = false;
  nuevaContrasenia: boolean = false;
  contrasenia: string = '';
  nuevaContraseniaText: string = '';
  repetirNuevaContraseniaText: string = '';
  progressBarWidth: number = 0;
  colorProgressBar: string = '';
  nivelSeguridad: string = '';
  eliminarCuenta: boolean = false;
  confirmacionEliminar: string = '';

  usuario: Usuario = new Usuario();
  mostrarSpinner: boolean = false;
  mensaje: string = '';

  constructor(
    private usuarioService: UsuarioService,
    private tokenService: TokenService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.usuarioService.obtenerUsuarioPorId(this.tokenService.getIdUsuario()).subscribe(usuario => {
      this.usuario = new Usuario(usuario);
    });
  }

  actualizarCorreo() {
    this.mostrarSpinner = true;
    this.mensaje = 'Actualizando correo electrónico...';

    if (GeneralUtils.validateEmail(this.usuario.Email)) {
      this.usuarioService.actualizarUsuario(this.usuario).subscribe({
        next: (response) => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeExito('Éxito', 'Correo actualizado correctamente.', 5000);
            this.validacionCorreo = false;
          }, 2000);
        },
        error: (err) => {
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error', 'Ha ocurrido un error al tratar de actualizar el correo electrónico, intente nuevamente.', 5000);
        }
      });
    } else {
      this.mostrarSpinner = false;
      this.toastService.mensajeAdvertencia('Advertencia', 'El correo electrónico no es válido.', 5000);
    }
  }

  @ViewChild('buttonSugerencia', { static: false }) buttonSugerenciaRef?: ElementRef;

  async validarContrasenia() {
    this.usuarioService.verificarContrasenia(this.tokenService.getIdUsuario(), this.contrasenia).subscribe({
      next: (response) => {
        if (response === true) {
          this.nuevaContrasenia = true;
          const buttonSugerencia = this.buttonSugerenciaRef?.nativeElement as HTMLElement;
          buttonSugerencia.classList.add('mostrarSugerencia');
        }
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al tratar de verificar la contraseña, intente nuevamente.', 5000);
      }
    });
  }

  validarSeguridadContrasenia(contrasenia: string) {
    // Expresiones regulares para validar la seguridad de la contraseña
    const seguridadMuyAlta = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\w\s]).{12,}$/;
    const seguridadAlta = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\w\s]).{8,}$/;
    const seguridadMedia = /^(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    const seguridadBaja = /^(.{8,})$/;

    if (contrasenia.length < 8) {
      this.progressBarWidth = 10;
      this.colorProgressBar = 'red';
      this.nivelSeguridad = 'Baja';
      return;
    }

    if (seguridadMuyAlta.test(contrasenia)) {
      this.progressBarWidth = 100;
      this.colorProgressBar = 'green';
      this.nivelSeguridad = 'Muy Alta';
      return;
    }

    if (seguridadAlta.test(contrasenia)) {
      this.progressBarWidth = 75;
      this.colorProgressBar = 'yellow';
      this.nivelSeguridad = 'Alta';
      return;
    }

    if (seguridadMedia.test(contrasenia)) {
      this.progressBarWidth = 50;
      this.colorProgressBar = 'orange';
      this.nivelSeguridad = 'Media';
      return;
    }

    if (seguridadBaja.test(contrasenia)) {
      this.progressBarWidth = 25;
      this.colorProgressBar = 'red';
      this.nivelSeguridad = 'Baja';
      return;
    }
  }

  actualizarContrasenia() {
    if (this.nuevaContraseniaText.length < 8)
      this.toastService.mensajeAdvertencia('Advertencia', 'La contraseña debe tener al menos 8 caracteres.', 5000);
    else if (this.nuevaContraseniaText === this.repetirNuevaContraseniaText) {
      this.mostrarSpinner = true;
      this.mensaje = 'Actualizando contraseña...';
      this.usuarioService.cambiarContrasenia(this.tokenService.getIdUsuario(), this.nuevaContraseniaText).subscribe({
        next: (response) => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeExito('Éxito', 'Contraseña actualizada correctamente.', 5000);
            this.tokenService.logOut();
            this.usuarioService.logout();
            window.location.reload();
          }, 2000);
        },
        error: (err) => {
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error', 'Ha ocurrido un error al tratar de actualizar la contraseña, intente nuevamente.', 5000);
        }
      });
    } else {
      this.toastService.mensajeAdvertencia('Advertencia', 'Las contraseñas no coinciden.', 5000);
    }

  }

  confirmacionEliminarCuenta() {
    if (this.confirmacionEliminar === 'ELIMINAR MI CUENTA') {
      this.toastService.mensajeExito('Éxito', 'Tu cuenta fue eliminada correctamente.', 5000);
    }

    this.eliminarCuenta = false;
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { CodigoService } from 'src/app/services/Empresa/codigo.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-codigo-emprendimiento',
  templateUrl: './codigo-emprendimiento.component.html',
  styleUrls: ['./codigo-emprendimiento.component.css']
})
export class CodigoEmprendimientoComponent implements OnInit {
  generalUtils: GeneralUtils = new GeneralUtils();

  constructor(
    private codigoService: CodigoService,
    private toastService: ToastService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  validarCodigo(codigo: string) {
    this.codigoService.validarCodigo(codigo).subscribe({
      next: (respuesta) => {
        if (respuesta) {
          sessionStorage.setItem('codigoValidado', 'true');
          sessionStorage.setItem('codigo', this.generalUtils.encryptId(codigo));
          this.toastService.mensajeExito('El código fue validado correctamente.', 'Registra tu emprendimiento para comenzar a vender tus productos.', 5000);
          this.router.navigate(['/registro-emprendimiento']);
        }
        else
          this.toastService.mensajeAdvertencia('Código inexistente', 'Compruebe que el código introducido sea el correcto.', 5000);
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al validar el código.', 5000);
      }
    });
  }
}

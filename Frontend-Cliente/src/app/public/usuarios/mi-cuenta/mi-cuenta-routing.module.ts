import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { MiCuentaComponent } from "./mi-cuenta.components";
import { PerfilComponent } from "./perfil/perfil.component";
import { SeguridadComponent } from "./seguridad/seguridad.component";
import { HistorialComprasComponent } from "./historial-compras/historial-compras.component";
import { CodigoEmprendimientoComponent } from "./codigo-emprendimiento/codigo-emprendimiento.component";
import { loginGuard } from "src/app/core/guard/login-guard.guard";

const routes: Routes = [
	{
		path: '', component: MiCuentaComponent, children:
			[
				{ path: 'mi-cuenta', component: PerfilComponent, canActivate: [loginGuard] },
				{ path: 'seguridad', component: SeguridadComponent, canActivate: [loginGuard] },
        { path: 'codigo', component: CodigoEmprendimientoComponent, canActivate: [loginGuard]}
			]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class MiCuentaRoutingModule {

}

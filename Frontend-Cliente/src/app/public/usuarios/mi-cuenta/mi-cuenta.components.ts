import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { TokenService } from 'src/app/services/Usuario/token.service';

@Component({
  selector: 'app-mi-cuenta',
  templateUrl: './mi-cuenta.component.html',
  styleUrls: ['./mi-cuenta.component.css']
})

export class MiCuentaComponent implements OnInit {
  nombre: string = '';
  correo: string = '';
  imageURL: string = '';
  tieneEmpresa: boolean = false;
  codigoValidado: boolean = false;

  constructor(
    private userService: UsuarioService,
    private tokenService: TokenService
    ) { }

  ngOnInit(): void {
    this.userService.obtenerUsuarioPorId(this.tokenService.getIdUsuario()).subscribe({
      next: (usuario: any) => {
        this.nombre = usuario.nombre;
        this.correo = usuario.email;
        this.imageURL = usuario.imagen || 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Usuarios_Image%2F9a75bead3057213ff4647a8a508c9daf1c763715616f79aeeeea076f0a6d1046.jfif?alt=media&token=dd0d5749-9f6e-4a9b-8bfa-3aac9a85a909';
      }
    });

    //Recupermos el estado del codigo validado
    this.codigoValidado = sessionStorage.getItem('codigoValidado') == 'true' ? true : false;
    this.tieneEmpresa = this.tokenService.getIdEmprendimiento() > 0 ? true : false;
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastService } from '../../../../services/other/toastService.service';

@Component({
  selector: 'app-historial-compras',
  templateUrl: './historial-compras.component.html',
  styleUrls: ['./historial-compras.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HistorialComprasComponent implements OnInit {

  constructor(
    private toastService: ToastService
  ) { }

  ngOnInit() {
  }

  descargarHistorial() {
    this.toastService.mensajeInfo('Descarga iniciada', 'La descarga de tu historial de compras ya comenzo, también enviamos a tu correo electrónico otra copia con tu historial.', 8000);
  }
}

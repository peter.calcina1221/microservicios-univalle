import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { MiCuentaRoutingModule } from './mi-cuenta-routing.module';

import { MiCuentaComponent } from './mi-cuenta.components';
import { PerfilComponent } from './perfil/perfil.component';
import { SeguridadComponent } from './seguridad/seguridad.component';
import { HistorialComprasComponent } from './historial-compras/historial-compras.component';
import { CodigoEmprendimientoComponent } from './codigo-emprendimiento/codigo-emprendimiento.component';


@NgModule ({
    imports: [
        MiCuentaRoutingModule,
        SharedModule
    ],
    declarations: [
        MiCuentaComponent,
        PerfilComponent,
        SeguridadComponent,
        HistorialComprasComponent,
        CodigoEmprendimientoComponent
    ],
    exports: [],
    providers: []
})

export class MiCuentaModule {
    constructor () {}
}

import { Component, EventEmitter, Input, Output, ViewEncapsulation, OnInit } from '@angular/core';
import { set } from 'date-fns';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { verificacionSMSService } from 'src/app/services/verificacionSMS/verificacionSMS.service';

interface Genero {
  name: string;
  code: string;
}
@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class CrearUsuarioComponent implements OnInit {
  @Input() nuevoUsuarioVisible: boolean = false;
  @Output() eventoCerrarModalNuevoUsuario = new EventEmitter();
  @Output() toggleModals = new EventEmitter();

  loginVisible: boolean = false;
  dialogSMS: boolean = false;

  fechaMinima: Date = new Date(1930, 0, 1);
  fechaMaxima: Date = new Date();
  usuario: Usuario = new Usuario();
  password: string = '';
  generos: Genero[] = [];
  complemento: string = '';

  existeUsuario: boolean = false;
  existeEmail: boolean = false;
  mostrarTag: boolean = false;
  mostrarTagEmail: boolean = false;
  codigoVerificacion: string = '';
  tiempoEspera: number = 0;
  mensajeSpinner: string = '';
  mostrarSpinner: boolean = false;

  timerID: NodeJS.Timeout | null = null;

  constructor(
    private usuarioService: UsuarioService,
    private toastService: ToastService,
    private verificacionSMSService: verificacionSMSService
  ) { }

  ngOnInit(): void {
    this.generos = [
      { name: 'Masculino', code: 'M' },
      { name: 'Femenino', code: 'F' }
    ];

    this.usuario.FechaNacimiento = this.fechaMaxima = new Date(new Date().getFullYear() - 17, new Date().getMonth(), new Date().getDate());
  }

  registrar(): void {
    this.mensajeSpinner = 'Registrando usuario...';
    this.mostrarSpinner = true;
    this.dialogSMS = false;

    this.complemento !== '' ? this.usuario.Ci = this.usuario.Ci + '-' + this.complemento : this.usuario.Ci = this.usuario.Ci;
    this.usuario.FechaNacimiento = set(this.usuario.FechaNacimiento, { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });

    this.usuarioService.nuevo(this.usuario).subscribe({
      next: () => {
        setTimeout(() => {
          this.mostrarSpinner = false;
          this.toastService.mensajeExito('Usuario registrado', 'Se ha registrado correctamente el usuario', 5000);
          this.cerrarModal();
          this.mostrarLogin();
        }, 2000);
      },
      error: (e) => {
        console.log(e);
        setTimeout(() => {
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error al registrar', 'Ha ocurrido un error al registrar el usuario', 5000);
        }, 2000);
      }
    });
  }

  abrirVerificacionSMS(): void {
    const camposAValidar = ['nombre', 'primerApellido', 'fechaNacimiento', 'ci', 'email', 'password', 'genero', 'numeroTelefono', 'nombreUsuario'];

    if (this.usuario.Password.length < 8)
      this.toastService.mensajeError('Contraseña muy corta', 'La contraseña debe tener al menos 8 caracteres.', 5000);
    else if (this.usuario.Password !== this.password)
      this.toastService.mensajeError('Contraseñas no coinciden', 'Las contraseñas ingresadas no coinciden.', 5000);
    else if (!GeneralUtils.validateEmptyFields(this.usuario, camposAValidar))
      this.toastService.mensajeError('Campos vacíos', 'Por favor, llene los campos obligatorios.', 5000);
    else if (!GeneralUtils.validateCellphone(this.usuario.NumeroTelefono))
      this.toastService.mensajeError('Número de teléfono inválido', 'El número de teléfono ingresado no es válido.', 5000);
    else if (!GeneralUtils.validateEmail(this.usuario.Email))
      this.toastService.mensajeError('Correo electrónico inválido', 'El correo electrónico ingresado no es válido.', 5000);
    else if (this.existeUsuario)
      this.toastService.mensajeError('Nombre de usuario ya existe', 'El nombre de usuario ingresado ya existe.', 5000);
    else if (this.existeEmail)
      this.toastService.mensajeError('Correo electrónico ya existe', 'El correo electrónico ingresado ya existe.', 5000);
    else {
      this.mensajeSpinner = 'Enviando código de verificación...';
      this.mostrarSpinner = true;

      this.verificacionSMSService.envioSMS(this.usuario.NumeroTelefono).subscribe({
        next: () => {
          setTimeout(() => {
            this.dialogSMS = true;
            this.mostrarSpinner = false;
            this.toastService.mensajeInfo('SMS enviado', 'Se ha enviado un SMS a su número de teléfono con un código de verificación', 5000);
          }, 2000);

          this.tiempoEspera = 60;
          setInterval(() => {
            if (this.tiempoEspera > 0) this.tiempoEspera--;
          }, 1000);
        },
        error: () => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeError('Error al enviar SMS', 'Ha ocurrido un error al enviar el SMS', 5000);
          }, 2000);
        }
      });
    }
  }

  validarSMS(): void {
    this.verificacionSMSService.verificarCodigo(this.codigoVerificacion).subscribe({
      next: (response) => {
        response.mensaje === 'El código es válido.' ? this.registrar() : this.toastService.mensajeError('Código inválido', 'El código ingresado no es válido', 5000);
      }, error: () => {
        this.toastService.mensajeError('Error de validación', 'Ha ocurrido un error al validar el código', 5000);
      }
    });
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      propiedad !== 'complemento' ? this.usuario[propiedad] = texto.value : this.complemento = texto.value;
    }
  }

  cerrarModal() {
    this.mostrarTag = false;
    this.eventoCerrarModalNuevoUsuario.emit();
  }

  mostrarLogin() {
    this.toggleModals.emit();
  }

  verificarNombreUsuario() {
    this.mostrarTag = true;
    if (this.timerID) {
      clearTimeout(this.timerID);
    }

    if (this.usuario.NombreUsuario !== '') {
      this.timerID = setTimeout(() => {
        this.usuarioService.verificarNombreUsuario(this.usuario.NombreUsuario).subscribe({
          next: (response) => {
            this.existeUsuario = response;
          }, error: (e) => {
            this.toastService.mensajeError('Error al verificar nombre de usuario', 'Ha ocurrido un error al verificar el nombre de usuario', 5000);
          }
        });

      }, 500);
    } else {
      this.mostrarTag = false;
    }
  }

  verificarEmail() {
    this.mostrarTagEmail = true;
    if (this.usuario.Email !== '') {
      setTimeout(() => {
        this.usuarioService.verificarEmail(this.usuario.Email).subscribe({
          next: (response) => {
            this.existeEmail = response;
          }, error: (e) => {
            console.log(e)
            this.toastService.mensajeError('Error al verificar email', 'Ha ocurrido un error al verificar el email', 5000);
          }
        });
      }, 500);
    } else {
      this.mostrarTagEmail = false;
    }
  }
}

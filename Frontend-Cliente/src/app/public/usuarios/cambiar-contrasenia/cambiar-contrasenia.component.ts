import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CambiarPassword } from 'src/app/core/models/Usuario/CambiarPassword';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-cambiar-contrasenia',
  templateUrl: './cambiar-contrasenia.component.html',
  styleUrls: ['./cambiar-contrasenia.component.css']
})
export class CambiarContraseniaComponent implements OnInit {

  constructor(
    private activateRoute: ActivatedRoute,
    public usuarioService: UsuarioService,
    private router: Router,
    private toastService: ToastService
  ) { }

  password: string = "";
  password2: string = "";
  tokenPassword: string = "";
  mostrarSpinner: boolean = false;
  cambiarPassword?: CambiarPassword;
  componenteCambiarContraseniaActivo: boolean = true;

  ngOnInit(): void {

  }

  Actualizar() {
    const camposRequeridos = ['password', 'confirmarPassword', 'tokenPassword']
    this.tokenPassword = this.activateRoute.snapshot.params['tokenPassword'];
    this.cambiarPassword = new CambiarPassword(this.password, this.password2, this.tokenPassword);

    if (!GeneralUtils.validateEmptyFields(this.cambiarPassword, camposRequeridos)) {
      this.toastService.mensajeError('Error', 'Por favor, introduzca una contraseña.', 5000);
    } else if (this.cambiarPassword.password.length < 8) {
      this.toastService.mensajeError('Error', 'La contraseña debe tener al menos 8 caracteres.', 5000);
    } else if (this.password != this.password2) {
      this.toastService.mensajeError('Error', 'Las contraseñas no coinciden.', 5000);
    } else {
      this.mostrarSpinner = true;
      this.usuarioService.reestablecerPassword(this.cambiarPassword).subscribe({
        next: () => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeExito('Contraseña actualizada', 'Su contraseña ha sido actualizada correctamente.', 5000);
            this.router.navigate(['/']);
          }, 2000);
        },
        error: (e) => {
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error', 'Ha ocurrido un error al tratar de actualizar la contraseña, intente nuevamente.', 5000);
        }
      });
    }
  }
}

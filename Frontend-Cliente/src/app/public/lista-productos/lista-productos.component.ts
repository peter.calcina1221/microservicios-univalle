import { Component, OnInit } from '@angular/core';
import { CategoriaProducto } from 'src/app/core/models/Producto/CategoriaProducto';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { CategoriaProductoService } from 'src/app/services/Producto/categoria.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { FilterService } from 'primeng/api';
import { CarritoService } from 'src/app/services/other/carritoService.service';
import { CategoriaParamService } from 'src/app/services/other/categoriaParamService.service';


interface Filtro {
  nombre: string;
  valor: any;
}
@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {
  opcionSeleccionada: string = 'precio_asc';
  ordenDropdown: any[] = [];
  checkboxCategorias: any[] = [];
  productos: Producto[] = [];
  productosOriginales: Producto[] = [];

  //Filtros para la lista de productos
  filtros: Filtro[] = [];
  nombreProd: string = '';
  minPrecio: number = 0;
  maxPrecio: number = 0;
  categoriasSeleccionadas: any[] = [];
  categoriaParametro: boolean = false;

  //Variables para el dialog de Detalle
  mostrarDialogDetalle: boolean = false;
  dialogId: number = 0;
  isProduct: boolean = true;

  constructor(
    private productoService: ProductoService,
    private categoriaProductoService: CategoriaProductoService,
    private filterService: FilterService,
    private carritoSerivice: CarritoService,
    private categoriaParamService: CategoriaParamService
  ) { }

  ngOnInit() {
    this.cargarDropdownOrden();
    this.cargarDatosProducto();
    this.cargarCheckboxCategoria();
    this.carritoSerivice.ventaConfirmada$.subscribe(ventaConfirmada => {
      if (ventaConfirmada) {
        this.cargarDatosProducto();
        this.carritoSerivice.confirmarVenta(false);
      }
    });
  }

  cargarDropdownOrden() {
    this.ordenDropdown = [
      { label: 'Precio: más bajo primero', value: 'precio_asc' },
      { label: 'Precio: más alto primero', value: 'precio_desc' },
      { label: 'Nombre: A-Z', value: 'nombre_asc' },
      { label: 'Nombre: Z-A', value: 'nombre_desc' }
    ];
  }

  cargarCheckboxCategoria() {
    this.categoriaProductoService.obtenerCategoriasProducto().subscribe({
      next: (categorias: CategoriaProducto[]) => {
        categorias.forEach(categoria => {
          let categoriaAux = new CategoriaProducto(categoria);
          this.checkboxCategorias.push({ label: categoriaAux.Nombre, idCategoria: categoriaAux.IdCategoria, seleccionado: false });
        });

        this.categoriaParamService.categoriaSeleccionada$.subscribe(categoria => {
          if (categoria) {
            this.categoriaParametro = true;
            this.agregarFiltroCategoria(categoria);
          }
        });

        this.categoriaParamService.quitarCategoria();
      }
    });
  }

  cargarDatosProducto() {
    this.productos = [];

    this.productoService.obtenerProductosActivos().subscribe(
      productos => {
        productos.forEach(producto => {
          this.productos.push(new Producto(producto));
        });

        this.productosOriginales = this.productos;
        this.productos.sort((a, b) => a.PrecioVenta - b.PrecioVenta);
      }
    );
  }

  agregarFiltroNombre(nombreProducto: string) {
    nombreProducto === '' ?
      this.quitarFiltroNombre('Nombre: ') :
      this.agregarFiltro({ nombre: 'Nombre: ', valor: nombreProducto });
  }

  agregarFiltroPrecio() {
    //Comprobamos que los precios sean mayor a 0 para agregarlos al filtro y sino los quitamos
    this.minPrecio > 0 ?
      this.agregarFiltro({ nombre: 'Precio mínimo: Bs. ', valor: this.minPrecio }) :
      this.quitarFiltroNombre('Precio mínimo: Bs. ');

    this.maxPrecio > 0 ?
      this.agregarFiltro({ nombre: 'Precio máximo: Bs. ', valor: this.maxPrecio }) :
      this.quitarFiltroNombre('Precio máximo: Bs. ');
  }

  agregarFiltroCategoria(categoria: any) {
    //Comprobamos que el filtro no exista y si existe quitamos el filtro
    if (!this.filtros.some(f => f.valor === categoria)) {
      this.agregarFiltro({ nombre: 'Categoría: ', valor: categoria });
    } else {
      this.quitarFiltro(categoria);
    }
  }

  agregarFiltro(filtro: Filtro) {
    //Comprobamos que el filtro no exista y si existe reemplazamos el valor
    if (this.filtros.find(f => f.nombre == filtro.nombre && filtro.nombre != 'Categoría: ')) {
      this.filtros = this.filtros.map(f => {
        if (f.nombre == filtro.nombre) {
          f.valor = filtro.valor;
        }
        return f;
      });
    } else {
      if (filtro.nombre == 'Categoría: ')
        this.categoriasSeleccionadas.push(filtro.valor);
      this.filtros.push(filtro);
    }

    this.aplicarFiltro();
  }

  /**
   * @param {string} nombre Nombre del filtro a quitar
   * @description Quita el filtro de la lista de filtros en función del nombre que se le pase
   */
  quitarFiltroNombre(nombre: string) {
    this.filtros = this.filtros.filter(f => f.nombre != nombre);
    this.aplicarFiltro();
  }

  /**
   * @param {string} valor Valor del filtro a quitar
   * @description Quita el filtro de la lista de filtros en función del valor que se le pase
   */
  quitarFiltro(valor: any) {
    this.categoriaParametro = false;
    this.filtros = this.filtros.filter(f => f.valor != valor);
    this.categoriasSeleccionadas = this.categoriasSeleccionadas.filter(c => c != valor);
    this.aplicarFiltro();
  }

  /**
   * @param {string} valor Valor del filtro a restablecer
   * @description Envia el valor del filtro al método quitarFiltro() y actualiza los valores de los filtros restantes
   */
  restablecerFiltro(valor: any) {
    this.quitarFiltro(valor);

    // Actualizar los valores de los filtros restantes
    this.nombreProd = this.filtros.find(f => f.nombre == 'Nombre: ')?.valor || '';
    this.minPrecio = this.filtros.find(f => f.nombre == 'Precio mínimo: Bs. ')?.valor || 0;
    this.maxPrecio = this.filtros.find(f => f.nombre == 'Precio máximo: Bs. ')?.valor || 0;

    //Desmarcar el checkbox según el valor que se haya quitado
    this.checkboxCategorias.forEach(checkbox => {
      if (checkbox.label === valor) {
        checkbox.seleccionado = false;
      }
    });
  }

  limpiarFiltros() {
    this.categoriaParametro = false;
    this.filtros = [];
    this.nombreProd = '';
    this.minPrecio = 0;
    this.maxPrecio = 0;
    this.checkboxCategorias.forEach(checkbox => {
      checkbox.seleccionado = false;
    });
    this.aplicarFiltro();
  }

  //TODO: Implementar el FilterService de PrimeNG para filtrar los productos por nombre, categoria, emprendimiento, etc.
  aplicarFiltro() {
    this.productos = this.productosOriginales;
    this.filtros.forEach(filtro => {
      switch (filtro.nombre) {
        case 'Nombre: ':
          this.productos = this.filterService.filter(this.productos, ['nombre'], filtro.valor, 'contains');
          break;
        case 'Precio mínimo: Bs. ':
          this.productos = this.filterService.filter(this.productos, ['precioVenta'], filtro.valor, 'gte');
          break;
        case 'Precio máximo: Bs. ':
          this.productos = this.filterService.filter(this.productos, ['precioVenta'], filtro.valor, 'lte');
          break;
        case 'Categoría: ':
          this.productos = this.filterService.filter(this.productos, ['categoriaProducto.nombre'], this.categoriasSeleccionadas, 'in');
          break;
        default:
          break;
      }
    });
  }

  ordenarProductos() {
    switch (this.opcionSeleccionada) {
      case 'precio_asc':
        this.productos.sort((a, b) => a.PrecioVenta - b.PrecioVenta);
        break;
      case 'precio_desc':
        this.productos.sort((a, b) => b.PrecioVenta - a.PrecioVenta);
        break;
      case 'nombre_asc':
        this.productos.sort((a, b) => a.Nombre.localeCompare(b.Nombre));
        break;
      case 'nombre_desc':
        this.productos.sort((a, b) => b.Nombre.localeCompare(a.Nombre));
        break;
      default:
        // No se seleccionó ninguna opción válida, mostrar la lista sin cambios
        break;
    }
  }

  recibirDatos(datos: { id: number, show: boolean, isProduct: boolean }): void {
    this.dialogId = datos.id;
    this.mostrarDialogDetalle = datos.show;
    this.isProduct = datos.isProduct;
  }
}

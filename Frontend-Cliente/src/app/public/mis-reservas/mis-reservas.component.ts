import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { Reserva } from 'src/app/core/models/Reserva/Reserva';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';
import { ReservaService } from 'src/app/services/Reserva/reserva.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';

@Component({
  selector: 'app-mis-reservas',
  templateUrl: './mis-reservas.component.html',
  styleUrls: ['./mis-reservas.component.css']
})
export class MisReservasComponent {
  productos: any[] = [];
  reservasRechazadas: any[] = [];
  layout: 'list' | 'grid' = 'list';
  estadoReserva!: MenuItem[];
  estadoReservaRechazado!: MenuItem[];
  estadoActivo: number = 0;
  reserva: Reserva = new Reserva();
  general: GeneralUtils = new GeneralUtils();

  //Filtros
  opcionesFiltro: SelectItem[] = [];
  filtroSeleccionado: SelectItem | undefined;
  ordenar: number = 0;
  ordenarPor: string = '';
  productosOriginales: any[] = [];

  //Overlay Loading
  mostrarSpinner: boolean = false;

  constructor(
    private reservaService: ReservaService,
    private productoService: ProductoService,
    private emprendimientoService: EmprendimientoService,
    private toastService: ToastService,
    private tokenService: TokenService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.cargarReservas();

    this.estadoReserva = [
      { label: 'Esperando confirmación' },
      { label: 'Confirmado' },
      { label: 'Listo para recoger' },
      { label: 'Entregado' }
    ];

    this.estadoReservaRechazado = [
      { label: 'Esperando confirmación' },
      { label: 'Rechazado'},
      { label: 'Listo para recoger' },
      { label: 'Entregado' }
    ];

    this.opcionesFiltro = [
      { label: 'Esperando confirmación', value: 0 },
      { label: 'Confirmado', value: 1 },
      { label: 'Listo para recoger', value: 2 },
      { label: 'Entregado', value: 3 },
    ];
  }

  cargarReservas() {
    this.productos = [];
    this.mostrarSpinner = true;

    this.reservaService.obtenerReservasPorIdUsuario(this.tokenService.getIdUsuario()).subscribe({
      next: (reservas) => {
        let detalleReservas: DetalleReserva[] = [];
        let fechasReservas: any[] = [];
        let fechasRecojo: any[] = [];
        reservas.forEach((reserva) => {
          const reservaAux = new Reserva(reserva);
          detalleReservas = detalleReservas.concat(reservaAux.DetalleReservas);
          fechasReservas.push(new Date(reservaAux.FechaSolicitud).toLocaleDateString());
          fechasRecojo.push(new Date(reservaAux.FechaEntrega).toLocaleDateString());
        });

        this.construirEstructuraProductos(detalleReservas, fechasReservas, fechasRecojo)
        .then((productosConstruidos) => { this.productosOriginales = this.productos = productosConstruidos; })
        .then(() => { this.mostrarSpinner = false; });
      },
      error: (err: any) => {
        this.toastService.mensajeError('Error', 'No se pudieron cargar las reservas', 4000);
      }
    });
  }

  async construirEstructuraProductos(detalleReservas: DetalleReserva[], fechasReservas: any[], fechasRecojo: any[]): Promise<any[]> {
    const productosConstruidos: any[] = [];

    for (const detalleReserva of detalleReservas) {
      const detalleReservaNuevo = new DetalleReserva(detalleReserva);

      try {
        const producto = new Producto(await lastValueFrom(this.productoService.obtenerProductoPorId(detalleReservaNuevo.IdProducto)));
        const emprendimiento = new Emprendimiento(await lastValueFrom(this.emprendimientoService.obtenerEmprendimientoPorId(producto.IdEmpresa)));
        const estadoActivoMap: {[key: string]: number} = {
          '1': 0,
          '2': 1,
          '4': 2,
        };

        ['5', '6'].forEach(key => {
          estadoActivoMap[key] = 3;
        });

        this.estadoActivo = estadoActivoMap[detalleReservaNuevo.Estado] || 0;

        if (detalleReservaNuevo.Estado !== '3')
          productosConstruidos.push({
            idProducto: producto.IdProducto,
            idDetalleReserva: detalleReservaNuevo.IdDetalleReserva,
            idEmpresa: producto.IdEmpresa,
            Empresa: emprendimiento.NombreEmpresa,
            Nombre: producto.Nombre,
            ImgUrl: producto.ImagenesProducto[0].ImgUrl,
            Categoria: producto.CategoriaProducto.Nombre,
            Cantidad: detalleReservaNuevo.Cantidad,
            PrecioVenta: producto.PrecioVenta,
            FechaReserva: fechasReservas[detalleReservas.indexOf(detalleReserva)],
            FechaRecojo: fechasRecojo[detalleReservas.indexOf(detalleReserva)],
            Estado: detalleReservaNuevo.Estado,
            EstadoActivo: this.estadoActivo,
          });
        else
          this.reservasRechazadas.push({
            idProducto: producto.IdProducto,
            idDetalleReserva: detalleReservaNuevo.IdDetalleReserva,
            idEmpresa: producto.IdEmpresa,
            Empresa: emprendimiento.NombreEmpresa,
            Nombre: producto.Nombre,
            ImgUrl: producto.ImagenesProducto[0].ImgUrl,
            Categoria: producto.CategoriaProducto.Nombre,
            Cantidad: detalleReservaNuevo.Cantidad,
            PrecioVenta: producto.PrecioVenta,
            FechaReserva: fechasReservas[detalleReservas.indexOf(detalleReserva)],
            FechaRecojo: fechasRecojo[detalleReservas.indexOf(detalleReserva)],
            Estado: detalleReservaNuevo.Estado,
          });
      } catch (err) {
        this.toastService.mensajeError('Error', 'No se pudieron cargar los productos comprados', 4000);
      }
    }

    return productosConstruidos;
  }

  cambioOrden(event: any) {
    const value = event.value;
    const label = event.originalEvent.target.innerText;
    this.productos = this.productosOriginales;

    if (value !== null) {
      const nuevoOrden = this.productos.filter((producto) => producto.EstadoActivo === value);
      if (nuevoOrden.length > 0) this.productos = nuevoOrden;
      else this.toastService.mensajeInfo('Información de reservas', `No hay reservas para mostrar con el estado: '${label}'.`, 4000);
    }
  }

  obtenerEstado(estadoActivo: number): string {
    return this.reserva.obtenerEstadoReserva(estadoActivo);
  }

  verEmprendimiento(idEmprendimiento: number) {
    const newId = this.general.encryptId(idEmprendimiento);
    this.router.navigate(['/vistaEmprendimiento', newId]);
  }
}

import { Component } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})

export class InicioComponent {

  loginVisible: boolean = false;
  nuevoUsuarioVisible: boolean = false;
  emailRecuperacionVisible: boolean = false;

  constructor() { }

  toggleModals() {
    this.loginVisible = !this.loginVisible;
    this.nuevoUsuarioVisible = !this.nuevoUsuarioVisible;
  }

  mostrarLogin() {
    this.loginVisible = true;
  }

  abrirCorreoRecuperacion() {
    this.emailRecuperacionVisible = true;
  }

  cerrarModalLogin() {
    this.loginVisible = false;
  }

  cerrarModalNuevoUsuario() {
    this.nuevoUsuarioVisible = false;
  }

  cerrarModalCorreoRecuperacion() {
    this.emailRecuperacionVisible = false;
  }
}

import { AfterViewInit, Component, ElementRef, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { th } from 'date-fns/locale';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ContactoComponent implements AfterViewInit {

  nombre: string = "";
  correo: string = "";
  mensaje: string = "";

  //Mapa
  @ViewChild('divMapa') mapa!: ElementRef;
  @ViewChild('inputDireccion') inputDireccion!: ElementRef;

  departamentoSeleccionado: number = 0;
  map!: google.maps.Map;
  marker!: google.maps.Marker;

  constructor(private render2: Renderer2, private toastService: ToastService) { }

  ngAfterViewInit(): void {
    if (navigator.geolocation) {
      this.cargarMapa();
    }
  }

  async cargarMapa() {
    const latitud = -17.3840025;
    const longitud = -66.1514373;

    const mapOptions: google.maps.MapOptions = {
      center: new google.maps.LatLng(latitud, longitud),
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    this.map = new google.maps.Map(this.render2.selectRootElement(this.mapa.nativeElement), mapOptions);

    const marker = new google.maps.Marker({
      position: this.map.getCenter(),
      draggable: false,
      map: this.map,
    });

    marker.setMap(this.map);
    this.marker = marker;
  }

  enviar() {
    if(GeneralUtils.validateEmptyFields({nombre: this.nombre, correo: this.correo, mensaje: this.mensaje} ,['nombre','correo','mensaje'])) {
      if(GeneralUtils.validateEmail(this.correo)) {
        //Enviamos el correo
        this.toastService.mensajeExito("Exito", "El correo se envio correctamente", 5000);
      } else {
        this.toastService.mensajeError("Error", "El correo no es valido", 5000);
      }
    }
    else {
      this.toastService.mensajeError("Error", "Debe llenar todos los campos", 5000);
    }
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      propiedad === 'nombre' ? this.nombre = texto.value : this.mensaje = texto.value;
    }
  }

}

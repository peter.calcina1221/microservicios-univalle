import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';

import { PublicComponent } from './public.component';
import { HomeComponent } from './home/container/home.component';
import { InicioComponent } from './inicio/container/inicio.component';
import { QuienesComponent } from './quienes/quienes.component';
import { ContactoComponent } from './contacto/contacto.component';
import { CambiarContraseniaComponent } from './usuarios/cambiar-contrasenia/cambiar-contrasenia.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { LoginComponent } from './usuarios/login/login.component';
import { EmailRecuperacionComponent } from './usuarios/email-recuperacion/email-recuperacion.component';
import { TarjetaCategoriaComponent } from './home/components/tarjeta-categoria/tarjeta-categoria.component';
import { CarruselComponent } from './home/components/carrusel/carrusel.component';
import { ListaCategoriaComponent } from './lista-categoria/lista-categoria.component';
import { BannerCategoriasComponent } from './lista-categoria/components/banner-categorias/banner-categorias.component';
import { BannerListaEmprendimientoComponent } from './emprendimiento/components/banner-lista-emprendimiento/banner-lista-emprendimiento.component';
import { BannerRegistroEmprendimientoComponent } from './emprendimiento/components/banner-registro-emprendimiento/banner-registro-emprendimiento.component';
import { CardCategoriasComponent } from './lista-categoria/components/card-categorias/card-categorias.component';
import { ListaEmprendimientoComponent } from './emprendimiento/lista-emprendimiento/lista-emprendimiento.component';
import { RegistroEmprendimientoComponent } from './emprendimiento/registro-emprendimiento/registro-emprendimiento.component';
import { TarjetaEmprendimientoComponent } from './emprendimiento/components/tarjeta-emprendimiento/tarjeta-emprendimiento.component';
import { PerfilEmprendimientoComponent } from './emprendimiento/perfil-emprendimiento/perfil-emprendimiento.component';
import { VistaEmprendimientoComponent } from './emprendimiento/vista-emprendimiento/vista-emprendimiento.component';
import { CrudProductosComponent } from './emprendimiento/components/crud-productos/crud-productos.component';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { ListaReservasComponent } from './emprendimiento/lista-reservas/lista-reservas.component';
import { ListaVentasComponent } from './emprendimiento/lista-ventas/lista-ventas.component';
import { CalificacionesComponent } from './emprendimiento/calificaciones/calificaciones.component';
import { MisComprasComponent } from './mis-compras/mis-compras.component';
import { MisReservasComponent } from './mis-reservas/mis-reservas.component';
import { CalificacionComponent } from './calificacion/calificacion.component';
import { ConfirmarVentaComponent } from './confirmar-venta/confirmar-venta.component';
import { ReportesComponent } from './emprendimiento/reportes/reportes.component';
import { VentasTotalesComponent } from './emprendimiento/reportes/components/ventas-totales/ventas-totales.component';
import { VentasUltimaSemanaComponent } from './emprendimiento/reportes/components/ventas-ultima-semana/ventas-ultima-semana.component';
import { ProductoMasGeneraVentasComponent } from './emprendimiento/reportes/components/producto-mas-genera-ventas/producto-mas-genera-ventas.component';
import { stockActualProductosComponent } from './emprendimiento/reportes/components/stock-actual-productos/stock-actual-productos.component';
import { ProductosMasVendidosComponent } from './emprendimiento/reportes/components/productos-mas-vendidos/productos-mas-vendidos.component';
import { StockReponerProductosComponent } from './emprendimiento/reportes/components/stock-reponer-productos/stock-reponer-productos.component';
import { ReservasTotalesComponent } from './emprendimiento/reportes/components/reservas-totales/reservas-totales.component';
import { IngresosReservasVsVentasComponent } from './emprendimiento/reportes/components/ingresos-reservas-vs-ventas/ingresos-reservas-vs-ventas.component';
import { PuntuacionRecibidaComponent } from './emprendimiento/reportes/components/puntuacion-recibida/puntuacion-recibida.component';
import { DescargarImprimirReporteComponent } from './emprendimiento/reportes/components/descargar-imprimir-reporte/descargar-imprimir-reporte.component';

@NgModule ({
    imports: [
        PublicRoutingModule,
        SharedModule,
    ],
    declarations: [
        PublicComponent,
        HomeComponent,
        LoginComponent,
        InicioComponent,
        QuienesComponent,
        ContactoComponent,
        EmailRecuperacionComponent,
        CambiarContraseniaComponent,
        ListaCategoriaComponent,
        CrearUsuarioComponent,
        TarjetaCategoriaComponent,
        CarruselComponent,
        BannerCategoriasComponent,
        BannerListaEmprendimientoComponent,
        BannerRegistroEmprendimientoComponent,
        CardCategoriasComponent,
        ListaEmprendimientoComponent,
        RegistroEmprendimientoComponent,
        VistaEmprendimientoComponent,
        TarjetaEmprendimientoComponent,
        PerfilEmprendimientoComponent,
        CrudProductosComponent,
        ListaProductosComponent,
        ListaReservasComponent,
        ListaVentasComponent,
        ReportesComponent,
        VentasTotalesComponent,
        VentasUltimaSemanaComponent,
        ProductoMasGeneraVentasComponent,
        stockActualProductosComponent,
        ProductosMasVendidosComponent,
        StockReponerProductosComponent,
        ReservasTotalesComponent,
        IngresosReservasVsVentasComponent,
        PuntuacionRecibidaComponent,
        DescargarImprimirReporteComponent,
        CalificacionesComponent,
        MisComprasComponent,
        MisReservasComponent,
        CalificacionComponent,
        ConfirmarVentaComponent,
    ],
    exports: [],
    providers: []
})

export class PublicModule {
    constructor () {}
}

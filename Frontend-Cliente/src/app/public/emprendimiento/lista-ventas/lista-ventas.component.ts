import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { ToastService } from 'src/app/services/other/toastService.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { forkJoin, of, switchMap } from 'rxjs';
import { UsuarioService, } from 'src/app/services/Usuario/usuario.service';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { VentaService } from 'src/app/services/Venta/venta.service';
import { Email } from 'src/app/core/utils/Email';


@Component({
  selector: 'app-lista-ventas',
  templateUrl: './lista-ventas.component.html',
  styleUrls: ['./lista-ventas.component.css']
})

export class ListaVentasComponent implements OnInit {
  ventasSeleccionadas = [];
  detalleVentas: DetalleVenta[] = [];
  ventas: any[] = [];
  venta: any = {
    detalle: new DetalleVenta()
  };
  detalleVenta: DetalleVenta = new DetalleVenta();
  dialogDetalleVenta: boolean = false;
  estados: any[] = [
    { label: 'En proceso', value: '1' },
    { label: 'Listo para entregar', value: '2' },
    { label: 'Entregado', value: ['3', '4'] },
  ];

  @ViewChild('dt') dt!: Table;

  email: Email = new Email();

  constructor(
    private ventaService: VentaService,
    private detalleVentaService: DetalleVentaService,
    private productoService: ProductoService,
    private userService: UsuarioService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.cargarVentas();
  }

  cargarVentas() {
    this.ventas = [];

    this.detalleVentaService.obtenerVentasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).pipe(
      switchMap(detalleVentas => {
        this.detalleVentas = detalleVentas.map(detalleVenta => new DetalleVenta(detalleVenta));
        const productoIds = this.detalleVentas.map(detalleVenta => detalleVenta.IdProducto);
        const usuarioIds = this.detalleVentas.map(detalleVenta => detalleVenta.Venta.IdUsuario);
        if (productoIds.length === 0 || usuarioIds.length === 0) return of([]);
        const _productos$ = forkJoin(productoIds.map(id => this.productoService.obtenerProductoPorId(id)));
        const _usuarios$ = forkJoin(usuarioIds.map(id => this.userService.obtenerUsuarioPorId(id)));

        return forkJoin([_productos$, _usuarios$]);
      })
    ).subscribe({
      next: ([productos, usuarios]: any) => {
        this.ventas = this.detalleVentas.map((detalleVenta, index) => {
          const producto = new Producto(productos[index]);
          const usuario = new Usuario(usuarios[index]);
          return {
            Producto: producto.Nombre,
            Usuario: usuario.Nombre + ' ' + usuario.PrimerApellido + ' ' + usuario.SegundoApellido,
            Telefono: usuario.NumeroTelefono,
            Correo: usuario.Email,
            detalle: new DetalleVenta(detalleVenta),
          };
        });
      },
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Error al cargar las ventas', 3000);
      }
    });
  }

  busqueda(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const inputValue = inputElement.value;
    this.dt.filter(inputValue, 'detalle.codigo', 'contains');
  }

  obtenerFecha(fecha: Date) {
    return new Date(fecha).toLocaleDateString();
  }

  actualizarEstadoVenta(venta: any) {
    venta.detalle.Estado = venta.detalle.Estado === '1' ? '2' : '3';
    venta.detalle.FechaActualizacion = new Date();

    this.detalleVentaService.actualizarDetalleVenta(venta.detalle).subscribe({
      next: (response) => {
        this.toastService.mensajeExito('Exito', 'Estado de la venta actualizado.', 3000);
        this.enviarCorreo(venta.detalle.Estado, venta);
        this.cargarVentas();
      },
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Error al actualizar el estado.', 3000);
      }
    });
    this.dialogDetalleVenta = false;
  }

  mostrarDetalleVenta(venta: any) {
    this.dialogDetalleVenta = true;
    this.venta = venta;
  }

  obtenerEstadoVenta(estado: string) {
    switch (estado) {
      case '1':
        return 'En proceso';
      case '2':
        return 'Listo para entregar';
      case '3':
        return 'Entregado';
      case '4':
        return 'Entregado';
      default:
        return 'No definido';
    }
  }

  obtenerColorEstado(estado: string) {
    switch (estado) {
      case '1':
        return 'td td-orange';
      case '2':
        return 'td td-green';
      case '3':
        return 'td td-blue';
      case '4':
        return 'td td-blue';
      default:
        return 'td td-red';
    }
  }

  enviarCorreo(estado: any, venta: any) {
    let primerMensaje =
      `<p>Estimado/a ${venta.Usuario},</p>
        <p>El producto <strong>"${venta.Producto}"</strong>  que compro  ya se encuentra listo para ser entregado.</p>
        <p>Detalles del producto:</p>
        <p><strong>Nombre: </strong> ${venta.Producto} / <strong>Cantidad: </strong> ${venta.detalle.Cantidad} / <strong>Total: </strong> ${venta.detalle.SubTotal}  </p>
        <p><strong>Fecha de compra: </strong>${this.obtenerFecha(venta.detalle.FechaRegistro)}</p>
        <p>Saludos cordiales,</p>
        <p>${this.tokenService.getNombreEmprendimiento()}</p> `;
    let segundoMensaje =
      `<p>Estimado / a ${venta.Usuario},</p>
        <p>El producto <strong>"${venta.Producto}"</strong> que compro ya fue entregado.</p>
        <p>Detalles del producto:</p>
        <p><strong>Nombre: </strong> ${venta.Producto} / <strong>Cantidad: </strong> ${venta.detalle.Cantidad} / <strong>Total: </strong> ${venta.detalle.SubTotal}  </p>
        <p><strong>Fecha de compra: </strong>${this.obtenerFecha(venta.detalle.FechaRegistro)}</p>
        <p>Gracias por su compra, esperamos poder servirle nuevamente.</p>
        <p>Saludos cordiales, </p>
        <p> ${this.tokenService.getNombreEmprendimiento()} </p>`;

    this.email.IdUsuario = venta.detalle.venta.idUsuario;
    this.email.MailTo = venta.Correo;
    this.email.Subject = venta.Usuario;
    this.email.Titulo = this.tokenService.getNombreEmprendimiento();
    this.email.Mensaje = estado === '2' ? primerMensaje : segundoMensaje;
    this.email.UrlImagen = estado === '2' ?
      'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2Fimagen2.png?alt=media&token=3762d231-e260-4290-bc7e-9b0db6c1310d' :
      'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FImagen3.png?alt=media&token=f6ff13d8-c57f-4b3c-9351-9125d1d95bd4';

    this.ventaService.enviarCorreo(this.email).subscribe({
      error: (error) => {
        console.log(error);
      }
    });
  }

  filtrarVentas(event: any) {
    this.dt.filter(event, 'detalle.estado', 'in');
  }
}

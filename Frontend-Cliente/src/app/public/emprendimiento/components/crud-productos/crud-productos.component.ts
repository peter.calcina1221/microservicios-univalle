import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { Table } from 'primeng/table';
import { FileUploadHandlerEvent, FileUploadModule } from 'primeng/fileupload';
import { getDownloadURL, list, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { CategoriaProductoService } from 'src/app/services/Producto/categoria.service';
import { CategoriaProducto } from 'src/app/core/models/Producto/CategoriaProducto';
import { ConfirmationService } from 'primeng/api';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { ImagenProducto } from 'src/app/core/models/Producto/ImagenProducto';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-crud-productos',
  templateUrl: './crud-productos.component.html',
  styleUrls: ['./crud-productos.component.css']
})

export class CrudProductosComponent implements OnInit {
  @Input() idEmprendimiento: number = 0;
  estadoSeleccionado: number = 1;
  file?: File;
  producto: Producto = new Producto();
  productos: Producto[] = [];
  categoriasProducto: CategoriaProducto[] = [];
  listaImagenes: any[] = [];
  listaOriginalImagenes: any[] = [];
  productosSeleccionados: [] = [];
  estados: any[] = [];
  unidadMedidaSIN: any[] = [];
  codigoProductoSIN: any[] = [];
  imgUrl: string = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Productos_Image%2FIlustraci%C3%B3n%20de%20concepto%20de%20nuevo%20producto%20_%20Vector%20Gratis.jpg?alt=media&token=acf1147b-1409-43ac-a934-3c98da9eb56b';
  tituloDialogConfirmacion: string = '';
  operacion: string = '';
  estaInhabilitado: boolean = true;
  productDialog: boolean = false;
  isProductoEdit: boolean = false;
  dialogImagenVisible: boolean = false;
  mostrarSpinner: boolean = false;
  mensajeSpinner: string = '';
  mostrarTag: boolean = false;
  existeCodigo: boolean = false;

  @ViewChild('dt') dt!: Table;
  @ViewChild('fileUpload') fileUpload: any;

  //Campos para la validación
  camposRequeridos = ['nombre', 'codigo', 'descripcion', 'precioCosto', 'precioVenta', 'stockTotal', 'stockMinimo', 'codigoUnidadSIN', 'codigoProductoSIN'];

  constructor(
    private productoService: ProductoService,
    private categoriaService: CategoriaProductoService,
    private confirmationService: ConfirmationService,
    private storage: Storage,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.cargarDatosProductos();
    this.cargarCategorias();
    this.cargarProductoSIN();
    this.cargarUnidadSIN();

    this.estados = [
      { nombre: 'Productos activos', value: 1 },
      { nombre: 'Productos inactivos', value: 0 }
    ];
  }

  cargarProductoSIN() {
    this.productoService.obtenerProductosSIN().subscribe({
      next: (productos: any[]) => {
        productos.map((producto: any) => {
          this.codigoProductoSIN.push({
            nombre: producto.nombre,
            value: producto.valor
          });
        });
      }
    });
  }

  cargarUnidadSIN() {
    this.productoService.obtenerUnidadSIN().subscribe({
      next: (unidades: any[]) => {
        unidades.map((unidad: any) => {
          this.unidadMedidaSIN.push({
            nombre: unidad.nombre,
            value: unidad.valor
          });
        });
      }
    });
  }

  cargarDatosProductos() {
    this.productos = [];

    this.productoService.obtenerProductosPorIdEmprendimientoEstado(this.idEmprendimiento, this.estadoSeleccionado).subscribe({
      next: (productos: Producto[]) => {
        productos.forEach(producto => {
          this.producto = new Producto(producto);
          this.producto.ImagenesProducto = this.producto.ImagenesProducto.sort((a) => {
            return a.Principal ? -1 : 1;
          });

          this.productos.push(new Producto(this.producto));
        });

        this.limpiarProducto();
        this.verificarProductos(this.productos);
      }
    });
  }

  verificarProductos(productos: Producto[]) {
    if (productos.length === 0) {
      if (this.estadoSeleccionado === 1) this.toastService.mensajeInfo('Sin productos', 'No se encontraron productos activos.', 3000);
      else if (this.estadoSeleccionado === 0) this.toastService.mensajeInfo('Sin productos', 'No se encontraron productos inactivos.', 3000);
    }
  }

  cargarCategorias() {
    this.categoriaService.obtenerCategoriasProducto().subscribe({
      next: (categorias: CategoriaProducto[]) => {
        categorias.forEach(categoria => {
          this.categoriasProducto.push(new CategoriaProducto(categoria));
        });
      }
    });
  }

  async guardarProductoNuevo() {
    this.producto.IdEmpresa = this.idEmprendimiento;
    this.producto.StockRestante = this.producto.StockTotal;

    if (!GeneralUtils.validateEmptyFields(this.producto, this.camposRequeridos)) {
      this.toastService.mensajeAdvertencia('Campos vacios', 'Debe completar todos los campos requeridos.', 3000);
    } else if (this.producto.CategoriaProducto.IdCategoria === null || this.producto.CategoriaProducto.IdCategoria === 0) {
      this.toastService.mensajeAdvertencia('Campos vacios', 'Debe seleccionar una categoría.', 3000);
    } else if (!this.file) {
      this.toastService.mensajeAdvertencia('Imagen no seleccionada', 'Debe seleccionar una imagen para el producto.', 3000);
    }
    else {
      const url = await this.obtenerUrlImagen();
      let imagenProducto = new ImagenProducto();
      imagenProducto.ImgUrl = url;
      imagenProducto.Principal = true;
      this.producto.ImagenesProducto = [imagenProducto];
      this.mostrarSpinner = true;
      this.mensajeSpinner = 'Registrando producto...';

      this.productoService.nuevoProducto(this.producto).subscribe({
        next: (response) => {
          if (response.status == 201) {
            setTimeout(() => {
              this.mostrarSpinner = false;
              this.toastService.mensajeExito('Registro exitoso', 'El producto fue registrado exitosamente.', 3000);
              this.cargarDatosProductos();
              this.limpiarProducto();
              this.productDialog = false;
              this.mostrarTag = false;
            }, 2000);
          }
          else {
            this.mostrarSpinner = false;
            this.toastService.mensajeError('Error', 'Ha ocurrido un error en el registro, intente nuevamente.', 3000);
          }
        },
        error: (error) => {
          console.log(error);
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error', 'Ha ocurrido un error en el registro, intente nuevamente.', 3000);
        }
      });
    }
  }

  openDialogCrearProducto() {
    this.isProductoEdit = false;
    this.productDialog = true
  }

  openDialogEditarProducto(producto: Producto) {
    this.producto = producto;
    this.productDialog = true;
    this.isProductoEdit = true;
  }

  editarProducto() {
    if (!GeneralUtils.validateEmptyFields(this.producto, this.camposRequeridos)) {
      this.toastService.mensajeAdvertencia('Campos vacios', 'Debe completar todos los campos requeridos.', 3000);
    } else if (this.producto.CategoriaProducto.IdCategoria === null || this.producto.CategoriaProducto.IdCategoria === 0) {
      this.toastService.mensajeAdvertencia('Campos vacios', 'Debe seleccionar una categoría.', 3000);
    }
    else {
      this.mostrarSpinner = true;
      this.mensajeSpinner = 'Actualizando producto...';

      this.productoService.actualizarProducto(this.producto).subscribe({
        next: () => {
          setTimeout(() => {
            this.toastService.mensajeExito('Actualización exitosa', 'El producto fue actualizado correctamente.', 3000);
            this.cargarDatosProductos();
            this.limpiarProducto();
            this.mostrarSpinner = false;
            this.productDialog = false;
            this.confirmationService.close();
          }, 2000);
        },
        error: (error) => {
          console.log(error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error en la actualización, intente nuevamente.', 3000);
          this.mostrarSpinner = false;
        }
      });
    }
  }

  confirmacionEliminarProducto() {
    this.producto.Estado = '0';
    this.mostrarSpinner = true;
    this.mensajeSpinner = 'Eliminando producto...';

    this.productoService.actualizarProducto(this.producto).subscribe({
      next: () => {
        setTimeout(() => {
          this.toastService.mensajeExito('Eliminación exitosa', 'El producto fue eliminado correctamente.', 3000);
          this.cancelar();
          this.cargarDatosProductos();
          this.limpiarProducto();
          this.mostrarSpinner = false;
        }, 2000);
      },
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Ha ocurrido un error en la eliminación, intente nuevamente.', 3000);
        this.mostrarSpinner = false;
      }
    });
  }

  confirmacionEliminarProductos() {
    //Cambiamos el estado a 0 de los productos seleccionados
    let contadorSolicitudesExitosas = 0;
    this.mostrarSpinner = true;
    this.mensajeSpinner = 'Eliminando productos...';

    this.productosSeleccionados.forEach((producto: Producto) => {
      producto.Estado = '0';
      this.productoService.actualizarProducto(producto).subscribe({
        next: (response) => {
          contadorSolicitudesExitosas++;

          if (contadorSolicitudesExitosas === this.productosSeleccionados.length) {
            setTimeout(() => {
              this.toastService.mensajeExito('Eliminación exitosa', 'Los productos fueron eliminados correctamente.', 3000);
              this.cancelar();
              this.cargarDatosProductos();
              this.limpiarProducto();
              this.mostrarSpinner = false;
            }, 2000);
          }
        },
        error: (error) => {
          console.log(error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error en la eliminación, intente nuevamente.', 3000);
          this.mostrarSpinner = false;
        }
      });
    });
  }

  activarProducto() {
    this.producto.Estado = '1';
    this.productoService.actualizarProducto(this.producto).subscribe({
      next: (response) => {
        this.toastService.mensajeExito('Activación exitosa', 'El producto fue activado correctamente.', 3000);
        this.cancelar();
        this.cargarDatosProductos();
        this.limpiarProducto();
      },
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Ha ocurrido un error en la activación, intente nuevamente.', 3000);
      }
    });
  }

  activarProductos() {
    //Cambiamos el estado a 1 de los productos seleccionados
    let contadorSolicitudesExitosas = 0;

    this.productosSeleccionados.forEach((producto: Producto) => {
      producto.Estado = '1';
      this.productoService.actualizarProducto(producto).subscribe({
        next: (response) => {
          contadorSolicitudesExitosas++;

          if (contadorSolicitudesExitosas === this.productosSeleccionados.length) {
            this.toastService.mensajeExito('Activación exitosa', 'Los productos fueron activados correctamente.', 3000);
            this.cancelar();
            this.cargarDatosProductos();
            this.limpiarProducto();
          }
        },
        error: (error) => {
          console.log(error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error en la activación, intente nuevamente.', 3000);
        }
      });
    });
  }

  async editarImagenesProducto() {
    this.confirmationService.close();
    this.dialogImagenVisible = false;

    //Añadimos las Urls de las imagenes al emprendimiento
    await this.obtenerUrlsImagen();
    let imagenProducto: ImagenProducto;

    //Reemplazamos la lista de imagenes del emprendimiento por la lista de imagenes actualizada
    this.producto.ImagenesProducto = [];

    this.listaImagenes.forEach((item) => {
      imagenProducto = new ImagenProducto();
      imagenProducto.IdImagenProducto = item.idImagen ? item.idImagen : 0;
      imagenProducto.ImgUrl = item.imgUrl;
      imagenProducto.Principal = item.principal;
      this.producto.ImagenesProducto.push(imagenProducto);
    });

    this.productoService.actualizarProducto(this.producto).subscribe({
      next: (respuesta) => {
        this.toastService.mensajeExito('Actualización', `Las imagenes del producto "${this.producto.Nombre}" fueron actualizadas con éxito.`, 5000);
        this.cargarDatosProductos();
        this.limpiarProducto();
        this.limpiarListaImagenes();
      }
    });
  }

  limpiarProducto() {
    this.producto = new Producto();
    this.productosSeleccionados = [];
  }

  limpiarListaImagenes() {
    this.listaImagenes = [];
    this.listaOriginalImagenes = [];
  }

  busqueda(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const inputValue = inputElement.value;
    this.dt.filterGlobal(inputValue, 'contains');
  }

  abrirDialogEdicionImagen(producto: Producto) {
    this.producto = producto;
    this.listaImagenes = [];
    this.listaImagenes = producto.ImagenesProducto.map(item => ({
      imgUrl: item.ImgUrl,
      principal: item.Principal,
      idImagen: item.IdImagenProducto
    }));

    this.listaOriginalImagenes = this.listaImagenes.slice();
    this.dialogImagenVisible = true;
  }

  obtenerImagen(event: FileUploadHandlerEvent) {
    const file = event.files[0];
    this.file = file;
    this.mostrarImagen(file);
  }

  mostrarImagen(file: File) {
    const reader = new FileReader();

    reader.onload = (event: any) => {
      this.imgUrl = event.target.result;
    };

    reader.readAsDataURL(file);
    this.fileUpload.clear();
  }

  async obtenerUrlImagen(): Promise<string> {
    const fileName = GeneralUtils.encryptFileName(this.file!.name) + '.' + this.file!.name.split('.').pop();
    const imgRef = ref(this.storage, `Productos_Image/${fileName}`);

    try {
      await uploadBytes(imgRef, this.file!);
      const url = await getDownloadURL(imgRef);
      return url;
    } catch (error) {
      console.log('Error al obtener la URL pública', error);
      this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba subir la imagen, intente nuevamente.', 6000);
      return '';
    }
  }

  async obtenerUrlsImagen() {
    for (let i = 0; i < this.listaImagenes.length; i++) {
      if (this.listaImagenes[i].file) {
        const fileName = GeneralUtils.encryptFileName(this.listaImagenes[i].file.name) + '.' + this.listaImagenes[i].file.name.split('.').pop();
        const imgRef = ref(this.storage, `Productos_Image/${fileName}`);

        try {
          await uploadBytes(imgRef, this.listaImagenes[i].file);
          const url = await getDownloadURL(imgRef);
          this.listaImagenes[i].imgUrl = url;
        } catch (error) {
          console.log('Error al obtener la URL pública', error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba subir la imagen, intente nuevamente.', 7000);
          this.listaImagenes[i].imgUrl = '';
        }
      }
    }
  }

  agregarImagen(event: FileUploadHandlerEvent, fileUpload: any) {
    const file = event.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.listaImagenes.push({ imgUrl: reader.result, principal: false, file: file });
      fileUpload.clear();
    };

    this.estaInhabilitado = false;
  }

  actualizarListaImagenes() {
    this.listaImagenes.forEach((item, index) => {
      item.principal = index === 0 ? true : false;
    });

    this.estaInhabilitado = this.listaOriginalImagenes[0].principal ? true : false;
  }

  eliminarImagen(imagen: any) {
    if (imagen.principal) {
      this.listaImagenes[1].principal = true;
    }

    this.listaImagenes = this.listaImagenes.filter(item => item.imgUrl !== imagen.imgUrl);
    this.estaInhabilitado = JSON.stringify(this.listaOriginalImagenes) === JSON.stringify(this.listaImagenes) ? true : false;
  }

  dialogConfirmacion(operacion: string, productoSeleccionado?: Producto) {
    let mensaje = '';
    this.operacion = operacion;

    switch (operacion) {
      case 'MP':
        this.tituloDialogConfirmacion = 'Eliminar productos';
        mensaje = '¿Está seguro que desea eliminar los productos seleccionados?';
        break;
      case 'UP':
        this.producto = productoSeleccionado!;
        this.tituloDialogConfirmacion = 'Eliminar producto ' + productoSeleccionado?.Nombre;
        mensaje = '¿Está seguro que desea eliminar el producto seleccionado?';
        break;
      case 'EP':
        this.tituloDialogConfirmacion = 'Edición del producto';
        mensaje = '¿Está seguro que desea guardar los cambios realizados?';
        break;
      case 'EI':
        this.tituloDialogConfirmacion = 'Edición de imagenes'
        mensaje = '¿Está seguro que desea guardar los cambios realizados?';
        break;
      case 'AP':
        this.producto = productoSeleccionado!;
        this.tituloDialogConfirmacion = 'Habilitar producto ' + productoSeleccionado?.Nombre;
        mensaje = '¿Está seguro que desea habilitar el producto seleccionado?';
        break;
      case 'HP':
        this.tituloDialogConfirmacion = 'Habilitar productos'
        mensaje = '¿Está seguro que desea habilitar los productos seleccionados?';
        break;
    }

    this.confirmationService.confirm({
      message: mensaje,
      icon: 'pi pi-exclamation-triangle',
      key: 'dialogEditProducto',
    });
  }

  //Cancelamos la operación
  cancelar() {
    this.confirmationService.close();
  }

  guardarCambios(operacion: string) {
    switch (operacion) {
      case 'MP':
        this.confirmacionEliminarProductos();
        break;
      case 'UP':
        this.confirmacionEliminarProducto();
        break;
      case 'EP':
        this.editarProducto();
        break;
      case 'EI':
        this.editarImagenesProducto();
        break;
      case 'AP':
        this.activarProducto();
        break;
      case 'HP':
        this.activarProductos();
        break;
    }
  }

  onTabChange(event: any) {
    const panelSeleccionado = event.index;

    const panels = document.querySelectorAll('.p-dialog .p-dialog-content');
    panels.forEach((panel: any) => {
      panel.style.overflowY = panelSeleccionado === 0 ? 'auto' : 'initial';
    });
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      this.producto[propiedad] = texto.value;
    }
  }

  validarCodigo() {
    if (this.producto.Codigo.length <= 0) {
      this.mostrarTag = false;
    } else {
      this.mostrarTag = true;
      this.productoService.validarCodigoProducto(this.producto.Codigo, this.idEmprendimiento).subscribe({
        next: (response) => {
          console.log(response);
          this.existeCodigo = response ? true : false;
        }
      });
    }
  }
}

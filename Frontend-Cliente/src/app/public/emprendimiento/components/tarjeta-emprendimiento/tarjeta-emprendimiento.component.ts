import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';
import { CategoriaService } from 'src/app/services/Empresa/categoria.service';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { Estadistica } from '../../../../core/models/Calificacion/Estadistica';

@Component({
  selector: 'app-tarjeta-emprendimiento',
  templateUrl: './tarjeta-emprendimiento.component.html',
  styleUrls: ['./tarjeta-emprendimiento.component.css']
})
export class TarjetaEmprendimientoComponent implements OnInit {
  @Input() emprendimiento: Emprendimiento = new Emprendimiento();
  @Output() enviarDatos = new EventEmitter<{ id: number; show: boolean; isProduct: boolean }>();

  calificacion: number = 0;
  general: GeneralUtils = new GeneralUtils();

  constructor(
    private router: Router,
    private estadisticaService: EstadisticaService,
  ) { }

  ngOnInit() {
    this.emprendimiento = new Emprendimiento(this.emprendimiento);
    this.emprendimiento.ImagenesEmpresa = this.emprendimiento.ImagenesEmpresa.filter((imagen) => imagen.Principal === true);

    this.obtenerCalificacion();
  }

  obtenerCalificacion(): void {
    this.estadisticaService.obtenerCalificacion(this.emprendimiento.IdEmpresa).subscribe((estadistica: Estadistica) => (this.calificacion = new Estadistica(estadistica).Promedio));
  }

  mostrarInformacion(idEmprendimiento: number): void {
    const datos = { id: idEmprendimiento, show: true, isProduct: false };
    this.enviarDatos.emit(datos);
  }

  verEmprendimiento(idEmprendimiento: number) {
    const newId = this.general.encryptId(idEmprendimiento);
    this.router.navigate(['/vistaEmprendimiento', newId]);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, forkJoin, of, switchMap } from 'rxjs';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';
import { ReservaService } from 'src/app/services/Reserva/reserva.service';
import { Producto } from '../../../core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { ToastService } from 'src/app/services/other/toastService.service';
import { Table } from 'primeng/table';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { Email } from 'src/app/core/utils/Email';

interface Estados {
  label: string;
  value: string | string[];
}

@Component({
  selector: 'app-lista-reservas',
  templateUrl: './lista-reservas.component.html',
  styleUrls: ['./lista-reservas.component.css']
})

export class ListaReservasComponent implements OnInit {
  reservas: any[] = [];
  detalleReservas: DetalleReserva[] = [];
  reserva: any = {
    detalle: new DetalleReserva()
  };
  dialogDetalleReserva: boolean = false;
  estados: Estados[] | undefined;
  estado: Estados | undefined;

  @ViewChild('dt') dt!: Table;

  constructor(
    private detalleReservaService: DetalleReservaService,
    private productoService: ProductoService,
    private userService: UsuarioService,
    private reservaService: ReservaService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.cargarReservas();
    this.estados = [
      { label: 'Pendiente', value: '1' },
      { label: 'Confirmado', value: '2' },
      { label: 'Rechazado', value: '3' },
      { label: 'Listo para retirar', value: '4' },
      { label: 'Entregado', value: ['5', '6'] }
    ];
  }

  cargarReservas() {
    const _reservas$ = this.detalleReservaService.obtenerReservasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento());
    this.construirDetalleReserva(_reservas$);

    this.estado?.value ? this.filtrarReservas(this.estado.value) : null;
  }

  construirDetalleReserva(detalleReserva: Observable<DetalleReserva[]>) {
    this.reservas = [];

    detalleReserva.pipe(
      switchMap(reservas => {
        this.detalleReservas = reservas.map(detalleReserva => new DetalleReserva(detalleReserva));
        const productoIds = this.detalleReservas.map(detalleReserva => detalleReserva.IdProducto);
        const usuarioIds = this.detalleReservas.map(detalleReserva => detalleReserva.Reserva.IdUsuario);
        if (productoIds.length === 0 || usuarioIds.length === 0) return of([]);
        const _productos$ = forkJoin(productoIds.map(id => this.productoService.obtenerProductoPorId(id)));
        const _usuarios$ = forkJoin(usuarioIds.map(id => this.userService.obtenerUsuarioPorId(id)));

        return forkJoin([_productos$, _usuarios$]);
      })
    ).subscribe({
      next: ([productos, usuarios]: any) => {
        this.reservas = this.detalleReservas.map((detalleReserva, index) => {
          const producto = new Producto(productos[index]);
          const usuario = new Usuario(usuarios[index]);

          return {
            detalle: new DetalleReserva(detalleReserva),
            Producto: producto.Nombre,
            Usuario: usuario.Nombre + ' ' + usuario.PrimerApellido + ' ' + usuario.SegundoApellido,
            Correo: usuario.Email,
            Telefono: usuario.NumeroTelefono
          };
        });
      },
      error: (err) => {
        console.log(err);
        this.toastService.mensajeError('Error', 'No se pudo cargar las reservas', 4000);
      }
    });
  }

  /**
 * Metodo para actualizar el estado de la reserva
 * @param { DetalleReserva } reserva Obtiene los datos del detalleReserva para actualizar.
 * @param { string } estado Recibe solo 2 valores, este parametro es opcional.
 * - C: Confirmar reserva.
 * - R: Rechazar reserva.
 */
  actualizarEstadoReserva(reserva: any, estado?: string) {
    let nuevoEstado = this.obtenerNuevoEstado(reserva.detalle.Estado, estado);
    reserva.detalle.Estado = nuevoEstado;
    reserva.detalle.FechaActualizacion = new Date();

    this.detalleReservaService.actualizarDetalleReserva(reserva.detalle).subscribe({
      next: (response) => {
        this.toastService.mensajeExito('Exito', 'Estado de la reserva actualizado.', 3000);
        this.enviarCorreo(reserva);
        this.cargarReservas();
      },
      error: (error) => {
        this.toastService.mensajeError('Error', 'Error al actualizar el estado.', 3000);
      }
    });

    this.dialogDetalleReserva = false;
  }

  obtenerNuevoEstado(estado: string, confirmacionReserva?: string) {
    switch (estado) {
      case '1':
        return confirmacionReserva === 'C' ? '2' : '3';
      case '2':
        return '4';
      case '4':
        return '5';
      default:
        return '1';
    }
  }

  mostrarDetalleReserva(reserva: any) {
    this.dialogDetalleReserva = true;
    this.reserva = reserva;
  }

  obtenerFecha(fecha: Date) {
    return new Date(fecha).toLocaleDateString();
  }

  obtenerColorEstado(estado: string) {
    switch (estado) {
      case '1':
        return 'pendiente';
      case '2':
        return 'confirmado';
      case '3':
        return 'rechazado';
      case '4':
        return 'listoRetirar';
      case '5':
        return 'entregrado';
      case '6':
        return 'entregrado';
      default:
        return 'pendiente';
    }
  }

  obtenerEstadoReserva(estado: string) {
    switch (estado) {
      case '1':
        return 'Pendiente';
      case '2':
        return 'Confirmado';
      case '3':
        return 'Rechazado';
      case '4':
        return 'Listo para retirar';
      case '5':
        return 'Entregado';
      case '6':
        return 'Entregado';
      default:
        return 'No definido';
    }
  }

  busqueda(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const inputValue = inputElement.value;
    this.dt.filter(inputValue, 'detalle.codigoReserva', 'contains');
  }

  filtrarReservas(event: any) {
    this.dt.filter(event, 'detalle.estado', 'in');
  }

  enviarCorreo(reserva: any) {
    let email = new Email();
    email.IdUsuario = reserva.detalle.Reserva.IdUsuario;
    email.MailTo = reserva.Correo;
    email.Subject = reserva.Usuario;
    email.Titulo = this.tokenService.getNombreEmprendimiento();
    switch (reserva.detalle.Estado) {
      case '2':
        email.Mensaje = `
        Tu reserva ha sido confirmada <br>
        Aquí tienes los detalles de tu reserva: <br>
        <strong>Producto:</strong> ${reserva.Producto} <br>
        <strong>Fecha de reserva:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaSolicitud)} <br>
        <strong>Fecha de recojo:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaEntrega)} <br>
        <strong>Código de reserva:</strong> ${reserva.detalle.CodigoReserva} <br>
        <strong>Costo total:</strong> ${reserva.detalle.SubTotal}Bs <br>
        <strong>Estado de reserva:</strong> <br>
        `;
        email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FRESERVA2.png?alt=media&token=581dfa29-22f7-411c-8bde-513fc2362845';
        break;
      case '3':
        email.Mensaje = `
        Lamentamos informarte que tu reserva para el producto <strong>${reserva.Producto}</strong> ha sido rechazada. <br>
        Aquí tienes los detalles de tu reserva: <br>
        <strong>Fecha de reserva:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaSolicitud)} <br>
        <strong>Fecha de recojo:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaEntrega)} <br>
        <strong>Código de reserva:</strong> ${reserva.detalle.CodigoReserva} <br>
        <strong>Costo total:</strong> ${reserva.detalle.SubTotal}Bs <br>
        <strong>Estado de reserva:</strong> Rechazado.<br>
        Agradecemos tu comprensión y esperamos poder servirte nuevamente. <br>
        `;
        email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FRESERVA5.png?alt=media&token=7655ae5c-c765-46b9-b64f-0fa7264276d3';
        break;
      case '4':
        email.Mensaje = `
        Tu reserva esta lista para ser retirada <br>
        Aquí tienes los detalles de tu reserva: <br>
        <strong>Producto:</strong> ${reserva.Producto} <br>
        <strong>Fecha de reserva:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaSolicitud)} <br>
        <strong>Fecha de recojo:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaEntrega)} <br>
        <strong>Código de reserva:</strong> ${reserva.detalle.CodigoReserva} <br>
        <strong>Costo total:</strong> ${reserva.detalle.SubTotal}Bs <br>
        <strong>Estado de reserva:</strong> <br>
        `;
        email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FRESERVA3.png?alt=media&token=89fd3cae-c49a-4637-b3d2-82651f468fc4';
        break;
      case '5':
        email.Mensaje = `
        Tu reserva ha sido entregada <br>
        Aquí tienes los detalles de tu reserva: <br>
        <strong>Producto:</strong> ${reserva.Producto} <br>
        <strong>Fecha de reserva:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaSolicitud)} <br>
        <strong>Fecha de recojo:</strong> ${this.obtenerFecha(reserva.detalle.Reserva.FechaEntrega)} <br>
        <strong>Código de reserva:</strong> ${reserva.detalle.CodigoReserva} <br>
        <strong>Costo total:</strong> ${reserva.detalle.SubTotal}Bs <br>
        <strong>Estado de reserva:</strong> ENTREGADO. <br>
        ¡Gracias por tu compra! Esperamos que disfrutes de tu producto. <br>
        `;
        email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FRESERVA4.png?alt=media&token=a3c3cfb7-cc1f-4ab5-becb-92bb07860faa';
        break;
    }

    this.reservaService.enviarCorreo(email).subscribe({
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Error al enviar el correo.', 3000);
      }
    });
  }
}

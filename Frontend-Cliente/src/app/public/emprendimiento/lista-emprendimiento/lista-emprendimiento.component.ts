import { Component, OnInit } from '@angular/core';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';

@Component({
  selector: 'app-lista-emprendimiento',
  templateUrl: './lista-emprendimiento.component.html',
  styleUrls: ['./lista-emprendimiento.component.css']
})
export class ListaEmprendimientoComponent implements OnInit {
  mostrarDialogDetalle: boolean = false;
  dialogId: number = 0;
  isProduct: boolean = false;
  emprendimientos: Emprendimiento[] = []

  constructor(
    private emprendimientosService: EmprendimientoService
  ) { }

  ngOnInit() {
    this.cargarEmprendimientos();
  }

  recibirDatos(datos: { id: number, show: boolean, isProduct: boolean }): void {
    this.dialogId = datos.id;
    this.mostrarDialogDetalle = datos.show;
    this.isProduct = datos.isProduct;
  }

  cargarEmprendimientos(): void {
    this.emprendimientosService.obtenerEmprendimientos().subscribe({
      next: (emprendimientos: Emprendimiento[]) => {
        this.emprendimientos = emprendimientos;
      }
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { GeneralUtils } from '../../../core/utils/General-utils';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';
import { Estadistica } from '../../../core/models/Calificacion/Estadistica';

@Component({
  selector: 'app-vista-emprendimiento',
  templateUrl: './vista-emprendimiento.component.html',
  styleUrls: ['./vista-emprendimiento.component.css']
})
export class VistaEmprendimientoComponent implements OnInit {
  productos: Producto[] = [];
  emprendimiento: Emprendimiento = new Emprendimiento();
  imagenPrincipal: string = '';
  listaImagenes: any[] = [];
  generalUtils: GeneralUtils = new GeneralUtils();
  calificacion: number = 0;
  //Variables para el dialog de Detalle
  mostrarDialogDetalle: boolean = false;
  dialogId: number = 0;
  isProduct: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private emprendimientoService: EmprendimientoService,
    private productoService: ProductoService,
    private estadisticaService: EstadisticaService
  ) { }

  async ngOnInit() {
    window.scrollTo(0, 0);
    await this.cargarEmprendimiento();
  }

  async cargarEmprendimiento() {
    const idEmprendimiento = this.generalUtils.decryptId(this.activatedRoute.snapshot.paramMap.get('idEmprendimiento')!);
    this.emprendimientoService.obtenerEmprendimientoPorId(idEmprendimiento).subscribe(
      (emprendimiento) => {
        this.emprendimiento = new Emprendimiento(emprendimiento);
        this.imagenPrincipal = this.emprendimiento.ImagenesEmpresa.find(item => item.Principal === true)!.ImgUrl;
        this.listaImagenes = this.emprendimiento.ImagenesEmpresa.sort((a) => {
          return a.Principal ? -1 : 1;
        }).map(item => ({
          imgUrl: item.ImgUrl,
          principal: item.Principal,
          idImagen: item.IdImagenEmpresa
        }));

        this.cargarProductos(this.emprendimiento.IdEmpresa);
        this.cargarCalificacion(this.emprendimiento.IdEmpresa);
      }
    );
  }

  cargarProductos(idEmprendimiento: number) {
    this.productoService.obtenerProductosPorIdEmprendimientoEstado(idEmprendimiento, 1).subscribe((productos) => {
      this.productos = productos.map(item => new Producto(item));
    });
  }

  cargarCalificacion(idEmprendimiento: number) {
    this.estadisticaService.obtenerCalificacion(idEmprendimiento).subscribe((estadistica) => { this.calificacion = new Estadistica(estadistica).Promedio; });
  }

  cambiarImagenPrincipal(imagen: string) {
    this.imagenPrincipal = imagen;
  }

  recibirDatos(datos: { id: number, show: boolean, isProduct: boolean }): void {
    this.dialogId = datos.id;
    this.mostrarDialogDetalle = datos.show;
    this.isProduct = datos.isProduct;
  }
}

/// <reference types="@types/googlemaps" />
import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FileUploadHandlerEvent } from 'primeng/fileupload';
import { getDownloadURL, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { CategoriaService } from 'src/app/services/Empresa/categoria.service';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DepartamentoService } from '../../../services/Empresa/departamento.service';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { ImagenEmpresa } from 'src/app/core/models/Empresa/ImagenEmpresa';
import { ToastService } from 'src/app/services/other/toastService.service';
import { EstadisticaService } from 'src/app/services/Calificacion/estadistica.service';
import { Estadistica } from 'src/app/core/models/Calificacion/Estadistica';

interface Categorias {
  nombreCategoria: string,
  idCategoria: number
}

interface Departamentos {
  nombre: string,
  idDepartamento: number
}
@Component({
  selector: 'app-perfil-emprendimiento',
  templateUrl: './perfil-emprendimiento.component.html',
  providers: [ConfirmationService],
  styleUrls: ['./perfil-emprendimiento.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PerfilEmprendimientoComponent implements OnInit, AfterViewInit {
  listaImagenes: any[] = [];
  listaOriginalImagenes: any[] = [];
  imagenPrincipal: string = '';
  dialogVisible: boolean = false;
  dialogImagenVisible: boolean = false;
  calificacion: number = 0;
  emprendimiento: Emprendimiento = new Emprendimiento();
  categorias: Categorias[] = [];
  generalUtils: GeneralUtils = new GeneralUtils();

  ejecutarMetodoEditarImagen: boolean = false;
  estaInhabilitado: boolean = true;
  file: File[] = [];

  // * Variables para el mapa
  @ViewChild('divMapa') mapa!: ElementRef;
  @ViewChild('inputDireccion') inputDireccion!: ElementRef;

  departamentos: Departamentos[] = [];
  departamentoSeleccionado: number = 0;
  map!: google.maps.Map;
  marker!: google.maps.Marker;

  constructor(
    private toastService: ToastService,
    private emprendimientoService: EmprendimientoService,
    private categoriaService: CategoriaService,
    private tokenService: TokenService,
    private confirmationService: ConfirmationService,
    private departamentoService: DepartamentoService,
    private estadisticaService: EstadisticaService,
    private storage: Storage,
    private render2: Renderer2,
  ) { }

  async ngOnInit() {
    await this.cargarDatosEmprendimiento();
    this.obtenerCategorias();
    this.cargarDepartamentos();
  }

  ngAfterViewInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        await this.cargarMapa(position);
        this.cargarAutocompletado();
      });
    }
  }

  cambiarImagenPrincipal(imagen: string) {
    this.imagenPrincipal = imagen;
  }

  async cargarDatosEmprendimiento() {
    this.emprendimientoService.obtenerEmprendimientoPorIdUsuario(this.tokenService.getIdUsuario()).subscribe({
      next: (emprendimiento: Emprendimiento) => {
        console.log(emprendimiento);
        this.emprendimiento = new Emprendimiento(emprendimiento);
        this.imagenPrincipal = this.emprendimiento.ImagenesEmpresa.find(item => item.Principal === true)!.ImgUrl;
        this.listaImagenes = this.emprendimiento.ImagenesEmpresa.sort((a) => {
          return a.Principal ? -1 : 1;
        }).map(item => ({
          imgUrl: item.ImgUrl,
          principal: item.Principal,
          idImagen: item.IdImagenEmpresa
        }));

        this.listaOriginalImagenes = this.listaImagenes.slice();

        this.estadisticaService.obtenerCalificacion(this.emprendimiento.IdEmpresa).subscribe({
          next: (calificacion) => {
            this.calificacion = new Estadistica(calificacion).Promedio;
          },
          error: (error) => {
            console.log(error);
            this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba obtener la calificación del emprendimiento.', 5000);
          }
        });
      }
    });
  }

  obtenerCategorias() {
    this.categoriaService.obtenerCategorias().subscribe({
      next: (listaCategorias: any[]) => {
        this.categorias = listaCategorias.map(item => ({
          nombreCategoria: item.nombre,
          idCategoria: item.idCategoriaEmpresa
        }));
      }
    });
  }

  cargarDepartamentos() {
    this.departamentoService.obtenerDepartamentos().subscribe({
      next: (departamentos: any[]) => {
        this.departamentos = departamentos.map(item => ({
          nombre: item.nombre,
          idDepartamento: item.idDepartamento
        }));
      }
    });
  }

  abrirDialogImagen() {
    this.ejecutarMetodoEditarImagen = true;
    this.dialogImagenVisible = true;
  }

  agregarImagen(event: FileUploadHandlerEvent, fileUpload: any) {
    const file = event.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => {
      this.listaImagenes.push({ imgUrl: reader.result, principal: false, file: file });
      fileUpload.clear();
    };

    this.estaInhabilitado = false;
  }

  /**
   * Establecemos la primera imagen de la lista como principal
   */
  actualizarLista() {
    this.listaImagenes.forEach((item, index) => {
      item.principal = index === 0 ? true : false;
    });

    this.estaInhabilitado = this.listaOriginalImagenes[0].principal ? true : false;
  }

  quitarImagen(imagen: any) {
    if (imagen.principal) {
      this.listaImagenes[1].principal = true;
    }

    this.listaImagenes = this.listaImagenes.filter(item => item.imgUrl !== imagen.imgUrl);
    this.estaInhabilitado = JSON.stringify(this.listaOriginalImagenes) === JSON.stringify(this.listaImagenes) ? true : false;
  }

  dialogConfirmacion(tipoMensaje: number) {
    this.confirmationService.confirm({
      message: tipoMensaje === 1 ? '¿Está seguro que desea guardar los cambios realizados?' : '¿Estás seguro de cambiar las imagenes? Esta acción modificara las imagenes ya elegidas anteriormente.',
      icon: 'pi pi-exclamation-triangle',
      key: 'dialogEditPerfil',
    });
  }

  //Cancelamos la edición de la información del emprendimiento
  cancelar() {
    this.confirmationService.close();
  }

  //Actualizamos los datos del emprendimiento
  guardarCambios() {
    this.confirmationService.close();
    const camposRequeridos = ['categoriaEmpresa', 'nombreEmpresa', 'direccion', 'descripcion', 'numeroCelular', 'latitud', 'longitud', 'departamento'];
    if (!GeneralUtils.validateEmptyFields(this.emprendimiento, camposRequeridos)) {
      this.toastService.mensajeAdvertencia('Campos vacíos', 'Debe llenar todos los campos requeridos.', 5000);
    } else if (!GeneralUtils.validateCellphone(this.emprendimiento.NumeroCelular)) {
      this.toastService.mensajeAdvertencia('Error', 'El número de celular ingresado no es válido.', 5000);
    } else if (!GeneralUtils.validateLongitudLatitud(this.emprendimiento.Longitud, this.emprendimiento.Latitud)) {
      this.toastService.mensajeAdvertencia('Ubicación inválida', 'Debe seleccionar una ubicación en el mapa.', 5000);
    } else {
      this.emprendimientoService.actualizarEmprendimiento(this.emprendimiento).subscribe({
        next: (respuesta) => {
          this.toastService.mensajeExito('Éxito', 'Los datos del emprendimiento fueron actualizados con éxito.', 5000);
          this.cargarDatosEmprendimiento();
          this.dialogVisible = false;
        },
        error: (error) => {
          console.log(error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba actualizar los datos del emprendimiento, intente nuevamente.', 5000);
        }
      });
    }
  }

  async actualizarImagenesEmprendimiento() {
    this.confirmationService.close();
    this.dialogImagenVisible = false;

    //Añadimos las Urls de las imagenes al emprendimiento
    await this.obtenerUrlImagen();
    let imagenEmpresa: ImagenEmpresa;

    //Reemplazamos la lista de imagenes del emprendimiento por la lista de imagenes actualizada
    this.emprendimiento.ImagenesEmpresa = [];

    this.listaImagenes.forEach((item) => {
      imagenEmpresa = new ImagenEmpresa();
      imagenEmpresa.IdImagenEmpresa = item.idImagen ? item.idImagen : 0;
      imagenEmpresa.ImgUrl = item.imgUrl;
      imagenEmpresa.Principal = item.principal;
      this.emprendimiento.ImagenesEmpresa.push(imagenEmpresa);
    });

    this.emprendimientoService.actualizarEmprendimiento(this.emprendimiento).subscribe({
      next: (respuesta) => {
        this.toastService.mensajeExito('Éxito', 'Las imagenes del emprendimiento fueron actualizadas con éxito.', 5000);
        this.cargarDatosEmprendimiento();
      },
      error: (error) => {
        console.log(error);
        this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba actualizar las imagenes del emprendimiento, intente nuevamente.', 5000);
      }
    });
  }

  async obtenerUrlImagen() {
    for (let i = 0; i < this.listaImagenes.length; i++) {
      if (this.listaImagenes[i].file) {
        const fileName = GeneralUtils.encryptFileName(this.listaImagenes[i].file.name) + '.' + this.listaImagenes[i].file.name.split('.').pop();
        const imgRef = ref(this.storage, `Emprendimientos_Image/${fileName}`);

        try {
          await uploadBytes(imgRef, this.listaImagenes[i].file);
          const url = await getDownloadURL(imgRef);
          this.listaImagenes[i].imgUrl = url;
        } catch (error) {
          console.log('Error al obtener la URL pública', error);
          this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba subir la imagen, intente nuevamente.', 5000);
          this.listaImagenes[i].imgUrl = '';
        }
      }
    }
  }

  // * Métodos para el mapa
  async cargarMapa(position: any, departamento?: boolean) {
    let latitud = position.coords.latitude;
    let longitud = position.coords.longitude;

    if ((this.emprendimiento.Latitud !== 0 || this.emprendimiento.Longitud !== 0) && !departamento) {
      latitud = this.emprendimiento.Latitud;
      longitud = this.emprendimiento.Longitud;
    }

    const mapOptions: google.maps.MapOptions = {
      center: new google.maps.LatLng(latitud, longitud),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDoubleClickZoom: true,
    };

    this.map = new google.maps.Map(this.render2.selectRootElement(this.mapa.nativeElement), mapOptions);

    const marker = new google.maps.Marker({
      position: this.map.getCenter(),
      draggable: true,
      map: this.map,
      title: 'Arrastre el marcador para seleccionar la ubicación'
    });

    marker.setMap(this.map);
    this.marker = marker;

    google.maps.event.addListener(this.map, 'dblclick', (event: google.maps.MapMouseEvent) => {
      // Obtener la ubicación del doble clic
      const location = event.latLng;

      // Establecer la posición del marcador en la ubicación del doble clic
      this.marker.setPosition(location);

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = location.lat();
      this.emprendimiento.Longitud = location.lng();

      //Obtenemos la direccion de las calles adyacentes al marker
      this.obtenerDireccion(location);
    });

    // Agregar escucha de eventos para el arrastre del marcador
    google.maps.event.addListener(this.marker, 'dragend', () => {
      // Obtener la nueva ubicación del marcador
      const location = this.marker.getPosition()!;

      // Obtener la dirección de las calles adyacentes a la nueva ubicación
      this.obtenerDireccion(location);

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = location.lat();
      this.emprendimiento.Longitud = location.lng();
    });
  }

  cargarAutocompletado() {
    const autocomplete = new google.maps.places.Autocomplete(this.inputDireccion.nativeElement, {
      componentRestrictions: { country: 'bo' },
      fields: ['address_components', 'geometry'],
      types: ['address']
    });

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        this.toastService.mensajeAdvertencia('Error', 'No se encontró la dirección ingresada.', 5000);
        return;
      }

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = place.geometry.location.lat();
      this.emprendimiento.Longitud = place.geometry.location.lng();

      // Actualizar la posición del marcador
      this.cargarMapa({ coords: { latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() } });
    });
  }

  obtenerDireccion(location: google.maps.LatLng) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: location }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.emprendimiento.Direccion = results[0].formatted_address;
        }
      }
    });
  }

  cargarNuevaUbicacion() {
    switch (this.emprendimiento.Departamento.IdDepartamento) {
      case 1:
        //Mapa en la capital de Pando
        this.cargarMapa({ coords: { latitude: -11.01952, longitude: -68.7837 } }, true);
        break;
      case 2:
        //Mapa en la capital de Beni
        this.cargarMapa({ coords: { latitude: -14.83333, longitude: -64.9 } }, true);
        break;
      case 3:
        //Mapa en la capital de Santa Cruz
        this.cargarMapa({ coords: { latitude: -17.8, longitude: -63.16667 } }, true);
        break;
      case 4:
        //Mapa en la capital de Cochabamba
        this.cargarMapa({ coords: { latitude: -17.3895, longitude: -66.1568 } }, true);
        break;
      case 5:
        //Mapa en la capital de La Paz
        this.cargarMapa({ coords: { latitude: -16.5, longitude: -68.15 } }, true);
        break;
      case 6:
        //Mapa en la capital de Oruro
        this.cargarMapa({ coords: { latitude: -17.98333, longitude: -67.15 } }, true);
        break;
      case 7:
        //Mapa en la capital de Potosí
        this.cargarMapa({ coords: { latitude: -19.58361, longitude: -65.75306 } }, true);
        break;
      case 8:
        //Mapa en la capital de Tarija
        this.cargarMapa({ coords: { latitude: -21.53549, longitude: -64.72956 } }, true);
        break;
      case 9:
        //Mapa en la capital de Chuquisaca
        this.cargarMapa({ coords: { latitude: -19.03333, longitude: -65.26274 } }, true);
        break;
    }
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      this.emprendimiento[propiedad] = texto.value;
    }
  }
}

/// <reference types="@types/googlemaps" />
import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';
import { getDownloadURL, ref, Storage, uploadBytes } from '@angular/fire/storage';
import { FileUploadHandlerEvent } from 'primeng/fileupload';

import { Emprendimiento } from 'src/app/core/models/Empresa/Emprendimiento';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { EmprendimientoService } from 'src/app/services/Empresa/emprendimiento.service';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { CategoriaService } from 'src/app/services/Empresa/categoria.service';
import { DepartamentoService } from 'src/app/services/Empresa/departamento.service';
import { ImagenEmpresa } from 'src/app/core/models/Empresa/ImagenEmpresa';
import { ToastService } from 'src/app/services/other/toastService.service';
import { CodigoService } from 'src/app/services/Empresa/codigo.service';
import { CodigoEmpresa } from 'src/app/core/models/Empresa/CodigoEmpresa';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';

interface Departamentos {
  nombre: string,
  idDepartamento: number
}

@Component({
  selector: 'app-registro-emprendimiento',
  templateUrl: './registro-emprendimiento.component.html',
  styleUrls: ['./registro-emprendimiento.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class RegistroEmprendimientoComponent implements OnInit, AfterViewInit {
  categorias: any[] = [];
  emprendimiento: Emprendimiento = new Emprendimiento();
  imgUrl: string = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Emprendimientos_Image%2Femprendimiento_foto.jpg?alt=media&token=47bbad79-1460-48b1-9506-890d184fb778';
  imgCargada: boolean = false;
  isDragOver: boolean = false;
  file?: File;
  generalUtils: GeneralUtils = new GeneralUtils();
  mostrarSpinner: boolean = false;

  // * Variables para el mapa
  @ViewChild('divMapa') mapa!: ElementRef;
  @ViewChild('inputDireccion') inputDireccion!: ElementRef;

  departamentos: Departamentos[] = [];
  departamentoSeleccionado: number = 0;
  map!: google.maps.Map;
  marker!: google.maps.Marker;

  constructor(
    private tokenService: TokenService,
    private emprendimientoService: EmprendimientoService,
    private categoriaService: CategoriaService,
    private departamentoService: DepartamentoService,
    private codigoService: CodigoService,
    private usuarioService: UsuarioService,
    private storage: Storage,
    private toastService: ToastService,
    private render2: Renderer2,
  ) { }

  ngOnInit() {
    this.cargarDatos();
  }

  ngAfterViewInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        await this.cargarMapa(position);
        this.cargarAutocompletado();
      });
    } else {
      this.toastService.mensajeAdvertencia('Error', 'Tu navegador no soporta la geolocalización.', 5000);
    }
  }

  async registrarEmprendimiento() {
    this.emprendimiento.IdUsuario = this.tokenService.getIdUsuario();
    this.emprendimiento.Departamento.IdDepartamento = this.departamentoSeleccionado;
    this.emprendimiento.FechaRegistro = new Date();
    this.emprendimiento.FechaActualizacion = new Date();

    if (this.emprendimiento.NIT === null || this.emprendimiento.NIT === undefined || this.emprendimiento.NIT === 0) {
      this.emprendimiento.NIT = 861270;
    }

    if (this.emprendimiento.RazonSocial === null || this.emprendimiento.RazonSocial === undefined || this.emprendimiento.RazonSocial === '') {
      this.emprendimiento.RazonSocial = 'UNIVALLE';
    }
    const camposRequeridos = ['nombreEmpresa', 'direccion', 'descripcion', 'numeroCelular', 'nit', 'razonSocial', 'latitud', 'longitud'];

    if (!GeneralUtils.validateEmptyFields(this.emprendimiento, camposRequeridos) || this.emprendimiento.CategoriaEmpresa.IdCategoriaEmpresa === null || this.emprendimiento.Departamento.IdDepartamento === null) {
      this.toastService.mensajeAdvertencia('Campos vacíos', 'Debe llenar todos los campos requeridos.', 5000);
    } else if (!GeneralUtils.validateCellphone(this.emprendimiento.NumeroCelular)) {
      this.toastService.mensajeAdvertencia('Número de celular inválido', 'El número de celular debe tener 8 dígitos.', 5000);
    } else if (!GeneralUtils.validateLongitudLatitud(this.emprendimiento.Longitud, this.emprendimiento.Latitud)) {
      this.toastService.mensajeAdvertencia('Ubicación inválida', 'Debe seleccionar una ubicación en el mapa.', 5000);
    } else if (!this.imgCargada) {
      this.toastService.mensajeAdvertencia('Imagen no cargada', 'Debe seleccionar una imagen para el emprendimiento.', 5000);
    } else {
      //Obtener la URL de la imagen y luego registrar el emprendimiento
      const url = await this.obtenerUrlImagen();
      let imagenEmpresa = new ImagenEmpresa();
      imagenEmpresa.ImgUrl = url;
      imagenEmpresa.Principal = true;
      this.emprendimiento.ImagenesEmpresa = [imagenEmpresa];
      this.mostrarSpinner = true;

      this.emprendimientoService.registrarEmprendimiento(this.emprendimiento).subscribe({
        next: (emprendimiento) => {
          setTimeout(() => {
            this.mostrarSpinner = false;
            this.toastService.mensajeExito('Éxito', 'Emprendimiento registrado correctamente.', 5000);
            let codigoEmprendimiento = new CodigoEmpresa();
            const codigo = this.generalUtils.decryptId(sessionStorage.getItem('codigo')!);
            codigoEmprendimiento.IdUsuario = this.tokenService.getIdUsuario();
            codigoEmprendimiento.NumeroCodigo = codigo;
            codigoEmprendimiento.Estado = '0';
            this.codigoService.actualizarCodigo(codigo, codigoEmprendimiento).subscribe({
              next: (codigo) => {
                this.toastService.mensajeExito('Éxito', 'Código de emprendimiento desactivado.', 5000);
              },
              error: (err) => {
                this.toastService.mensajeError('Error', 'No se pudo desactivar el código de emprendimiento, comuníquese con CAMEBOL.', 5000);
              }
            });

            this.usuarioService.agregarRolEmprendedora(this.tokenService.getIdUsuario()).subscribe({
              next: (usuario) => {
                this.toastService.mensajeExito('Éxito', 'Rol de emprendedora agregado correctamente.', 5000);

                this.tokenService.logOut();
                this.usuarioService.logout();
                window.location.reload();
              },
              error: (err) => {
                console.log(err);
                this.toastService.mensajeError('Error', 'No se pudo agregar el rol de emprendedora, comuníquese con CAMEBOL.', 5000);
              }
            });
          }, 2000);
        },
        error: (err) => {
          console.log(err);
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error', 'No se pudo registrar el emprendimiento, intente nuevamente.', 5000);
        }
      });
    }
  }

  cargarDatos() {
    this.categoriaService.obtenerCategorias().subscribe({
      next: (categorias: any[]) => {
        this.categorias = categorias.map(item => ({
          nombre: item.nombre,
          idCategoria: item.idCategoriaEmpresa
        }));
      }
    });

    this.departamentoService.obtenerDepartamentos().subscribe({
      next: (departamentos: any[]) => {
        this.departamentos = departamentos.map(item => ({
          nombre: item.nombre,
          idDepartamento: item.idDepartamento
        }));
      }
    });
  }

  // * Métodos para la subida de imagen
  onDragOver(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
    event.dataTransfer!.dropEffect = 'copy';
    this.isDragOver = true;
  }

  onDragLeave(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.isDragOver = false;
  }

  onDrop(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    this.isDragOver = false;

    const files = event.dataTransfer?.files;
    if (files && files.length > 0) {
      const reader = new FileReader();

      reader.onload = (e) => {
        this.imgUrl = e.target?.result as string;
        this.imgCargada = true;
      };

      this.file = files[0];
      reader.readAsDataURL(files[0]);
    }
  }

  mostrarImagen(file: File) {
    const reader = new FileReader();

    reader.onload = (event: any) => {
      this.imgUrl = event.target.result;
      this.imgCargada = true;
    };

    reader.readAsDataURL(file);
  }

  obtenerImagen(event: FileUploadHandlerEvent) {
    const file = event.files[0];
    this.file = file;
    this.mostrarImagen(file);
  }

  async obtenerUrlImagen(): Promise<string> {
    const fileName = GeneralUtils.encryptFileName(this.file!.name) + '.' + this.file!.name.split('.').pop();
    const imgRef = ref(this.storage, `Emprendimientos_Image/${fileName}`);

    try {
      await uploadBytes(imgRef, this.file!);
      const url = await getDownloadURL(imgRef);
      return url;
    } catch (error) {
      console.log('Error al obtener la URL pública', error);
      this.toastService.mensajeError('Error', 'Ha ocurrido un error mientras se intentaba subir la imagen, intente nuevamente.', 5000);
      return '';
    }
  }

  // * Métodos para el mapa
  async cargarMapa(position: any) {
    const latitud = position.coords.latitude;
    const longitud = position.coords.longitude;

    const mapOptions: google.maps.MapOptions = {
      center: new google.maps.LatLng(latitud, longitud),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDoubleClickZoom: true,
    };

    this.map = new google.maps.Map(this.render2.selectRootElement(this.mapa.nativeElement), mapOptions);

    const marker = new google.maps.Marker({
      position: this.map.getCenter(),
      draggable: true,
      map: this.map,
      title: 'Arrastre el marcador para seleccionar la ubicación'
    });

    marker.setMap(this.map);
    this.marker = marker;

    google.maps.event.addListener(this.map, 'dblclick', (event: google.maps.MapMouseEvent) => {
      // Obtener la ubicación del doble clic
      const location = event.latLng;

      // Establecer la posición del marcador en la ubicación del doble clic
      this.marker.setPosition(location);

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = location.lat();
      this.emprendimiento.Longitud = location.lng();

      //Obtenemos la direccion de las calles adyacentes al marker
      this.obtenerDireccion(location);
    });

    // Agregar escucha de eventos para el arrastre del marcador
    google.maps.event.addListener(this.marker, 'dragend', () => {
      // Obtener la nueva ubicación del marcador
      const location = this.marker.getPosition()!;

      // Obtener la dirección de las calles adyacentes a la nueva ubicación
      this.obtenerDireccion(location);

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = location.lat();
      this.emprendimiento.Longitud = location.lng();
    });
  }

  cargarAutocompletado() {
    const autocomplete = new google.maps.places.Autocomplete(this.inputDireccion.nativeElement, {
      componentRestrictions: { country: 'bo' },
      fields: ['address_components', 'geometry'],
      types: ['address']
    });

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        this.toastService.mensajeAdvertencia('Error', 'No se encontró la dirección ingresada.', 5000);
        return;
      }

      // Actualizar la latitud y longitud del emprendimiento
      this.emprendimiento.Latitud = place.geometry.location.lat();
      this.emprendimiento.Longitud = place.geometry.location.lng();

      // Actualizar la posición del marcador
      this.cargarMapa({ coords: { latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() } });
    });
  }

  obtenerDireccion(location: google.maps.LatLng) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ location: location }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.emprendimiento.Direccion = results[0].formatted_address;
        }
      }
    });
  }

  cargarNuevaUbicacion() {
    switch (this.departamentoSeleccionado) {
      case 1:
        //Mapa en la capital de Pando
        this.cargarMapa({ coords: { latitude: -11.01952, longitude: -68.7837 } });
        break;
      case 2:
        //Mapa en la capital de Beni
        this.cargarMapa({ coords: { latitude: -14.83333, longitude: -64.9 } });
        break;
      case 3:
        //Mapa en la capital de Santa Cruz
        this.cargarMapa({ coords: { latitude: -17.8, longitude: -63.16667 } });
        break;
      case 4:
        //Mapa en la capital de Cochabamba
        this.cargarMapa({ coords: { latitude: -17.3895, longitude: -66.1568 } });
        break;
      case 5:
        //Mapa en la capital de La Paz
        this.cargarMapa({ coords: { latitude: -16.5, longitude: -68.15 } });
        break;
      case 6:
        //Mapa en la capital de Oruro
        this.cargarMapa({ coords: { latitude: -17.98333, longitude: -67.15 } });
        break;
      case 7:
        //Mapa en la capital de Potosí
        this.cargarMapa({ coords: { latitude: -19.58361, longitude: -65.75306 } });
        break;
      case 8:
        //Mapa en la capital de Tarija
        this.cargarMapa({ coords: { latitude: -21.53549, longitude: -64.72956 } });
        break;
      case 9:
        //Mapa en la capital de Chuquisaca
        this.cargarMapa({ coords: { latitude: -19.03333, longitude: -65.26274 } });
        break;
    }
  }

  validarTexto(textoValidar: Event, propiedad: string) {
    const texto = textoValidar.target as HTMLInputElement;
    if (!GeneralUtils.validateSpecialCharacters(texto.value)) {
      texto.value = texto.value.slice(0, -1);
      this.emprendimiento[propiedad] = texto.value;
    }
  }
}

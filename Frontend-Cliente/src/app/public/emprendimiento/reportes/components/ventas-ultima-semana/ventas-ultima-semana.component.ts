import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { endOfWeek, startOfWeek, subWeeks } from 'date-fns';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-ventas-ultima-semana',
  templateUrl: './ventas-ultima-semana.component.html',
  styleUrls: ['./ventas-ultima-semana.component.css']
})
export class VentasUltimaSemanaComponent implements OnInit {
  chart?: Chart;

  //Reporte de ventas
  ventasSemana: any;
  opcionVentasSemana: any;

  @ViewChild('ventasSemana') ventasSemanaRef: ElementRef = new ElementRef(null);

  constructor(
    private detalleVentaService: DetalleVentaService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteVentasPorTiempo();
  }

  ngOnDestroy() {
    if (this.chart) this.chart.destroy();
  }

  reporteVentasPorTiempo() {
    const fechaActual = new Date();
    let fechaFin: any = endOfWeek(subWeeks(fechaActual, 1), { weekStartsOn: 0 });
    let fechaInicio: any = startOfWeek(fechaFin, { weekStartsOn: 1 });

    fechaFin = fechaFin.toISOString().slice(0, 10);
    fechaInicio = fechaInicio.toISOString().slice(0, 10);

    this.detalleVentaService.obtenerVentasPorFechasIdEmprendimiento(fechaInicio, fechaFin, this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        const ventasPorDia = [0, 0, 0, 0, 0, 0, 0];
        data.map((venta) => {
          const ventaAux = new DetalleVenta(venta);
          const fechaVenta = new Date(ventaAux.FechaRegistro);
          const posicion = fechaVenta.getDay() - 1;
          ventasPorDia[posicion === -1 ? 6 : posicion] += Number.parseInt(ventaAux.SubTotal.toFixed(2));
        });

        if (this.chart) this.chart.destroy();

        this.chart = new Chart(this.ventasSemanaRef.nativeElement, {
          type: 'bar',
          data: {
            labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
            datasets: [
              {
                type: 'bar',
                label: 'Ventas en el día (Bs)',
                backgroundColor: this.graficoService.documentStyle.getPropertyValue('--green-500'),
                data: ventasPorDia,
              },
            ]
          },
          options: this.graficoService.opcionesGeneralesGraficoBarraX()
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las ventas por semana.', 5000);
      }
    });
  }

}

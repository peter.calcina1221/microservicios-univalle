import { Component, OnInit } from '@angular/core';
import { Calificacion } from 'src/app/core/models/Calificacion/Calificacion';
import { CalificacionService } from 'src/app/services/Calificacion/calificacion.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-puntuacion-recibida',
  templateUrl: './puntuacion-recibida.component.html',
  styleUrls: ['./puntuacion-recibida.component.css']
})
export class PuntuacionRecibidaComponent implements OnInit {
  calificacion: number = 0;
  totalCalificaciones: number = 0;
  calificaciones: any;
  opcionCalificaciones: any;

  constructor(
    private calificacionService: CalificacionService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteCalificaciones();
  }

  reporteCalificaciones() {
    this.calificacionService.obtenerCalificaciones(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let calificaciones: number[] = [0, 0, 0, 0, 0];
        data.map((calificacion) => {
          const calificacionAux = new Calificacion(calificacion);
          calificaciones[calificacionAux.Puntuacion - 1]++;
          this.calificacion += calificacionAux.Puntuacion;
        });

        this.calificacion = this.calificacion / data.length;
        this.totalCalificaciones = data.length;
        this.calificaciones = {
          labels: ['1 estrella', '2 estrellas', '3 estrellas', '4 estrellas', '5 estrellas'],
          datasets: [this.graficoService.datasetGraficos(calificaciones, 'Calificaciones', 5, 'bar')]
        };

        this.opcionCalificaciones = this.graficoService.opcionesGeneralesGraficoBarraX();
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las calificaciones.', 5000);
      }
    });
  };

}

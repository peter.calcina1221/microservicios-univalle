import { Component, OnInit } from '@angular/core';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-ingresos-reservas-vs-ventas',
  templateUrl: './ingresos-reservas-vs-ventas.component.html',
  styleUrls: ['./ingresos-reservas-vs-ventas.component.css']
})
export class IngresosReservasVsVentasComponent implements OnInit {
  reservasVsVentas: any;
  opcionReservasVsVentas: any;

  constructor(
    private detalleVentaService: DetalleVentaService,
    private detalleReservaService: DetalleReservaService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteIngresoPorVentasvsReservas();
  }

  reporteIngresoPorVentasvsReservas() {
    this.detalleVentaService.obtenerVentasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let gananciasVentas: number = 0;
        let gananciasReservas: number = 0;

        data.map((venta) => {
          const ventaAux = new DetalleVenta(venta);
          if (ventaAux.Estado === '3' || ventaAux.Estado === '4') gananciasVentas += ventaAux.SubTotal;
        });

        this.detalleReservaService.obtenerReservasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
          next: (data) => {
            data.map((reserva) => {
              const reservaAux = new DetalleReserva(reserva);
              if (reservaAux.Estado === '5' || reservaAux.Estado === '6') gananciasReservas += reservaAux.SubTotal;
            });

            this.reservasVsVentas = {
              labels: [''],
              datasets: [
                this.graficoService.datasetGraficos([gananciasVentas], 'Ingresos por ventas (Bs)', 1),
                this.graficoService.datasetGraficos([gananciasReservas], 'Ingresos por reservas (Bs)', 1, '', 2)
              ]
            };

            this.opcionReservasVsVentas = this.graficoService.opcionesGeneralGraficoBarraY();
          }
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de los ingresos por ventas vs reservas.', 5000);
      }
    });
  }

}

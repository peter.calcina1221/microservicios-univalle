import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-producto-mas-genera-ventas',
  templateUrl: './producto-mas-genera-ventas.component.html',
  styleUrls: ['./producto-mas-genera-ventas.component.css']
})
export class ProductoMasGeneraVentasComponent implements OnInit {

  //Reporte de ventas
  productosQueGeneranVenta: any;
  opcionProductosQueGeneranVenta: any;

   constructor(
    private productoService: ProductoService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteProductosQueGeneranVenta();
  }

  reporteProductosQueGeneranVenta() {
    this.productoService.obtenerProductosSubtotalesPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let nombreProductos: string[] = [];
        let subtotalesProductos: number[] = [];

        data.map((producto) => {
          const productoAux = new Producto(producto);
          nombreProductos.push(productoAux.Nombre);
          subtotalesProductos.push(productoAux.SubTotal);
        });

        this.productosQueGeneranVenta = {
          labels: nombreProductos,
          datasets: [this.graficoService.datasetGraficos(subtotalesProductos, 'Total (Bs)', 3)]
        };

        this.opcionProductosQueGeneranVenta = this.graficoService.opcionesGeneralesGraficoCircularDona(nombreProductos);
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de los productos que más generan venta.', 5000);
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-stock-actual-productos',
  templateUrl: './stock-actual-productos.component.html',
  styleUrls: ['./stock-actual-productos.component.css']
})
export class stockActualProductosComponent implements OnInit {
  //Reporte stock de productos
  productosStock: any;
  opcionProductosStock: any;

  constructor(
    private productoService: ProductoService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }


  ngOnInit() {
    this.reporteProductosStock();
  }

  reporteProductosStock() {
    this.productoService.obtenerProductosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let nombreProductos: string[] = [];
        let stockProductos: number[] = [];

        data.map((producto) => {
          const productoAux = new Producto(producto);
          nombreProductos.push(productoAux.Nombre);
          stockProductos.push(productoAux.StockRestante);
        });

        this.productosStock = {
          labels: nombreProductos,
          datasets: [this.graficoService.datasetGraficos(stockProductos, 'Stock', 3)]
        };

        this.opcionProductosStock = this.graficoService.opcionesGeneralesGraficoCircularDona(nombreProductos);
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte del stock de productos.', 5000);
      }
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-reservas-totales',
  templateUrl: './reservas-totales.component.html',
  styleUrls: ['./reservas-totales.component.css']
})
export class ReservasTotalesComponent implements OnInit {
  reservasTotales: any;
  opcionReservas: any;

   constructor(
    private detalleReservaService: DetalleReservaService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteReservasTotales();
  }

  reporteReservasTotales() {
    this.detalleReservaService.obtenerReservasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let reservasPorConfirmar: number = 0;
        let reservasRechazadas: number = 0;
        let reservasConfirmadas: number = 0;
        let reservasPorRetirar: number = 0;
        let reservasEntregadas: number = 0;

        data.map((reserva) => {
          const reservaAux = new DetalleReserva(reserva);
          if (reservaAux.Estado === '1') reservasPorConfirmar++;
          if (reservaAux.Estado === '2') reservasConfirmadas++;
          if (reservaAux.Estado === '3') reservasRechazadas++;
          if (reservaAux.Estado === '4') reservasPorRetirar++;
          if (reservaAux.Estado === '5' || reservaAux.Estado === '6') reservasEntregadas++;
        });

        this.reservasTotales = {
          labels: ['Por confirmar', 'Confirmadas', 'Rechazadas', 'Por retirar', 'Entregadas'],
          datasets: [this.graficoService.datasetGraficos([reservasPorConfirmar, reservasConfirmadas, reservasRechazadas, reservasPorRetirar, reservasEntregadas], 'Reservas', 5)]
        };

        this.opcionReservas = this.graficoService.opcionesGeneralesGraficoCircular();
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las reservas totales.', 5000);
      }
    });
  }
}

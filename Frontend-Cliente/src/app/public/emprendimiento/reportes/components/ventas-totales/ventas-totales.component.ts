import { Component, OnInit } from '@angular/core';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-ventas-totales',
  templateUrl: './ventas-totales.component.html',
  styleUrls: ['./ventas-totales.component.css']
})
export class VentasTotalesComponent implements OnInit {
  //Reporte de ventas
  ventasTotales: any;
  opcionVentas: any;
  ventasTerminadas: number = 0;
  ventasEnProceso: number = 0;
  gananciasVentaTerminada: number = 0;
  gananciasVentaEnProceso: number = 0;

  constructor(
    private detalleVentaService: DetalleVentaService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteVentasTotales();
  }

   /* Reportes Ventas e Ingresos */
   reporteVentasTotales() {
    this.detalleVentaService.obtenerVentasPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        data.map((venta) => {
          const ventaAux = new DetalleVenta(venta);
          if (ventaAux.Estado === '3' || ventaAux.Estado === '4') {
            this.ventasTerminadas++;
            this.gananciasVentaTerminada += ventaAux.SubTotal;
          }
          else {
            this.ventasEnProceso++;
            this.gananciasVentaEnProceso += ventaAux.SubTotal;
          }
        });

        this.ventasTotales = {
          labels: ['Ventas terminadas', 'Ventas en proceso'],
          datasets: [this.graficoService.datasetGraficos([this.ventasTerminadas, this.ventasEnProceso], 'Ventas (Bs)', 2)]
        }

        this.opcionVentas = this.graficoService.opcionesGeneralesGraficoCircular();
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las ventas totales.', 5000);
      }
    });
  }
}

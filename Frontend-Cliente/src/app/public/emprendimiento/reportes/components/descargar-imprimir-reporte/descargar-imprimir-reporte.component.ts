import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { CalificacionService } from 'src/app/services/Calificacion/calificacion.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { DetalleReservaService } from 'src/app/services/Reserva/detalleReserva.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { DetalleVentaService } from 'src/app/services/Venta/detalleVenta.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { DetalleReserva } from '../../../../../core/models/Reserva/DetalleReserva';
import { Calificacion } from 'src/app/core/models/Calificacion/Calificacion';
import { forkJoin, map, switchMap } from 'rxjs';

@Component({
  selector: 'app-descargar-imprimir-reporte',
  templateUrl: './descargar-imprimir-reporte.component.html',
  styleUrls: ['./descargar-imprimir-reporte.component.css']
})

export class DescargarImprimirReporteComponent implements OnInit {
  hayProductoSeleccionado: boolean = false;
  rangoGeneral: Date[] | undefined;
  rangoVentas: Date[] | undefined;
  rangoInventario: Date[] | undefined;
  rangoReservas: Date[] | undefined;
  rangoCalificacion: Date[] | undefined;
  productos: any[] = [];
  productoSeleccionado: any = null;

  fechaInicio: string = '';
  fechaFin: string = '';

  //Reporte de ventas
  ventasTotales: number = 0;
  ventasTerminadas: number = 0;
  ventasEnProceso: number = 0;
  gananciasVentaTerminada: number = 0;
  gananciasVentaEnProceso: number = 0;
  ventasPorDias: any;
  gananciaPorDias: any;
  diasDeLaSemana: string[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  productosQueGeneranVenta: any;

  //Reporte stock de productos
  productosStock: any;
  reponerStock: any;

  //Reporte de reservas
  reservasTotales: any[] = [];
  totalReservas: number = 0;

  //Reporte de calificaciones
  calificacion: number = 0;
  totalCalificaciones: number = 0;
  calificaciones: number[] = [0, 0, 0, 0, 0];

  @ViewChild('reporte') reporte: ElementRef = new ElementRef(null);

  constructor(
    private detalleVentaService: DetalleVentaService,
    private productoService: ProductoService,
    private detalleReservaService: DetalleReservaService,
    private calificacionService: CalificacionService,
    private toastService: ToastService,
    private tokenService: TokenService

  ) { }

  ngOnInit(): void {
    this.cargarProductos();
    this.cargarReportes(null);
  }

  async descargarReporte() {
    const doc = new jsPDF();
    const contenido = this.reporte.nativeElement;

    const canvas = await html2canvas(contenido);
    const imgData = canvas.toDataURL('image/png');
    const imgAltura = canvas.height * 208 / canvas.width;
    doc.addImage(imgData, 0, 0, 208, imgAltura);
    doc.save('reporte.pdf');
  }

  async imprimirReporte() {
    const printContents = this.reporte.nativeElement;

    const canvas = await html2canvas(printContents);
    const imgData = (canvas).toDataURL('image/png');

    const newWin = window.open('', '_blank');
    if (newWin !== null) {
      const page = newWin.document.open();
      page.write('<html><head><title>Reporte</title></head><body><img src="' + imgData + '" style="width:100%;height:auto;"/></body></html>');
      page.close();

      newWin.onload = function () {
        newWin.print();
      }
    } else {
      console.error('No se pudo abrir la ventana de impresión');
    }
  }

  cargarReportes(rango: any) {
    this.reporteVentasEnSemanaYTotal(this.rangoVentas || rango);
    this.reporteProductosQueGeneranVenta(this.rangoVentas || rango);
    this.reporteProductosStock();
    this.reporteStockReponerProductos();
    this.reporteReservasTotales(this.rangoReservas || rango);
    this.reporteCalificaciones(this.rangoCalificacion || rango);
  }

  cargarReportesGeneral() {
    this.cargarReportes(this.rangoGeneral);
    this.rangoVentas = this.rangoReservas = this.rangoInventario = this.rangoCalificacion = this.rangoGeneral;
  }

  aumentarFiltroProducto() {
    this.cargarReportes(null);
  }

  cargarReporteVentas() {
    this.rangoGeneral = undefined;
    this.reporteVentasEnSemanaYTotal(this.rangoVentas);
    this.reporteProductosQueGeneranVenta(this.rangoVentas);
  }

  cargarReporteInventario() {
    this.rangoGeneral = undefined;
    this.reporteProductosStock();
    this.reporteStockReponerProductos();
  }

  cargarReporteReservas() {
    this.rangoGeneral = undefined;
    this.reporteReservasTotales(this.rangoReservas);
  }

  cargarReporteCalificacion() {
    this.rangoGeneral = undefined;
    this.reporteCalificaciones(this.rangoCalificacion);
  }


  /* Reportes Ventas e Ingresos */
  async reporteVentasEnSemanaYTotal(rango: any) {
    await this.calcularFechaInicioFin(rango);

    this.detalleVentaService.obtenerVentasPorFechasIdEmprendimiento(this.fechaInicio, this.fechaFin, this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        this.ventasPorDias = [0, 0, 0, 0, 0, 0, 0];
        this.gananciaPorDias = [0, 0, 0, 0, 0, 0, 0];
        this.ventasTerminadas = 0;
        this.ventasEnProceso = 0;
        this.gananciasVentaTerminada = 0;
        this.gananciasVentaEnProceso = 0;

        data.map((venta) => {
          const ventaAux = new DetalleVenta(venta);
          if (this.productoSeleccionado !== null) {
            if (ventaAux.IdProducto === this.productoSeleccionado) {
              if (ventaAux.Estado === '3' || ventaAux.Estado === '4') {
                this.ventasTerminadas++;
                this.gananciasVentaTerminada += ventaAux.SubTotal;
              } else {
                this.ventasEnProceso++;
                this.gananciasVentaEnProceso += ventaAux.SubTotal;
              }

              const fechaVenta = new Date(ventaAux.FechaRegistro);
              const posicion = fechaVenta.getDay() - 1;
              this.ventasPorDias[posicion === -1 ? 6 : posicion] += 1;
              this.gananciaPorDias[posicion === -1 ? 6 : posicion] += ventaAux.SubTotal;
            }
          } else {
            if (ventaAux.Estado === '3' || ventaAux.Estado === '4') {
              this.ventasTerminadas++;
              this.gananciasVentaTerminada += ventaAux.SubTotal;
            } else {
              this.ventasEnProceso++;
              this.gananciasVentaEnProceso += ventaAux.SubTotal;
            }

            const fechaVenta = new Date(ventaAux.FechaRegistro);
            const posicion = fechaVenta.getDay() - 1;
            this.ventasPorDias[posicion === -1 ? 6 : posicion] += 1;
            this.gananciaPorDias[posicion === -1 ? 6 : posicion] += ventaAux.SubTotal;
          }
        });

        this.ventasTotales = this.ventasTerminadas + this.ventasEnProceso;
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las ventas por semana.', 5000);
      }
    });
  }

  async reporteProductosQueGeneranVenta(rango: any) {
    await this.calcularFechaInicioFin(rango);

    this.productoService.obtenerProductosSubtotalesPorIdEmprendimientoYFecha(this.tokenService.getIdEmprendimiento(), this.fechaInicio, this.fechaFin).subscribe({
      next: (data) => {
        console.log(data);
        this.productosQueGeneranVenta = [];

        data.map((productoNuevo) => {
          const productoAux = new Producto(productoNuevo);

          if (this.productoSeleccionado !== null) {
            if (productoAux.IdProducto === this.productoSeleccionado) {
              if (this.productosQueGeneranVenta.length < 10) {
                this.productosQueGeneranVenta.push({ nombre: productoAux.Nombre, subTotal: productoAux.SubTotal });
              } else {
                this.productosQueGeneranVenta.sort((a: { subTotal: number; }, b: { subTotal: number; }) => {
                  if (a.subTotal > b.subTotal) return -1;
                  if (a.subTotal < b.subTotal) return 1;
                  return 0;
                });

                if (productoAux.SubTotal > this.productosQueGeneranVenta[9].subTotal) {
                  this.productosQueGeneranVenta.pop();
                  this.productosQueGeneranVenta.push({ nombre: productoAux.Nombre, subTotal: productoAux.SubTotal });
                }
              }
            }
          } else {
            if (this.productosQueGeneranVenta.length < 10) {
              this.productosQueGeneranVenta.push({ nombre: productoAux.Nombre, subTotal: productoAux.SubTotal });
            } else {
              this.productosQueGeneranVenta.sort((a: { subTotal: number; }, b: { subTotal: number; }) => {
                if (a.subTotal > b.subTotal) return -1;
                if (a.subTotal < b.subTotal) return 1;
                return 0;
              });

              if (productoAux.SubTotal > this.productosQueGeneranVenta[9].subTotal) {
                this.productosQueGeneranVenta.pop();
                this.productosQueGeneranVenta.push({ nombre: productoAux.Nombre, subTotal: productoAux.SubTotal });
              }
            }
          }
        });

        //Ordenar de mayor a menor ganancia
        this.productosQueGeneranVenta.sort((a: { subTotal: number; }, b: { subTotal: number; }) => {
          if (a.subTotal > b.subTotal) return -1;
          if (a.subTotal < b.subTotal) return 1;
          return 0;
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de los productos que más generan venta.', 5000);
        console.log(err);
      }
    });
  }

  /* Reportes Inventario */
  async reporteProductosStock() {
    this.productoService.obtenerProductosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        this.productosStock = [];

        data.map((productoNuevo) => {
          const productoAux = new Producto(productoNuevo);
          if (this.productoSeleccionado !== null) {
            if (productoAux.IdProducto === this.productoSeleccionado)
              this.productosStock.push({ nombre: productoAux.Nombre, stock: productoAux.StockRestante });
          } else {
            this.productosStock.push({ nombre: productoAux.Nombre, stock: productoAux.StockRestante });
          }
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte del stock de productos.', 5000);
      }
    });
  }

  async reporteStockReponerProductos() {
    this.productoService.obtenerProductosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        this.reponerStock = [];

        data.map((productoNuevo) => {
          const productoAux = new Producto(productoNuevo);

          if (this.productoSeleccionado !== null) {
            if (productoAux.IdProducto === this.productoSeleccionado)
              this.reponerStock.push({ nombre: productoAux.Nombre, stock: productoAux.StockRestante, stockMinimo: productoAux.StockMinimo });
          } else this.reponerStock.push({ nombre: productoAux.Nombre, stock: productoAux.StockRestante, stockMinimo: productoAux.StockMinimo });
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte del stock para reponer los productos.', 5000);
      }
    });
  }

  /* Reporte Reservas */
  async reporteReservasTotales(rango: any) {
    await this.calcularFechaInicioFin(rango);
    this.reservasTotales = [];
    this.totalReservas = 0;

    this.detalleReservaService.obtenerReservasPorIdEmprendimientoYFecha(this.tokenService.getIdEmprendimiento(), this.fechaInicio, this.fechaFin).pipe(
      switchMap(reservas => {
        let detalleReservas = reservas.map(detalleReserva => new DetalleReserva(detalleReserva));
        const reservas$ = detalleReservas.map((reserva) => this.productoService.obtenerProductoPorId(reserva.IdProducto).pipe(
          map((producto) => ({ ...reserva, producto }))
        ));
        return forkJoin(reservas$);
      })
    ).subscribe({
      next: (data: any) => {
        data.forEach((reserva: any) => {
          if (this.productoSeleccionado !== null) {
            if (reserva.idProducto === this.productoSeleccionado) {
              let productoEncontrado = this.reservasTotales.find(p => p.producto === reserva.producto.nombre);

              if (!productoEncontrado) {
                productoEncontrado = {
                  idProducto: reserva.IdProducto,
                  producto: reserva.producto.nombre,
                  reservasPorConfirmar: 0,
                  reservasConfirmadas: 0,
                  reservasRechazadas: 0,
                  reservasPorRetirar: 0,
                  reservasEntregadas: 0
                };

                this.reservasTotales.push(productoEncontrado);
              }

              if (reserva.estado === '1') productoEncontrado.reservasPorConfirmar++;
              if (reserva.estado === '2') productoEncontrado.reservasConfirmadas++;
              if (reserva.estado === '3') productoEncontrado.reservasRechazadas++;
              if (reserva.estado === '4') productoEncontrado.reservasPorRetirar++;
              if (reserva.estado === '5' || reserva.estado === '6') productoEncontrado.reservasEntregadas++;

              this.totalReservas++;
            }
          } else {
            let productoEncontrado = this.reservasTotales.find(p => p.producto === reserva.producto.nombre);
            if (!productoEncontrado) {
              productoEncontrado = {
                producto: reserva.producto.nombre,
                reservasPorConfirmar: 0,
                reservasConfirmadas: 0,
                reservasRechazadas: 0,
                reservasPorRetirar: 0,
                reservasEntregadas: 0
              };
              this.reservasTotales.push(productoEncontrado);
            }

            if (reserva.estado === '1') productoEncontrado.reservasPorConfirmar++;
            if (reserva.estado === '2') productoEncontrado.reservasConfirmadas++;
            if (reserva.estado === '3') productoEncontrado.reservasRechazadas++;
            if (reserva.estado === '4') productoEncontrado.reservasPorRetirar++;
            if (reserva.estado === '5' || reserva.estado === '6') productoEncontrado.reservasEntregadas++;

            this.totalReservas++;
          }
        });
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las reservas totales.', 5000);
      }
    });
  }

  /* Reportes Calificaciones */
  async reporteCalificaciones(rango: any) {
    await this.calcularFechaInicioFin(rango);
    this.calificacion = 0;
    this.totalCalificaciones = 0;
    this.calificaciones = [0, 0, 0, 0, 0];

    this.calificacionService.obtenerCalificacionesPorFecha(this.tokenService.getIdEmprendimiento(), this.fechaInicio, this.fechaFin).subscribe({
      next: (data) => {
        data.map((calificacion) => {
          const calificacionAux = new Calificacion(calificacion);
          this.calificaciones[calificacionAux.Puntuacion - 1]++;
          this.calificacion += calificacionAux.Puntuacion;
        });

        this.calificacion = Math.round((this.calificacion / data.length) * 10) / 10 || 0;
        this.totalCalificaciones = data.length || 0;
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de las calificaciones.', 5000);
      }
    });
  }

  /* Funciones auxiliares */
  async calcularFechaInicioFin(rango: any) {
    if (!rango) {
      const fechaActual = new Date();
      fechaActual.setMonth(fechaActual.getMonth() - 3);
      this.fechaInicio = fechaActual.toISOString().slice(0, 10);
      this.fechaFin = new Date().toISOString().slice(0, 10);
    } else {
      this.fechaInicio = rango[0].toISOString().slice(0, 10);
      this.fechaFin = rango[1].toISOString().slice(0, 10);
    }
  }

  cargarProductos() {
    this.productoService.obtenerProductosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data: any[]) => {
        this.productos = data.map((producto) => ({
          nombre: producto.nombre,
          idProducto: producto.idProducto
        }));
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al cargar los productos.', 5000);
      }
    });
  }

  limpiarFiltros() {
    this.productoSeleccionado = null;
    this.rangoGeneral = undefined;
    this.rangoVentas = undefined;
    this.rangoInventario = undefined;
    this.rangoReservas = undefined;
    this.rangoCalificacion = undefined;
    this.cargarReportes(null);
  }
}

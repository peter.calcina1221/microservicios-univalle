import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-stock-reponer-productos',
  templateUrl: './stock-reponer-productos.component.html',
  styleUrls: ['./stock-reponer-productos.component.css']
})
export class StockReponerProductosComponent implements OnInit {
  reponerStock: any;
  opcionReponerStock: any;

  constructor(
    private productoService: ProductoService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteStockReponerProductos();
  }

  reporteStockReponerProductos() {
    this.productoService.obtenerProductosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (data) => {
        let stockMinimoProductos: number[] = [];
        let stockRestanteProductos: number[] = [];
        let nombreProductos: string[] = [];

        data.map((producto) => {
          const productoAux = new Producto(producto);
          nombreProductos.push(productoAux.Nombre);
          stockMinimoProductos.push(productoAux.StockMinimo);
          stockRestanteProductos.push(productoAux.StockRestante);
        });

        this.reponerStock = {
          labels: nombreProductos,
          datasets: [this.graficoService.datasetGraficos(stockRestanteProductos, 'Stock restante', 1), this.graficoService.datasetGraficos(stockMinimoProductos, 'Stock mínimo', 1, '', 2)]
        };

        this.opcionReponerStock = this.graficoService.opcionesGeneralGraficoBarraY();
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte del stock para reponer los productos.', 5000);
      }
    });
  }

}

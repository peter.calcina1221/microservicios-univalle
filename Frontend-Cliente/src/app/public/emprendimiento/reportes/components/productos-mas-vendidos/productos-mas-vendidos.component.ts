import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { ProductoService } from 'src/app/services/Producto/producto.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { GraficoService } from 'src/app/services/other/graficoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';

@Component({
  selector: 'app-productos-mas-vendidos',
  templateUrl: './productos-mas-vendidos.component.html',
  styleUrls: ['./productos-mas-vendidos.component.css']
})
export class ProductosMasVendidosComponent implements OnInit {
  productosMasVendidos: any;
  opcionProductosMasVendidos: any;

  constructor(
    private productoService: ProductoService,
    private graficoService: GraficoService,
    private toastService: ToastService,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.reporteProductosMasVendidos();
  }

  reporteProductosMasVendidos() {
    this.productoService.obtenerProductosMasVendidosPorIdEmprendimiento(this.tokenService.getIdEmprendimiento(), 5).subscribe({
      next: (data) => {
        let nombreProductos: string[] = [];
        let cantidadVendida: number[] = [];

        data.map((producto) => {
          const productoAux = new Producto(producto);
          nombreProductos.push(productoAux.Nombre);
          cantidadVendida.push(productoAux.CantidadVendida);
        });

        this.productosMasVendidos = {
          labels: nombreProductos,
          datasets: [this.graficoService.datasetGraficos(cantidadVendida, 'Cantidad vendida', 3)]
        };

        this.opcionProductosMasVendidos = this.graficoService.opcionesGeneralesGraficoCircularDona(nombreProductos);
      },
      error: (err) => {
        this.toastService.mensajeError('Error', 'Ha ocurrido un error al obtener el reporte de los productos más vendidos.', 5000);
      }
    });
  }
}

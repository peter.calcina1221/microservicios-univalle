import { Component, OnInit } from '@angular/core';
import { CalificacionService } from '../../../services/Calificacion/calificacion.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { Calificacion } from 'src/app/core/models/Calificacion/Calificacion';
import { UsuarioService } from 'src/app/services/Usuario/usuario.service';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-calificaciones',
  templateUrl: './calificaciones.component.html',
  styleUrls: ['./calificaciones.component.css']
})
export class CalificacionesComponent implements OnInit {
  calificacionesUsuario: any[] = [];
  calificaciones: Calificacion[] = [];
  constructor(
    private calificacionService: CalificacionService,
    private usuarioService: UsuarioService,
    private toastService: ToastService,
    private tokenService: TokenService,
  ) { }

  ngOnInit() {
    this.cargarCalificacionesRecibidas();
  }

  cargarCalificacionesRecibidas() {
    this.calificacionService.obtenerCalificaciones(this.tokenService.getIdEmprendimiento()).subscribe({
      next: (calificaciones) => {
        this.calificaciones = calificaciones.map(calificacion => new Calificacion(calificacion));
        this.calificacionesUsuario = [];

        let observables = this.calificaciones.map(calificacion =>
          this.usuarioService.obtenerUsuarioPorId(calificacion.IdUsuario)
        );

        forkJoin(observables).subscribe({
          next: (usuarios) => {
            usuarios.forEach((usuario, index) => {
              let nuevoUsuario = new Usuario(usuario);
              let detalleCalificacion = {
                FechaCalificacion: new Date(this.calificaciones[index].FechaRegistro).toLocaleDateString(),
                Puntaje: this.calificaciones[index].Puntuacion,
                NombreUsuario: nuevoUsuario.Nombre + ' ' + nuevoUsuario.PrimerApellido + ' ' + nuevoUsuario.SegundoApellido,
                FotoPerfil: nuevoUsuario.ImgUrl ? nuevoUsuario.ImgUrl : 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Usuarios_Image%2F9a75bead3057213ff4647a8a508c9daf1c763715616f79aeeeea076f0a6d1046.jfif?alt=media&token=dd0d5749-9f6e-4a9b-8bfa-3aac9a85a909',
                ComentarioCalificacion: this.calificaciones[index].Comentario
              };

              this.calificacionesUsuario.push(detalleCalificacion);
            });
          },
          error: (err) => this.toastService.mensajeError('Error', 'No se pudo cargar los detalles del usuario, intentelo más tarde.', 5000)
        });
      },
      error: (err) => this.toastService.mensajeError('Error', 'No se pudo cargar las calificaciones recibidas, intentelo más tarde.', 5000)
    });
  }

}

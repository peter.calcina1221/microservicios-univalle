import { Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-quienes',
  templateUrl: './quienes.component.html',
  styleUrls: ['./quienes.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class QuienesComponent {

  constructor() { }

}

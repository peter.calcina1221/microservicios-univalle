import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Pago } from 'src/app/core/models/Pago/Pago';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { Venta } from 'src/app/core/models/Venta/Venta';
import { GeneralUtils } from 'src/app/core/utils/General-utils';
import { PagoService } from 'src/app/services/Pago/pago.service';
import { VentaService } from 'src/app/services/Venta/venta.service';
import { CarritoService } from 'src/app/services/other/carritoService.service';
import { ToastService } from 'src/app/services/other/toastService.service';
import { TokenService } from 'src/app/services/Usuario/token.service';
import { Email } from 'src/app/core/utils/Email';

@Component({
  selector: 'app-confirmar-venta',
  templateUrl: './confirmar-venta.component.html',
  styleUrls: ['./confirmar-venta.component.css']
})

export class ConfirmarVentaComponent implements OnInit {
  linkPago: SafeResourceUrl = '';
  pago: Pago = new Pago();
  mostrarSpinner: boolean = false;
  mensaje: string = '';

  //Variables para la venta
  listaProductosVenta: any[] = [];
  listaProductosReserva: Producto[] = [];
  venta: Venta = new Venta();
  detalleVentas: DetalleVenta[] = [];
  ventaFallida: boolean = false;
  tipoDocumentos: any[] = [];

  constructor(
    private carritoService: CarritoService,
    private ventaService: VentaService,
    private toastService: ToastService,
    private tokenService: TokenService,
    private pagoService: PagoService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) { }

  async ngOnInit() {
    this.mostrarSpinner = true;
    this.mensaje = 'Generando link de pago...';
    await this.crearObjetoVenta();
    await this.cargarLinkPago();
    this.tipoDocumentos = [
      { nombre: "CEX - CEDULA DE IDENTIDAD DE EXTRANJERO", codigo: "2" },
      { nombre: "CI - CEDULA DE IDENTIDAD", codigo: "1" },
      { nombre: "NIT - NUMERO DE IDENTIFICACION TRIBUTARIA", codigo: "5" },
      { nombre: "OD - OTRO DOCUMENTO DE IDENTIDAD", codigo: "4" },
      { nombre: "PAS - PASAPORTE", codigo: "3" }
    ];
  }

  get productos(): any[] {
    return this.carritoService.productos;
  }

  async cargarLinkPago() {
    this.ventaService.generarLinkPago(this.venta).subscribe({
      next: (response) => {
        this.mostrarSpinner = false;
        this.linkPago = this.sanitizer.bypassSecurityTrustResourceUrl(response.mensaje);
        this.pago.OrderCode = response.orderCode;
        this.venta.OrderCode = this.pago.OrderCode;
      },
      error: (error) => {
        console.log("Error", error)
      }
    });
  }

  async crearObjetoVenta() {
    this.listaProductosVenta = this.productos;

    this.venta.IdUsuario = this.tokenService.getIdUsuario();
    this.venta.FechaRegistro = new Date();
    this.venta.FechaActualizacion = new Date();
    this.venta.CodigoVenta = this.venta.generarCodigoVenta();

    for (let i = 0; i < this.listaProductosVenta.length; i++) {
      let detalleVenta: DetalleVenta = new DetalleVenta();
      detalleVenta.IdProducto = this.listaProductosVenta[i].IdProducto;
      detalleVenta.IdEmpresa = this.listaProductosVenta[i].IdEmpresa;
      detalleVenta.Cantidad = this.listaProductosVenta[i].Cantidad;
      detalleVenta.PrecioUnitario = this.listaProductosVenta[i].PrecioVenta;
      detalleVenta.SubTotal = this.listaProductosVenta[i].Cantidad * this.listaProductosVenta[i].PrecioVenta;
      this.venta.Total += detalleVenta.SubTotal;
      detalleVenta.Codigo = this.venta.CodigoVenta;
      detalleVenta.FechaRegistro = new Date();
      detalleVenta.FechaActualizacion = new Date();
      this.detalleVentas.push(detalleVenta);
    }

    this.venta.DetalleVentas = this.detalleVentas;
  }

  finalizarVenta() {
    const camposValidar = ['razonSocial', 'nit', 'correo', 'tipoDocumento'];
    if (GeneralUtils.validateEmptyFields(this.venta, camposValidar)) {
      this.pagoService.confirmarPago(this.pago).subscribe({
        next: (response) => {
          if (response.OrderPaymentStatus.Status === 'IN_PROCESS') {
            this.toastService.mensajeAdvertencia('Pago en proceso', 'Primero debe pagar antes de finalizar la compra, si ya realizo el pago espere un momento para la confirmación.', 4000);
          }

          if (response.OrderPaymentStatus.Status === 'APPROVED') {
            this.confirmarTransaccion();
          }
        },
        error: (error) => {
          console.log("Error", error)
        }
      });
    } else {
      this.toastService.mensajeAdvertencia('Campos vacíos', 'Debe llenar todos los campos para finalizar la compra.', 4000);
    }
  }

  confirmarTransaccion() {
    this.mostrarSpinner = true;
    this.mensaje = 'Realizando la compra...';

    this.ventaService.realizarVenta(this.venta).subscribe({
      next: (response) => {
        setTimeout(() => {
          this.mostrarSpinner = false;
          this.toastService.mensajeExito('Compra realizada', 'Se realizó la compra exitosamente.', 4000);
          this.enviarCorreo();
          this.carritoService.vaciarCarrito();
          this.carritoService.confirmarVenta(true);
          this.router.navigate(['/home']);
        }, 2000);
      },
      error: (error) => {
        console.log("Error", error)
        setTimeout(() => {
          this.mostrarSpinner = false;
          this.toastService.mensajeError('Error al confirmar compra', 'Ocurrió un error al confirmar la compra.', 4000);
          this.router.navigate(['/listaproductos']);
        }, 2000);
      }
    });
  }

  enviarCorreo() {
    let email = new Email();
    let mensajeProductos = `
    ¡Gracias por tu compra! ¡Estamos encantados de que hayas comprado nuestros productos! <br>
    ${this.productos.map((producto, index) => {
      return `<strong>Producto: </strong> ${producto.Nombre} / <strong>Cantidad: </strong> ${this.venta.DetalleVentas[index].Cantidad} / <strong>Precio: </strong> ${this.venta.DetalleVentas[index].SubTotal} <br>`;
    })}
    Estado actual de tu compra: <br>`;

    let mensajeProducto = `
    ¡Gracias por tu compra! ¡Estamos encantados de que hayas comprado nuestro producto! <br>
    <strong>Producto: </strong> ${this.productos[0].Nombre} / <strong>Cantidad: </strong> ${this.venta.DetalleVentas[0].Cantidad} / <strong>Precio: </strong> ${this.venta.DetalleVentas[0].SubTotal} <br>
    Estado actual de tu compra: <br>`;

    email.IdUsuario = this.tokenService.getIdUsuario();
    email.Titulo = 'Compra realizada';
    email.Subject = this.tokenService.getUserName();
    email.Mensaje = this.venta.DetalleVentas.length > 1 ? mensajeProductos : mensajeProducto;
    email.UrlImagen = 'https://firebasestorage.googleapis.com/v0/b/image-storage-camebol.appspot.com/o/Envios_Image%2FImagen1.png?alt=media&token=84a96527-54a3-4ac8-9268-82edd85d49e0';

    this.ventaService.enviarCorreo(email).subscribe({
      error: (error) => {
        console.log("Error", error)
        this.toastService.mensajeError('Error al enviar correo', 'Ocurrió un error al enviar el correo.', 4000);
      }
    });
  }

  regresar() {
    this.router.navigate(['/listaproductos']);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoriaProducto } from 'src/app/core/models/Producto/CategoriaProducto';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { CategoriaProductoService } from 'src/app/services/Producto/categoria.service';
import { ProductoService } from 'src/app/services/Producto/producto.service';

@Component({
  selector: 'app-lista-categoria',
  templateUrl: './lista-categoria.component.html',
  styleUrls: ['./lista-categoria.component.css']
})
export class ListaCategoriaComponent implements OnInit {
  categoriasProducto: CategoriaProducto[] = [];

  constructor(private categoriaService: CategoriaProductoService) { }

  ngOnInit() {
    this.cargarCategorias();
  }

  cargarCategorias() {
    this.categoriaService.obtenerCategoriasProducto().subscribe({
      next: (categorias: CategoriaProducto[]) => {
        categorias.forEach((categoria) => {
          this.categoriasProducto.push(new CategoriaProducto(categoria))
        });
      }
    });
  }
}

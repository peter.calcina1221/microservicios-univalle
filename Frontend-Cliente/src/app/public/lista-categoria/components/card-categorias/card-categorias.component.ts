import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriaProducto } from 'src/app/core/models/Producto/CategoriaProducto';
import { CategoriaParamService } from 'src/app/services/other/categoriaParamService.service';

@Component({
  selector: 'app-card-categoria',
  templateUrl: './card-categorias.component.html',
  styleUrls: ['./card-categorias.component.css']
})
export class CardCategoriasComponent implements OnInit {
  @Input() categoria: CategoriaProducto = new CategoriaProducto();

  constructor(
    private route: Router,
    private categoriaParamService: CategoriaParamService
    ) { }

  ngOnInit() {
    this.categoria = new CategoriaProducto(this.categoria);
  }

  cargarListaProductos() {
    this.categoriaParamService.seleccionarCategoria(this.categoria.Nombre);
    this.route.navigate(['/listaproductos']);
  }
}

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PublicComponent } from './public.component';
import { HomeComponent } from "./home/container/home.component";
import { InicioComponent } from "./inicio/container/inicio.component";
import { QuienesComponent } from './quienes/quienes.component';
import { ContactoComponent } from "./contacto/contacto.component";
import { CambiarContraseniaComponent } from "./usuarios/cambiar-contrasenia/cambiar-contrasenia.component";
import { ListaCategoriaComponent } from "./lista-categoria/lista-categoria.component";
import { ListaEmprendimientoComponent } from "./emprendimiento/lista-emprendimiento/lista-emprendimiento.component";
import { PerfilEmprendimientoComponent } from "./emprendimiento/perfil-emprendimiento/perfil-emprendimiento.component";
import { RegistroEmprendimientoComponent } from "./emprendimiento/registro-emprendimiento/registro-emprendimiento.component";
import { ListaProductosComponent } from "./lista-productos/lista-productos.component";
import { ListaReservasComponent } from "./emprendimiento/lista-reservas/lista-reservas.component";
import { ListaVentasComponent } from "./emprendimiento/lista-ventas/lista-ventas.component";
import { ReportesComponent } from "./emprendimiento/reportes/reportes.component";
import { VistaEmprendimientoComponent } from "./emprendimiento/vista-emprendimiento/vista-emprendimiento.component";
import { MisComprasComponent } from "./mis-compras/mis-compras.component";
import { ConfirmarVentaComponent } from "./confirmar-venta/confirmar-venta.component";
import { CalificacionComponent } from "./calificacion/calificacion.component";
import { MisReservasComponent } from "./mis-reservas/mis-reservas.component";
import { loginGuard } from "../core/guard/login-guard.guard";
import { rolesGuard } from "../core/guard/roles-guard.guard";
import { emprendimientoGuard } from "../core/guard/emprendimiento-guard.guard";
import { codigoEmprendimientoGuard } from "../core/guard/codigoEmprendimiento-guard.guard";
import { CalificacionesComponent } from "./emprendimiento/calificaciones/calificaciones.component";
import { DescargarImprimirReporteComponent } from "./emprendimiento/reportes/components/descargar-imprimir-reporte/descargar-imprimir-reporte.component";

const routes: Routes = [
  {
    path: '', component: PublicComponent, children:
      [
        { path: '', redirectTo: 'inicio', pathMatch: 'full' },
        { path: 'home', component: HomeComponent, canActivate: [loginGuard] },
        { path: 'inicio', component: InicioComponent },
        { path: 'quienes', component: QuienesComponent },
        { path: 'contacto', component: ContactoComponent },
        { path: 'cambiar-contrasenia/:tokenPassword', component: CambiarContraseniaComponent },
        { path: 'listaproductos', component: ListaProductosComponent, canActivate: [loginGuard] },
        { path: 'categorias', component: ListaCategoriaComponent, canActivate: [loginGuard] },
        { path: 'emprendimientos', component: ListaEmprendimientoComponent, canActivate: [loginGuard] },
        { path: 'mis-compras', component: MisComprasComponent, canActivate: [loginGuard] },
        { path: 'mis-reservas', component: MisReservasComponent, canActivate: [loginGuard] },
        { path: 'calificacion', component: CalificacionComponent, canActivate: [loginGuard]},
        { path: 'calificaciones', component: CalificacionesComponent, canActivate: [loginGuard, rolesGuard]},
        { path: 'confirmar-venta', component: ConfirmarVentaComponent, canActivate: [loginGuard] }, //Validar si tiene productos en el carrito
        { path: 'registro-emprendimiento', component: RegistroEmprendimientoComponent, canActivate: [loginGuard, emprendimientoGuard, codigoEmprendimientoGuard] },
        { path: 'mi-emprendimiento', component: PerfilEmprendimientoComponent, canActivate: [loginGuard, rolesGuard] },
        { path: 'vistaEmprendimiento/:idEmprendimiento', component: VistaEmprendimientoComponent, canActivate: [loginGuard] },
        { path: 'reservas', component: ListaReservasComponent, canActivate: [loginGuard, rolesGuard] },
        { path: 'ventas', component: ListaVentasComponent, canActivate: [loginGuard, rolesGuard] },
        { path: 'reportes', component: ReportesComponent, canActivate: [loginGuard, rolesGuard] },
        { path: 'descargarReporte', component: DescargarImprimirReporteComponent, canActivate: [loginGuard, rolesGuard]},
        { path: 'perfil', loadChildren: () => import('./usuarios/mi-cuenta/mi-cuenta.module').then(m => m.MiCuentaModule), canActivate: [loginGuard] },
      ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PublicRoutingModule {

}

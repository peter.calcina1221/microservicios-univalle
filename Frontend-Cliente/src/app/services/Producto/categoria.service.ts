import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDto } from '../../core/models/Usuario/jwt-dto';
import { CategoriaProducto } from '../../core/models/Producto/CategoriaProducto';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';

@Injectable({
  providedIn: 'root'
})
export class CategoriaProductoService {

  private _categoriaProductoUrl = urls.categoriaProductoUrl;

  constructor(private httpClient: HttpClient) { }

  public obtenerCategoriasProducto(): Observable<CategoriaProducto[]> {
    return this.httpClient.get<CategoriaProducto[]>(`${this._categoriaProductoUrl}listaIdOrganizacionAndEstado/1/1`);
  }

}

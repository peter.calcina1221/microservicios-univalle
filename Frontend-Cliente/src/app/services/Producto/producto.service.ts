import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDto } from '../../core/models/Usuario/jwt-dto';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { Producto } from 'src/app/core/models/Producto/Producto';
import { urls } from 'src/app/core/configuration/urls';
import { TokenService } from '../Usuario/token.service';

@Injectable({
  providedIn: 'root'
})

export class ProductoService {

  private _productoUrl = urls.productoUrl;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

  /*
   * Métodos GET
  */
  public obtenerProductosActivos(): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}listaPorIdOrganizacionAndEstado/1/1`);
  }

  public obtenerProductoPorId(idProducto: number): Observable<Producto> {
    return this.httpClient.get<Producto>(`${this._productoUrl}obtenerProductoPorId/${idProducto}`);
  }

  public obtenerProductosPorIdEmprendimientoEstado(idEmprendimiento: number, estado: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}obtenerProductosPorIdEmpresaEstado/${idEmprendimiento}/${estado}`);
  }

  public obtenerProductosPorIdEmprendimiento(idEmprendimiento: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}obtenerProductosPorIdEmpresa/${idEmprendimiento}`);
  }

  public obtenerProductoPorIdCategoria(idCategoria: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}listaId/${idCategoria}`);
  }

  public obtenerProductosMasVendidosPorIdOrganizacion(idOrganizacion: number, limite: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}obtenerProductosMasVendidosPorOrganizacion/${idOrganizacion}/${limite}`);
  }

  public obtenerProductosMasVendidosPorIdEmprendimiento(idEmprendimiento: number, limite: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}obtenerProductosMasVendidosPorEmpresa/${idEmprendimiento}/${limite}`);
  }

  public obtenerProductosMasCompradosPorIdUsuario(idUsuario: number, limite: number): Observable<Producto[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}obtenerProductosCompradosPorUsuarios/${idUsuario}/${limite}`);
  }

  public obtenerProductosSubtotalesPorIdEmprendimiento(idEmprendimiento: number): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._productoUrl}obtenerProductosSubTotalesPorEmpresa/${idEmprendimiento}`);
  }

  public obtenerProductosSubtotalesPorIdEmprendimientoYFecha(idEmprendimiento: number, fechaInicio: string, fechaFin: string): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._productoUrl}obtenerProductosSubTotalesPorEmpresaYFecha/${idEmprendimiento}/${fechaInicio}/${fechaFin}`);
  }

  public obtenerProductosSIN(): Observable<any[]> {
    return this.httpClient.get<Producto[]>(`${this._productoUrl}productoSIN`);
  }

  public obtenerUnidadSIN(): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._productoUrl}unidadSIN`);
  }

  public obtenerNombreUnidadSIN(codigoSIN: string): Observable<string> {
    return this.httpClient.get<string>(`${this._productoUrl}obtenerNombreUnidadSIN/${codigoSIN}`);
  }

  public validarCodigoProducto(codigo: string, idEmpresa: number): Observable<boolean> {
    return this.httpClient.get<boolean>(`${this._productoUrl}checkExisteCodigoPorEmpresa/${codigo}/${idEmpresa}`);
  }

  /*
    * Métodos POST
   */
  public nuevoProducto(producto: Producto): Observable<any> {
    return this.httpClient.post<any>(`${this._productoUrl}nuevo`, producto, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  /*
  * Métodos PUT
  */

  public actualizarProducto(producto: Producto): Observable<any> {
    return this.httpClient.put<any>(`${this._productoUrl}actualizar/${producto.IdProducto}`, producto, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

}

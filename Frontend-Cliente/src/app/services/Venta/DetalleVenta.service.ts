import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';
import { DetalleVenta } from 'src/app/core/models/Venta/DetalleVenta';
import { TokenService } from '../Usuario/token.service';

@Injectable({
  providedIn: 'root'
})
export class DetalleVentaService {

  private _detalleVentaUrl = urls.detalleVentaUrl;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

  /*
  * Métodos GET
  */

  obtenerVentasPorIdEmprendimiento(idEmprendimiento: number): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}listaPorIdEmpresa/${idEmprendimiento}`);
  }

  obtenerVentasPorIdEmprendimientoCodigo(idEmprendimiento: number, codigo: string): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}listaPorIdEmpresaYCodigo/${idEmprendimiento}/${codigo}`);
  }

  obtenerVentasPorIdEmprendimientoEstado(idEmprendimiento: number, estado: string): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}listaPorIdEmpresaYEstado/${idEmprendimiento}/${estado}`);
  }

  obtenerVentasPorIdVenta(idVenta: number): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}listaPorIdVenta/${idVenta}`);
  }

  obtenerVentasPorFechasIdEmprendimiento(fechaInicio: string, fechaFin: string, idEmprendimiento: number): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}listaPorFechasYEmpresa/${fechaInicio}/${fechaFin}/${idEmprendimiento}`);
  }

  obtenerVentasPorIdUsuarioYEstado(idUsuario: number): Observable<any[]> {
    return this.httpClient.get<any[]>(`${this._detalleVentaUrl}ListarPorUsuarioYEstado/${idUsuario}/3`);
  }

  /*
  * Métodos PUT
  */

  actualizarDetalleVenta(detalleVenta: DetalleVenta): Observable<any> {
    return this.httpClient.put<any>(`${this._detalleVentaUrl}actualizar/${detalleVenta.IdDetalleVenta}`, detalleVenta, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  actualizarPorEstado(idDetalleVenta: number, estado: number): Observable<any> {
    return this.httpClient.put<any>(`${this._detalleVentaUrl}actualizarEstado/${idDetalleVenta}/${estado}`, null);
  }
}

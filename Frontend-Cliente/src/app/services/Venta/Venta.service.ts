import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urls } from 'src/app/core/configuration/urls';
import { Venta } from 'src/app/core/models/Venta/Venta';
import { Email } from '../../core/utils/Email';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  private _ventaUrl = urls.ventaUrl;
  constructor(private httpClient: HttpClient) { }

  /*
    * Métodos GET
  */

  public obtenerVentas(): Observable<Venta[]> {
    return this.httpClient.get<Venta[]>(`${this._ventaUrl}lista`);
  }

  obtenerVentasPorIdOrganizacionIdUsuario(idUsuario: number): Observable<Venta[]> {
    return this.httpClient.get<Venta[]>(`${this._ventaUrl}listaPorIdUsuarioYIdOrganizacion/${idUsuario}/1`);
  }

  public obtenerVentasPorIdOrganizacionIdUsuarioEstado(idUsuario: number, estado: string): Observable<Venta[]> {
    return this.httpClient.get<Venta[]>(`${this._ventaUrl}listaPorIdUsuarioYEstado/${idUsuario}/${estado}/1`);
  }

  public obtenerPorIdVenta(): Observable<Venta> {
    return this.httpClient.get<Venta>(`${this._ventaUrl}obtenerPorIdVenta/17`);
  }

  /*
   * Métodos POST
   */

  public realizarVenta(venta: Venta): Observable<any> {
    return this.httpClient.post<any>(`${this._ventaUrl}transaccionVenta`, venta);
  }

  public generarLinkPago(venta: Venta): Observable<any> {
    return this.httpClient.post<any>(`${this._ventaUrl}generarLinkPago`, venta);
  }

  public enviarCorreo(email: Email): Observable<any> {
    return this.httpClient.post<any>(`${this._ventaUrl}enviarCorreo`, email);
  }

  /*
   * Métodos PUT
   */

  public actualizarVenta(venta: Venta): Observable<any> {
    return this.httpClient.put<any>(`${this._ventaUrl}actualizar/${venta.IdVenta}`, venta);
  }
}

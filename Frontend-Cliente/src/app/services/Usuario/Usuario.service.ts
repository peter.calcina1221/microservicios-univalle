import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDto } from '../../core/models/Usuario/jwt-dto';
import { Usuario } from 'src/app/core/models/Usuario/Usuario';
import { urls } from '../../core/configuration/urls';
import { LoginUsuario } from 'src/app/core/models/Usuario/login-usuario';
import { Email } from 'src/app/core/models/Usuario/Email';
import { CambiarPassword } from 'src/app/core/models/Usuario/CambiarPassword';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
  private estaLogueado = 'isLoggedIn';
  private _usuarioUrl = urls.usuarioUrl;
  private _emailUrl = urls.emailUrl;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

  /*
    * Métodos GET
  */

  public obtenerUsuarioPorId(idUsuario: number): Observable<Usuario> {
    return this.httpClient.get<Usuario>(`${this._usuarioUrl}listaId/${idUsuario}`, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  /*
    * Métodos POST
  */

  public nuevo(usuario: Usuario): Observable<any> {
    return this.httpClient.post<any>(`${this._usuarioUrl}nuevo`, usuario);
  }

  public login(loginUsuario: LoginUsuario): Observable<JwtDto> {
    return this.httpClient.post<JwtDto>(`${this._usuarioUrl}login`, loginUsuario);
  }

  public enviarEmailPassword(email: Email): Observable<any> {
    return this.httpClient.post<any>(`${this._emailUrl}enviar`, email)
  }

  public reestablecerPassword(cambiarPassword: CambiarPassword): Observable<any> {
    return this.httpClient.post<any>(`${this._emailUrl}cambiarPassword`, cambiarPassword)
  }

  public cambiarContrasenia(idUsuario: number, contrasenia: string): Observable<string> {
    return this.httpClient.post<string>(`${this._usuarioUrl}cambiarPassword`, { idUsuario: idUsuario, password: contrasenia }, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  public verificarNombreUsuario(nombreUsuario: string): Observable<boolean> {
    return this.httpClient.post<boolean>(`${this._usuarioUrl}verficarNombreUsuario/${nombreUsuario}`, null);
  }

  public verificarEmail(email: string): Observable<boolean> {
    return this.httpClient.post<boolean>(`${this._usuarioUrl}verficarEmail/${email}/1`, null);
  }

  public verificarContrasenia(idUsuario: number, contrasenia: string): Observable<boolean> {
    return this.httpClient.post<boolean>(`${this._usuarioUrl}verificarPassword`, { idUsuario: idUsuario, password: contrasenia }, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }
  /*
    * Métodos PUT
  */

  actualizarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.put<Usuario>(`${this._usuarioUrl}actualizar/${usuario.IdUsuario}`, usuario, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  actualizarImagenPerfil(usuario: Usuario): Observable<string> {
    return this.httpClient.put<string>(`${this._usuarioUrl}actualizarImgPerfil/`, usuario, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  agregarRolEmprendedora(idUsuario: number): Observable<string> {
    return this.httpClient.put<string>(`${this._usuarioUrl}agregarRolEmprendedora/${idUsuario}`, null, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  /*
   * Otros métodos
  */

  isLogin() {
    sessionStorage.setItem(this.estaLogueado, 'true');
  }

  logout() {
    sessionStorage.removeItem(this.estaLogueado);
    sessionStorage.setItem(this.estaLogueado, 'false');
    sessionStorage.removeItem('codigo');
    sessionStorage.removeItem('codigoValidado');
  }

  validacion(): boolean {
    const estaLogueadoValue = sessionStorage.getItem(this.estaLogueado);
    return estaLogueadoValue === null || estaLogueadoValue === 'false';
  }
}

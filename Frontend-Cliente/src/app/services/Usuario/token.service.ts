import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private _roles: Array<string> = [];
  private _userName: string = '';
  private _idUsuario: number = 0;
  private _idEmprendimiento: number = 0;
  private _nombreEmprendimiento: string = '';

  constructor(private jwtHelper: JwtHelperService) { }

  public DecryptJTW(): void {
    const token = this.jwtHelper.decodeToken(localStorage.getItem('token') || '');
    if (token) {
      this.setUserName(token.sub);
      this.setIdUsuario(token.idUsuario);
      this.setIdEmprendimiento(token.idEmpresa);
      this.setAuthorities(token.rol);
      this.setNombreEmprendimiento(token.nombreEmpresa);
    }
  }

  public setToken(token: string): void {
    localStorage.removeItem('token');
    localStorage.setItem('token', token);
    this.DecryptJTW();
  }

  public getToken(): string {
    return localStorage.getItem('token') ?? '';
  }

  public setUserName(userName: string): void {
    this._userName = userName;
  }

  public getUserName(): string {
    return this._userName;
  }

  public setIdUsuario(idUsuario: number): void {
    this._idUsuario = idUsuario;
  }

  public getIdUsuario(): number {
    return this._idUsuario;
  }

  public setIdEmprendimiento(idEmprendimiento: number): void {
    this._idEmprendimiento = idEmprendimiento;
  }

  public getIdEmprendimiento(): number {
    return this._idEmprendimiento;
  }

  public setNombreEmprendimiento(nombreEmprendimiento: string): void {
    this._nombreEmprendimiento = nombreEmprendimiento;
  }

  public getNombreEmprendimiento(): string {
    return this._nombreEmprendimiento;
  }

  public setAuthorities(authorities: string[]): void {
    this._roles = authorities;
  }

  public getAuthorities(): string[] {
    let roles: any = [];
    if (this._roles) {
      this._roles.map((rol: any) => {
        roles.push(rol.authority);
      });
    }
    return roles;
  }

  public logOut(): void {
    localStorage.clear();
    this.setIdUsuario(0);
    this.setIdEmprendimiento(0);
    this.setUserName('');
    this.setAuthorities([]);
  }
}

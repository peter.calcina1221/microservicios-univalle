import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';
import { Pago } from 'src/app/core/models/Pago/Pago';

@Injectable({
  providedIn: 'root'
})

export class PagoService {

  private _pagoUrl = urls.pagoUrl;

  constructor(private httpClient: HttpClient) { }

  public confirmarPago(pago: Pago): Observable<any> {
    return this.httpClient.post<any>(`${this._pagoUrl}confirmar/`, pago);
  }
}

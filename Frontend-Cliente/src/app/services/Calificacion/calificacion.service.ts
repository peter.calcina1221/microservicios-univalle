import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoriaProducto } from '../../core/models/Producto/CategoriaProducto';
import { urls } from 'src/app/core/configuration/urls';
import { Calificacion } from 'src/app/core/models/Calificacion/Calificacion';

@Injectable({
  providedIn: 'root'
})
export class CalificacionService {

  private _calificacionUrl = urls.calificacionUrl;

  constructor(private httpClient: HttpClient) { }

  public calificar(calificacion: Calificacion): Observable<CategoriaProducto[]> {
    return this.httpClient.post<CategoriaProducto[]>(`${this._calificacionUrl}nuevo`, calificacion);
  }

  public obtenerCalificaciones(idEmpresa: number): Observable<Calificacion[]> {
    return this.httpClient.get<Calificacion[]>(`${this._calificacionUrl}listaPorIdOrganizacionIdElementoTipoElemento/1/${idEmpresa}/Empresa`);
  }

  public obtenerCalificacionesPorFecha(idEmpresa: number, fechaInicio: string, fechaFin: string): Observable<Calificacion[]> {
    return this.httpClient.get<Calificacion[]>(`${this._calificacionUrl}listaPorIdOrganizacionIdElementoTipoElementoYFecha/1/${idEmpresa}/Empresa/${fechaInicio}/${fechaFin}`);
  }
}

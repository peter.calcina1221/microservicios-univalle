import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urls } from 'src/app/core/configuration/urls';
import { Estadistica } from 'src/app/core/models/Calificacion/Estadistica';

@Injectable({
  providedIn: 'root'
})
export class EstadisticaService {

  private _estadisticaUrl = urls.estadisticaUrl;

  constructor(private httpClient: HttpClient) { }

  public obtenerCalificacion(idEmpresa: number): Observable<Estadistica> {
    return this.httpClient.get<Estadistica>(`${this._estadisticaUrl}obtenerIdOrganizacionIdElementoTipoElemento/1/${idEmpresa}/Empresa`);
  }
}

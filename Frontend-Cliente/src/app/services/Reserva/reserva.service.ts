import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urls } from 'src/app/core/configuration/urls';
import { Reserva } from 'src/app/core/models/Reserva/Reserva';
import { Email } from 'src/app/core/utils/Email';

@Injectable({
  providedIn: 'root'
})

export class ReservaService {

  private _reservaUrl = urls.reservaUrl;

  constructor(private httpClient: HttpClient) { }

  // * Esta sección solo contiene métodos GET
  public obtenerReservasPorIdUsuario(idUsuario: number): Observable<Reserva[]> {
    return this.httpClient.get<Reserva[]>(`${this._reservaUrl}listaPorIdUsuario/${idUsuario}`);
  }

  // * Esta sección solo contiene métodos POST
  public registrarReserva(reserva: Reserva): Observable<any> {
    return this.httpClient.post<any>(`${this._reservaUrl}nuevo`, reserva);
  }

  public enviarCorreo(email: Email): Observable<any> {
    return this.httpClient.post<any>(`${this._reservaUrl}enviarCorreo`, email);
  }

  // * Esta sección solo contiene métodos PUT
  public actualizarReserva(reserva: Reserva): Observable<any> {
    return this.httpClient.put<any>(`${this._reservaUrl}actualizar/${reserva.IdReserva}`, reserva);
  }
}

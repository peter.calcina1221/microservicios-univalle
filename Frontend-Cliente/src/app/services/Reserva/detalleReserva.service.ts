import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urls } from 'src/app/core/configuration/urls';
import { DetalleReserva } from 'src/app/core/models/Reserva/DetalleReserva';
import { Reserva } from 'src/app/core/models/Reserva/Reserva';

@Injectable({
  providedIn: 'root'
})

export class DetalleReservaService {

  private _detalleReserva = urls.detalleReservaUrl;

  constructor(private httpClient: HttpClient) { }

  // * Esta sección solo contiene métodos GET
  public obtenerReservasPorIdEmprendimiento(idEmprendimiento: number): Observable<DetalleReserva[]> {
    return this.httpClient.get<DetalleReserva[]>(`${this._detalleReserva}listaPorIdEmpresa/${idEmprendimiento}`);
  }

  public obtenerReservasPorIdEmprendimientoYFecha(idEmprendimiento: number, fechaInicio: string, fechaFin: string): Observable<DetalleReserva[]> {
    return this.httpClient.get<DetalleReserva[]>(`${this._detalleReserva}listaPorIdEmpresaYFecha/${idEmprendimiento}/${fechaInicio}/${fechaFin}`);
  }

  public obtenerReservasPorIdEmprendimientoEstado(idEmprendimiento: number, estado: number): Observable<DetalleReserva[]> {
    return this.httpClient.get<DetalleReserva[]>(`${this._detalleReserva}listaPorIdEmpresaYEstado/${idEmprendimiento}/${estado}`);
  }

  public obtenerReservasPorIdUsuaioYEstado(idUsuario: number, estado: number): Observable<DetalleReserva[]> {
    return this.httpClient.get<DetalleReserva[]>(`${this._detalleReserva}ListarPorUsuarioYEstado/${idUsuario}/${estado}`);
  }

  // * Esta sección solo contiene métodos PUT
  public actualizarDetalleReserva(detalleReserva: DetalleReserva): Observable<any> {
    return this.httpClient.put<any>(`${this._detalleReserva}actualizar/${detalleReserva.IdDetalleReserva}`, detalleReserva);
  }

  public actualizarPorEstado(idDetalleReserva: number, estado: number): Observable<any> {
    return this.httpClient.put<any>(`${this._detalleReserva}actualizarEstado/${idDetalleReserva}/${estado}`, null);
  }
}

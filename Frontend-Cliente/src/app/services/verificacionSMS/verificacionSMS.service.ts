import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urls } from 'src/app/core/configuration/urls';

@Injectable({
  providedIn: 'root'
})

export class verificacionSMSService {

  private _verificacionSMS = urls.verificacionSMSUrl;

  constructor(private httpClient: HttpClient) { }

  public envioSMS(numero: string): Observable<any> {
    return this.httpClient.post<any>(`${this._verificacionSMS}envioSMS`, {numero: '+591' + numero});
  }

  public verificarCodigo(codigo: string): Observable<any> {
    return this.httpClient.post<any>(`${this._verificacionSMS}verificarCodigo`, {codigoVerificacion: codigo});
  }
}

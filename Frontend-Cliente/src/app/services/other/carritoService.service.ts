import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';
import { Producto } from 'src/app/core/models/Producto/Producto';

@Injectable({
  providedIn: 'root'
})

export class CarritoService {
  productos: any[] = [];
  ventaConfirmada: boolean = false;
  private _productos$ = new BehaviorSubject<any[]>(this.productos);
  private _ventaConfirmada$ = new BehaviorSubject<boolean>(this.ventaConfirmada);

  constructor(private cookieService: CookieService) {
    //Recuperamos los productos del carrito desde las cookies
    if (this.cookieService.check('carrito')) {
      this.productos = JSON.parse(this.cookieService.get('carrito'));
      this._productos$.next(this.productos);
    }
  }

  get productos$() {
    return this._productos$.asObservable();
  }

  get ventaConfirmada$() {
    return this._ventaConfirmada$;
  }

  agregarProducto(producto: any): Promise<{ success: boolean }> {
    return new Promise<{ success: boolean }>((resolve, reject) => {
      let productoEncontrado = this.productos.find((productoItem: any) => productoItem.IdProducto === producto.IdProducto);

      if (productoEncontrado) {
        productoEncontrado.Cantidad++;
        this._productos$.next(this.productos);
        resolve({ success: true });
      } else {
        if (this.productos.push(producto)) {
          this._productos$.next(this.productos);
          resolve({ success: true });
        } else {
          reject({ success: false });
        }
      }

      //Actualizamos las cookies
      this.actualizarCookie();
    });
  }

  quitarProducto(producto: Producto): void {
    const index = this.productos.indexOf(producto);
    if (index !== -1) {
      this.productos.splice(index, 1);
      this._productos$.next(this.productos);
      this.actualizarCookie();
    }
  }

  vaciarCarrito(): void {
    this.productos = [];
    this._productos$.next(this.productos);
    this.cookieService.delete('carrito');
  }

  obtenerCostoTotalProductos(): number {
    let total: number = 0;

    this.productos.forEach(producto => {
      total += producto.PrecioVenta * producto.Cantidad;
    });

    return total;
  }

  reducirCantidadProducto(producto: any): void {
    this.productos.forEach(productoItem => {
      if (productoItem.IdProducto === producto.IdProducto) {
        productoItem.Cantidad--;
        return;
      }
    });

    this.actualizarCookie();
  }

  aumentarCantidadProducto(producto: any): void {
    this.productos.forEach(productoItem => {
      if (productoItem.IdProducto === producto.IdProducto) {
        productoItem.Cantidad++;
        return;
      }
    });

    this.actualizarCookie();
  }

  confirmarVenta(ventaRealizada: boolean): void {
    this.ventaConfirmada = ventaRealizada;
    this._ventaConfirmada$.next(this.ventaConfirmada);
  }

  actualizarCookie(): void {
    this.cookieService.set('carrito', JSON.stringify(this.productos));
  }
}

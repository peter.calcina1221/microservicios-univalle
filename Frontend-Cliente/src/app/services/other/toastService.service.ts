import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})

export class ToastService {
  constructor(private messageService: MessageService) {}

  mensajeExito(titulo: string, message: string, tiempo: number): void {
    this.messageService.add({ severity: 'success', summary: titulo, detail: message, life: tiempo });
  }

  mensajeInfo(titulo: string, message: string, tiempo: number): void {
    this.messageService.add({ severity: 'info', summary: titulo, detail: message, life: tiempo });
  }

  mensajeAdvertencia(titulo: string, message: string, tiempo: number): void {
    this.messageService.add({ severity: 'warn', summary: titulo, detail: message, life: tiempo });
  }

  mensajeError(titulo: string, message: string, tiempo: number): void {
    this.messageService.add({ severity: 'error', summary: titulo, detail: message, life: tiempo });
  }
}

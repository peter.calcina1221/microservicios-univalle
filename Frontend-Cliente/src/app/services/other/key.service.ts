import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class KeyService {
  private configUrl = '../assets/secret-key.json'; // Reemplaza esto con la URL correcta del archivo JSON

  constructor(private http: HttpClient) {}

  getApiKey(): Observable<any> {
    return this.http.get<any>(this.configUrl);
  }
}

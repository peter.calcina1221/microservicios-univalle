import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaParamService {
  private categoriaSeleccionadaSubject = new BehaviorSubject<string | null>(null);
  categoriaSeleccionada$ = this.categoriaSeleccionadaSubject.asObservable();

  seleccionarCategoria(categoria: string) {
    this.categoriaSeleccionadaSubject.next(categoria);
  }

  quitarCategoria() {
    this.categoriaSeleccionadaSubject.next(null);
  }
}

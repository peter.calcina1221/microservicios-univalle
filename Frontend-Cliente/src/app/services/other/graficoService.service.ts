import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GraficoService {

  //Colores para los graficos
  readonly documentStyle = getComputedStyle(document.documentElement);
  readonly textColor = this.documentStyle.getPropertyValue('--text-color');
  readonly textColorSecondary = this.documentStyle.getPropertyValue('--text-color-secondary');
  readonly surfaceBorder = this.documentStyle.getPropertyValue('--surface-border');

  /**
     * Método para crear un dataset para los gráficos
     * @param dataDataset Array de datos
     * @param labelDataset Nombre del dataset
     * @param coloresDataset Cantidad de colores que se utilizarán
     * @param typeDataset Tipo de gráfico
     * @param esOtroDataset Si es otro dataset, se le asigna un color específico
     * - 2: Es el segundo dataset y se le asigna el color Amarillo
     * - 3: Es el tercer dataset y se le asigna el color Rojo
     * - 4: Es el cuarto dataset y se le asigna el color Rosa
     * @returns Objeto con los datos del dataset
     */
  datasetGraficos(dataDataset: any, labelDataset: string, coloresDataset: number, typeDataset?: string, esOtroDataset?: number) {
    const colorProperties = [
      '--green',
      '--yellow',
      '--blue',
      '--red',
      '--teal',
      '--pink'
    ];

    let backgroundColors: string[] = [];
    let hoverBackgroundColors: string[] = [];

    if (!esOtroDataset) {
      for (let i = 0; i < coloresDataset; i++) {
        const color = colorProperties[i] + '-500';
        backgroundColors.push(this.documentStyle.getPropertyValue(color));
        hoverBackgroundColors.push(this.documentStyle.getPropertyValue(color.replace('500', '400')));
      }
    } else {
      const defaultColor = colorProperties[5] + '-500';
      const color = colorProperties[esOtroDataset - 1] + '-500';

      backgroundColors = [this.documentStyle.getPropertyValue(defaultColor)];
      hoverBackgroundColors = [this.documentStyle.getPropertyValue(defaultColor.replace('500', '400'))];

      if (color) {
        backgroundColors = [this.documentStyle.getPropertyValue(color)];
        hoverBackgroundColors = [this.documentStyle.getPropertyValue(color.replace('500', '400'))];
      }
    }

    return {
      type: typeDataset || null,
      label: labelDataset,
      backgroundColor: backgroundColors,
      borderColor: this.documentStyle.getPropertyValue('--surface-a'),
      hoverBackgroundColor: hoverBackgroundColors,
      data: dataDataset,
    };
  }

  //Métodos para los gráficos
  opcionesGeneralesGraficoCircularDona(nombreProductos: string[]) {
    return {
      cutout: '60%',
      plugins: {
        legend: {
          display: nombreProductos.length <= 5 ? true : false,
          labels: {
            color: this.textColor,
            usePointStyle: true
          }
        }
      }
    };
  }

  opcionesGeneralesGraficoCircular() {
    return {
      plugins: {
        legend: {
          labels: {
            usePointStyle: true,
            color: this.textColor
          }
        }
      }
    };
  }

  opcionesGeneralesGraficoBarraX() {
    return {
      maintainAspectRatio: false,
      aspectRatio: 0.8,
      plugins: {
        tooltips: {
          mode: 'index',
          intersect: false
        },
        legend: {
          labels: {
            usePointStyle: true,
            color: this.textColor
          }
        }
      }
    }
  }

  opcionesGeneralGraficoBarraY() {
    return {
      indexAxis: 'y',
      maintainAspectRatio: false,
      aspectRatio: 0.8,
      plugins: {
        legend: {
          labels: {
            usePointStyle: true,
            color: this.textColor
          }
        }
      }
    };
  }
}

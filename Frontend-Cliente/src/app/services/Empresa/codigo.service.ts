import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';
import { CodigoEmpresa } from 'src/app/core/models/Empresa/CodigoEmpresa';
@Injectable({
  providedIn: 'root'
})
export class CodigoService {

  private _codigoUrl = urls.codigoUrl;

  constructor(private httpClient: HttpClient) { }

  public validarCodigo(codigoEnviado: string): Observable<any> {
    return this.httpClient.post<any>(`${this._codigoUrl}verificar/${codigoEnviado}`, null);
  }

  public actualizarCodigo(codigo: string, codigoEmpresa: CodigoEmpresa) {
    return this.httpClient.put<any>(`${this._codigoUrl}actualizar/${codigo}`, codigoEmpresa);
  }
}

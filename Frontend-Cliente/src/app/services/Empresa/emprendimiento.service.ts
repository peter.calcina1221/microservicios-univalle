import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDto } from '../../core/models/Usuario/jwt-dto';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';
import { TokenService } from '../Usuario/token.service';

@Injectable({
  providedIn: 'root'
})
export class EmprendimientoService {

  private _empresaUrl = urls.empresaUrl;

  constructor(private httpClient: HttpClient, private tokenService: TokenService) { }

  // * Esta sección solo contiene métodos GET
  public obtenerEmprendimientos(): Observable<Emprendimiento[]> {
    return this.httpClient.get<Emprendimiento[]>(`${this._empresaUrl}listaPorIdOrganizacionAndEstado/1/1`);
  }

  public obtenerEmprendimientoPorId(idEmprendimiento: number): Observable<any> {
    return this.httpClient.get<any>(`${this._empresaUrl}obtenerEmpresaPorId/${idEmprendimiento}`);
  }

  public obtenerEmprendimientoPorIdUsuario(idEmprendimiento: number): Observable<Emprendimiento> {
    return this.httpClient.get<Emprendimiento>(`${this._empresaUrl}empresaPorIdUsuario/${idEmprendimiento}`);
  }

  public obtenerEmprendimientosQueMasVenden(idOrganizacion: number, limite: number): Observable<Emprendimiento[]> {
    return this.httpClient.get<Emprendimiento[]>(`${this._empresaUrl}listaEmpresasQueMasVenden/${idOrganizacion}/${limite}`);
  }

  public obtenerEmprendimientosSolicitadosPorUsuario(idUsuario: number, limite: number): Observable<Emprendimiento[]> {
    return this.httpClient.get<Emprendimiento[]>(`${this._empresaUrl}listaEmpresasQueMasCompranPorUsuario/${idUsuario}/${limite}`);
  }
  // * Esta sección solo contiene métodos POST
  public registrarEmprendimiento(emprendimiento: Emprendimiento): Observable<any> {
    return this.httpClient.post<any>(`${this._empresaUrl}nuevo`, emprendimiento, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }

  // * Esta sección solo contiene métodos PUT
  public actualizarEmprendimiento(emprendimiento: Emprendimiento): Observable<any> {
    return this.httpClient.put<any>(`${this._empresaUrl}actualizar/${emprendimiento.IdEmpresa}`, emprendimiento, { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.tokenService.getToken()}` } });
  }
}

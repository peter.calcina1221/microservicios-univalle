import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { Departamento } from 'src/app/core/models/Empresa/Departamento';
import { urls } from 'src/app/core/configuration/urls';
@Injectable({
  providedIn: 'root'
})

export class DepartamentoService {

  private _departamentoUrl = urls.departamentoUrl;
  constructor(private httpClient: HttpClient) { }

  public obtenerDepartamentos(): Observable<Departamento[]> {
    return this.httpClient.get<Departamento[]>(`${this._departamentoUrl}lista`);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Emprendimiento } from '../../core/models/Empresa/Emprendimiento';
import { map } from 'rxjs/operators';
import { urls } from 'src/app/core/configuration/urls';
import { Categoria } from 'src/app/core/models/Empresa/Categoria';
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private _categoriaUrl = urls.categoriaEmpresaUrl;

  constructor(private httpClient: HttpClient) { }

  public obtenerCategorias(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(`${this._categoriaUrl}listaIdOrganizacionAndEstado/1/1`);
  }
}
